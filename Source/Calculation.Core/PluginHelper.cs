﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Calculation.Core
{
    public static class PluginHelper
    {
        public static IEnumerable<Assembly> LoadPlugins(string baseDirectory)
        {
            var pluginDirectory = baseDirectory;

            if (!baseDirectory.EndsWith("plugins", StringComparison.OrdinalIgnoreCase))
            {
                pluginDirectory = Path.Combine(baseDirectory, "Plugins");
            }

            if (!Directory.Exists(pluginDirectory))
                Directory.CreateDirectory(pluginDirectory);

            var pluginFiles = Directory.GetFiles(pluginDirectory, "*.dll");
            return pluginFiles.Select(pluginFile => Assembly.Load(File.ReadAllBytes(pluginFile))).ToList();
        }
    }
}