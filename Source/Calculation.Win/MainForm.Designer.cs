﻿using System.ComponentModel;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTreeList;

namespace Calculation.Win
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonInsert = new DevExpress.XtraBars.BarButtonItem();
            this.InsertMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.InsertMenuCalculation = new DevExpress.XtraBars.BarButtonItem();
            this.InsertMenuExistingCalculation = new DevExpress.XtraBars.BarButtonItem();
            this.InsertMenuCalculationRule = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonDelete = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonClose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonCopy = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonCut = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonPaste = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonMoveUp = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonMoveDown = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonPrint = new DevExpress.XtraBars.BarButtonItem();
            this.reportMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonUndo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonRedo = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonConvert = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.btnTailRulesInsert = new DevExpress.XtraBars.BarButtonItem();
            this.btnTailRulesDelete = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonOpen = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonMultiSelect = new DevExpress.XtraBars.BarCheckItem();
            this.rpMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgEdit = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgClipboard = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgNavigation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgPrint = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgTailRules = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgMisc = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.gcNavigation = new DevExpress.XtraEditors.GroupControl();
            this.treeListCalculations = new DevExpress.XtraTreeList.TreeList();
            this.tlcolDescription = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemRichTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this.gcMain = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gcGrid = new DevExpress.XtraEditors.GroupControl();
            this.dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.groupTotals = new DevExpress.XtraEditors.GroupControl();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.edtColumnFilter = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.TotalCostPriceHoursTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.calculationTotalsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TotalCostPriceWorkActivityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalCostPriceItemsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalCostPriceOutsourcedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalCostPriceTailRulesTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPriceHoursTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPriceWorkActivityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPriceItemsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPriceOutsourcedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPriceTailRulesTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferenceHoursTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferenceWorkActivityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferenceItemsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferenceOutsourcedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.edtPriceFilter = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TotalCostPriceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPriceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferenceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TOTALKGSTEELTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TOTALSURFACEAREATextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalPricePerKgTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalSalesPricePerKgTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferencePerKgTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalDifferencePercentageTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.edtShowPerKiloFields = new DevExpress.XtraEditors.CheckEdit();
            this.TotalDifferencePercentageOverSalesPriceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForTotalCostPriceTailRules = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalDifferenceHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgCostPrices = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalCostPriceHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCostPriceWorkActivity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCostPriceItems = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCostPriceOutsourced = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCostPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTotalPricePerKg = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgSalesPrices = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalSalesPriceHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSalesPriceWorkActivity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSalesPriceItems = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSalesPriceOutsourced = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSalesPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForTotalSalesPriceTailRules = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalSalesPricePerKg = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgDifferences = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTotalDifferenceWorkActivity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalDifferenceItems = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalDifferenceOutsourced = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTotalDifferencePerKg = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalDifferencePercentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalDifference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalMargenPercentageOverSalesPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgMisc = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lciShowPriceTypes = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTOTALKGSTEEL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTOTALSURFACEAREA = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.dragDropEventsGridView = new DevExpress.Utils.DragDrop.DragDropEvents(this.components);
            this.tabPane = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabPageTotals = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabPageTailRules = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.gcTailRules = new DevExpress.XtraGrid.GridControl();
            this.gvTailRules = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsertMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigation)).BeginInit();
            this.gcNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListCalculations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).BeginInit();
            this.gcGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTotals)).BeginInit();
            this.groupTotals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtColumnFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceHoursTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationTotalsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceWorkActivityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceItemsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceOutsourcedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceTailRulesTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceHoursTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceWorkActivityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceItemsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceOutsourcedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceTailRulesTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceHoursTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceWorkActivityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceItemsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceOutsourcedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtPriceFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALKGSTEELTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALSURFACEAREATextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPricePerKgTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPricePerKgTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferencePerKgTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferencePercentageTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtShowPerKiloFields.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceTailRules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCostPrices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceWorkActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceOutsourced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalPricePerKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSalesPrices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceWorkActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceOutsourced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceTailRules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPricePerKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDifferences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceWorkActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceOutsourced)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferencePerKg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferencePercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalMargenPercentageOverSalesPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMisc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciShowPriceTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALKGSTEEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALSURFACEAREA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane)).BeginInit();
            this.tabPane.SuspendLayout();
            this.tabPageTotals.SuspendLayout();
            this.tabPageTailRules.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTailRules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTailRules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1.Panel1)).BeginInit();
            this.splitContainerControl1.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1.Panel2)).BeginInit();
            this.splitContainerControl1.Panel2.SuspendLayout();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonControl1.SearchEditItem,
            this.barButtonInsert,
            this.barButtonDelete,
            this.barButtonSave,
            this.barButtonClose,
            this.barButtonCopy,
            this.barButtonCut,
            this.barButtonPaste,
            this.barButtonMoveUp,
            this.barButtonMoveDown,
            this.barButtonPrint,
            this.barButtonUndo,
            this.barButtonRedo,
            this.barButtonConvert,
            this.skinRibbonGalleryBarItem1,
            this.btnTailRulesInsert,
            this.btnTailRulesDelete,
            this.InsertMenuCalculation,
            this.InsertMenuCalculationRule,
            this.InsertMenuExistingCalculation,
            this.barButtonOpen,
            this.barButtonMultiSelect});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 31;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpMain});
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1190, 158);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            // 
            // barButtonInsert
            // 
            this.barButtonInsert.ActAsDropDown = true;
            this.barButtonInsert.AllowDrawArrow = false;
            this.barButtonInsert.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonInsert.Caption = "Toevoegen";
            this.barButtonInsert.DropDownControl = this.InsertMenu;
            this.barButtonInsert.Id = 1;
            this.barButtonInsert.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonInsert.ImageOptions.Image")));
            this.barButtonInsert.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonInsert.ImageOptions.LargeImage")));
            this.barButtonInsert.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.barButtonInsert.Name = "barButtonInsert";
            this.barButtonInsert.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // InsertMenu
            // 
            this.InsertMenu.ItemLinks.Add(this.InsertMenuCalculation);
            this.InsertMenu.ItemLinks.Add(this.InsertMenuExistingCalculation);
            this.InsertMenu.ItemLinks.Add(this.InsertMenuCalculationRule);
            this.InsertMenu.Name = "InsertMenu";
            this.InsertMenu.Ribbon = this.ribbonControl1;
            // 
            // InsertMenuCalculation
            // 
            this.InsertMenuCalculation.Caption = "Calculatie";
            this.InsertMenuCalculation.Id = 22;
            this.InsertMenuCalculation.Name = "InsertMenuCalculation";
            // 
            // InsertMenuExistingCalculation
            // 
            this.InsertMenuExistingCalculation.Caption = "Bestaande Calculatie";
            this.InsertMenuExistingCalculation.Id = 25;
            this.InsertMenuExistingCalculation.Name = "InsertMenuExistingCalculation";
            // 
            // InsertMenuCalculationRule
            // 
            this.InsertMenuCalculationRule.Caption = "Calculatie regel";
            this.InsertMenuCalculationRule.Id = 23;
            this.InsertMenuCalculationRule.Name = "InsertMenuCalculationRule";
            // 
            // barButtonDelete
            // 
            this.barButtonDelete.Caption = "Verwijderen";
            this.barButtonDelete.Id = 2;
            this.barButtonDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonDelete.ImageOptions.Image")));
            this.barButtonDelete.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonDelete.ImageOptions.LargeImage")));
            this.barButtonDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D));
            this.barButtonDelete.Name = "barButtonDelete";
            this.barButtonDelete.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonSave
            // 
            this.barButtonSave.Caption = "Opslaan";
            this.barButtonSave.Id = 3;
            this.barButtonSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonSave.ImageOptions.Image")));
            this.barButtonSave.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonSave.ImageOptions.LargeImage")));
            this.barButtonSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S));
            this.barButtonSave.Name = "barButtonSave";
            this.barButtonSave.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonClose
            // 
            this.barButtonClose.Caption = "Sluiten";
            this.barButtonClose.Id = 4;
            this.barButtonClose.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonClose.ImageOptions.Image")));
            this.barButtonClose.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonClose.ImageOptions.LargeImage")));
            this.barButtonClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E));
            this.barButtonClose.Name = "barButtonClose";
            this.barButtonClose.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonCopy
            // 
            this.barButtonCopy.Caption = "Kopiëren";
            this.barButtonCopy.Id = 5;
            this.barButtonCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonCopy.ImageOptions.Image")));
            this.barButtonCopy.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonCopy.ImageOptions.LargeImage")));
            this.barButtonCopy.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C));
            this.barButtonCopy.Name = "barButtonCopy";
            this.barButtonCopy.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonCut
            // 
            this.barButtonCut.Caption = "Knippen";
            this.barButtonCut.Id = 6;
            this.barButtonCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonCut.ImageOptions.Image")));
            this.barButtonCut.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonCut.ImageOptions.LargeImage")));
            this.barButtonCut.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X));
            this.barButtonCut.Name = "barButtonCut";
            this.barButtonCut.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonPaste
            // 
            this.barButtonPaste.Caption = "Plakken";
            this.barButtonPaste.Id = 7;
            this.barButtonPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonPaste.ImageOptions.Image")));
            this.barButtonPaste.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonPaste.ImageOptions.LargeImage")));
            this.barButtonPaste.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V));
            this.barButtonPaste.Name = "barButtonPaste";
            this.barButtonPaste.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonMoveUp
            // 
            this.barButtonMoveUp.Caption = "Omhoog";
            this.barButtonMoveUp.Id = 8;
            this.barButtonMoveUp.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonMoveUp.ImageOptions.Image")));
            this.barButtonMoveUp.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonMoveUp.ImageOptions.LargeImage")));
            this.barButtonMoveUp.Name = "barButtonMoveUp";
            // 
            // barButtonMoveDown
            // 
            this.barButtonMoveDown.Caption = "Omlaag";
            this.barButtonMoveDown.Id = 9;
            this.barButtonMoveDown.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonMoveDown.ImageOptions.Image")));
            this.barButtonMoveDown.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonMoveDown.ImageOptions.LargeImage")));
            this.barButtonMoveDown.Name = "barButtonMoveDown";
            // 
            // barButtonPrint
            // 
            this.barButtonPrint.ActAsDropDown = true;
            this.barButtonPrint.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonPrint.Caption = "Print";
            this.barButtonPrint.DropDownControl = this.reportMenu;
            this.barButtonPrint.Id = 10;
            this.barButtonPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonPrint.ImageOptions.Image")));
            this.barButtonPrint.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonPrint.ImageOptions.LargeImage")));
            this.barButtonPrint.Name = "barButtonPrint";
            // 
            // reportMenu
            // 
            this.reportMenu.Name = "reportMenu";
            this.reportMenu.Ribbon = this.ribbonControl1;
            // 
            // barButtonUndo
            // 
            this.barButtonUndo.Caption = "Undo";
            this.barButtonUndo.Enabled = false;
            this.barButtonUndo.Id = 11;
            this.barButtonUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonUndo.ImageOptions.Image")));
            this.barButtonUndo.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonUndo.ImageOptions.LargeImage")));
            this.barButtonUndo.Name = "barButtonUndo";
            // 
            // barButtonRedo
            // 
            this.barButtonRedo.Caption = "Redo";
            this.barButtonRedo.Enabled = false;
            this.barButtonRedo.Id = 12;
            this.barButtonRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonRedo.ImageOptions.Image")));
            this.barButtonRedo.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonRedo.ImageOptions.LargeImage")));
            this.barButtonRedo.Name = "barButtonRedo";
            // 
            // barButtonConvert
            // 
            this.barButtonConvert.Caption = "Converteer regels";
            this.barButtonConvert.Id = 16;
            this.barButtonConvert.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonConvert.ImageOptions.Image")));
            this.barButtonConvert.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonConvert.ImageOptions.LargeImage")));
            this.barButtonConvert.Name = "barButtonConvert";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 18;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // btnTailRulesInsert
            // 
            this.btnTailRulesInsert.Caption = "Toevoegen";
            this.btnTailRulesInsert.Id = 19;
            this.btnTailRulesInsert.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTailRulesInsert.ImageOptions.Image")));
            this.btnTailRulesInsert.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTailRulesInsert.ImageOptions.LargeImage")));
            this.btnTailRulesInsert.Name = "btnTailRulesInsert";
            // 
            // btnTailRulesDelete
            // 
            this.btnTailRulesDelete.Caption = "Verwijderen";
            this.btnTailRulesDelete.Id = 20;
            this.btnTailRulesDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTailRulesDelete.ImageOptions.Image")));
            this.btnTailRulesDelete.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnTailRulesDelete.ImageOptions.LargeImage")));
            this.btnTailRulesDelete.Name = "btnTailRulesDelete";
            // 
            // barButtonOpen
            // 
            this.barButtonOpen.Caption = "Open calculatie";
            this.barButtonOpen.Id = 26;
            this.barButtonOpen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonOpen.ImageOptions.Image")));
            this.barButtonOpen.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonOpen.ImageOptions.LargeImage")));
            this.barButtonOpen.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O));
            this.barButtonOpen.Name = "barButtonOpen";
            this.barButtonOpen.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonOpen_ItemClick);
            // 
            // barButtonMultiSelect
            // 
            this.barButtonMultiSelect.Caption = "Multi select";
            this.barButtonMultiSelect.Id = 30;
            this.barButtonMultiSelect.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonMultiSelect.ImageOptions.Image")));
            this.barButtonMultiSelect.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonMultiSelect.ImageOptions.LargeImage")));
            this.barButtonMultiSelect.Name = "barButtonMultiSelect";
            this.barButtonMultiSelect.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // rpMain
            // 
            this.rpMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgEdit,
            this.rpgClipboard,
            this.rpgNavigation,
            this.rpgPrint,
            this.rpgTailRules,
            this.rpgMisc});
            this.rpMain.Name = "rpMain";
            this.rpMain.Text = "Algemeen";
            // 
            // rpgEdit
            // 
            this.rpgEdit.ItemLinks.Add(this.barButtonInsert);
            this.rpgEdit.ItemLinks.Add(this.barButtonDelete);
            this.rpgEdit.ItemLinks.Add(this.barButtonSave);
            this.rpgEdit.Name = "rpgEdit";
            this.rpgEdit.Text = "Aanpassen";
            // 
            // rpgClipboard
            // 
            this.rpgClipboard.ItemLinks.Add(this.barButtonCopy);
            this.rpgClipboard.ItemLinks.Add(this.barButtonPaste);
            this.rpgClipboard.ItemLinks.Add(this.barButtonCut);
            this.rpgClipboard.Name = "rpgClipboard";
            this.rpgClipboard.Text = "Klembord";
            // 
            // rpgNavigation
            // 
            this.rpgNavigation.ItemLinks.Add(this.barButtonMoveUp);
            this.rpgNavigation.ItemLinks.Add(this.barButtonMoveDown);
            this.rpgNavigation.Name = "rpgNavigation";
            this.rpgNavigation.Text = "Navigatie";
            // 
            // rpgPrint
            // 
            this.rpgPrint.ItemLinks.Add(this.barButtonPrint);
            this.rpgPrint.Name = "rpgPrint";
            this.rpgPrint.Text = "Printen";
            // 
            // rpgTailRules
            // 
            this.rpgTailRules.ItemLinks.Add(this.btnTailRulesInsert);
            this.rpgTailRules.ItemLinks.Add(this.btnTailRulesDelete);
            this.rpgTailRules.Name = "rpgTailRules";
            this.rpgTailRules.Text = "Staart regels";
            this.rpgTailRules.Visible = false;
            // 
            // rpgMisc
            // 
            this.rpgMisc.ItemLinks.Add(this.barButtonUndo);
            this.rpgMisc.ItemLinks.Add(this.barButtonRedo);
            this.rpgMisc.ItemLinks.Add(this.barButtonConvert);
            this.rpgMisc.ItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.rpgMisc.ItemLinks.Add(this.barButtonMultiSelect);
            this.rpgMisc.ItemLinks.Add(this.barButtonOpen);
            this.rpgMisc.ItemLinks.Add(this.barButtonClose);
            this.rpgMisc.Name = "rpgMisc";
            this.rpgMisc.Text = "Overig";
            // 
            // gcNavigation
            // 
            this.gcNavigation.Controls.Add(this.treeListCalculations);
            this.gcNavigation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcNavigation.Location = new System.Drawing.Point(0, 0);
            this.gcNavigation.Name = "gcNavigation";
            this.gcNavigation.Padding = new System.Windows.Forms.Padding(5);
            this.gcNavigation.Size = new System.Drawing.Size(1190, 168);
            this.gcNavigation.TabIndex = 1;
            this.gcNavigation.Text = "Navigatie";
            // 
            // treeListCalculations
            // 
            this.treeListCalculations.AllowDrop = true;
            this.treeListCalculations.Appearance.FocusedCell.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.treeListCalculations.Appearance.FocusedCell.Options.UseBackColor = true;
            this.treeListCalculations.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlcolDescription});
            this.treeListCalculations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListCalculations.KeyFieldName = "Id";
            this.treeListCalculations.Location = new System.Drawing.Point(7, 28);
            this.treeListCalculations.Name = "treeListCalculations";
            this.treeListCalculations.OptionsBehavior.AutoPopulateColumns = false;
            this.treeListCalculations.OptionsBehavior.Editable = false;
            this.treeListCalculations.OptionsView.TreeLineStyle = DevExpress.XtraTreeList.LineStyle.Solid;
            this.treeListCalculations.ParentFieldName = "FK_MAINCALCULATION";
            this.treeListCalculations.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRichTextEdit1});
            this.treeListCalculations.Size = new System.Drawing.Size(1176, 133);
            this.treeListCalculations.TabIndex = 0;
            // 
            // tlcolDescription
            // 
            this.tlcolDescription.Caption = "Omschrijving";
            this.tlcolDescription.FieldName = "DESCRIPTION";
            this.tlcolDescription.Name = "tlcolDescription";
            this.tlcolDescription.Visible = true;
            this.tlcolDescription.VisibleIndex = 0;
            // 
            // repositoryItemRichTextEdit1
            // 
            this.repositoryItemRichTextEdit1.Name = "repositoryItemRichTextEdit1";
            this.repositoryItemRichTextEdit1.ShowCaretInReadOnly = false;
            // 
            // gcMain
            // 
            this.gcMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcMain.Location = new System.Drawing.Point(7, 28);
            this.gcMain.MainView = this.gridView1;
            this.gcMain.MenuManager = this.ribbonControl1;
            this.gcMain.Name = "gcMain";
            this.gcMain.Size = new System.Drawing.Size(1176, 230);
            this.gcMain.TabIndex = 2;
            this.gcMain.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.behaviorManager1.SetBehaviors(this.gridView1, new DevExpress.Utils.Behaviors.Behavior[] {
            ((DevExpress.Utils.Behaviors.Behavior)(DevExpress.Utils.DragDrop.DragDropBehavior.Create(typeof(DevExpress.XtraGrid.Extensions.ColumnViewDragDropSource), true, true, true, true, this.dragDropEventsGridView)))});
            this.gridView1.GridControl = this.gcMain;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gcGrid
            // 
            this.gcGrid.Controls.Add(this.gcMain);
            this.gcGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcGrid.Location = new System.Drawing.Point(0, 0);
            this.gcGrid.Name = "gcGrid";
            this.gcGrid.Padding = new System.Windows.Forms.Padding(5);
            this.gcGrid.Size = new System.Drawing.Size(1190, 265);
            this.gcGrid.TabIndex = 4;
            this.gcGrid.Text = "Calculatie";
            // 
            // dockManager
            // 
            this.dockManager.Form = this;
            this.dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // groupTotals
            // 
            this.groupTotals.Controls.Add(this.dataLayoutControl1);
            this.groupTotals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupTotals.Location = new System.Drawing.Point(0, 0);
            this.groupTotals.Name = "groupTotals";
            this.groupTotals.ShowCaption = false;
            this.groupTotals.Size = new System.Drawing.Size(1190, 261);
            this.groupTotals.TabIndex = 0;
            this.groupTotals.Text = "groupControl1";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.edtColumnFilter);
            this.dataLayoutControl1.Controls.Add(this.TotalCostPriceHoursTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostPriceWorkActivityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostPriceItemsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostPriceOutsourcedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostPriceTailRulesTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPriceHoursTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPriceWorkActivityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPriceItemsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPriceOutsourcedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPriceTailRulesTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferenceHoursTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferenceWorkActivityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferenceItemsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferenceOutsourcedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.edtPriceFilter);
            this.dataLayoutControl1.Controls.Add(this.TotalCostPriceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPriceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferenceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TOTALKGSTEELTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TOTALSURFACEAREATextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalPricePerKgTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalSalesPricePerKgTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferencePerKgTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferencePercentageTextEdit);
            this.dataLayoutControl1.Controls.Add(this.edtShowPerKiloFields);
            this.dataLayoutControl1.Controls.Add(this.TotalDifferencePercentageOverSalesPriceTextEdit);
            this.dataLayoutControl1.DataSource = this.calculationTotalsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalCostPriceTailRules,
            this.ItemForTotalDifferenceHours});
            this.dataLayoutControl1.Location = new System.Drawing.Point(2, 2);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(744, 141, 1344, 567);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1186, 257);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // edtColumnFilter
            // 
            this.edtColumnFilter.Location = new System.Drawing.Point(1004, 117);
            this.edtColumnFilter.MenuManager = this.ribbonControl1;
            this.edtColumnFilter.Name = "edtColumnFilter";
            this.edtColumnFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtColumnFilter.Size = new System.Drawing.Size(158, 20);
            this.edtColumnFilter.StyleController = this.dataLayoutControl1;
            this.edtColumnFilter.TabIndex = 29;
            // 
            // TotalCostPriceHoursTextEdit
            // 
            this.TotalCostPriceHoursTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalCostPriceHours", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalCostPriceHoursTextEdit.Location = new System.Drawing.Point(152, 45);
            this.TotalCostPriceHoursTextEdit.MenuManager = this.ribbonControl1;
            this.TotalCostPriceHoursTextEdit.Name = "TotalCostPriceHoursTextEdit";
            this.TotalCostPriceHoursTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalCostPriceHoursTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalCostPriceHoursTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalCostPriceHoursTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalCostPriceHoursTextEdit.Properties.Mask.EditMask = "N2";
            this.TotalCostPriceHoursTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostPriceHoursTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostPriceHoursTextEdit.Properties.ReadOnly = true;
            this.TotalCostPriceHoursTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalCostPriceHoursTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostPriceHoursTextEdit.TabIndex = 4;
            // 
            // calculationTotalsBindingSource
            // 
            this.calculationTotalsBindingSource.DataSource = typeof(Calculation.Core.Models.CalculationTotals);
            // 
            // TotalCostPriceWorkActivityTextEdit
            // 
            this.TotalCostPriceWorkActivityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalCostPriceWorkActivity", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalCostPriceWorkActivityTextEdit.Location = new System.Drawing.Point(152, 69);
            this.TotalCostPriceWorkActivityTextEdit.MenuManager = this.ribbonControl1;
            this.TotalCostPriceWorkActivityTextEdit.Name = "TotalCostPriceWorkActivityTextEdit";
            this.TotalCostPriceWorkActivityTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalCostPriceWorkActivityTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalCostPriceWorkActivityTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalCostPriceWorkActivityTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalCostPriceWorkActivityTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalCostPriceWorkActivityTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostPriceWorkActivityTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostPriceWorkActivityTextEdit.Properties.ReadOnly = true;
            this.TotalCostPriceWorkActivityTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalCostPriceWorkActivityTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostPriceWorkActivityTextEdit.TabIndex = 5;
            // 
            // TotalCostPriceItemsTextEdit
            // 
            this.TotalCostPriceItemsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalCostPriceItems", true));
            this.TotalCostPriceItemsTextEdit.Location = new System.Drawing.Point(152, 93);
            this.TotalCostPriceItemsTextEdit.MenuManager = this.ribbonControl1;
            this.TotalCostPriceItemsTextEdit.Name = "TotalCostPriceItemsTextEdit";
            this.TotalCostPriceItemsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalCostPriceItemsTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalCostPriceItemsTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalCostPriceItemsTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalCostPriceItemsTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalCostPriceItemsTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostPriceItemsTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostPriceItemsTextEdit.Properties.ReadOnly = true;
            this.TotalCostPriceItemsTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalCostPriceItemsTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostPriceItemsTextEdit.TabIndex = 6;
            // 
            // TotalCostPriceOutsourcedTextEdit
            // 
            this.TotalCostPriceOutsourcedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalCostPriceOutsourced", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalCostPriceOutsourcedTextEdit.Location = new System.Drawing.Point(152, 117);
            this.TotalCostPriceOutsourcedTextEdit.MenuManager = this.ribbonControl1;
            this.TotalCostPriceOutsourcedTextEdit.Name = "TotalCostPriceOutsourcedTextEdit";
            this.TotalCostPriceOutsourcedTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalCostPriceOutsourcedTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalCostPriceOutsourcedTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalCostPriceOutsourcedTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalCostPriceOutsourcedTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalCostPriceOutsourcedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostPriceOutsourcedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostPriceOutsourcedTextEdit.Properties.ReadOnly = true;
            this.TotalCostPriceOutsourcedTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalCostPriceOutsourcedTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostPriceOutsourcedTextEdit.TabIndex = 7;
            // 
            // TotalCostPriceTailRulesTextEdit
            // 
            this.TotalCostPriceTailRulesTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalCostPriceTailRules", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalCostPriceTailRulesTextEdit.Location = new System.Drawing.Point(179, 148);
            this.TotalCostPriceTailRulesTextEdit.MenuManager = this.ribbonControl1;
            this.TotalCostPriceTailRulesTextEdit.Name = "TotalCostPriceTailRulesTextEdit";
            this.TotalCostPriceTailRulesTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalCostPriceTailRulesTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalCostPriceTailRulesTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalCostPriceTailRulesTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalCostPriceTailRulesTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalCostPriceTailRulesTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostPriceTailRulesTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostPriceTailRulesTextEdit.Properties.ReadOnly = true;
            this.TotalCostPriceTailRulesTextEdit.Size = new System.Drawing.Size(101, 20);
            this.TotalCostPriceTailRulesTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostPriceTailRulesTextEdit.TabIndex = 8;
            // 
            // TotalSalesPriceHoursTextEdit
            // 
            this.TotalSalesPriceHoursTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPriceHours", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalSalesPriceHoursTextEdit.Location = new System.Drawing.Point(436, 45);
            this.TotalSalesPriceHoursTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPriceHoursTextEdit.Name = "TotalSalesPriceHoursTextEdit";
            this.TotalSalesPriceHoursTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPriceHoursTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPriceHoursTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPriceHoursTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPriceHoursTextEdit.Properties.Mask.EditMask = "N2";
            this.TotalSalesPriceHoursTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPriceHoursTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPriceHoursTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPriceHoursTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPriceHoursTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPriceHoursTextEdit.TabIndex = 9;
            // 
            // TotalSalesPriceWorkActivityTextEdit
            // 
            this.TotalSalesPriceWorkActivityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPriceWorkActivity", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalSalesPriceWorkActivityTextEdit.Location = new System.Drawing.Point(436, 69);
            this.TotalSalesPriceWorkActivityTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPriceWorkActivityTextEdit.Name = "TotalSalesPriceWorkActivityTextEdit";
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPriceWorkActivityTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPriceWorkActivityTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPriceWorkActivityTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPriceWorkActivityTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPriceWorkActivityTextEdit.TabIndex = 10;
            // 
            // TotalSalesPriceItemsTextEdit
            // 
            this.TotalSalesPriceItemsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPriceItems", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalSalesPriceItemsTextEdit.Location = new System.Drawing.Point(436, 93);
            this.TotalSalesPriceItemsTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPriceItemsTextEdit.Name = "TotalSalesPriceItemsTextEdit";
            this.TotalSalesPriceItemsTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPriceItemsTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPriceItemsTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPriceItemsTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPriceItemsTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalSalesPriceItemsTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPriceItemsTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPriceItemsTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPriceItemsTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPriceItemsTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPriceItemsTextEdit.TabIndex = 11;
            // 
            // TotalSalesPriceOutsourcedTextEdit
            // 
            this.TotalSalesPriceOutsourcedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPriceOutsourced", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalSalesPriceOutsourcedTextEdit.Location = new System.Drawing.Point(436, 117);
            this.TotalSalesPriceOutsourcedTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPriceOutsourcedTextEdit.Name = "TotalSalesPriceOutsourcedTextEdit";
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPriceOutsourcedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPriceOutsourcedTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPriceOutsourcedTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPriceOutsourcedTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPriceOutsourcedTextEdit.TabIndex = 12;
            // 
            // TotalSalesPriceTailRulesTextEdit
            // 
            this.TotalSalesPriceTailRulesTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPriceTailRules", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalSalesPriceTailRulesTextEdit.Location = new System.Drawing.Point(436, 141);
            this.TotalSalesPriceTailRulesTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPriceTailRulesTextEdit.Name = "TotalSalesPriceTailRulesTextEdit";
            this.TotalSalesPriceTailRulesTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPriceTailRulesTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPriceTailRulesTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPriceTailRulesTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPriceTailRulesTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalSalesPriceTailRulesTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPriceTailRulesTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPriceTailRulesTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPriceTailRulesTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPriceTailRulesTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPriceTailRulesTextEdit.TabIndex = 13;
            // 
            // TotalDifferenceHoursTextEdit
            // 
            this.TotalDifferenceHoursTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalDifferenceHours", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalDifferenceHoursTextEdit.Location = new System.Drawing.Point(747, 42);
            this.TotalDifferenceHoursTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferenceHoursTextEdit.Name = "TotalDifferenceHoursTextEdit";
            this.TotalDifferenceHoursTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferenceHoursTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferenceHoursTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalDifferenceHoursTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalDifferenceHoursTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalDifferenceHoursTextEdit.Properties.ReadOnly = true;
            this.TotalDifferenceHoursTextEdit.Size = new System.Drawing.Size(101, 20);
            this.TotalDifferenceHoursTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferenceHoursTextEdit.TabIndex = 14;
            // 
            // TotalDifferenceWorkActivityTextEdit
            // 
            this.TotalDifferenceWorkActivityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalDifferenceWorkActivity", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalDifferenceWorkActivityTextEdit.Location = new System.Drawing.Point(720, 67);
            this.TotalDifferenceWorkActivityTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferenceWorkActivityTextEdit.Name = "TotalDifferenceWorkActivityTextEdit";
            this.TotalDifferenceWorkActivityTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferenceWorkActivityTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferenceWorkActivityTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalDifferenceWorkActivityTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalDifferenceWorkActivityTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalDifferenceWorkActivityTextEdit.Properties.ReadOnly = true;
            this.TotalDifferenceWorkActivityTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferenceWorkActivityTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferenceWorkActivityTextEdit.TabIndex = 15;
            // 
            // TotalDifferenceItemsTextEdit
            // 
            this.TotalDifferenceItemsTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalDifferenceItems", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalDifferenceItemsTextEdit.Location = new System.Drawing.Point(720, 91);
            this.TotalDifferenceItemsTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferenceItemsTextEdit.Name = "TotalDifferenceItemsTextEdit";
            this.TotalDifferenceItemsTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferenceItemsTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferenceItemsTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalDifferenceItemsTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalDifferenceItemsTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalDifferenceItemsTextEdit.Properties.ReadOnly = true;
            this.TotalDifferenceItemsTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferenceItemsTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferenceItemsTextEdit.TabIndex = 16;
            // 
            // TotalDifferenceOutsourcedTextEdit
            // 
            this.TotalDifferenceOutsourcedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalDifferenceOutsourced", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.TotalDifferenceOutsourcedTextEdit.Location = new System.Drawing.Point(720, 115);
            this.TotalDifferenceOutsourcedTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferenceOutsourcedTextEdit.Name = "TotalDifferenceOutsourcedTextEdit";
            this.TotalDifferenceOutsourcedTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferenceOutsourcedTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferenceOutsourcedTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalDifferenceOutsourcedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalDifferenceOutsourcedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalDifferenceOutsourcedTextEdit.Properties.ReadOnly = true;
            this.TotalDifferenceOutsourcedTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferenceOutsourcedTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferenceOutsourcedTextEdit.TabIndex = 17;
            // 
            // edtPriceFilter
            // 
            this.edtPriceFilter.Location = new System.Drawing.Point(1004, 45);
            this.edtPriceFilter.MenuManager = this.ribbonControl1;
            this.edtPriceFilter.Name = "edtPriceFilter";
            this.edtPriceFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.edtPriceFilter.Properties.Items.AddRange(new object[] {
            "Beide",
            "Kostprijs",
            "Verkoopprijs"});
            this.edtPriceFilter.Properties.Sorted = true;
            this.edtPriceFilter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.edtPriceFilter.Size = new System.Drawing.Size(158, 20);
            this.edtPriceFilter.StyleController = this.dataLayoutControl1;
            this.edtPriceFilter.TabIndex = 19;
            // 
            // TotalCostPriceTextEdit
            // 
            this.TotalCostPriceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalCostPrice", true));
            this.TotalCostPriceTextEdit.Location = new System.Drawing.Point(152, 189);
            this.TotalCostPriceTextEdit.MenuManager = this.ribbonControl1;
            this.TotalCostPriceTextEdit.Name = "TotalCostPriceTextEdit";
            this.TotalCostPriceTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalCostPriceTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalCostPriceTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalCostPriceTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalCostPriceTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalCostPriceTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostPriceTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostPriceTextEdit.Properties.ReadOnly = true;
            this.TotalCostPriceTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalCostPriceTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostPriceTextEdit.TabIndex = 20;
            // 
            // TotalSalesPriceTextEdit
            // 
            this.TotalSalesPriceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPrice", true));
            this.TotalSalesPriceTextEdit.Location = new System.Drawing.Point(436, 189);
            this.TotalSalesPriceTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPriceTextEdit.Name = "TotalSalesPriceTextEdit";
            this.TotalSalesPriceTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPriceTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPriceTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPriceTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPriceTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalSalesPriceTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPriceTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPriceTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPriceTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPriceTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPriceTextEdit.TabIndex = 21;
            // 
            // TotalDifferenceTextEdit
            // 
            this.TotalDifferenceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalDifference", true));
            this.TotalDifferenceTextEdit.Location = new System.Drawing.Point(720, 140);
            this.TotalDifferenceTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferenceTextEdit.Name = "TotalDifferenceTextEdit";
            this.TotalDifferenceTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferenceTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferenceTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalDifferenceTextEdit.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.TotalDifferenceTextEdit.Properties.MaskSettings.Set("mask", "C2");
            this.TotalDifferenceTextEdit.Properties.ReadOnly = true;
            this.TotalDifferenceTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferenceTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferenceTextEdit.TabIndex = 22;
            // 
            // TOTALKGSTEELTextEdit
            // 
            this.TOTALKGSTEELTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TOTALKGSTEEL", true));
            this.TOTALKGSTEELTextEdit.Location = new System.Drawing.Point(1004, 69);
            this.TOTALKGSTEELTextEdit.MenuManager = this.ribbonControl1;
            this.TOTALKGSTEELTextEdit.Name = "TOTALKGSTEELTextEdit";
            this.TOTALKGSTEELTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TOTALKGSTEELTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TOTALKGSTEELTextEdit.Properties.Mask.EditMask = "N2";
            this.TOTALKGSTEELTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TOTALKGSTEELTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TOTALKGSTEELTextEdit.Properties.ReadOnly = true;
            this.TOTALKGSTEELTextEdit.Size = new System.Drawing.Size(158, 20);
            this.TOTALKGSTEELTextEdit.StyleController = this.dataLayoutControl1;
            this.TOTALKGSTEELTextEdit.TabIndex = 23;
            // 
            // TOTALSURFACEAREATextEdit
            // 
            this.TOTALSURFACEAREATextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TOTALSURFACEAREA", true));
            this.TOTALSURFACEAREATextEdit.Location = new System.Drawing.Point(1004, 93);
            this.TOTALSURFACEAREATextEdit.MenuManager = this.ribbonControl1;
            this.TOTALSURFACEAREATextEdit.Name = "TOTALSURFACEAREATextEdit";
            this.TOTALSURFACEAREATextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TOTALSURFACEAREATextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TOTALSURFACEAREATextEdit.Properties.Mask.EditMask = "N2";
            this.TOTALSURFACEAREATextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TOTALSURFACEAREATextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TOTALSURFACEAREATextEdit.Properties.ReadOnly = true;
            this.TOTALSURFACEAREATextEdit.Size = new System.Drawing.Size(158, 20);
            this.TOTALSURFACEAREATextEdit.StyleController = this.dataLayoutControl1;
            this.TOTALSURFACEAREATextEdit.TabIndex = 24;
            // 
            // TotalPricePerKgTextEdit
            // 
            this.TotalPricePerKgTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalPricePerKg", true));
            this.TotalPricePerKgTextEdit.Location = new System.Drawing.Point(152, 213);
            this.TotalPricePerKgTextEdit.MenuManager = this.ribbonControl1;
            this.TotalPricePerKgTextEdit.Name = "TotalPricePerKgTextEdit";
            this.TotalPricePerKgTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.TotalPricePerKgTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalPricePerKgTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalPricePerKgTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalPricePerKgTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalPricePerKgTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalPricePerKgTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalPricePerKgTextEdit.Properties.ReadOnly = true;
            this.TotalPricePerKgTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalPricePerKgTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalPricePerKgTextEdit.TabIndex = 25;
            // 
            // TotalSalesPricePerKgTextEdit
            // 
            this.TotalSalesPricePerKgTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalSalesPricePerKg", true));
            this.TotalSalesPricePerKgTextEdit.Location = new System.Drawing.Point(436, 213);
            this.TotalSalesPricePerKgTextEdit.MenuManager = this.ribbonControl1;
            this.TotalSalesPricePerKgTextEdit.Name = "TotalSalesPricePerKgTextEdit";
            this.TotalSalesPricePerKgTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.TotalSalesPricePerKgTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TotalSalesPricePerKgTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalSalesPricePerKgTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalSalesPricePerKgTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalSalesPricePerKgTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalSalesPricePerKgTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalSalesPricePerKgTextEdit.Properties.ReadOnly = true;
            this.TotalSalesPricePerKgTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalSalesPricePerKgTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalSalesPricePerKgTextEdit.TabIndex = 26;
            // 
            // TotalDifferencePerKgTextEdit
            // 
            this.TotalDifferencePerKgTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "TotalDifferencePerKg", true));
            this.TotalDifferencePerKgTextEdit.Location = new System.Drawing.Point(720, 212);
            this.TotalDifferencePerKgTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferencePerKgTextEdit.Name = "TotalDifferencePerKgTextEdit";
            this.TotalDifferencePerKgTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferencePerKgTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferencePerKgTextEdit.Properties.Mask.EditMask = "C2";
            this.TotalDifferencePerKgTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalDifferencePerKgTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalDifferencePerKgTextEdit.Properties.ReadOnly = true;
            this.TotalDifferencePerKgTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferencePerKgTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferencePerKgTextEdit.TabIndex = 27;
            // 
            // TotalDifferencePercentageTextEdit
            // 
            this.TotalDifferencePercentageTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "DifferencePercentageOverCostPrice", true));
            this.TotalDifferencePercentageTextEdit.Location = new System.Drawing.Point(720, 164);
            this.TotalDifferencePercentageTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferencePercentageTextEdit.Name = "TotalDifferencePercentageTextEdit";
            this.TotalDifferencePercentageTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferencePercentageTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferencePercentageTextEdit.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.TotalDifferencePercentageTextEdit.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.TotalDifferencePercentageTextEdit.Properties.MaskSettings.Set("mask", "p");
            this.TotalDifferencePercentageTextEdit.Properties.ReadOnly = true;
            this.TotalDifferencePercentageTextEdit.Properties.UseMaskAsDisplayFormat = true;
            this.TotalDifferencePercentageTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferencePercentageTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferencePercentageTextEdit.TabIndex = 28;
            // 
            // edtShowPerKiloFields
            // 
            this.edtShowPerKiloFields.Location = new System.Drawing.Point(876, 141);
            this.edtShowPerKiloFields.MenuManager = this.ribbonControl1;
            this.edtShowPerKiloFields.Name = "edtShowPerKiloFields";
            this.edtShowPerKiloFields.Properties.Caption = "Toon \'per kilo\' velden";
            this.edtShowPerKiloFields.Size = new System.Drawing.Size(286, 20);
            this.edtShowPerKiloFields.StyleController = this.dataLayoutControl1;
            this.edtShowPerKiloFields.TabIndex = 30;
            // 
            // TotalDifferencePercentageOverSalesPriceTextEdit
            // 
            this.TotalDifferencePercentageOverSalesPriceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationTotalsBindingSource, "DifferencePercentageOverSalesPrice", true));
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Location = new System.Drawing.Point(720, 188);
            this.TotalDifferencePercentageOverSalesPriceTextEdit.MenuManager = this.ribbonControl1;
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Name = "TotalDifferencePercentageOverSalesPriceTextEdit";
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.MaskSettings.Set("mask", "p");
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.ReadOnly = true;
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties.UseMaskAsDisplayFormat = true;
            this.TotalDifferencePercentageOverSalesPriceTextEdit.Size = new System.Drawing.Size(128, 20);
            this.TotalDifferencePercentageOverSalesPriceTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalDifferencePercentageOverSalesPriceTextEdit.TabIndex = 31;
            // 
            // ItemForTotalCostPriceTailRules
            // 
            this.ItemForTotalCostPriceTailRules.Control = this.TotalCostPriceTailRulesTextEdit;
            this.ItemForTotalCostPriceTailRules.Location = new System.Drawing.Point(0, 106);
            this.ItemForTotalCostPriceTailRules.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceTailRules.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceTailRules.Name = "ItemForTotalCostPriceTailRules";
            this.ItemForTotalCostPriceTailRules.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceTailRules.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalCostPriceTailRules.Text = "Staartkosten";
            this.ItemForTotalCostPriceTailRules.TextSize = new System.Drawing.Size(152, 13);
            // 
            // ItemForTotalDifferenceHours
            // 
            this.ItemForTotalDifferenceHours.Control = this.TotalDifferenceHoursTextEdit;
            this.ItemForTotalDifferenceHours.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalDifferenceHours.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceHours.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceHours.Name = "ItemForTotalDifferenceHours";
            this.ItemForTotalDifferenceHours.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceHours.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalDifferenceHours.Text = "Manuren";
            this.ItemForTotalDifferenceHours.TextSize = new System.Drawing.Size(152, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1186, 257);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgCostPrices,
            this.lcgSalesPrices,
            this.lcgDifferences,
            this.lcgMisc});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1166, 237);
            // 
            // lcgCostPrices
            // 
            this.lcgCostPrices.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalCostPriceHours,
            this.ItemForTotalCostPriceWorkActivity,
            this.ItemForTotalCostPriceItems,
            this.ItemForTotalCostPriceOutsourced,
            this.ItemForTotalCostPrice,
            this.simpleSeparator2,
            this.emptySpaceItem2,
            this.ItemForTotalPricePerKg});
            this.lcgCostPrices.Location = new System.Drawing.Point(0, 0);
            this.lcgCostPrices.Name = "lcgCostPrices";
            this.lcgCostPrices.Size = new System.Drawing.Size(284, 237);
            this.lcgCostPrices.Text = "Kostprijzen";
            // 
            // ItemForTotalCostPriceHours
            // 
            this.ItemForTotalCostPriceHours.Control = this.TotalCostPriceHoursTextEdit;
            this.ItemForTotalCostPriceHours.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalCostPriceHours.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceHours.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceHours.Name = "ItemForTotalCostPriceHours";
            this.ItemForTotalCostPriceHours.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceHours.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalCostPriceHours.Text = "Manuren";
            this.ItemForTotalCostPriceHours.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalCostPriceWorkActivity
            // 
            this.ItemForTotalCostPriceWorkActivity.Control = this.TotalCostPriceWorkActivityTextEdit;
            this.ItemForTotalCostPriceWorkActivity.Location = new System.Drawing.Point(0, 24);
            this.ItemForTotalCostPriceWorkActivity.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceWorkActivity.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceWorkActivity.Name = "ItemForTotalCostPriceWorkActivity";
            this.ItemForTotalCostPriceWorkActivity.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceWorkActivity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalCostPriceWorkActivity.Text = "Arbeid";
            this.ItemForTotalCostPriceWorkActivity.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalCostPriceItems
            // 
            this.ItemForTotalCostPriceItems.Control = this.TotalCostPriceItemsTextEdit;
            this.ItemForTotalCostPriceItems.Location = new System.Drawing.Point(0, 48);
            this.ItemForTotalCostPriceItems.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceItems.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceItems.Name = "ItemForTotalCostPriceItems";
            this.ItemForTotalCostPriceItems.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceItems.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalCostPriceItems.Text = "Materialen";
            this.ItemForTotalCostPriceItems.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalCostPriceOutsourced
            // 
            this.ItemForTotalCostPriceOutsourced.Control = this.TotalCostPriceOutsourcedTextEdit;
            this.ItemForTotalCostPriceOutsourced.Location = new System.Drawing.Point(0, 72);
            this.ItemForTotalCostPriceOutsourced.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceOutsourced.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceOutsourced.Name = "ItemForTotalCostPriceOutsourced";
            this.ItemForTotalCostPriceOutsourced.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPriceOutsourced.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalCostPriceOutsourced.Text = "Uitbesteedwerk";
            this.ItemForTotalCostPriceOutsourced.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalCostPrice
            // 
            this.ItemForTotalCostPrice.Control = this.TotalCostPriceTextEdit;
            this.ItemForTotalCostPrice.Location = new System.Drawing.Point(0, 144);
            this.ItemForTotalCostPrice.Name = "ItemForTotalCostPrice";
            this.ItemForTotalCostPrice.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalCostPrice.Text = "Totaal kostprijs";
            this.ItemForTotalCostPrice.TextSize = new System.Drawing.Size(116, 13);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 143);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(260, 1);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 96);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(260, 47);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTotalPricePerKg
            // 
            this.ItemForTotalPricePerKg.Control = this.TotalPricePerKgTextEdit;
            this.ItemForTotalPricePerKg.Location = new System.Drawing.Point(0, 168);
            this.ItemForTotalPricePerKg.Name = "ItemForTotalPricePerKg";
            this.ItemForTotalPricePerKg.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalPricePerKg.Text = "Per kilo staal";
            this.ItemForTotalPricePerKg.TextSize = new System.Drawing.Size(116, 13);
            // 
            // lcgSalesPrices
            // 
            this.lcgSalesPrices.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalSalesPriceHours,
            this.ItemForTotalSalesPriceWorkActivity,
            this.ItemForTotalSalesPriceItems,
            this.ItemForTotalSalesPriceOutsourced,
            this.ItemForTotalSalesPrice,
            this.simpleSeparator1,
            this.ItemForTotalSalesPriceTailRules,
            this.ItemForTotalSalesPricePerKg,
            this.emptySpaceItem5});
            this.lcgSalesPrices.Location = new System.Drawing.Point(284, 0);
            this.lcgSalesPrices.Name = "lcgSalesPrices";
            this.lcgSalesPrices.Size = new System.Drawing.Size(284, 237);
            this.lcgSalesPrices.Text = "Verkoopprijzen";
            // 
            // ItemForTotalSalesPriceHours
            // 
            this.ItemForTotalSalesPriceHours.Control = this.TotalSalesPriceHoursTextEdit;
            this.ItemForTotalSalesPriceHours.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalSalesPriceHours.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceHours.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceHours.Name = "ItemForTotalSalesPriceHours";
            this.ItemForTotalSalesPriceHours.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceHours.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalSalesPriceHours.Text = "Manuren";
            this.ItemForTotalSalesPriceHours.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalSalesPriceWorkActivity
            // 
            this.ItemForTotalSalesPriceWorkActivity.Control = this.TotalSalesPriceWorkActivityTextEdit;
            this.ItemForTotalSalesPriceWorkActivity.Location = new System.Drawing.Point(0, 24);
            this.ItemForTotalSalesPriceWorkActivity.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceWorkActivity.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceWorkActivity.Name = "ItemForTotalSalesPriceWorkActivity";
            this.ItemForTotalSalesPriceWorkActivity.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceWorkActivity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalSalesPriceWorkActivity.Text = "Arbeid";
            this.ItemForTotalSalesPriceWorkActivity.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalSalesPriceItems
            // 
            this.ItemForTotalSalesPriceItems.Control = this.TotalSalesPriceItemsTextEdit;
            this.ItemForTotalSalesPriceItems.Location = new System.Drawing.Point(0, 48);
            this.ItemForTotalSalesPriceItems.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceItems.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceItems.Name = "ItemForTotalSalesPriceItems";
            this.ItemForTotalSalesPriceItems.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceItems.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalSalesPriceItems.Text = "Materialen";
            this.ItemForTotalSalesPriceItems.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalSalesPriceOutsourced
            // 
            this.ItemForTotalSalesPriceOutsourced.Control = this.TotalSalesPriceOutsourcedTextEdit;
            this.ItemForTotalSalesPriceOutsourced.Location = new System.Drawing.Point(0, 72);
            this.ItemForTotalSalesPriceOutsourced.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceOutsourced.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceOutsourced.Name = "ItemForTotalSalesPriceOutsourced";
            this.ItemForTotalSalesPriceOutsourced.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceOutsourced.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalSalesPriceOutsourced.Text = "Uitbesteedwerk";
            this.ItemForTotalSalesPriceOutsourced.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalSalesPrice
            // 
            this.ItemForTotalSalesPrice.Control = this.TotalSalesPriceTextEdit;
            this.ItemForTotalSalesPrice.Location = new System.Drawing.Point(0, 144);
            this.ItemForTotalSalesPrice.Name = "ItemForTotalSalesPrice";
            this.ItemForTotalSalesPrice.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPrice.Text = "Totaal verkoopprijs";
            this.ItemForTotalSalesPrice.TextSize = new System.Drawing.Size(116, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 143);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(260, 1);
            // 
            // ItemForTotalSalesPriceTailRules
            // 
            this.ItemForTotalSalesPriceTailRules.Control = this.TotalSalesPriceTailRulesTextEdit;
            this.ItemForTotalSalesPriceTailRules.Location = new System.Drawing.Point(0, 96);
            this.ItemForTotalSalesPriceTailRules.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceTailRules.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceTailRules.Name = "ItemForTotalSalesPriceTailRules";
            this.ItemForTotalSalesPriceTailRules.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPriceTailRules.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalSalesPriceTailRules.Text = "Staartkosten";
            this.ItemForTotalSalesPriceTailRules.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalSalesPricePerKg
            // 
            this.ItemForTotalSalesPricePerKg.Control = this.TotalSalesPricePerKgTextEdit;
            this.ItemForTotalSalesPricePerKg.Location = new System.Drawing.Point(0, 168);
            this.ItemForTotalSalesPricePerKg.Name = "ItemForTotalSalesPricePerKg";
            this.ItemForTotalSalesPricePerKg.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalSalesPricePerKg.Text = "Per kilo staal";
            this.ItemForTotalSalesPricePerKg.TextSize = new System.Drawing.Size(116, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(260, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgDifferences
            // 
            this.lcgDifferences.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTotalDifferenceWorkActivity,
            this.ItemForTotalDifferenceItems,
            this.ItemForTotalDifferenceOutsourced,
            this.simpleSeparator3,
            this.simpleSeparator5,
            this.emptySpaceItem4,
            this.ItemForTotalDifferencePerKg,
            this.ItemForTotalDifferencePercentage,
            this.ItemForTotalDifference,
            this.ItemForTotalMargenPercentageOverSalesPrice});
            this.lcgDifferences.Location = new System.Drawing.Point(568, 0);
            this.lcgDifferences.Name = "lcgDifferences";
            this.lcgDifferences.Size = new System.Drawing.Size(284, 237);
            this.lcgDifferences.Text = "Verschillen";
            // 
            // ItemForTotalDifferenceWorkActivity
            // 
            this.ItemForTotalDifferenceWorkActivity.Control = this.TotalDifferenceWorkActivityTextEdit;
            this.ItemForTotalDifferenceWorkActivity.Location = new System.Drawing.Point(0, 22);
            this.ItemForTotalDifferenceWorkActivity.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceWorkActivity.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceWorkActivity.Name = "ItemForTotalDifferenceWorkActivity";
            this.ItemForTotalDifferenceWorkActivity.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceWorkActivity.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalDifferenceWorkActivity.Text = "Arbeid";
            this.ItemForTotalDifferenceWorkActivity.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalDifferenceItems
            // 
            this.ItemForTotalDifferenceItems.Control = this.TotalDifferenceItemsTextEdit;
            this.ItemForTotalDifferenceItems.Location = new System.Drawing.Point(0, 46);
            this.ItemForTotalDifferenceItems.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceItems.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceItems.Name = "ItemForTotalDifferenceItems";
            this.ItemForTotalDifferenceItems.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceItems.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalDifferenceItems.Text = "Materialen";
            this.ItemForTotalDifferenceItems.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalDifferenceOutsourced
            // 
            this.ItemForTotalDifferenceOutsourced.Control = this.TotalDifferenceOutsourcedTextEdit;
            this.ItemForTotalDifferenceOutsourced.Location = new System.Drawing.Point(0, 70);
            this.ItemForTotalDifferenceOutsourced.MaxSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceOutsourced.MinSize = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceOutsourced.Name = "ItemForTotalDifferenceOutsourced";
            this.ItemForTotalDifferenceOutsourced.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferenceOutsourced.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTotalDifferenceOutsourced.Text = "Uitbesteedwerk";
            this.ItemForTotalDifferenceOutsourced.TextSize = new System.Drawing.Size(116, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 94);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(260, 1);
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.Location = new System.Drawing.Point(0, 191);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.Size = new System.Drawing.Size(260, 1);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(260, 22);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTotalDifferencePerKg
            // 
            this.ItemForTotalDifferencePerKg.Control = this.TotalDifferencePerKgTextEdit;
            this.ItemForTotalDifferencePerKg.Location = new System.Drawing.Point(0, 167);
            this.ItemForTotalDifferencePerKg.Name = "ItemForTotalDifferencePerKg";
            this.ItemForTotalDifferencePerKg.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferencePerKg.Text = "Per kilo staal";
            this.ItemForTotalDifferencePerKg.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalDifferencePercentage
            // 
            this.ItemForTotalDifferencePercentage.Control = this.TotalDifferencePercentageTextEdit;
            this.ItemForTotalDifferencePercentage.Location = new System.Drawing.Point(0, 119);
            this.ItemForTotalDifferencePercentage.Name = "ItemForTotalDifferencePercentage";
            this.ItemForTotalDifferencePercentage.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifferencePercentage.Text = "Opslag %";
            this.ItemForTotalDifferencePercentage.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalDifference
            // 
            this.ItemForTotalDifference.Control = this.TotalDifferenceTextEdit;
            this.ItemForTotalDifference.Location = new System.Drawing.Point(0, 95);
            this.ItemForTotalDifference.Name = "ItemForTotalDifference";
            this.ItemForTotalDifference.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalDifference.Text = "Verwacht rendement";
            this.ItemForTotalDifference.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTotalMargenPercentageOverSalesPrice
            // 
            this.ItemForTotalMargenPercentageOverSalesPrice.Control = this.TotalDifferencePercentageOverSalesPriceTextEdit;
            this.ItemForTotalMargenPercentageOverSalesPrice.Location = new System.Drawing.Point(0, 143);
            this.ItemForTotalMargenPercentageOverSalesPrice.Name = "ItemForTotalMargenPercentageOverSalesPrice";
            this.ItemForTotalMargenPercentageOverSalesPrice.Size = new System.Drawing.Size(260, 24);
            this.ItemForTotalMargenPercentageOverSalesPrice.Text = "Rendement %";
            this.ItemForTotalMargenPercentageOverSalesPrice.TextSize = new System.Drawing.Size(116, 13);
            // 
            // lcgMisc
            // 
            this.lcgMisc.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.lciShowPriceTypes,
            this.ItemForTOTALKGSTEEL,
            this.ItemForTOTALSURFACEAREA,
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.lcgMisc.Location = new System.Drawing.Point(852, 0);
            this.lcgMisc.Name = "lcgMisc";
            this.lcgMisc.Size = new System.Drawing.Size(314, 237);
            this.lcgMisc.Text = "Overig";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(290, 72);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lciShowPriceTypes
            // 
            this.lciShowPriceTypes.Control = this.edtPriceFilter;
            this.lciShowPriceTypes.Location = new System.Drawing.Point(0, 0);
            this.lciShowPriceTypes.Name = "lciShowPriceTypes";
            this.lciShowPriceTypes.Size = new System.Drawing.Size(290, 24);
            this.lciShowPriceTypes.Text = "Toon prijzen:";
            this.lciShowPriceTypes.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTOTALKGSTEEL
            // 
            this.ItemForTOTALKGSTEEL.Control = this.TOTALKGSTEELTextEdit;
            this.ItemForTOTALKGSTEEL.CustomizationFormText = "Totaal kilo staal:";
            this.ItemForTOTALKGSTEEL.Location = new System.Drawing.Point(0, 24);
            this.ItemForTOTALKGSTEEL.Name = "ItemForTOTALKGSTEEL";
            this.ItemForTOTALKGSTEEL.Size = new System.Drawing.Size(290, 24);
            this.ItemForTOTALKGSTEEL.Text = "Totaal kilo staal:";
            this.ItemForTOTALKGSTEEL.TextSize = new System.Drawing.Size(116, 13);
            // 
            // ItemForTOTALSURFACEAREA
            // 
            this.ItemForTOTALSURFACEAREA.Control = this.TOTALSURFACEAREATextEdit;
            this.ItemForTOTALSURFACEAREA.Location = new System.Drawing.Point(0, 48);
            this.ItemForTOTALSURFACEAREA.Name = "ItemForTOTALSURFACEAREA";
            this.ItemForTOTALSURFACEAREA.Size = new System.Drawing.Size(290, 24);
            this.ItemForTOTALSURFACEAREA.Text = "Totaal m2 conservering:";
            this.ItemForTOTALSURFACEAREA.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.edtColumnFilter;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(290, 24);
            this.layoutControlItem1.Text = "Actieve kolommen:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.edtShowPerKiloFields;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(290, 24);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // dragDropEventsGridView
            // 
            this.dragDropEventsGridView.DragOver += new DevExpress.Utils.DragDrop.DragOverEventHandler(this.dragDropEventsGridView_DragOver);
            this.dragDropEventsGridView.DragDrop += new DevExpress.Utils.DragDrop.DragDropEventHandler(this.dragDropEventsGridView_DragDrop);
            // 
            // tabPane
            // 
            this.tabPane.Controls.Add(this.tabPageTotals);
            this.tabPane.Controls.Add(this.tabPageTailRules);
            this.tabPane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabPane.Location = new System.Drawing.Point(0, 601);
            this.tabPane.Name = "tabPane";
            this.tabPane.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabPageTotals,
            this.tabPageTailRules});
            this.tabPane.RegularSize = new System.Drawing.Size(1190, 294);
            this.tabPane.SelectedPage = this.tabPageTotals;
            this.tabPane.Size = new System.Drawing.Size(1190, 294);
            this.tabPane.TabIndex = 9;
            this.tabPane.Text = "tabPane1";
            // 
            // tabPageTotals
            // 
            this.tabPageTotals.Caption = "Totalen";
            this.tabPageTotals.Controls.Add(this.groupTotals);
            this.tabPageTotals.Name = "tabPageTotals";
            this.tabPageTotals.Size = new System.Drawing.Size(1190, 261);
            // 
            // tabPageTailRules
            // 
            this.tabPageTailRules.Caption = "Staartregels";
            this.tabPageTailRules.Controls.Add(this.gcTailRules);
            this.tabPageTailRules.Name = "tabPageTailRules";
            this.tabPageTailRules.Size = new System.Drawing.Size(1172, 239);
            // 
            // gcTailRules
            // 
            this.gcTailRules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcTailRules.Location = new System.Drawing.Point(0, 0);
            this.gcTailRules.MainView = this.gvTailRules;
            this.gcTailRules.MenuManager = this.ribbonControl1;
            this.gcTailRules.Name = "gcTailRules";
            this.gcTailRules.Size = new System.Drawing.Size(1172, 239);
            this.gcTailRules.TabIndex = 0;
            this.gcTailRules.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTailRules});
            // 
            // gvTailRules
            // 
            this.gvTailRules.GridControl = this.gcTailRules;
            this.gvTailRules.Name = "gvTailRules";
            this.gvTailRules.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gvTailRules.OptionsBehavior.AutoPopulateColumns = false;
            this.gvTailRules.OptionsCustomization.AllowColumnMoving = false;
            this.gvTailRules.OptionsCustomization.AllowColumnResizing = false;
            this.gvTailRules.OptionsCustomization.AllowSort = false;
            this.gvTailRules.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            this.gvTailRules.OptionsView.ShowGroupPanel = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 158);
            this.splitContainerControl1.Name = "splitContainerControl1";
            // 
            // splitContainerControl1.Panel1
            // 
            this.splitContainerControl1.Panel1.Controls.Add(this.gcNavigation);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            // 
            // splitContainerControl1.Panel2
            // 
            this.splitContainerControl1.Panel2.Controls.Add(this.gcGrid);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1190, 443);
            this.splitContainerControl1.SplitterPosition = 168;
            this.splitContainerControl1.TabIndex = 10;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.ActAsDropDown = true;
            this.barButtonItem2.AllowDrawArrow = false;
            this.barButtonItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem2.Caption = "Toevoegen";
            this.barButtonItem2.DropDownControl = this.InsertMenu;
            this.barButtonItem2.Id = 1;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "moveup_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "movedown_16x16.png");
            // 
            // MainForm
            // 
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1190, 895);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.tabPane);
            this.Controls.Add(this.ribbonControl1);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("MainForm.IconOptions.Icon")));
            this.MinimumSize = new System.Drawing.Size(1192, 896);
            this.Name = "MainForm";
            this.Ribbon = this.ribbonControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ridder Calculate - Main form";
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InsertMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNavigation)).EndInit();
            this.gcNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListCalculations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcGrid)).EndInit();
            this.gcGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupTotals)).EndInit();
            this.groupTotals.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtColumnFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceHoursTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationTotalsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceWorkActivityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceItemsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceOutsourcedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceTailRulesTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceHoursTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceWorkActivityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceItemsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceOutsourcedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceTailRulesTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceHoursTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceWorkActivityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceItemsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceOutsourcedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtPriceFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostPriceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPriceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferenceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALKGSTEELTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALSURFACEAREATextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalPricePerKgTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalSalesPricePerKgTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferencePerKgTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferencePercentageTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtShowPerKiloFields.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalDifferencePercentageOverSalesPriceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceTailRules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgCostPrices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceWorkActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPriceOutsourced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCostPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalPricePerKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgSalesPrices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceWorkActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceOutsourced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPriceTailRules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalSalesPricePerKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDifferences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceWorkActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferenceOutsourced)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferencePerKg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifferencePercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalMargenPercentageOverSalesPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMisc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciShowPriceTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALKGSTEEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALSURFACEAREA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane)).EndInit();
            this.tabPane.ResumeLayout(false);
            this.tabPageTotals.ResumeLayout(false);
            this.tabPageTailRules.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTailRules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTailRules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1.Panel1)).EndInit();
            this.splitContainerControl1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1.Panel2)).EndInit();
            this.splitContainerControl1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private DevExpress.XtraBars.BarCheckItem barButtonMultiSelect;

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private RibbonPage rpMain;
        private RibbonPageGroup rpgPrint;
        private GroupControl gcNavigation;
        private GridControl gcMain;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gridView1;
        private DevExpress.XtraBars.BarButtonItem barButtonInsert;
        private BarButtonItem barButtonDelete;
        private BarButtonItem barButtonSave;
        private BarButtonItem barButtonClose;
        private BarButtonItem barButtonCopy;
        private BarButtonItem barButtonCut;
        private BarButtonItem barButtonPaste;
        private BarButtonItem barButtonMoveUp;
        private BarButtonItem barButtonMoveDown;
        private BarButtonItem barButtonPrint;
        private BarButtonItem barButtonUndo;
        private BarButtonItem barButtonRedo;
        private RibbonPageGroup rpgEdit;
        private RibbonPageGroup rpgClipboard;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgMisc;
        private RibbonPageGroup rpgNavigation;
        private DevExpress.XtraTreeList.TreeList treeListCalculations;
        private GroupControl gcGrid;
        private PopupMenu reportMenu;
        private BarButtonItem barButtonConvert;
        private DevExpress.XtraBars.Docking.DockManager dockManager;
        private GroupControl groupTotals;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private TextEdit TotalCostPriceHoursTextEdit;
        private System.Windows.Forms.BindingSource calculationTotalsBindingSource;
        private TextEdit TotalCostPriceWorkActivityTextEdit;
        private TextEdit TotalCostPriceItemsTextEdit;
        private TextEdit TotalCostPriceOutsourcedTextEdit;
        private TextEdit TotalCostPriceTailRulesTextEdit;
        private TextEdit TotalSalesPriceHoursTextEdit;
        private TextEdit TotalSalesPriceWorkActivityTextEdit;
        private TextEdit TotalSalesPriceItemsTextEdit;
        private TextEdit TotalSalesPriceOutsourcedTextEdit;
        private TextEdit TotalSalesPriceTailRulesTextEdit;
        private TextEdit TotalDifferenceHoursTextEdit;
        private TextEdit TotalDifferenceWorkActivityTextEdit;
        private TextEdit TotalDifferenceItemsTextEdit;
        private TextEdit TotalDifferenceOutsourcedTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostPriceHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostPriceWorkActivity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostPriceItems;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostPriceOutsourced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostPriceTailRules;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPriceHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPriceWorkActivity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPriceItems;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPriceOutsourced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPriceTailRules;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifferenceHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifferenceWorkActivity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifferenceOutsourced;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifferenceItems;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraLayout.LayoutControlGroup lcgCostPrices;
        private DevExpress.XtraLayout.LayoutControlGroup lcgSalesPrices;
        private DevExpress.XtraLayout.LayoutControlGroup lcgDifferences;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private ComboBoxEdit edtPriceFilter;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMisc;
        private DevExpress.XtraLayout.LayoutControlItem lciShowPriceTypes;
        private TextEdit TotalCostPriceTextEdit;
        private TextEdit TotalSalesPriceTextEdit;
        private TextEdit TotalDifferenceTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCostPrice;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPrice;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifference;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.Navigation.TabPane tabPane;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabPageTotals;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabPageTailRules;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private GridControl gcTailRules;
        private GridView gvTailRules;
        private BarButtonItem btnTailRulesInsert;
        private BarButtonItem btnTailRulesDelete;
        private RibbonPageGroup rpgTailRules;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcolDescription;
        private PopupMenu InsertMenu;
        private BarButtonItem InsertMenuCalculation;
        private BarButtonItem InsertMenuCalculationRule;
        private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit repositoryItemRichTextEdit1;
        private TextEdit TOTALKGSTEELTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTOTALKGSTEEL;
        private TextEdit TOTALSURFACEAREATextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTOTALSURFACEAREA;
        private TextEdit TotalPricePerKgTextEdit;
        private TextEdit TotalSalesPricePerKgTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalPricePerKg;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalSalesPricePerKg;
        private TextEdit TotalDifferencePerKgTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifferencePerKg;
        private BarButtonItem InsertMenuExistingCalculation;
        private TextEdit TotalDifferencePercentageTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalDifferencePercentage;
        private BarButtonItem barButtonOpen;
        private BarButtonItem barButtonItem2;
        private CheckedComboBoxEdit edtColumnFilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.Utils.DragDrop.DragDropEvents dragDropEventsGridView;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private CheckEdit edtShowPerKiloFields;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private TextEdit TotalDifferencePercentageOverSalesPriceTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalMargenPercentageOverSalesPrice;
    }
}

