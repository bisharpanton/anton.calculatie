﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Calculation.Core.Annotations;
using Calculation.Core.Classes;
using Calculation.Core.Classes.Base;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    [Table("C_CALCULATIONTAILRULES")]
    public partial class CalculationTailRule : IDatabaseObject
    {
        private double _percentage;
        private double _amount;
        private double _total;
        private string _description;

        [PrimaryKey]
        public int Id { get; set; }

        public event EventHandler<ExtendedObjectChangedEventArgs> CustomPropertyChanged;

        [Column("FK_CALCULATION")]
        public int CalculationId { get; set; }

        [Ignore]
        public Calculation Calculation { get; set; }

        [Column("SEQUENCENUMBER")]
        public int SequenceNumber { get; set; }

        [Column("PERCENTAGE")]
        public double Percentage
        {
            get { return _percentage; }
            set
            {
                if (value.Equals(_percentage)) return;
                //_percentage = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _percentage, value);
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnCustomPropertyChanged<T>(ref T privateField, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (Equals(newValue, privateField))
                return;

            var oldValue = privateField;
            privateField = newValue;

            if (DisableDataChanges || DatabaseHelper.IsLoading)
                return;

            //Recalculate(propertyName);

            CustomPropertyChanged?.Invoke(this, new ExtendedObjectChangedEventArgs(this, propertyName, oldValue, newValue));
        }

        [Column("AMOUNT")]
        public double Amount
        {
            get { return _amount; }
            set
            {
                if (value.Equals(_amount)) return;
                //_amount = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _amount, value);
            }
        }

        [Column("TOTAL")]
        public double Total
        {
            get { return _total; }
            set
            {
                if (value.Equals(_total)) return;
                //_total = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _total, value);
            }
        }

        [Column("DESCRIPTION")]
        public string Description
        {
            get { return _description; }
            set
            {
                if (value == _description) return;
                _description = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [Ignore]
        public bool DisableDataChanges { get; set; }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (DisableDataChanges)
                return;

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int CompareTo(object obj)
        {
            return 0;
        }
    }
}