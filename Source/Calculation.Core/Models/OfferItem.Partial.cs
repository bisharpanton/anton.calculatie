﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class OfferItem
    {
        private Offer _offer;

        [ForeignKey("FK_OFFER")]
        public Offer Offer
        {
            get { return _offer; }
            set
            {
                _offer = value;
                _FK_OFFER = value == null ? 0 : value.Id;
            }
        }
    }
}