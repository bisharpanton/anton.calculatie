//  <auto-generated />
namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_OUTSOURCEDACTIVITY
    ///  </summary>
    ///  	BATCHDEPENDENT, CODE, CREATOR, DATECHANGED, DATECREATED, DESCRIPTION, EXTERNALKEY, FK_R_OUTSOURCEDACTIVITY, FK_UNIT, FK_WORKFLOWSTATE
    ///  	ISPUMP, MAINSUPPLIER, MEMO, PK_R_OUTSOURCEDACTIVITY, PLAINTEXT_MEMO, PLANABLE, PRICEENTRYINDETAILTABLE, PURCHASEPRICE, RECORDLINK, REGISTRATIONPATH
    ///  	SALESMARKUP, UNMARKETABLE, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = '7adacd31-864a-40fd-b845-432654eb55d8' AND (ISSECRET = 0)
    ///  	Included columns: CODE, DESCRIPTION, FK_UNIT, PURCHASEPRICE, SALESMARKUP, ISPUMP, UNMARKETABLE
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_OUTSOURCEDACTIVITY")]
    public partial class OutsourcedActivity
    {
        
        private string _CODE;
        
        private string _DESCRIPTION;
        
        private int _FK_UNIT;
        
        private bool _ISPUMP;
        
        private int _id;
        
        private System.Nullable<double> _PURCHASEPRICE;
        
        private System.Nullable<double> _SALESMARKUP;
        
        private bool _UNMARKETABLE;
        
        /// <summary>
        /// 	Code
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 10
        /// </remarks>
        [Column("CODE")]
        public virtual string CODE
        {
            get
            {
                if (string.IsNullOrEmpty(this._CODE))
                {
                    return "";
                }
                return this._CODE;
            }
            set
            {
                this._CODE = value;
            }
        }
        
        /// <summary>
        /// 	Description
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 80
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Unit
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OUTSOURCEDACTIVITY
        /// </remarks>
        [Column("FK_UNIT")]
        public virtual int FK_UNIT
        {
            get
            {
                return this._FK_UNIT;
            }
            set
            {
                this._FK_UNIT = value;
            }
        }
        
        /// <summary>
        /// 	Is pomp
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("ISPUMP")]
        public virtual bool ISPUMP
        {
            get
            {
                return this._ISPUMP;
            }
            set
            {
                this._ISPUMP = value;
            }
        }
        
        /// <summary>
        /// 	Outsourced activity id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_OUTSOURCEDACTIVITY")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Purchase price
        /// </summary>
        /// <remarks>
        /// 	DataType: Money
        /// 	Size: 0
        /// </remarks>
        [Column("PURCHASEPRICE")]
        public virtual System.Nullable<double> PURCHASEPRICE
        {
            get
            {
                return this._PURCHASEPRICE;
            }
            set
            {
                this._PURCHASEPRICE = value;
            }
        }
        
        /// <summary>
        /// 	Sales markup
        /// </summary>
        /// <remarks>
        /// 	DataType: Float
        /// 	Size: 8
        /// </remarks>
        [Column("SALESMARKUP")]
        public virtual System.Nullable<double> SALESMARKUP
        {
            get
            {
                return this._SALESMARKUP;
            }
            set
            {
                this._SALESMARKUP = value;
            }
        }
        
        /// <summary>
        /// 	Unmarketable
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("UNMARKETABLE")]
        public virtual bool UNMARKETABLE
        {
            get
            {
                return this._UNMARKETABLE;
            }
            set
            {
                this._UNMARKETABLE = value;
            }
        }
    }
}
