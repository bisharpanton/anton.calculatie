﻿using System.Collections.Generic;
using Calculation.Core.Models;
using DevExpress.XtraEditors;

namespace Calculation.Win
{
    public partial class CalculationLogForm : XtraForm
    {
        public CalculationLogForm(IEnumerable<Core.Models.CalculationLog> logs)
        {
            InitializeComponent();
            calculationLogBindingSource.DataSource = logs;
            tabPageDetails.Enabled = false;

            barButtonOpenCalculationRule.ItemClick += (sender, args) =>
            {
                var calculationLog = (CalculationLog)detailLayoutControlBindingSource.DataSource;
                var calculationRule = calculationLog.CalculationRule;
                new CalculationRuleDetails(calculationRule).ShowDialog();
            };

            gvLogs.FocusedRowChanged += (sender, args) =>
            {
                tabPageDetails.Enabled = true;
                var focusedCalculationLog = (CalculationLog) gvLogs.GetRow(args.FocusedRowHandle);
                detailLayoutControlBindingSource.DataSource = focusedCalculationLog;
                barButtonOpenCalculationRule.Enabled = focusedCalculationLog.CalculationRule != null;
            };

            gvLogs.DoubleClick += (sender, args) =>
            {
                var pt = gcLogs.PointToClient(MousePosition);
                var info = gvLogs.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    var clickedCalculationLog = (CalculationLog) gvLogs.GetRow(info.RowHandle);
                    detailLayoutControlBindingSource.DataSource = clickedCalculationLog;
                    barButtonOpenCalculationRule.Enabled = clickedCalculationLog.CalculationRule != null;
                    tabControl.SelectedTabPage = tabPageDetails;
                }
            };
        }
    }
}