﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class RidderScript : WorkflowScriptInfo
{
    public void Execute()
    {
        //GMUNK
        //Sla calculatie plat

        var calculationPk = (int)this.RecordId;

        if (calculationPk == 0)
        {
            throw new Exception("Het id van de calculatie is niet correct!");
        }

        var rsCalculationExport = GetRecordset("C_FLATCALCULATION", "", string.Format("FK_CALCULATION = {0}", calculationPk), "");

        if (rsCalculationExport.RecordCount > 0)
        {
            rsCalculationExport.UpdateWhenMoveRecord = false;
            rsCalculationExport.MoveFirst();

            while (!rsCalculationExport.EOF)
            {
                rsCalculationExport.Delete();
                rsCalculationExport.MoveNext();
            }

            rsCalculationExport.MoveFirst();
            rsCalculationExport.Update();
        }

        rsCalculationExport = ExportCalculation(calculationPk, rsCalculationExport, calculationPk);

        if(rsCalculationExport.RecordCount == 0)
        {
            return;
        }

        rsCalculationExport.Update();
    }

    public ScriptRecordset ExportCalculation(int mainCalculationId, ScriptRecordset rsCalculationExport, int calculationId, int level = 1, string tree = "", double multiplier = 1)
    {
        var levelTree = tree;
        if (string.IsNullOrEmpty(levelTree))
        {
            levelTree += level;
        }

        int index = 1;

        var rsCalculationRules = GetRecordset("C_CALCULATIONRULE", "PK_C_CALCULATIONRULE, FK_SUBCALCULATION", 
            string.Format("FK_CALCULATION = {0}", calculationId), "");
        rsCalculationRules.MoveFirst();

        foreach (var calculationRule in rsCalculationRules.DataTable.AsEnumerable())
        {
            var recordTree = string.Format("{0}.{1}", levelTree, index);
            rsCalculationExport.AddNew();
            rsCalculationExport.SetFieldValue("FK_CALCULATION", mainCalculationId);
            rsCalculationExport.SetFieldValue("FK_CALCULATIONRULE", calculationRule.Field<int>("PK_C_CALCULATIONRULE"));
            rsCalculationExport.SetFieldValue("LEVEL", level);
            rsCalculationExport.SetFieldValue("TREE", recordTree);

            var subCalculation = calculationRule.Field<int?>("FK_SUBCALCULATION") ?? 0;
            if (subCalculation > 0)
            {
                rsCalculationExport = ExportCalculation(mainCalculationId, rsCalculationExport, subCalculation, level + 1, recordTree); //, multiplier * quantity);
            }

            index++;
        }

        return rsCalculationExport;
    }
}