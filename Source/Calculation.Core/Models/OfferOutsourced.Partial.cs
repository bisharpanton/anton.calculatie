﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class OfferOutSourced
    {
        [ForeignKey("FK_OFFER")]
        public Offer Offer{ get; set; }
    }
}