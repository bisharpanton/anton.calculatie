﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class RidderScript : WorkflowScriptInfo
{
    public double ItemSalesPrice;

    public void Execute()
    {
        if (System.Diagnostics.Debugger.IsAttached)
            System.Diagnostics.Debugger.Break();

        var calculationId = (int)RecordId;

        var rsQuantityMainCalculation = GetRecordset("C_CALCULATION", "QUANTITY", string.Format("PK_C_CALCULATION = '{0}'", calculationId), "");
        rsQuantityMainCalculation.MoveFirst();
        double quantityMainCalculation = ((double)rsQuantityMainCalculation.Fields["QUANTITY"].Value == 0.0) ? 1.0 : (double)rsQuantityMainCalculation.Fields["QUANTITY"].Value;

        Log(LogType.Info, calculationId, "Start converteren hoofd calculatie");
        var mainCalculation = ConvertCalculationToAssembly(calculationId, quantityMainCalculation);
        //Log(LogType.Info, calculationId, "Start converteren sub calculatie's");
        //ConvertSubCalculations(mainCalculation);

        // Add assembly to offer
        if (mainCalculation.FK_ASSEMBLY.HasValue && mainCalculation.FK_OFFER.HasValue)
        {
            Log(LogType.Info, calculationId, "Geconverteerde calculatie toevoegen aan offerte");
            var rsOfferDetailAssembly = GetRecordset("R_OFFERDETAILASSEMBLY", "", string.Format("FK_ASSEMBLY = {0} AND FK_OFFER = {1}", mainCalculation.FK_ASSEMBLY, mainCalculation.FK_OFFER), "");
            rsOfferDetailAssembly.UseDataChanges = true;
            if (rsOfferDetailAssembly.RecordCount == 0)
            {
                rsOfferDetailAssembly.AddNew();
                rsOfferDetailAssembly.SetFieldValue("FK_OFFER", mainCalculation.FK_OFFER);
                rsOfferDetailAssembly.SetFieldValue("FK_ASSEMBLY", mainCalculation.FK_ASSEMBLY);
                rsOfferDetailAssembly.SetFieldValue("USEITEMPRICE", true);
                rsOfferDetailAssembly.SetFieldValue("QUANTITY", quantityMainCalculation);
                rsOfferDetailAssembly.SetFieldValue("AANTALM2", quantityMainCalculation);
                rsOfferDetailAssembly.SetFieldValue("NETSALESAMOUNT", ItemSalesPrice * quantityMainCalculation);
                rsOfferDetailAssembly.Update();
            }
            else
            {
                rsOfferDetailAssembly.MoveFirst();
                /*
                var qty = (double)rsOfferDetailAssembly.GetField("QUANTITY").Value;
                rsOfferDetailAssembly.SetFieldValue("QUANTITY", qty);*/
                rsOfferDetailAssembly.SetFieldValue("QUANTITY", quantityMainCalculation);
                rsOfferDetailAssembly.SetFieldValue("AANTALM2", quantityMainCalculation);
                rsOfferDetailAssembly.SetFieldValue("NETSALESAMOUNT", ItemSalesPrice * quantityMainCalculation);
                rsOfferDetailAssembly.Update();
            }
        }
    }

    public void ConvertSubCalculations(Calculation parentCalculation, int subcalculation, double quantity, double quantityMainCalculation)
    {
        var subCalculationRules = GetRecordset("C_CALCULATIONRULE", "",
            string.Format("FK_CALCULATION = '{0}' AND FK_SUBCALCULATION = {1}", parentCalculation.Id, subcalculation), "")
            .DataTable.AsEnumerable().Select(CalculationRule.FromDataRow);

        if (!subCalculationRules.Any())
            return;

        var subCalculations = new List<Calculation>();

        foreach (var subCalculationRule in subCalculationRules)
        {
            var result = ConvertCalculationToAssembly(subCalculationRule.FK_SUBCALCULATION.Value, 1.0);
            //ConvertSubCalculations(result);
            subCalculations.Add(result);
        }

        var rsAssemblyDetailSubAssembly = GetRecordset("R_ASSEMBLYDETAILSUBASSEMBLY", "", "PK_R_ASSEMBLYDETAILSUBASSEMBLY = -1", "");

        foreach (var subCalculation in subCalculations)
        {
            rsAssemblyDetailSubAssembly.AddNew();
            rsAssemblyDetailSubAssembly.SetFieldValue("FK_ASSEMBLY", parentCalculation.FK_ASSEMBLY);
            rsAssemblyDetailSubAssembly.SetFieldValue("FK_SUBASSEMBLY", subCalculation.FK_ASSEMBLY);
            rsAssemblyDetailSubAssembly.SetFieldValue("DESCRIPTION", subCalculation.DESCRIPTION);
            rsAssemblyDetailSubAssembly.SetFieldValue("QUANTITY", quantity / quantityMainCalculation);
        }

        rsAssemblyDetailSubAssembly.Update2();
    }

    public Calculation ConvertCalculationToAssembly(int calculationId, double quantityMainCalculation)
    {
        var rsCurrentCalculation = GetRecordset("C_CALCULATION", "", string.Format("PK_C_CALCULATION = '{0}'", calculationId), "");
        rsCurrentCalculation.MoveFirst();

        if ((rsCurrentCalculation.Fields["FK_ASSEMBLY"].Value as int? ?? 0) <= 0)
        {
            var rsNewAssembly = GetRecordset("R_ASSEMBLY", "", "PK_R_ASSEMBLY = -1", "");
            rsNewAssembly.AddNew();
            rsNewAssembly.SetFieldValue("DESCRIPTION", rsCurrentCalculation.Fields["DESCRIPTION"].Value);
            rsNewAssembly.Update();
            rsCurrentCalculation.SetFieldValue("FK_ASSEMBLY", rsNewAssembly.Fields["PK_R_ASSEMBLY"].Value);
            rsCurrentCalculation.Update();
        }
        else
        {
            DeleteAllAssemblyDetailRecords(rsCurrentCalculation.Fields["FK_ASSEMBLY"].Value);
        }

        var currentCalculation = rsCurrentCalculation.DataTable.AsEnumerable()
            .Select(x => new Calculation
            {
                Id = (int)x["PK_C_CALCULATION"],
                DESCRIPTION = (string)x["DESCRIPTION"],
                FK_CALCULATIONTOTAL = x["FK_CALCULATIONTOTAL"] as int?,
                FK_ASSEMBLY = x["FK_ASSEMBLY"] as int?,
                QUANTITY = (double)x["QUANTITY"],
                FK_OFFER = x["FK_OFFER"] as int?
            })
            .First();

        SavePricesToAssemblyItem(currentCalculation, quantityMainCalculation);

        var calculationRules = GetRecordset("C_CALCULATIONRULE", "", string.Format("FK_CALCULATION = '{0}'", calculationId), "")
            .DataTable.AsEnumerable()
            .Select(CalculationRule.FromDataRow).ToArray();

        var assemblyId = currentCalculation.FK_ASSEMBLY.Value;
        foreach (var calculationRule in calculationRules)
        {
            if (calculationRule.QUANTITY < 1)
            {
                continue;
            }

            if (calculationRule.FK_SUBCALCULATION.HasValue)
            {
                ConvertSubCalculations(currentCalculation, calculationRule.FK_SUBCALCULATION.Value, calculationRule.QUANTITY, quantityMainCalculation);
            }
            if (calculationRule.FK_ITEM != null)
            {
                // add item record
                AddAssemblyDetailItem(calculationRule, assemblyId, quantityMainCalculation);
            }
            if (calculationRule.FK_OUTSOURCED != null)
            {
                // add outsourced record
                AddAssemblyDetailOutsourcedActivity(calculationRule, assemblyId, quantityMainCalculation);
            }
            if (calculationRule.FK_WORKACTIVITY != null && calculationRule.WORKACTIVITY_QUANTITYTOTAL > 0)
            {
                // add work activity record
                AddAssemblyDetailWorkActivity(calculationRule, assemblyId, quantityMainCalculation);
            }
            if (calculationRule.FK_SUBCALCULATION == null && calculationRule.FK_ITEM == null && calculationRule.FK_OUTSOURCED == null && calculationRule.FK_WORKACTIVITY == null)
            {
                // add misc record
                AddAssemblyDetailMisc(calculationRule, assemblyId, quantityMainCalculation);
            }
        }

        return currentCalculation;
    }

    private void SavePricesToAssemblyItem(Calculation currentCalculation, double quantityMainCalculation)
    {
        if (currentCalculation.FK_CALCULATIONTOTAL == null)
            return;

        var rsTotals = GetRecordset("C_CALCULATIONTOTALS", "TOTALCOSTPRICE, TOTALSALESPRICE", string.Format("PK_C_CALCULATIONTOTALS = '{0}'", currentCalculation.FK_CALCULATIONTOTAL), "");
        if (rsTotals.RecordCount <= 0) return;
        rsTotals.MoveFirst();

        this.EventsAndActions.Assemblies.Actions.CreateSemimanufacture(currentCalculation.FK_ASSEMBLY.Value);

        var rsAssembly = GetRecordset("R_ASSEMBLY", "FK_ITEM", string.Format("PK_R_ASSEMBLY = '{0}'", currentCalculation.FK_ASSEMBLY), "");
        if (rsAssembly.RecordCount <= 0) return;
        rsAssembly.MoveFirst();

        var rsItem = GetRecordset("R_ITEM", "", string.Format("PK_R_ITEM = '{0}'", rsAssembly.Fields["FK_ITEM"].Value), "");
        if (rsItem.RecordCount <= 0) return;
        rsItem.MoveFirst();
        rsItem.Fields["STANDARDCOST"].Value = (double)rsTotals.Fields["TOTALCOSTPRICE"].Value / quantityMainCalculation;
        rsItem.Update();

        var rsItemSupplier = GetRecordset("R_ITEMSUPPLIER", "", string.Format("FK_ITEM = '{0}'", rsAssembly.Fields["FK_ITEM"].Value), "");
        if (rsItemSupplier.RecordCount <= 0) return;
        rsItemSupplier.MoveFirst();

        var rsAssemblySettings = GetRecordset("R_ASSEMBLYSETTINGS", "FK_SEMIMANUFACTUREPRICENAME", "", "");
        rsAssemblySettings.MoveFirst();

        if (!(rsAssemblySettings.Fields["FK_SEMIMANUFACTUREPRICENAME"].Value as Guid?).HasValue)
        {
            throw new Exception("Geen prijsnaam gevonden in stuklijst instellingen.");
        }

        Guid priceName = (Guid) rsAssemblySettings.Fields["FK_SEMIMANUFACTUREPRICENAME"].Value;

        var rsItemPurchasePrice = GetRecordset("R_ITEMPURCHASEPRICE", "", 
            string.Format("FK_ITEMSUPPLIER = '{0}' AND FK_PRICENAME = '{1}'", rsItemSupplier.Fields["PK_R_ITEMSUPPLIER"].Value, priceName), "");
        if (rsItemPurchasePrice.RecordCount <= 0) return;
        rsItemPurchasePrice.MoveFirst();
        rsItemPurchasePrice.SetFieldValue("VALUE", (double)rsTotals.Fields["TOTALCOSTPRICE"].Value / quantityMainCalculation);
        rsItemPurchasePrice.Update();

        var rsItemSalesPrice = GetRecordset("R_ITEMSALESPRICE", "", 
            string.Format("FK_ITEM = '{0}' AND FK_PRICENAME = '{1}'", rsAssembly.Fields["FK_ITEM"].Value, priceName), "");
        if (rsItemSalesPrice.RecordCount <= 0) return;
        rsItemSalesPrice.MoveFirst();
        rsItemSalesPrice.SetFieldValue("VALUE", (double)rsTotals.Fields["TOTALSALESPRICE"].Value / quantityMainCalculation);
        rsItemSalesPrice.Update();

        ItemSalesPrice = (double)rsTotals.Fields["TOTALSALESPRICE"].Value / quantityMainCalculation;
    }

    private void DeleteAllAssemblyDetailRecords(object assemblyId)
    {
        DeleteAllItemsFromRecordset("R_ASSEMBLYDETAILMISC", string.Format("FK_ASSEMBLY = '{0}'", assemblyId));
        DeleteAllItemsFromRecordset("R_ASSEMBLYDETAILITEM", string.Format("FK_ASSEMBLY = '{0}'", assemblyId));
        DeleteAllItemsFromRecordset("R_ASSEMBLYDETAILOUTSOURCED", string.Format("FK_ASSEMBLY = '{0}'", assemblyId));
        DeleteAllItemsFromRecordset("R_ASSEMBLYDETAILWORKACTIVITY", string.Format("FK_ASSEMBLY = '{0}'", assemblyId));
        DeleteAllItemsFromRecordset("R_ASSEMBLYDETAILSUBASSEMBLY", string.Format("FK_ASSEMBLY = '{0}'", assemblyId));
    }

    private void DeleteAllItemsFromRecordset(string tableName, string filter)
    {
        try
        {
            var rsRecords = GetRecordset(tableName, "", filter, "");
            if (rsRecords.RecordCount > 0)
            {
                rsRecords.UpdateWhenMoveRecord = false;
                rsRecords.MoveFirst();
                while (!rsRecords.EOF)
                {
                    rsRecords.Delete();
                    rsRecords.MoveNext();
                }

                rsRecords.MoveFirst();
                rsRecords.Update();
            }
        }
        catch (Exception ex)
        {
            Log(LogType.Error, (int)RecordId, string.Format("Fout bij het verwijderen van '{0}': {1}", tableName, ex));
        }
    }

    public void AddAssemblyDetailMisc(CalculationRule calculationRule, int assemblyId, double quantityMainCalculation)
    {
        var rsAssemblySettings = GetRecordset("R_ASSEMBLYSETTINGS", "FK_DEFAULTMISC", "", "");
        rsAssemblySettings.MoveFirst();
        var defaultMiscId = rsAssemblySettings.Fields["FK_DEFAULTMISC"].Value as int? ?? 0;
        if (defaultMiscId == 0)
        {
            throw new Exception("Er is geen standaard divers ingesteld in de stuklijst instellingen");
        }

        var recordset = GetRecordset("R_ASSEMBLYDETAILMISC", "", "PK_R_ASSEMBLYDETAILMISC = -1", "");
        recordset.AddNew();
        recordset.SetFieldValue("FK_ASSEMBLY", assemblyId);
        recordset.SetFieldValue("FK_MISC", defaultMiscId);
        recordset.SetFieldValue("FK_UNIT", calculationRule.FK_UNIT);
        recordset.SetFieldValue("DESCRIPTION", calculationRule.PLAINTEXT_DESCRIPTION);
        recordset.SetFieldValue("QUANTITY", calculationRule.QUANTITY / quantityMainCalculation);

        if (calculationRule.ITEM_PRICEPERUNIT > 0)
        {
            recordset.SetFieldValue("COSTPRICE", calculationRule.ITEM_PRICEPERUNIT);
        }

        if (calculationRule.OUTSOURCED_PRICEPERUNIT > 0)
        {
            recordset.SetFieldValue("COSTPRICE", calculationRule.OUTSOURCED_PRICEPERUNIT);
        }

        // Done
        //calculationRule.FK_ASSEMBLY
        //calculationRule.FK_UNIT
        //calculationRule.QUANTITY
        //calculationRule.DESCRIPTION
        //calculationRule.ITEM_PRICEPERUNIT

        try
        {
            var sdkResult = recordset.Update();

            if (sdkResult == null || !sdkResult.HasError)
            {
                Log(LogType.Info, calculationRule.FK_CALCULATION, "Stuklijst regel divers toegevoegd", calculationRule.Id);
            }
            else
            {
                Log(LogType.Error, calculationRule.FK_CALCULATION, sdkResult.GetResult(), calculationRule.Id);
            }
        }
        catch (Exception e)
        {
            Log(LogType.Error, calculationRule.FK_CALCULATION, e.ToString(), calculationRule.Id);
        }
    }

    public void AddAssemblyDetailItem(CalculationRule calculationRule, int assemblyId, double quantityMainCalculation)
    {
        var itemSupplier = 0;
        if (calculationRule.FK_ALTERNATEITEMSUPPLIER.HasValue)
        {
            var rsItemSupplier = GetRecordset("R_ITEMSUPPLIER", "FK_RELATION", string.Format("PK_R_ITEMSUPPLIER = {0}", calculationRule.FK_ALTERNATEITEMSUPPLIER.Value), "");
            rsItemSupplier.MoveFirst();
            itemSupplier = (int)rsItemSupplier.Fields["FK_RELATION"].Value;
        }

        var recordset = GetRecordset("R_ASSEMBLYDETAILITEM", "", "PK_R_ASSEMBLYDETAILITEM = -1", "");
        recordset.UseDataChanges = true;
        recordset.AddNew();
        recordset.SetFieldValue("FK_ASSEMBLY", assemblyId);
        recordset.SetFieldValue("FK_ITEM", calculationRule.FK_ITEM);
        recordset.SetFieldValue("DESCRIPTION", calculationRule.PLAINTEXT_DESCRIPTION);
        recordset.SetFieldValue("QUANTITY", calculationRule.QUANTITY / quantityMainCalculation);
        recordset.SetFieldValue("USEODDCOSTPRICE", true);
        recordset.SetFieldValue("ODDCOSTPRICE", calculationRule.ITEM_PRICEPERUNIT);

        if (calculationRule.ITEM_LENGTHPERUNIT > 0)
            recordset.SetFieldValue("LENGTH", calculationRule.ITEM_LENGTHPERUNIT);

        if (calculationRule.ITEM_WIDTHPERUNIT > 0)
            recordset.SetFieldValue("WIDTH", calculationRule.ITEM_WIDTHPERUNIT);

        if (itemSupplier > 0)
            recordset.SetFieldValue("FK_SUPPLIER", itemSupplier);

        // Done
        //calculationRule.FK_ASSEMBLY
        //calculationRule.FK_ITEM
        //calculationRule.QUANTITY
        //calculationRule.DESCRIPTION
        //calculationRule.ITEM_PRICEPERUNIT
        //calculationRule.ITEM_LENGTHPERUNIT

        // Todo?
        //calculationRule.ITEM_SALESPRICEPERUNIT
        //calculationRule.ITEM_PRICETOTAL
        //calculationRule.ITEM_SALESPRICETOTAL
        //calculationRule.ITEM_LENGTHTOTAL
        //calculationRule.ITEM_PRICEPERKG
        //calculationRule.ITEM_SALESPRICEPERKG
        //calculationRule.ITEM_PRICEPERMTR
        //calculationRule.ITEM_SALESPRICEPERMTR

        try
        {
            var sdkResult = recordset.Update();

            if (sdkResult == null || !sdkResult.HasError)
            {
                Log(LogType.Info, calculationRule.FK_CALCULATION, "Stuklijst regel artikel toegevoegd", calculationRule.Id);
            }
            else
            {
                Log(LogType.Error, calculationRule.FK_CALCULATION, sdkResult.GetResult(), calculationRule.Id);
            }
        }
        catch (Exception e)
        {
            Log(LogType.Error, calculationRule.FK_CALCULATION, e.ToString(), calculationRule.Id);
        }
    }

    public void AddAssemblyDetailOutsourcedActivity(CalculationRule calculationRule, int assemblyId, double quantityMainCalculation)
    {
        var ubwSupplier = 0;
        if (calculationRule.FK_ALTERNATEUBWSUPPLIER.HasValue)
        {
            var rsUbwSupplier = GetRecordset("R_OUTSOURCEDPURCHASEPRICE", "FK_SUPPLIER", string.Format("PK_R_OUTSOURCEDPURCHASEPRICE = {0}", calculationRule.FK_ALTERNATEUBWSUPPLIER.Value), "");
            rsUbwSupplier.MoveFirst();
            ubwSupplier = (int)rsUbwSupplier.Fields["FK_SUPPLIER"].Value;
        }

        //Check op serie-afhankelijkheid van ubw
        var rsOutSourcedActivity = GetRecordset("R_OUTSOURCEDACTIVITY", "BATCHDEPENDENT", string.Format("PK_R_OUTSOURCEDACTIVITY = {0}", calculationRule.FK_OUTSOURCED), "");
        rsOutSourcedActivity.MoveFirst();
        bool batchDependent = (bool)rsOutSourcedActivity.Fields["BATCHDEPENDENT"].Value;

        var recordset = GetRecordset("R_ASSEMBLYDETAILOUTSOURCED", "", "PK_R_ASSEMBLYDETAILOUTSOURCED = -1", "");
        recordset.AddNew();
        recordset.SetFieldValue("FK_ASSEMBLY", assemblyId);
        recordset.SetFieldValue("FK_OUTSOURCEDACTIVITY", calculationRule.FK_OUTSOURCED);
        recordset.SetFieldValue("SEQUENCENUMBER", calculationRule.SEQUENCENUMBER);
        recordset.SetFieldValue("DESCRIPTION", calculationRule.PLAINTEXT_DESCRIPTION);
        double quantity = (batchDependent) ? calculationRule.QUANTITY / quantityMainCalculation : calculationRule.QUANTITY;
        recordset.SetFieldValue("QUANTITY", quantity);
        recordset.SetFieldValue("USEODDCOSTPRICE", true);
        recordset.SetFieldValue("ODDCOSTPRICE", calculationRule.OUTSOURCED_PRICEPERUNIT);
        recordset.SetFieldValue("BATCHDEPENDENT", batchDependent);


        if (ubwSupplier > 0)
            recordset.SetFieldValue("FK_SUPPLIER", ubwSupplier);

        // Done
        //calculationRule.FK_ASSEMBLY
        //calculationRule.FK_OUTSOURCED
        //calculationRule.QUANTITY
        //calculationRule.DESCRIPTION
        //calculationRule.OUTSOURCED_PRICEPERUNIT
        //calculationRule.SEQUENCENUMBER

        // Todo?
        //calculationRule.OUTSOURCED_SALESPRICEPERUNIT
        //calculationRule.OUTSOURCED_PRICETOTAL
        //calculationRule.OUTSOURCED_SALESPRICETOTAL
        try
        {
            var sdkResult = recordset.Update();

            if (sdkResult == null || !sdkResult.HasError)
            {
                Log(LogType.Info, calculationRule.FK_CALCULATION, "Stuklijst regel uitbesteed werk toegevoegd", calculationRule.Id);
            }
            else
            {
                Log(LogType.Error, calculationRule.FK_CALCULATION, sdkResult.GetResult(), calculationRule.Id);
            }
        }
        catch (Exception e)
        {
            Log(LogType.Error, calculationRule.FK_CALCULATION, e.ToString(), calculationRule.Id);
        }
    }

    public void AddAssemblyDetailWorkActivity(CalculationRule calculationRule, int assemblyId, double quantityMainCalculation)
    {
        var recordset = GetRecordset("R_ASSEMBLYDETAILWORKACTIVITY", "", "PK_R_ASSEMBLYDETAILWORKACTIVITY = -1", "");
        recordset.AddNew();
        recordset.SetFieldValue("FK_ASSEMBLY", assemblyId);
        recordset.UseDataChanges = true;
        recordset.SetFieldValue("FK_WORKACTIVITY", calculationRule.FK_WORKACTIVITY);
        recordset.UseDataChanges = false;
        recordset.SetFieldValue("SEQUENCENUMBER", calculationRule.SEQUENCENUMBER);
        recordset.SetFieldValue("DESCRIPTION", calculationRule.PLAINTEXT_DESCRIPTION);
        recordset.SetFieldValue("OPERATIONTIME", TimeSpan.FromHours(calculationRule.WORKACTIVITY_QUANTITYTOTAL / quantityMainCalculation).Ticks);

        // Done
        //calculationRule.FK_ASSEMBLY
        //calculationRule.FK_OUTSOURCED
        //calculationRule.DESCRIPTION
        //calculationRule.QUANTITY
        //calculationRule.SEQUENCENUMBER

        // Todo?
        //calculationRule.WORKACTIVITY_QUANTITYTOTAL
        //calculationRule.WORKACTIVITY_PRICEPERUNIT
        //calculationRule.WORKACTIVITY_SALESPRICEPERUNIT
        //calculationRule.WORKACTIVITY_PRICETOTAL
        //calculationRule.WORKACTIVITY_SALESPRICETOTAL
        //calculationRule.WORKACTIVITY_QUANTITY
        //calculationRule.WORKACTIVITY_QUANTITYTOTAL
        try
        {
            var sdkResult = recordset.Update();

            if (sdkResult == null || !sdkResult.HasError)
            {
                Log(LogType.Info, calculationRule.FK_CALCULATION, "Stuklijst regel bewerking toegevoegd", calculationRule.Id);
            }
            else
            {
                Log(LogType.Error, calculationRule.FK_CALCULATION, sdkResult.GetResult(), calculationRule.Id);
            }
        }
        catch (Exception e)
        {
            Log(LogType.Error, calculationRule.FK_CALCULATION, e.ToString(), calculationRule.Id);
        }
    }

    public void Log(LogType logType, int calculationId, string message, int? calculationRuleId = null)
    {
        var rsLog = this.GetRecordset("C_CALCULATIONLOG", "", "PK_C_CALCULATIONLOG = -1", "");
        rsLog.AddNew();
        rsLog.SetFieldValue("FK_CALCULATION", calculationId);

        if (calculationRuleId != null)
            rsLog.SetFieldValue("FK_CALCULATIONRULE", calculationRuleId);

        rsLog.SetFieldValue("TYPE", (int)logType);
        rsLog.SetFieldValue("MESSAGE", message);
        rsLog.Update();
    }

    public enum LogType
    {
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public class Calculation
    {
        public int Id { get; set; }
        public int? FK_ASSEMBLY { get; set; }
        public int? FK_CALCULATIONTOTAL { get; set; }
        public double QUANTITY { get; set; }
        public string DESCRIPTION { get; set; }
        public int? FK_OFFER { get; set; }
    }

    public class CalculationRule
    {
        public int Id { get; set; }
        public int FK_CALCULATION { get; set; }
        public int SEQUENCENUMBER { get; set; }
        public int FK_UNIT { get; set; }
        public string DESCRIPTION { get; set; }
        public string PLAINTEXT_DESCRIPTION { get; set; }
        public double QUANTITY { get; set; }
        public double PRICETOTAL { get; set; }
        public double SALESPRICETOTAL { get; set; }
        public double TOTALPRICEPERUNIT { get; set; }
        public double TOTALSALESPRICEPERUNIT { get; set; }
        public int? FK_ITEM { get; set; }
        public double ITEM_PRICEPERUNIT { get; set; }
        public double ITEM_PRICETOTAL { get; set; }
        public double ITEM_SALESPRICEPERUNIT { get; set; }
        public double ITEM_SALESPRICETOTAL { get; set; }
        public int? FK_WORKACTIVITY { get; set; }
        public double WORKACTIVITY_PRICEPERUNIT { get; set; }
        public double WORKACTIVITY_PRICETOTAL { get; set; }
        public double WORKACTIVITY_QUANTITY { get; set; }
        public double WORKACTIVITY_QUANTITYTOTAL { get; set; }
        public double WORKACTIVITY_SALESPRICEPERUNIT { get; set; }
        public double WORKACTIVITY_SALESPRICETOTAL { get; set; }
        public int? FK_OUTSOURCED { get; set; }
        public double OUTSOURCED_PRICEPERUNIT { get; set; }
        public double OUTSOURCED_PRICETOTAL { get; set; }
        public double OUTSOURCED_SALESPRICEPERUNIT { get; set; }
        public double OUTSOURCED_SALESPRICETOTAL { get; set; }
        public double ITEM_WIDTHTOTAL { get; set; }
        public double ITEM_WIDTHPERUNIT { get; set; }
        public double ITEM_LENGTHPERUNIT { get; set; }
        public double ITEM_LENGTHTOTAL { get; set; }
        public double ITEM_PRICEPERKG { get; set; }
        public double ITEM_SALESPRICEPERKG { get; set; }
        public double ITEM_PRICEPERMTR { get; set; }
        public double ITEM_SALESPRICEPERMTR { get; set; }
        public int? FK_SUBCALCULATION { get; set; }
        public int? FK_ALTERNATEITEMSUPPLIER { get; set; }
        public int? FK_ALTERNATEUBWSUPPLIER { get; set; }

        public static CalculationRule FromDataRow(DataRow x)
        {
            return new CalculationRule
            {
                Id = (int)x["PK_C_CALCULATIONRULE"],
                FK_CALCULATION = (int)x["FK_CALCULATION"],
                FK_SUBCALCULATION = x["FK_SUBCALCULATION"] as int?,
                SEQUENCENUMBER = (int)x["SEQUENCENUMBER"],
                FK_UNIT = (int)x["FK_UNIT"],
                DESCRIPTION = (string)x["DESCRIPTION"],
                PLAINTEXT_DESCRIPTION = (string)x["PLAINTEXT_DESCRIPTION"],
                QUANTITY = (double)x["QUANTITY"],
                PRICETOTAL = (double)x["PRICETOTAL"],
                SALESPRICETOTAL = (double)x["SALESPRICETOTAL"],
                TOTALPRICEPERUNIT = (double)x["TOTALPRICEPERUNIT"],
                TOTALSALESPRICEPERUNIT = (double)x["TOTALSALESPRICEPERUNIT"],
                // item fields
                FK_ITEM = x["FK_ITEM"] as int?,
                FK_ALTERNATEITEMSUPPLIER = x["FK_ALTERNATEITEMSUPPLIER"] as int?,
                ITEM_PRICEPERUNIT = (double)x["ITEM_PRICEPERUNIT"],
                ITEM_PRICETOTAL = (double)x["ITEM_PRICETOTAL"],
                ITEM_SALESPRICEPERUNIT = (double)x["ITEM_SALESPRICEPERUNIT"],
                ITEM_SALESPRICETOTAL = (double)x["ITEM_SALESPRICETOTAL"],
                ITEM_WIDTHPERUNIT = (double)x["ITEM_WIDTHPERUNIT"],
                ITEM_WIDTHTOTAL = (double)x["ITEM_WIDTHTOTAL"],
                ITEM_LENGTHPERUNIT = (double)x["ITEM_LENGTHPERUNIT"],
                ITEM_LENGTHTOTAL = (double)x["ITEM_LENGTHTOTAL"],
                ITEM_PRICEPERKG = (double)x["ITEM_PRICEPERKG"],
                ITEM_SALESPRICEPERKG = (double)x["ITEM_SALESPRICEPERKG"],
                ITEM_PRICEPERMTR = (double)x["ITEM_PRICEPERMTR"],
                ITEM_SALESPRICEPERMTR = (double)x["ITEM_SALESPRICEPERMTR"],
                // work activity fields
                FK_WORKACTIVITY = x["FK_WORKACTIVITY"] as int?,
                WORKACTIVITY_PRICEPERUNIT = (double)x["WORKACTIVITY_PRICEPERUNIT"],
                WORKACTIVITY_PRICETOTAL = (double)x["WORKACTIVITY_PRICETOTAL"],
                WORKACTIVITY_QUANTITY = (double)x["WORKACTIVITY_QUANTITY"],
                WORKACTIVITY_QUANTITYTOTAL = (double)x["WORKACTIVITY_QUANTITYTOTAL"],
                WORKACTIVITY_SALESPRICEPERUNIT = (double)x["WORKACTIVITY_SALESPRICEPERUNIT"],
                WORKACTIVITY_SALESPRICETOTAL = (double)x["WORKACTIVITY_SALESPRICETOTAL"],
                // outsourced activity fields
                FK_OUTSOURCED = x["FK_OUTSOURCED"] as int?,
                FK_ALTERNATEUBWSUPPLIER = x["FK_ALTERNATEUBWSUPPLIER"] as int?,
                OUTSOURCED_PRICEPERUNIT = (double)x["OUTSOURCED_PRICEPERUNIT"],
                OUTSOURCED_PRICETOTAL = (double)x["OUTSOURCED_PRICETOTAL"],
                OUTSOURCED_SALESPRICEPERUNIT = (double)x["OUTSOURCED_SALESPRICEPERUNIT"],
                OUTSOURCED_SALESPRICETOTAL = (double)x["OUTSOURCED_SALESPRICETOTAL"],
            };
        }

    }

    public void CloseScript()
    {





    }
}


