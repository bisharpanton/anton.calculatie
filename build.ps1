Push-Location "$PSScriptRoot"

Import-Module .\build-functions.psm1 -Force

$msbuild = Get-MsBuildPath
$nuget = "NuGet.exe"
$BuildNumber = @{ $true = $env:CI_PIPELINE_ID; $false=0 }[$env:CI_PIPELINE_ID -ne $NULL];

try {
    $CommitHash = $(git rev-parse --short HEAD)
    
    Write-Host "BuildNumber: $BuildNumber"
    Write-Host "CommitHash: $CommitHash"
    
    If (Test-Path ".\artifacts") { 
        Remove-Item -Recurse -Force ".\artifacts\*"
    }
    Else { 
        New-Item -ItemType Directory .\artifacts\ 
    }       
    
    Exec { & $nuget restore .\Source\ } "Error while restoring nuget packages."

    Exec { & $msbuild "Source\Calculation.Win\Calculation.Win.csproj" /t:Build /p:Configuration=Release`;BuildNumber=$BuildNumber`;CommitHash=$CommitHash } "Error while building project."    

    Exec { & $msbuild "Source\Calculation.Setup\Calculation.Setup.wixproj" /t:Build /p:Configuration=Release /p:Configuration=Release`;BuildNumber=$BuildNumber`;CommitHash=$CommitHash } "Error while building setup."
    Copy-Item -Path .\Source\Calculation.Setup\bin\Release\*.msi -Destination .\artifacts\
}
Finally {
    Pop-Location
}
