//  <auto-generated />
namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_UNIT
    ///  </summary>
    ///  	CALCULATEASMANHOUR, CODE, CREATOR, DATECHANGED, DATECREATED, DESCRIPTION, EXTERNALKEY, FACTOR, FK_R_UNIT, FK_WORKFLOWSTATE
    ///  	ISOCODE, PK_R_UNIT, QUANTITY, RECORDLINK, USERCHANGED
    ///  
    ///  	Filter used: FK_TABLEINFO = 'acab73be-e79c-42f2-bc65-6b069db688a7' AND (ISSECRET = 0)
    ///  	Included columns: CODE, DESCRIPTION, CALCULATEASMANHOUR
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_UNIT")]
    public partial class Unit
    {
        
        private bool _CALCULATEASMANHOUR;
        
        private string _CODE;
        
        private string _DESCRIPTION;
        
        private int _id;
        
        /// <summary>
        /// 	Calculeer als man uren
        /// </summary>
        /// <remarks>
        /// 	DataType: Boolean
        /// 	Size: 1
        /// </remarks>
        [Column("CALCULATEASMANHOUR")]
        public virtual bool CALCULATEASMANHOUR
        {
            get
            {
                return this._CALCULATEASMANHOUR;
            }
            set
            {
                this._CALCULATEASMANHOUR = value;
            }
        }
        
        /// <summary>
        /// 	Code
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 10
        /// </remarks>
        [Column("CODE")]
        public virtual string CODE
        {
            get
            {
                if (string.IsNullOrEmpty(this._CODE))
                {
                    return "";
                }
                return this._CODE;
            }
            set
            {
                this._CODE = value;
            }
        }
        
        /// <summary>
        /// 	Description
        /// </summary>
        /// <remarks>
        /// 	DataType: String
        /// 	Size: 80
        /// </remarks>
        [Column("DESCRIPTION")]
        public virtual string DESCRIPTION
        {
            get
            {
                if (string.IsNullOrEmpty(this._DESCRIPTION))
                {
                    return "";
                }
                return this._DESCRIPTION;
            }
            set
            {
                this._DESCRIPTION = value;
            }
        }
        
        /// <summary>
        /// 	Unit id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_UNIT")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }
}
