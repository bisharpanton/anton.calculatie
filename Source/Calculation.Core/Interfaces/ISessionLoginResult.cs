﻿namespace Calculation.Core.Interfaces
{
    public interface ISessionLoginResult
    {
        bool Success { get; set; }
        string ErrorMessage { get; set; }
    }
}