﻿namespace FlattenAssembly
{
    using System;
    using System.Collections.Generic;

    using Ridder.Common.ADO;
    using Ridder.Common.Script;
    using System.Reflection;
    using System.Linq;
    using System.Data;

    public static class ScriptRecordsetExtensions
    {
        public static IEnumerable<ScriptRecordset> AsEnumerable(this ScriptRecordset recordset)
        {
            recordset.MoveFirst();
            while (!recordset.EOF)
            {
                yield return recordset;
                recordset.MoveNext();
            }
        }

        public static IEnumerable<ScriptRecordset> AsEnumerableReverse(this ScriptRecordset recordset)
        {
            recordset.MoveLast();
            while (!recordset.BOF)
            {
                yield return recordset;
                recordset.MovePrevious();
            }
        }

        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int size)
        {
            var items = source as IList<TSource> ?? new List<TSource>(source);
            for (var i = 0; i < items.Count; i = i + size)
            {
                if (items.Count < i + size)
                {
                    size = items.Count - i;
                }
                var val = new TSource[size];

                for (int j = i, index = 0; j < i + size; j++)
                {
                    val[index++] = items[j];
                }
                yield return val;
            }
        }

        public static T Map<T>(DataRow rs) where T : new()
        {
            return Map(rs, new T());
        }

        public static T Map<T>(DataRow rs, T obj)
        {
            var type = obj.GetType();
            var props = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var col in rs.Table.Columns.Cast<DataColumn>())
            {
                var foundProp = col.ColumnName.Equals("PK_" + col.Table.TableName, StringComparison.OrdinalIgnoreCase)
                    ? props.FirstOrDefault(x => x.CanWrite && (x.Name.Equals(col.ColumnName, StringComparison.OrdinalIgnoreCase) || x.Name.Equals("Id", StringComparison.OrdinalIgnoreCase)))
                    : props.FirstOrDefault(x => x.CanWrite && x.Name.Equals(col.ColumnName, StringComparison.OrdinalIgnoreCase));

                if (foundProp == null)
                {
                    continue;
                }

                var value = rs[col.ColumnName];
                var targetVal = value;
                var targetType = foundProp.PropertyType;
                if (value == null || value == DBNull.Value)
                {
                    if (targetType == typeof(Guid))
                    {
                        targetVal = Guid.Empty;
                    }
                    else if (Nullable.GetUnderlyingType(targetType) != null)
                    {
                        targetVal = null;
                    }
                    else if (targetType.IsValueType)
                    {
                        targetVal = Activator.CreateInstance(targetType);
                    }
                    else
                    {
                        targetVal = null;
                    }
                }
                else if (foundProp.PropertyType.IsEnum)
                {
                    if (value is string)
                    {
                        targetVal = Enum.Parse(foundProp.PropertyType, (string)value);
                    }
                    else if (value is int)
                    {
                        targetVal = Enum.ToObject(foundProp.PropertyType, (int)value);
                    }
                }
                else if (!foundProp.PropertyType.IsInstanceOfType(targetVal))
                {
                    targetVal = Convert.ChangeType(value, foundProp.PropertyType);
                }

                foundProp.SetValue(obj, targetVal);
            }

            return obj;
        }

        public static void Map<T>(T obj, ScriptRecordset rs)
        {
            var type = obj.GetType();
            var props = type.GetProperties(BindingFlags.Instance | BindingFlags.Public).Where(x => x.CanRead);
            foreach (var prop in props)
            {
                var value = prop.GetValue(obj);
                var columnName = prop.Name.ToUpperInvariant();
                if (prop.Name.Equals("Id", StringComparison.OrdinalIgnoreCase))
                {
                    columnName = "PK_" + rs.TableName;
                }

                if (!prop.PropertyType.IsInstanceOfType(value))
                {
                    if (prop.PropertyType.IsEnum)
                    {
                        value = (int)value;
                    }
                    else
                    {
                        value = Convert.ChangeType(value, prop.PropertyType);
                    }
                }
                else if (prop.PropertyType.IsEnum)
                {
                    value = (int)value;
                }

                if (value == null)
                {
                    value = DBNull.Value;
                }

                // HACK: Guid conversion
                if (value is Guid && (Guid)value == Guid.Empty)
                {
                    value = DBNull.Value;
                }

                if (object.Equals(rs.Fields[columnName].Value, value))
                {
                    continue;
                }

                rs.SetFieldValue(columnName, value);
            }
        }

        public static T Map<T>(ScriptRecordset rs)
            where T : new()
        {
            return Map(rs, new T());
        }

        public static T Map<T>(ScriptRecordset rs, T obj)
        {
            return Map(rs.DataTable.Rows[(int)rs.AbsolutePosition], obj);
        }

        public static IEnumerable<T> As<T>(this ScriptRecordset rs)
            where T : new()
        {
            return rs.AsEnumerable().Select(Map<T>);
        }

        public static IEnumerable<T> As<T>(this IEnumerable<ScriptRecordset> rs)
            where T : new()
        {
            return rs.Select(Map<T>);
        }
    }
}