namespace Calculation.Core.Classes
{
    public enum DatabaseObjectStateEnum : int
    {
        Unmodified = 0,
        Modified = 1,
        New = 2,
        MarkForDelete = 3
    }
}