using System;
using System.Collections.Generic;
using System.ComponentModel;
using Calculation.Core.Classes.Base;

namespace Calculation.Core.Classes
{
    public class DatabaseObjectState
    {
        public IDatabaseObject DatabaseObject { get; }
        public DatabaseObjectStateEnum State { get; set; }

        public DatabaseObjectState(IDatabaseObject databaseObject)
        {
            DatabaseObject = databaseObject;
            State = DatabaseObjectStateEnum.Unmodified;

            //var notifyPropertyChanged = databaseObject as INotifyPropertyChanged;

            //if (notifyPropertyChanged == null)
            //    throw new Exception("Does not implement INotifyPropertyChanged");

            databaseObject.CustomPropertyChanged += (sender, args) =>
            {
                var dbObject = (IDatabaseObject)sender;
                if (dbObject.Id <= 0)
                {
                    State = DatabaseObjectStateEnum.New;
                }
                else if (State == DatabaseObjectStateEnum.MarkForDelete)
                {
                    State = DatabaseObjectStateEnum.MarkForDelete;
                }
                else
                {
                    State = DatabaseObjectStateEnum.Modified;
                }

                DatabaseHelper.ObjectChanged?.Invoke(this, args);
            };

            //notifyPropertyChanged.PropertyChanged += (sender, args) =>
            //{
            //    var dbObject = (IDatabaseObject)sender;
            //    if (dbObject.Id <= 0)
            //    {
            //        State = DatabaseObjectStateEnum.New;
            //    }
            //    else if (State == DatabaseObjectStateEnum.MarkForDelete)
            //    {
            //        State = DatabaseObjectStateEnum.MarkForDelete;
            //    }
            //    else
            //    {
            //        State = DatabaseObjectStateEnum.Modified;
            //    }
                
            //    DatabaseHelper.ObjectChanged?.Invoke(this, new ExtendedObjectChangedEventArgs(sender, DatabaseObjectStateEnum.Modified));
            //};
        }
    }
}