﻿using Calculation.Core.Models;

namespace Calculation.Core.Extensions
{
    public static class CalculationExtensions
    {
        public static Models.Calculation Clone(this Models.Calculation calculation)
        {
            var result = new Models.Calculation();
            result.DisableDataChanges = true;

            foreach (var property in result.GetType().GetProperties())
            {
                switch (property.Name)
                {
                    case nameof(Models.Calculation.DisableDataChanges):
                    case nameof(Models.Calculation.CalculationRules):
                    case nameof(Models.Calculation.CalculationTailRules):
                    case nameof(Models.Calculation.Calculations):
                    case nameof(Models.Calculation.Totals):
                    case nameof(Models.Calculation.FK_CALCULATIONTOTAL):
                    case nameof(Models.Calculation.MainCalculation):
                    case nameof(Models.Calculation.FK_MAINCALCULATION):
                    case nameof(Models.Calculation.Id):
                        continue;
                    default:
                        property.SetValue(result, property.GetValue(calculation));
                        break;
                }
            }

            result.Totals = calculation.Totals.Clone();
            result.Totals.Calculation = result;
            result.FK_CALCULATIONTOTAL = result.Totals.Id;

            result.DisableDataChanges = false;
            return result;
        }
        public static CalculationTotals Clone(this CalculationTotals totals)
        {
            var result = new CalculationTotals();

            foreach (var property in result.GetType().GetProperties())
            {
                if (!property.CanWrite)
                {
                    continue;
                }
                
                switch (property.Name)
                {
                    case nameof(CalculationTotals.Calculation):
                    case nameof(CalculationTotals.Id):
                        continue;
                    default:
                        property.SetValue(result, property.GetValue(totals));
                        break;
                }
            }

            return result;
        }

        public static CalculationRule Clone(this CalculationRule rule)
        {
            var result = new CalculationRule();

            foreach (var property in result.GetType().GetProperties())
            {
                switch (property.Name)
                {
                    case nameof(CalculationRule.Id):
                        continue;
                    default:
                        property.SetValue(result, property.GetValue(rule));
                        break;
                }
            }

            return result;
        }
    }
}