﻿namespace Calculation.Win
{
    partial class CalculationRuleDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.FK_CALCULATIONTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.calculationRuleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.FK_ITEMTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FK_OUTSOURCEDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FK_SUBCALCULATIONTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FK_UNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FK_WORKACTIVITYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FK_WORKFLOWSTATETextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_PRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_SALESPRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OUTSOURCED_PRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OUTSOURCED_SALESPRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SALESPRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TOTALPRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TOTALSALESPRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.USERCHANGEDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WORKACTIVITY_PRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WORKACTIVITY_QUANTITYTOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WORKACTIVITY_SALESPRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.RichTextEdit();
            this.CalculationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UnitTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WorkActivityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OutsourcedActivityTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubCalculationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QUANTITYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_PRICEPERKGTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_SALESPRICEPERKGTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_PRICEPERMTRTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_SALESPRICEPERMTRTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_LENGTHPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_WIDTHPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_PRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_SALESPRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OUTSOURCED_PRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WORKACTIVITY_QUANTITYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WORKACTIVITY_PRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PRICETOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_LENGTHTOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ITEM_WIDTHTOTALTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SEQUENCENUMBERTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForFK_CALCULATION = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFK_ITEM = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFK_OUTSOURCED = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFK_UNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFK_SUBCALCULATION = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFK_WORKACTIVITY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFK_WORKFLOWSTATE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUSERCHANGED = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalculation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubCalculation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQUANTITY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSEQUENCENUMBER = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_PRICEPERKG = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_SALESPRICEPERKG = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_PRICEPERMTR = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_SALESPRICEPERMTR = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_LENGTHPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_WIDTHPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_PRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_SALESPRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_LENGTHTOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_WIDTHTOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_PRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForITEM_SALESPRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForWORKACTIVITY_PRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWORKACTIVITY_QUANTITYTOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWORKACTIVITY_SALESPRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkActivity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWORKACTIVITY_QUANTITY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWORKACTIVITY_PRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.U = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOUTSOURCED_PRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOUTSOURCED_SALESPRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOutsourcedActivity = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOUTSOURCED_PRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSALESPRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTOTALPRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTOTALSALESPRICEPERUNIT = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPRICETOTAL = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FK_CALCULATIONTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationRuleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_ITEMTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_OUTSOURCEDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_SUBCALCULATIONTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_UNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_WORKACTIVITYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_WORKFLOWSTATETextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_PRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_SALESPRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SALESPRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALPRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALSALESPRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.USERCHANGEDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_PRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_QUANTITYTOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_SALESPRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkActivityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutsourcedActivityTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubCalculationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QUANTITYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICEPERKGTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICEPERKGTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICEPERMTRTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICEPERMTRTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_LENGTHPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_WIDTHPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_PRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_QUANTITYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PRICETOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_LENGTHTOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_WIDTHTOTALTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SEQUENCENUMBERTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_CALCULATION)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_ITEM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_OUTSOURCED)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_UNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_SUBCALCULATION)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_WORKACTIVITY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_WORKFLOWSTATE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUSERCHANGED)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubCalculation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQUANTITY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSEQUENCENUMBER)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICEPERKG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICEPERKG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICEPERMTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICEPERMTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_LENGTHPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_WIDTHPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_LENGTHTOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_WIDTHTOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_PRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_QUANTITYTOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_SALESPRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_QUANTITY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_PRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_SALESPRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.U)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_PRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_SALESPRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOutsourcedActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_PRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_SALESPRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSALESPRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALPRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALSALESPRICEPERUNIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPRICETOTAL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.FK_CALCULATIONTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FK_ITEMTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FK_OUTSOURCEDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FK_SUBCALCULATIONTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FK_UNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FK_WORKACTIVITYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FK_WORKFLOWSTATETextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_PRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_SALESPRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OUTSOURCED_PRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OUTSOURCED_SALESPRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SALESPRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TOTALPRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TOTALSALESPRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.USERCHANGEDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WORKACTIVITY_PRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WORKACTIVITY_QUANTITYTOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WORKACTIVITY_SALESPRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CalculationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ItemTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkActivityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OutsourcedActivityTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubCalculationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QUANTITYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_PRICEPERKGTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_SALESPRICEPERKGTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_PRICEPERMTRTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_SALESPRICEPERMTRTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_LENGTHPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_WIDTHPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_PRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_SALESPRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OUTSOURCED_PRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OUTSOURCED_SALESPRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WORKACTIVITY_QUANTITYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WORKACTIVITY_PRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WORKACTIVITY_SALESPRICEPERUNITTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PRICETOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_LENGTHTOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ITEM_WIDTHTOTALTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SEQUENCENUMBERTextEdit);
            this.dataLayoutControl1.DataSource = this.calculationRuleBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFK_CALCULATION,
            this.ItemForFK_ITEM,
            this.ItemForFK_OUTSOURCED,
            this.ItemForFK_UNIT,
            this.ItemForFK_SUBCALCULATION,
            this.ItemForFK_WORKACTIVITY,
            this.ItemForFK_WORKFLOWSTATE,
            this.ItemForId,
            this.ItemForUSERCHANGED});
            this.dataLayoutControl1.Location = new System.Drawing.Point(5, 5);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1196, 300, 450, 400);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(949, 591);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // FK_CALCULATIONTextEdit
            // 
            this.FK_CALCULATIONTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_CALCULATION", true));
            this.FK_CALCULATIONTextEdit.Location = new System.Drawing.Point(202, -44);
            this.FK_CALCULATIONTextEdit.Name = "FK_CALCULATIONTextEdit";
            this.FK_CALCULATIONTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_CALCULATIONTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_CALCULATIONTextEdit.Properties.Mask.EditMask = "N0";
            this.FK_CALCULATIONTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FK_CALCULATIONTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FK_CALCULATIONTextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_CALCULATIONTextEdit.StyleController = this.dataLayoutControl1;
            this.FK_CALCULATIONTextEdit.TabIndex = 4;
            // 
            // calculationRuleBindingSource
            // 
            this.calculationRuleBindingSource.DataSource = typeof(Calculation.Core.Models.CalculationRule);
            // 
            // FK_ITEMTextEdit
            // 
            this.FK_ITEMTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_ITEM", true));
            this.FK_ITEMTextEdit.Location = new System.Drawing.Point(202, 36);
            this.FK_ITEMTextEdit.Name = "FK_ITEMTextEdit";
            this.FK_ITEMTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.FK_ITEMTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_ITEMTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_ITEMTextEdit.Properties.Mask.EditMask = "N0";
            this.FK_ITEMTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FK_ITEMTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FK_ITEMTextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_ITEMTextEdit.StyleController = this.dataLayoutControl1;
            this.FK_ITEMTextEdit.TabIndex = 5;
            // 
            // FK_OUTSOURCEDTextEdit
            // 
            this.FK_OUTSOURCEDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_OUTSOURCED", true));
            this.FK_OUTSOURCEDTextEdit.Location = new System.Drawing.Point(202, -24);
            this.FK_OUTSOURCEDTextEdit.Name = "FK_OUTSOURCEDTextEdit";
            this.FK_OUTSOURCEDTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.FK_OUTSOURCEDTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_OUTSOURCEDTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_OUTSOURCEDTextEdit.Properties.Mask.EditMask = "N0";
            this.FK_OUTSOURCEDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FK_OUTSOURCEDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FK_OUTSOURCEDTextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_OUTSOURCEDTextEdit.StyleController = this.dataLayoutControl1;
            this.FK_OUTSOURCEDTextEdit.TabIndex = 6;
            // 
            // FK_SUBCALCULATIONTextEdit
            // 
            this.FK_SUBCALCULATIONTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_SUBCALCULATION", true));
            this.FK_SUBCALCULATIONTextEdit.Location = new System.Drawing.Point(202, -4);
            this.FK_SUBCALCULATIONTextEdit.Name = "FK_SUBCALCULATIONTextEdit";
            this.FK_SUBCALCULATIONTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.FK_SUBCALCULATIONTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_SUBCALCULATIONTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_SUBCALCULATIONTextEdit.Properties.Mask.EditMask = "N0";
            this.FK_SUBCALCULATIONTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FK_SUBCALCULATIONTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FK_SUBCALCULATIONTextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_SUBCALCULATIONTextEdit.StyleController = this.dataLayoutControl1;
            this.FK_SUBCALCULATIONTextEdit.TabIndex = 7;
            // 
            // FK_UNITTextEdit
            // 
            this.FK_UNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_UNIT", true));
            this.FK_UNITTextEdit.Location = new System.Drawing.Point(202, -20);
            this.FK_UNITTextEdit.Name = "FK_UNITTextEdit";
            this.FK_UNITTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.FK_UNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_UNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_UNITTextEdit.Properties.Mask.EditMask = "N0";
            this.FK_UNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FK_UNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FK_UNITTextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_UNITTextEdit.StyleController = this.dataLayoutControl1;
            this.FK_UNITTextEdit.TabIndex = 8;
            // 
            // FK_WORKACTIVITYTextEdit
            // 
            this.FK_WORKACTIVITYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_WORKACTIVITY", true));
            this.FK_WORKACTIVITYTextEdit.Location = new System.Drawing.Point(202, -24);
            this.FK_WORKACTIVITYTextEdit.Name = "FK_WORKACTIVITYTextEdit";
            this.FK_WORKACTIVITYTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.FK_WORKACTIVITYTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_WORKACTIVITYTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_WORKACTIVITYTextEdit.Properties.Mask.EditMask = "N0";
            this.FK_WORKACTIVITYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.FK_WORKACTIVITYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FK_WORKACTIVITYTextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_WORKACTIVITYTextEdit.StyleController = this.dataLayoutControl1;
            this.FK_WORKACTIVITYTextEdit.TabIndex = 9;
            // 
            // FK_WORKFLOWSTATETextEdit
            // 
            this.FK_WORKFLOWSTATETextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "FK_WORKFLOWSTATE", true));
            this.FK_WORKFLOWSTATETextEdit.Location = new System.Drawing.Point(202, -4);
            this.FK_WORKFLOWSTATETextEdit.Name = "FK_WORKFLOWSTATETextEdit";
            this.FK_WORKFLOWSTATETextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.FK_WORKFLOWSTATETextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.FK_WORKFLOWSTATETextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.FK_WORKFLOWSTATETextEdit.Size = new System.Drawing.Size(713, 20);
            this.FK_WORKFLOWSTATETextEdit.StyleController = this.dataLayoutControl1;
            this.FK_WORKFLOWSTATETextEdit.TabIndex = 10;
            // 
            // ITEM_PRICETOTALTextEdit
            // 
            this.ITEM_PRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_PRICETOTAL", true));
            this.ITEM_PRICETOTALTextEdit.Location = new System.Drawing.Point(230, 485);
            this.ITEM_PRICETOTALTextEdit.Name = "ITEM_PRICETOTALTextEdit";
            this.ITEM_PRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ITEM_PRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_PRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_PRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_PRICETOTALTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_PRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_PRICETOTALTextEdit.TabIndex = 11;
            // 
            // ITEM_SALESPRICETOTALTextEdit
            // 
            this.ITEM_SALESPRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_SALESPRICETOTAL", true));
            this.ITEM_SALESPRICETOTALTextEdit.Location = new System.Drawing.Point(230, 509);
            this.ITEM_SALESPRICETOTALTextEdit.Name = "ITEM_SALESPRICETOTALTextEdit";
            this.ITEM_SALESPRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ITEM_SALESPRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_SALESPRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_SALESPRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_SALESPRICETOTALTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_SALESPRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_SALESPRICETOTALTextEdit.TabIndex = 12;
            // 
            // OUTSOURCED_PRICETOTALTextEdit
            // 
            this.OUTSOURCED_PRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "OUTSOURCED_PRICETOTAL", true));
            this.OUTSOURCED_PRICETOTALTextEdit.Location = new System.Drawing.Point(694, 245);
            this.OUTSOURCED_PRICETOTALTextEdit.Name = "OUTSOURCED_PRICETOTALTextEdit";
            this.OUTSOURCED_PRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.OUTSOURCED_PRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.OUTSOURCED_PRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.OUTSOURCED_PRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.OUTSOURCED_PRICETOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.OUTSOURCED_PRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.OUTSOURCED_PRICETOTALTextEdit.TabIndex = 13;
            // 
            // OUTSOURCED_SALESPRICETOTALTextEdit
            // 
            this.OUTSOURCED_SALESPRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "OUTSOURCED_SALESPRICETOTAL", true));
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Location = new System.Drawing.Point(694, 269);
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Name = "OUTSOURCED_SALESPRICETOTALTextEdit";
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.OUTSOURCED_SALESPRICETOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.OUTSOURCED_SALESPRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.OUTSOURCED_SALESPRICETOTALTextEdit.TabIndex = 14;
            // 
            // IdTextEdit
            // 
            this.IdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "Id", true));
            this.IdTextEdit.Location = new System.Drawing.Point(655, 12);
            this.IdTextEdit.Name = "IdTextEdit";
            this.IdTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.IdTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.IdTextEdit.Properties.Mask.EditMask = "N0";
            this.IdTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.IdTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.IdTextEdit.Size = new System.Drawing.Size(260, 20);
            this.IdTextEdit.StyleController = this.dataLayoutControl1;
            this.IdTextEdit.TabIndex = 15;
            // 
            // SALESPRICETOTALTextEdit
            // 
            this.SALESPRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "SALESPRICETOTAL", true));
            this.SALESPRICETOTALTextEdit.Location = new System.Drawing.Point(694, 66);
            this.SALESPRICETOTALTextEdit.Name = "SALESPRICETOTALTextEdit";
            this.SALESPRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.SALESPRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SALESPRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SALESPRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.SALESPRICETOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.SALESPRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.SALESPRICETOTALTextEdit.TabIndex = 16;
            // 
            // TOTALPRICEPERUNITTextEdit
            // 
            this.TOTALPRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "TOTALPRICEPERUNIT", true));
            this.TOTALPRICEPERUNITTextEdit.Location = new System.Drawing.Point(694, 90);
            this.TOTALPRICEPERUNITTextEdit.Name = "TOTALPRICEPERUNITTextEdit";
            this.TOTALPRICEPERUNITTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TOTALPRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TOTALPRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TOTALPRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.TOTALPRICEPERUNITTextEdit.Size = new System.Drawing.Size(231, 20);
            this.TOTALPRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.TOTALPRICEPERUNITTextEdit.TabIndex = 17;
            // 
            // TOTALSALESPRICEPERUNITTextEdit
            // 
            this.TOTALSALESPRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "TOTALSALESPRICEPERUNIT", true));
            this.TOTALSALESPRICEPERUNITTextEdit.Location = new System.Drawing.Point(694, 114);
            this.TOTALSALESPRICEPERUNITTextEdit.Name = "TOTALSALESPRICEPERUNITTextEdit";
            this.TOTALSALESPRICEPERUNITTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TOTALSALESPRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TOTALSALESPRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TOTALSALESPRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.TOTALSALESPRICEPERUNITTextEdit.Size = new System.Drawing.Size(231, 20);
            this.TOTALSALESPRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.TOTALSALESPRICEPERUNITTextEdit.TabIndex = 18;
            // 
            // USERCHANGEDTextEdit
            // 
            this.USERCHANGEDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "USERCHANGED", true));
            this.USERCHANGEDTextEdit.Location = new System.Drawing.Point(202, 440);
            this.USERCHANGEDTextEdit.Name = "USERCHANGEDTextEdit";
            this.USERCHANGEDTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.USERCHANGEDTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.USERCHANGEDTextEdit.Size = new System.Drawing.Size(713, 20);
            this.USERCHANGEDTextEdit.StyleController = this.dataLayoutControl1;
            this.USERCHANGEDTextEdit.TabIndex = 19;
            // 
            // WORKACTIVITY_PRICETOTALTextEdit
            // 
            this.WORKACTIVITY_PRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WORKACTIVITY_PRICETOTAL", true));
            this.WORKACTIVITY_PRICETOTALTextEdit.Location = new System.Drawing.Point(694, 503);
            this.WORKACTIVITY_PRICETOTALTextEdit.Name = "WORKACTIVITY_PRICETOTALTextEdit";
            this.WORKACTIVITY_PRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.WORKACTIVITY_PRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WORKACTIVITY_PRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WORKACTIVITY_PRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.WORKACTIVITY_PRICETOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WORKACTIVITY_PRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.WORKACTIVITY_PRICETOTALTextEdit.TabIndex = 20;
            // 
            // WORKACTIVITY_QUANTITYTOTALTextEdit
            // 
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WORKACTIVITY_QUANTITYTOTAL", true));
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Location = new System.Drawing.Point(694, 431);
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Name = "WORKACTIVITY_QUANTITYTOTALTextEdit";
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Properties.Mask.EditMask = "N2";
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.WORKACTIVITY_QUANTITYTOTALTextEdit.TabIndex = 21;
            // 
            // WORKACTIVITY_SALESPRICETOTALTextEdit
            // 
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WORKACTIVITY_SALESPRICETOTAL", true));
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Location = new System.Drawing.Point(694, 527);
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Name = "WORKACTIVITY_SALESPRICETOTALTextEdit";
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.WORKACTIVITY_SALESPRICETOTALTextEdit.TabIndex = 22;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(230, 114);
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.DescriptionTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DescriptionTextEdit.Properties.OptionsExport.Rtf.ExportTheme = true;
            this.DescriptionTextEdit.Size = new System.Drawing.Size(230, 13);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 23;
            // 
            // CalculationTextEdit
            // 
            this.CalculationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "Calculation", true));
            this.CalculationTextEdit.Location = new System.Drawing.Point(230, 66);
            this.CalculationTextEdit.Name = "CalculationTextEdit";
            this.CalculationTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CalculationTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalculationTextEdit.Size = new System.Drawing.Size(230, 20);
            this.CalculationTextEdit.StyleController = this.dataLayoutControl1;
            this.CalculationTextEdit.TabIndex = 24;
            // 
            // UnitTextEdit
            // 
            this.UnitTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "Unit", true));
            this.UnitTextEdit.Location = new System.Drawing.Point(230, 155);
            this.UnitTextEdit.Name = "UnitTextEdit";
            this.UnitTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UnitTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.UnitTextEdit.Size = new System.Drawing.Size(230, 20);
            this.UnitTextEdit.StyleController = this.dataLayoutControl1;
            this.UnitTextEdit.TabIndex = 25;
            // 
            // ItemTextEdit
            // 
            this.ItemTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "Item", true));
            this.ItemTextEdit.Location = new System.Drawing.Point(230, 221);
            this.ItemTextEdit.Name = "ItemTextEdit";
            this.ItemTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ItemTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ItemTextEdit.StyleController = this.dataLayoutControl1;
            this.ItemTextEdit.TabIndex = 26;
            // 
            // WorkActivityTextEdit
            // 
            this.WorkActivityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WorkActivity", true));
            this.WorkActivityTextEdit.Location = new System.Drawing.Point(694, 383);
            this.WorkActivityTextEdit.Name = "WorkActivityTextEdit";
            this.WorkActivityTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WorkActivityTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WorkActivityTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WorkActivityTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkActivityTextEdit.TabIndex = 27;
            // 
            // OutsourcedActivityTextEdit
            // 
            this.OutsourcedActivityTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "OutsourcedActivity", true));
            this.OutsourcedActivityTextEdit.Location = new System.Drawing.Point(694, 221);
            this.OutsourcedActivityTextEdit.Name = "OutsourcedActivityTextEdit";
            this.OutsourcedActivityTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.OutsourcedActivityTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.OutsourcedActivityTextEdit.Size = new System.Drawing.Size(231, 20);
            this.OutsourcedActivityTextEdit.StyleController = this.dataLayoutControl1;
            this.OutsourcedActivityTextEdit.TabIndex = 28;
            // 
            // SubCalculationTextEdit
            // 
            this.SubCalculationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "SubCalculation", true));
            this.SubCalculationTextEdit.Location = new System.Drawing.Point(230, 90);
            this.SubCalculationTextEdit.Name = "SubCalculationTextEdit";
            this.SubCalculationTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SubCalculationTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SubCalculationTextEdit.Size = new System.Drawing.Size(230, 20);
            this.SubCalculationTextEdit.StyleController = this.dataLayoutControl1;
            this.SubCalculationTextEdit.TabIndex = 29;
            // 
            // QUANTITYTextEdit
            // 
            this.QUANTITYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "QUANTITY", true));
            this.QUANTITYTextEdit.Location = new System.Drawing.Point(230, 131);
            this.QUANTITYTextEdit.Name = "QUANTITYTextEdit";
            this.QUANTITYTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.QUANTITYTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.QUANTITYTextEdit.Properties.Mask.EditMask = "N2";
            this.QUANTITYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.QUANTITYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QUANTITYTextEdit.Size = new System.Drawing.Size(230, 20);
            this.QUANTITYTextEdit.StyleController = this.dataLayoutControl1;
            this.QUANTITYTextEdit.TabIndex = 30;
            // 
            // ITEM_PRICEPERKGTextEdit
            // 
            this.ITEM_PRICEPERKGTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_PRICEPERKG", true));
            this.ITEM_PRICEPERKGTextEdit.Location = new System.Drawing.Point(230, 245);
            this.ITEM_PRICEPERKGTextEdit.Name = "ITEM_PRICEPERKGTextEdit";
            this.ITEM_PRICEPERKGTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_PRICEPERKGTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_PRICEPERKGTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_PRICEPERKGTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_PRICEPERKGTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_PRICEPERKGTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_PRICEPERKGTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_PRICEPERKGTextEdit.TabIndex = 31;
            // 
            // ITEM_SALESPRICEPERKGTextEdit
            // 
            this.ITEM_SALESPRICEPERKGTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_SALESPRICEPERKG", true));
            this.ITEM_SALESPRICEPERKGTextEdit.Location = new System.Drawing.Point(230, 269);
            this.ITEM_SALESPRICEPERKGTextEdit.Name = "ITEM_SALESPRICEPERKGTextEdit";
            this.ITEM_SALESPRICEPERKGTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_SALESPRICEPERKGTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_SALESPRICEPERKGTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_SALESPRICEPERKGTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_SALESPRICEPERKGTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_SALESPRICEPERKGTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_SALESPRICEPERKGTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_SALESPRICEPERKGTextEdit.TabIndex = 32;
            // 
            // ITEM_PRICEPERMTRTextEdit
            // 
            this.ITEM_PRICEPERMTRTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_PRICEPERMTR", true));
            this.ITEM_PRICEPERMTRTextEdit.Location = new System.Drawing.Point(230, 293);
            this.ITEM_PRICEPERMTRTextEdit.Name = "ITEM_PRICEPERMTRTextEdit";
            this.ITEM_PRICEPERMTRTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_PRICEPERMTRTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_PRICEPERMTRTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_PRICEPERMTRTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_PRICEPERMTRTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_PRICEPERMTRTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_PRICEPERMTRTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_PRICEPERMTRTextEdit.TabIndex = 33;
            // 
            // ITEM_SALESPRICEPERMTRTextEdit
            // 
            this.ITEM_SALESPRICEPERMTRTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_SALESPRICEPERMTR", true));
            this.ITEM_SALESPRICEPERMTRTextEdit.Location = new System.Drawing.Point(230, 317);
            this.ITEM_SALESPRICEPERMTRTextEdit.Name = "ITEM_SALESPRICEPERMTRTextEdit";
            this.ITEM_SALESPRICEPERMTRTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_SALESPRICEPERMTRTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_SALESPRICEPERMTRTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_SALESPRICEPERMTRTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_SALESPRICEPERMTRTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_SALESPRICEPERMTRTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_SALESPRICEPERMTRTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_SALESPRICEPERMTRTextEdit.TabIndex = 34;
            // 
            // ITEM_LENGTHPERUNITTextEdit
            // 
            this.ITEM_LENGTHPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_LENGTHPERUNIT", true));
            this.ITEM_LENGTHPERUNITTextEdit.Location = new System.Drawing.Point(230, 341);
            this.ITEM_LENGTHPERUNITTextEdit.Name = "ITEM_LENGTHPERUNITTextEdit";
            this.ITEM_LENGTHPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_LENGTHPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_LENGTHPERUNITTextEdit.Properties.Mask.EditMask = "N2";
            this.ITEM_LENGTHPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_LENGTHPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_LENGTHPERUNITTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_LENGTHPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_LENGTHPERUNITTextEdit.TabIndex = 35;
            // 
            // ITEM_WIDTHPERUNITTextEdit
            // 
            this.ITEM_WIDTHPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_WIDTHPERUNIT", true));
            this.ITEM_WIDTHPERUNITTextEdit.Location = new System.Drawing.Point(230, 365);
            this.ITEM_WIDTHPERUNITTextEdit.Name = "ITEM_WIDTHPERUNITTextEdit";
            this.ITEM_WIDTHPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_WIDTHPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_WIDTHPERUNITTextEdit.Properties.Mask.EditMask = "N2";
            this.ITEM_WIDTHPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_WIDTHPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_WIDTHPERUNITTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_WIDTHPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_WIDTHPERUNITTextEdit.TabIndex = 36;
            // 
            // ITEM_PRICEPERUNITTextEdit
            // 
            this.ITEM_PRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_PRICEPERUNIT", true));
            this.ITEM_PRICEPERUNITTextEdit.Location = new System.Drawing.Point(230, 389);
            this.ITEM_PRICEPERUNITTextEdit.Name = "ITEM_PRICEPERUNITTextEdit";
            this.ITEM_PRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_PRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_PRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_PRICEPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_PRICEPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_PRICEPERUNITTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_PRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_PRICEPERUNITTextEdit.TabIndex = 37;
            // 
            // ITEM_SALESPRICEPERUNITTextEdit
            // 
            this.ITEM_SALESPRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_SALESPRICEPERUNIT", true));
            this.ITEM_SALESPRICEPERUNITTextEdit.Location = new System.Drawing.Point(230, 413);
            this.ITEM_SALESPRICEPERUNITTextEdit.Name = "ITEM_SALESPRICEPERUNITTextEdit";
            this.ITEM_SALESPRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_SALESPRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_SALESPRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.ITEM_SALESPRICEPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_SALESPRICEPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_SALESPRICEPERUNITTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_SALESPRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_SALESPRICEPERUNITTextEdit.TabIndex = 38;
            // 
            // OUTSOURCED_PRICEPERUNITTextEdit
            // 
            this.OUTSOURCED_PRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "OUTSOURCED_PRICEPERUNIT", true));
            this.OUTSOURCED_PRICEPERUNITTextEdit.Location = new System.Drawing.Point(694, 293);
            this.OUTSOURCED_PRICEPERUNITTextEdit.Name = "OUTSOURCED_PRICEPERUNITTextEdit";
            this.OUTSOURCED_PRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.OUTSOURCED_PRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.OUTSOURCED_PRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.OUTSOURCED_PRICEPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.OUTSOURCED_PRICEPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OUTSOURCED_PRICEPERUNITTextEdit.Size = new System.Drawing.Size(231, 20);
            this.OUTSOURCED_PRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.OUTSOURCED_PRICEPERUNITTextEdit.TabIndex = 39;
            // 
            // OUTSOURCED_SALESPRICEPERUNITTextEdit
            // 
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "OUTSOURCED_SALESPRICEPERUNIT", true));
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Location = new System.Drawing.Point(694, 317);
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Name = "OUTSOURCED_SALESPRICEPERUNITTextEdit";
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Size = new System.Drawing.Size(231, 20);
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.OUTSOURCED_SALESPRICEPERUNITTextEdit.TabIndex = 40;
            // 
            // WORKACTIVITY_QUANTITYTextEdit
            // 
            this.WORKACTIVITY_QUANTITYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WORKACTIVITY_QUANTITY", true));
            this.WORKACTIVITY_QUANTITYTextEdit.Location = new System.Drawing.Point(694, 407);
            this.WORKACTIVITY_QUANTITYTextEdit.Name = "WORKACTIVITY_QUANTITYTextEdit";
            this.WORKACTIVITY_QUANTITYTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WORKACTIVITY_QUANTITYTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WORKACTIVITY_QUANTITYTextEdit.Properties.Mask.EditMask = "N2";
            this.WORKACTIVITY_QUANTITYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WORKACTIVITY_QUANTITYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WORKACTIVITY_QUANTITYTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WORKACTIVITY_QUANTITYTextEdit.StyleController = this.dataLayoutControl1;
            this.WORKACTIVITY_QUANTITYTextEdit.TabIndex = 41;
            // 
            // WORKACTIVITY_PRICEPERUNITTextEdit
            // 
            this.WORKACTIVITY_PRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WORKACTIVITY_PRICEPERUNIT", true));
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Location = new System.Drawing.Point(694, 455);
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Name = "WORKACTIVITY_PRICEPERUNITTextEdit";
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WORKACTIVITY_PRICEPERUNITTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WORKACTIVITY_PRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.WORKACTIVITY_PRICEPERUNITTextEdit.TabIndex = 42;
            // 
            // WORKACTIVITY_SALESPRICEPERUNITTextEdit
            // 
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "WORKACTIVITY_SALESPRICEPERUNIT", true));
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Location = new System.Drawing.Point(694, 479);
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Name = "WORKACTIVITY_SALESPRICEPERUNITTextEdit";
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties.Mask.EditMask = "C2";
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Size = new System.Drawing.Size(231, 20);
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.StyleController = this.dataLayoutControl1;
            this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.TabIndex = 43;
            // 
            // PRICETOTALTextEdit
            // 
            this.PRICETOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "PRICETOTAL", true));
            this.PRICETOTALTextEdit.Location = new System.Drawing.Point(694, 42);
            this.PRICETOTALTextEdit.Name = "PRICETOTALTextEdit";
            this.PRICETOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.PRICETOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PRICETOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PRICETOTALTextEdit.Properties.Mask.EditMask = "C2";
            this.PRICETOTALTextEdit.Size = new System.Drawing.Size(231, 20);
            this.PRICETOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.PRICETOTALTextEdit.TabIndex = 44;
            // 
            // ITEM_LENGTHTOTALTextEdit
            // 
            this.ITEM_LENGTHTOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_LENGTHTOTAL", true));
            this.ITEM_LENGTHTOTALTextEdit.Location = new System.Drawing.Point(230, 437);
            this.ITEM_LENGTHTOTALTextEdit.Name = "ITEM_LENGTHTOTALTextEdit";
            this.ITEM_LENGTHTOTALTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ITEM_LENGTHTOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_LENGTHTOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_LENGTHTOTALTextEdit.Properties.Mask.EditMask = "N2";
            this.ITEM_LENGTHTOTALTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_LENGTHTOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_LENGTHTOTALTextEdit.TabIndex = 45;
            // 
            // ITEM_WIDTHTOTALTextEdit
            // 
            this.ITEM_WIDTHTOTALTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "ITEM_WIDTHTOTAL", true));
            this.ITEM_WIDTHTOTALTextEdit.Location = new System.Drawing.Point(230, 461);
            this.ITEM_WIDTHTOTALTextEdit.Name = "ITEM_WIDTHTOTALTextEdit";
            this.ITEM_WIDTHTOTALTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.ITEM_WIDTHTOTALTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ITEM_WIDTHTOTALTextEdit.Properties.Mask.EditMask = "N2";
            this.ITEM_WIDTHTOTALTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ITEM_WIDTHTOTALTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ITEM_WIDTHTOTALTextEdit.Size = new System.Drawing.Size(230, 20);
            this.ITEM_WIDTHTOTALTextEdit.StyleController = this.dataLayoutControl1;
            this.ITEM_WIDTHTOTALTextEdit.TabIndex = 46;
            // 
            // SEQUENCENUMBERTextEdit
            // 
            this.SEQUENCENUMBERTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.calculationRuleBindingSource, "SEQUENCENUMBER", true));
            this.SEQUENCENUMBERTextEdit.Location = new System.Drawing.Point(230, 42);
            this.SEQUENCENUMBERTextEdit.Name = "SEQUENCENUMBERTextEdit";
            this.SEQUENCENUMBERTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SEQUENCENUMBERTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SEQUENCENUMBERTextEdit.Properties.Mask.EditMask = "N0";
            this.SEQUENCENUMBERTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SEQUENCENUMBERTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SEQUENCENUMBERTextEdit.Size = new System.Drawing.Size(230, 20);
            this.SEQUENCENUMBERTextEdit.StyleController = this.dataLayoutControl1;
            this.SEQUENCENUMBERTextEdit.TabIndex = 47;
            // 
            // ItemForFK_CALCULATION
            // 
            this.ItemForFK_CALCULATION.Control = this.FK_CALCULATIONTextEdit;
            this.ItemForFK_CALCULATION.Location = new System.Drawing.Point(0, 24);
            this.ItemForFK_CALCULATION.Name = "ItemForFK_CALCULATION";
            this.ItemForFK_CALCULATION.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_CALCULATION.Text = "FK_CALCULATION";
            this.ItemForFK_CALCULATION.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFK_ITEM
            // 
            this.ItemForFK_ITEM.Control = this.FK_ITEMTextEdit;
            this.ItemForFK_ITEM.Location = new System.Drawing.Point(0, 24);
            this.ItemForFK_ITEM.Name = "ItemForFK_ITEM";
            this.ItemForFK_ITEM.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_ITEM.Text = "FK_ITEM";
            this.ItemForFK_ITEM.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFK_OUTSOURCED
            // 
            this.ItemForFK_OUTSOURCED.Control = this.FK_OUTSOURCEDTextEdit;
            this.ItemForFK_OUTSOURCED.Location = new System.Drawing.Point(0, 24);
            this.ItemForFK_OUTSOURCED.Name = "ItemForFK_OUTSOURCED";
            this.ItemForFK_OUTSOURCED.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_OUTSOURCED.Text = "FK_OUTSOURCED";
            this.ItemForFK_OUTSOURCED.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFK_UNIT
            // 
            this.ItemForFK_UNIT.Control = this.FK_UNITTextEdit;
            this.ItemForFK_UNIT.Location = new System.Drawing.Point(0, 48);
            this.ItemForFK_UNIT.Name = "ItemForFK_UNIT";
            this.ItemForFK_UNIT.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_UNIT.Text = "FK_UNIT";
            this.ItemForFK_UNIT.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFK_SUBCALCULATION
            // 
            this.ItemForFK_SUBCALCULATION.Control = this.FK_SUBCALCULATIONTextEdit;
            this.ItemForFK_SUBCALCULATION.Location = new System.Drawing.Point(0, 24);
            this.ItemForFK_SUBCALCULATION.Name = "ItemForFK_SUBCALCULATION";
            this.ItemForFK_SUBCALCULATION.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_SUBCALCULATION.Text = "FK_SUBCALCULATION";
            this.ItemForFK_SUBCALCULATION.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFK_WORKACTIVITY
            // 
            this.ItemForFK_WORKACTIVITY.Control = this.FK_WORKACTIVITYTextEdit;
            this.ItemForFK_WORKACTIVITY.Location = new System.Drawing.Point(0, 24);
            this.ItemForFK_WORKACTIVITY.Name = "ItemForFK_WORKACTIVITY";
            this.ItemForFK_WORKACTIVITY.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_WORKACTIVITY.Text = "FK_WORKACTIVITY";
            this.ItemForFK_WORKACTIVITY.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFK_WORKFLOWSTATE
            // 
            this.ItemForFK_WORKFLOWSTATE.Control = this.FK_WORKFLOWSTATETextEdit;
            this.ItemForFK_WORKFLOWSTATE.Location = new System.Drawing.Point(0, 24);
            this.ItemForFK_WORKFLOWSTATE.Name = "ItemForFK_WORKFLOWSTATE";
            this.ItemForFK_WORKFLOWSTATE.Size = new System.Drawing.Size(907, 24);
            this.ItemForFK_WORKFLOWSTATE.Text = "FK_WORKFLOWSTATE";
            this.ItemForFK_WORKFLOWSTATE.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForId
            // 
            this.ItemForId.Control = this.IdTextEdit;
            this.ItemForId.Location = new System.Drawing.Point(453, 0);
            this.ItemForId.Name = "ItemForId";
            this.ItemForId.Size = new System.Drawing.Size(454, 52);
            this.ItemForId.Text = "Id";
            this.ItemForId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForUSERCHANGED
            // 
            this.ItemForUSERCHANGED.Control = this.USERCHANGEDTextEdit;
            this.ItemForUSERCHANGED.Location = new System.Drawing.Point(0, 428);
            this.ItemForUSERCHANGED.Name = "ItemForUSERCHANGED";
            this.ItemForUSERCHANGED.Size = new System.Drawing.Size(907, 24);
            this.ItemForUSERCHANGED.Text = "USERCHANGED";
            this.ItemForUSERCHANGED.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(949, 591);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.U,
            this.layoutControlGroup5,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(929, 571);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDescription,
            this.ItemForCalculation,
            this.ItemForUnit,
            this.ItemForSubCalculation,
            this.ItemForQUANTITY,
            this.ItemForSEQUENCENUMBER});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(464, 179);
            this.layoutControlGroup3.Text = "Algemeen";
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.Location = new System.Drawing.Point(0, 72);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(440, 17);
            this.ItemForDescription.Text = "Omschrijving";
            this.ItemForDescription.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForCalculation
            // 
            this.ItemForCalculation.Control = this.CalculationTextEdit;
            this.ItemForCalculation.Location = new System.Drawing.Point(0, 24);
            this.ItemForCalculation.Name = "ItemForCalculation";
            this.ItemForCalculation.Size = new System.Drawing.Size(440, 24);
            this.ItemForCalculation.Text = "Calculatie";
            this.ItemForCalculation.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForUnit
            // 
            this.ItemForUnit.Control = this.UnitTextEdit;
            this.ItemForUnit.Location = new System.Drawing.Point(0, 113);
            this.ItemForUnit.Name = "ItemForUnit";
            this.ItemForUnit.Size = new System.Drawing.Size(440, 24);
            this.ItemForUnit.Text = "Eenheid";
            this.ItemForUnit.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForSubCalculation
            // 
            this.ItemForSubCalculation.Control = this.SubCalculationTextEdit;
            this.ItemForSubCalculation.Location = new System.Drawing.Point(0, 48);
            this.ItemForSubCalculation.Name = "ItemForSubCalculation";
            this.ItemForSubCalculation.Size = new System.Drawing.Size(440, 24);
            this.ItemForSubCalculation.Text = "Sub Calculatie";
            this.ItemForSubCalculation.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForQUANTITY
            // 
            this.ItemForQUANTITY.Control = this.QUANTITYTextEdit;
            this.ItemForQUANTITY.Location = new System.Drawing.Point(0, 89);
            this.ItemForQUANTITY.Name = "ItemForQUANTITY";
            this.ItemForQUANTITY.Size = new System.Drawing.Size(440, 24);
            this.ItemForQUANTITY.Text = "Aantal";
            this.ItemForQUANTITY.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForSEQUENCENUMBER
            // 
            this.ItemForSEQUENCENUMBER.Control = this.SEQUENCENUMBERTextEdit;
            this.ItemForSEQUENCENUMBER.Location = new System.Drawing.Point(0, 0);
            this.ItemForSEQUENCENUMBER.Name = "ItemForSEQUENCENUMBER";
            this.ItemForSEQUENCENUMBER.Size = new System.Drawing.Size(440, 24);
            this.ItemForSEQUENCENUMBER.Text = "Volg nummer";
            this.ItemForSEQUENCENUMBER.TextSize = new System.Drawing.Size(203, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForItem,
            this.ItemForITEM_PRICEPERKG,
            this.ItemForITEM_SALESPRICEPERKG,
            this.ItemForITEM_PRICEPERMTR,
            this.ItemForITEM_SALESPRICEPERMTR,
            this.ItemForITEM_LENGTHPERUNIT,
            this.ItemForITEM_WIDTHPERUNIT,
            this.ItemForITEM_PRICEPERUNIT,
            this.ItemForITEM_SALESPRICEPERUNIT,
            this.ItemForITEM_LENGTHTOTAL,
            this.ItemForITEM_WIDTHTOTAL,
            this.ItemForITEM_PRICETOTAL,
            this.ItemForITEM_SALESPRICETOTAL});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 179);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(464, 372);
            this.layoutControlGroup4.Text = "Artikel";
            // 
            // ItemForItem
            // 
            this.ItemForItem.Control = this.ItemTextEdit;
            this.ItemForItem.Location = new System.Drawing.Point(0, 0);
            this.ItemForItem.Name = "ItemForItem";
            this.ItemForItem.Size = new System.Drawing.Size(440, 24);
            this.ItemForItem.Text = "Artikel";
            this.ItemForItem.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_PRICEPERKG
            // 
            this.ItemForITEM_PRICEPERKG.Control = this.ITEM_PRICEPERKGTextEdit;
            this.ItemForITEM_PRICEPERKG.Location = new System.Drawing.Point(0, 24);
            this.ItemForITEM_PRICEPERKG.Name = "ItemForITEM_PRICEPERKG";
            this.ItemForITEM_PRICEPERKG.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_PRICEPERKG.Text = "Artikel prijs per kilo";
            this.ItemForITEM_PRICEPERKG.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_SALESPRICEPERKG
            // 
            this.ItemForITEM_SALESPRICEPERKG.Control = this.ITEM_SALESPRICEPERKGTextEdit;
            this.ItemForITEM_SALESPRICEPERKG.Location = new System.Drawing.Point(0, 48);
            this.ItemForITEM_SALESPRICEPERKG.Name = "ItemForITEM_SALESPRICEPERKG";
            this.ItemForITEM_SALESPRICEPERKG.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_SALESPRICEPERKG.Text = "Artikel verkoop prijs per kilo";
            this.ItemForITEM_SALESPRICEPERKG.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_PRICEPERMTR
            // 
            this.ItemForITEM_PRICEPERMTR.Control = this.ITEM_PRICEPERMTRTextEdit;
            this.ItemForITEM_PRICEPERMTR.Location = new System.Drawing.Point(0, 72);
            this.ItemForITEM_PRICEPERMTR.Name = "ItemForITEM_PRICEPERMTR";
            this.ItemForITEM_PRICEPERMTR.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_PRICEPERMTR.Text = "Artikel prijs per meter";
            this.ItemForITEM_PRICEPERMTR.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_SALESPRICEPERMTR
            // 
            this.ItemForITEM_SALESPRICEPERMTR.Control = this.ITEM_SALESPRICEPERMTRTextEdit;
            this.ItemForITEM_SALESPRICEPERMTR.Location = new System.Drawing.Point(0, 96);
            this.ItemForITEM_SALESPRICEPERMTR.Name = "ItemForITEM_SALESPRICEPERMTR";
            this.ItemForITEM_SALESPRICEPERMTR.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_SALESPRICEPERMTR.Text = "Artikel verkoop prijs per meter";
            this.ItemForITEM_SALESPRICEPERMTR.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_LENGTHPERUNIT
            // 
            this.ItemForITEM_LENGTHPERUNIT.Control = this.ITEM_LENGTHPERUNITTextEdit;
            this.ItemForITEM_LENGTHPERUNIT.Location = new System.Drawing.Point(0, 120);
            this.ItemForITEM_LENGTHPERUNIT.Name = "ItemForITEM_LENGTHPERUNIT";
            this.ItemForITEM_LENGTHPERUNIT.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_LENGTHPERUNIT.Text = "Artikel lengte per eenheid";
            this.ItemForITEM_LENGTHPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_WIDTHPERUNIT
            // 
            this.ItemForITEM_WIDTHPERUNIT.Control = this.ITEM_WIDTHPERUNITTextEdit;
            this.ItemForITEM_WIDTHPERUNIT.Location = new System.Drawing.Point(0, 144);
            this.ItemForITEM_WIDTHPERUNIT.Name = "ItemForITEM_WIDTHPERUNIT";
            this.ItemForITEM_WIDTHPERUNIT.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_WIDTHPERUNIT.Text = "Artikel breedte per eenheid";
            this.ItemForITEM_WIDTHPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_PRICEPERUNIT
            // 
            this.ItemForITEM_PRICEPERUNIT.Control = this.ITEM_PRICEPERUNITTextEdit;
            this.ItemForITEM_PRICEPERUNIT.Location = new System.Drawing.Point(0, 168);
            this.ItemForITEM_PRICEPERUNIT.Name = "ItemForITEM_PRICEPERUNIT";
            this.ItemForITEM_PRICEPERUNIT.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_PRICEPERUNIT.Text = "Artikel prijs per eenheid";
            this.ItemForITEM_PRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_SALESPRICEPERUNIT
            // 
            this.ItemForITEM_SALESPRICEPERUNIT.Control = this.ITEM_SALESPRICEPERUNITTextEdit;
            this.ItemForITEM_SALESPRICEPERUNIT.Location = new System.Drawing.Point(0, 192);
            this.ItemForITEM_SALESPRICEPERUNIT.Name = "ItemForITEM_SALESPRICEPERUNIT";
            this.ItemForITEM_SALESPRICEPERUNIT.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_SALESPRICEPERUNIT.Text = "Artikel verkoop prijs per eenheid";
            this.ItemForITEM_SALESPRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_LENGTHTOTAL
            // 
            this.ItemForITEM_LENGTHTOTAL.Control = this.ITEM_LENGTHTOTALTextEdit;
            this.ItemForITEM_LENGTHTOTAL.Location = new System.Drawing.Point(0, 216);
            this.ItemForITEM_LENGTHTOTAL.Name = "ItemForITEM_LENGTHTOTAL";
            this.ItemForITEM_LENGTHTOTAL.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_LENGTHTOTAL.Text = "Artikel totaal lengte";
            this.ItemForITEM_LENGTHTOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_WIDTHTOTAL
            // 
            this.ItemForITEM_WIDTHTOTAL.Control = this.ITEM_WIDTHTOTALTextEdit;
            this.ItemForITEM_WIDTHTOTAL.Location = new System.Drawing.Point(0, 240);
            this.ItemForITEM_WIDTHTOTAL.Name = "ItemForITEM_WIDTHTOTAL";
            this.ItemForITEM_WIDTHTOTAL.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_WIDTHTOTAL.Text = "Artikel totaal breedte";
            this.ItemForITEM_WIDTHTOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_PRICETOTAL
            // 
            this.ItemForITEM_PRICETOTAL.Control = this.ITEM_PRICETOTALTextEdit;
            this.ItemForITEM_PRICETOTAL.Location = new System.Drawing.Point(0, 264);
            this.ItemForITEM_PRICETOTAL.Name = "ItemForITEM_PRICETOTAL";
            this.ItemForITEM_PRICETOTAL.Size = new System.Drawing.Size(440, 24);
            this.ItemForITEM_PRICETOTAL.Text = "Artikel totaal prijs";
            this.ItemForITEM_PRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForITEM_SALESPRICETOTAL
            // 
            this.ItemForITEM_SALESPRICETOTAL.Control = this.ITEM_SALESPRICETOTALTextEdit;
            this.ItemForITEM_SALESPRICETOTAL.Location = new System.Drawing.Point(0, 288);
            this.ItemForITEM_SALESPRICETOTAL.Name = "ItemForITEM_SALESPRICETOTAL";
            this.ItemForITEM_SALESPRICETOTAL.Size = new System.Drawing.Size(440, 42);
            this.ItemForITEM_SALESPRICETOTAL.Text = "Artikel totaal verkoop prijs";
            this.ItemForITEM_SALESPRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWORKACTIVITY_PRICETOTAL,
            this.ItemForWORKACTIVITY_QUANTITYTOTAL,
            this.ItemForWORKACTIVITY_SALESPRICETOTAL,
            this.ItemForWorkActivity,
            this.ItemForWORKACTIVITY_QUANTITY,
            this.ItemForWORKACTIVITY_PRICEPERUNIT,
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT});
            this.layoutControlGroup6.Location = new System.Drawing.Point(464, 341);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(465, 210);
            this.layoutControlGroup6.Text = "Bewerking";
            // 
            // ItemForWORKACTIVITY_PRICETOTAL
            // 
            this.ItemForWORKACTIVITY_PRICETOTAL.Control = this.WORKACTIVITY_PRICETOTALTextEdit;
            this.ItemForWORKACTIVITY_PRICETOTAL.Location = new System.Drawing.Point(0, 120);
            this.ItemForWORKACTIVITY_PRICETOTAL.Name = "ItemForWORKACTIVITY_PRICETOTAL";
            this.ItemForWORKACTIVITY_PRICETOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForWORKACTIVITY_PRICETOTAL.Text = "Bewerking totaal prijs";
            this.ItemForWORKACTIVITY_PRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForWORKACTIVITY_QUANTITYTOTAL
            // 
            this.ItemForWORKACTIVITY_QUANTITYTOTAL.Control = this.WORKACTIVITY_QUANTITYTOTALTextEdit;
            this.ItemForWORKACTIVITY_QUANTITYTOTAL.Location = new System.Drawing.Point(0, 48);
            this.ItemForWORKACTIVITY_QUANTITYTOTAL.Name = "ItemForWORKACTIVITY_QUANTITYTOTAL";
            this.ItemForWORKACTIVITY_QUANTITYTOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForWORKACTIVITY_QUANTITYTOTAL.Text = "Bewerking totaal aantal";
            this.ItemForWORKACTIVITY_QUANTITYTOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForWORKACTIVITY_SALESPRICETOTAL
            // 
            this.ItemForWORKACTIVITY_SALESPRICETOTAL.Control = this.WORKACTIVITY_SALESPRICETOTALTextEdit;
            this.ItemForWORKACTIVITY_SALESPRICETOTAL.Location = new System.Drawing.Point(0, 144);
            this.ItemForWORKACTIVITY_SALESPRICETOTAL.Name = "ItemForWORKACTIVITY_SALESPRICETOTAL";
            this.ItemForWORKACTIVITY_SALESPRICETOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForWORKACTIVITY_SALESPRICETOTAL.Text = "Bewerking totaal verkoop prijs";
            this.ItemForWORKACTIVITY_SALESPRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForWorkActivity
            // 
            this.ItemForWorkActivity.Control = this.WorkActivityTextEdit;
            this.ItemForWorkActivity.Location = new System.Drawing.Point(0, 0);
            this.ItemForWorkActivity.Name = "ItemForWorkActivity";
            this.ItemForWorkActivity.Size = new System.Drawing.Size(441, 24);
            this.ItemForWorkActivity.Text = "Bewerking";
            this.ItemForWorkActivity.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForWORKACTIVITY_QUANTITY
            // 
            this.ItemForWORKACTIVITY_QUANTITY.Control = this.WORKACTIVITY_QUANTITYTextEdit;
            this.ItemForWORKACTIVITY_QUANTITY.Location = new System.Drawing.Point(0, 24);
            this.ItemForWORKACTIVITY_QUANTITY.Name = "ItemForWORKACTIVITY_QUANTITY";
            this.ItemForWORKACTIVITY_QUANTITY.Size = new System.Drawing.Size(441, 24);
            this.ItemForWORKACTIVITY_QUANTITY.Text = "Bewerking aantal";
            this.ItemForWORKACTIVITY_QUANTITY.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForWORKACTIVITY_PRICEPERUNIT
            // 
            this.ItemForWORKACTIVITY_PRICEPERUNIT.Control = this.WORKACTIVITY_PRICEPERUNITTextEdit;
            this.ItemForWORKACTIVITY_PRICEPERUNIT.Location = new System.Drawing.Point(0, 72);
            this.ItemForWORKACTIVITY_PRICEPERUNIT.Name = "ItemForWORKACTIVITY_PRICEPERUNIT";
            this.ItemForWORKACTIVITY_PRICEPERUNIT.Size = new System.Drawing.Size(441, 24);
            this.ItemForWORKACTIVITY_PRICEPERUNIT.Text = "Bewerking prijs per eenheid";
            this.ItemForWORKACTIVITY_PRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForWORKACTIVITY_SALESPRICEPERUNIT
            // 
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT.Control = this.WORKACTIVITY_SALESPRICEPERUNITTextEdit;
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT.Location = new System.Drawing.Point(0, 96);
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT.Name = "ItemForWORKACTIVITY_SALESPRICEPERUNIT";
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT.Size = new System.Drawing.Size(441, 24);
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT.Text = "Bewerking verkoop prijs per eenheid";
            this.ItemForWORKACTIVITY_SALESPRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // U
            // 
            this.U.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOUTSOURCED_PRICETOTAL,
            this.ItemForOUTSOURCED_SALESPRICETOTAL,
            this.ItemForOutsourcedActivity,
            this.ItemForOUTSOURCED_PRICEPERUNIT,
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT});
            this.U.Location = new System.Drawing.Point(464, 179);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(465, 162);
            this.U.Text = "Uitbesteed werk";
            // 
            // ItemForOUTSOURCED_PRICETOTAL
            // 
            this.ItemForOUTSOURCED_PRICETOTAL.Control = this.OUTSOURCED_PRICETOTALTextEdit;
            this.ItemForOUTSOURCED_PRICETOTAL.Location = new System.Drawing.Point(0, 24);
            this.ItemForOUTSOURCED_PRICETOTAL.Name = "ItemForOUTSOURCED_PRICETOTAL";
            this.ItemForOUTSOURCED_PRICETOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForOUTSOURCED_PRICETOTAL.Text = "Uitbesteed werk totaal prijs";
            this.ItemForOUTSOURCED_PRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForOUTSOURCED_SALESPRICETOTAL
            // 
            this.ItemForOUTSOURCED_SALESPRICETOTAL.Control = this.OUTSOURCED_SALESPRICETOTALTextEdit;
            this.ItemForOUTSOURCED_SALESPRICETOTAL.Location = new System.Drawing.Point(0, 48);
            this.ItemForOUTSOURCED_SALESPRICETOTAL.Name = "ItemForOUTSOURCED_SALESPRICETOTAL";
            this.ItemForOUTSOURCED_SALESPRICETOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForOUTSOURCED_SALESPRICETOTAL.Text = "Uitbesteed werk verkoop prijs";
            this.ItemForOUTSOURCED_SALESPRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForOutsourcedActivity
            // 
            this.ItemForOutsourcedActivity.Control = this.OutsourcedActivityTextEdit;
            this.ItemForOutsourcedActivity.Location = new System.Drawing.Point(0, 0);
            this.ItemForOutsourcedActivity.Name = "ItemForOutsourcedActivity";
            this.ItemForOutsourcedActivity.Size = new System.Drawing.Size(441, 24);
            this.ItemForOutsourcedActivity.Text = "Uitbesteed werk";
            this.ItemForOutsourcedActivity.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForOUTSOURCED_PRICEPERUNIT
            // 
            this.ItemForOUTSOURCED_PRICEPERUNIT.Control = this.OUTSOURCED_PRICEPERUNITTextEdit;
            this.ItemForOUTSOURCED_PRICEPERUNIT.Location = new System.Drawing.Point(0, 72);
            this.ItemForOUTSOURCED_PRICEPERUNIT.Name = "ItemForOUTSOURCED_PRICEPERUNIT";
            this.ItemForOUTSOURCED_PRICEPERUNIT.Size = new System.Drawing.Size(441, 24);
            this.ItemForOUTSOURCED_PRICEPERUNIT.Text = "Uitbesteed werk prijs per eenheid";
            this.ItemForOUTSOURCED_PRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForOUTSOURCED_SALESPRICEPERUNIT
            // 
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT.Control = this.OUTSOURCED_SALESPRICEPERUNITTextEdit;
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT.Location = new System.Drawing.Point(0, 96);
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT.Name = "ItemForOUTSOURCED_SALESPRICEPERUNIT";
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT.Size = new System.Drawing.Size(441, 24);
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT.Text = "Uitbesteed werk verkoop prijs per eenheid";
            this.ItemForOUTSOURCED_SALESPRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSALESPRICETOTAL,
            this.ItemForTOTALPRICEPERUNIT,
            this.ItemForTOTALSALESPRICEPERUNIT,
            this.ItemForPRICETOTAL});
            this.layoutControlGroup5.Location = new System.Drawing.Point(464, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(465, 179);
            this.layoutControlGroup5.Text = "Totalen";
            // 
            // ItemForSALESPRICETOTAL
            // 
            this.ItemForSALESPRICETOTAL.Control = this.SALESPRICETOTALTextEdit;
            this.ItemForSALESPRICETOTAL.Location = new System.Drawing.Point(0, 24);
            this.ItemForSALESPRICETOTAL.Name = "ItemForSALESPRICETOTAL";
            this.ItemForSALESPRICETOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForSALESPRICETOTAL.Text = "Totaal verkoop prijs";
            this.ItemForSALESPRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForTOTALPRICEPERUNIT
            // 
            this.ItemForTOTALPRICEPERUNIT.Control = this.TOTALPRICEPERUNITTextEdit;
            this.ItemForTOTALPRICEPERUNIT.Location = new System.Drawing.Point(0, 48);
            this.ItemForTOTALPRICEPERUNIT.Name = "ItemForTOTALPRICEPERUNIT";
            this.ItemForTOTALPRICEPERUNIT.Size = new System.Drawing.Size(441, 24);
            this.ItemForTOTALPRICEPERUNIT.Text = "Totaal prijs per eenheid";
            this.ItemForTOTALPRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForTOTALSALESPRICEPERUNIT
            // 
            this.ItemForTOTALSALESPRICEPERUNIT.Control = this.TOTALSALESPRICEPERUNITTextEdit;
            this.ItemForTOTALSALESPRICEPERUNIT.Location = new System.Drawing.Point(0, 72);
            this.ItemForTOTALSALESPRICEPERUNIT.Name = "ItemForTOTALSALESPRICEPERUNIT";
            this.ItemForTOTALSALESPRICEPERUNIT.Size = new System.Drawing.Size(441, 65);
            this.ItemForTOTALSALESPRICEPERUNIT.Text = "Totaal verkoop prijs per eenheid";
            this.ItemForTOTALSALESPRICEPERUNIT.TextSize = new System.Drawing.Size(203, 13);
            // 
            // ItemForPRICETOTAL
            // 
            this.ItemForPRICETOTAL.Control = this.PRICETOTALTextEdit;
            this.ItemForPRICETOTAL.Location = new System.Drawing.Point(0, 0);
            this.ItemForPRICETOTAL.Name = "ItemForPRICETOTAL";
            this.ItemForPRICETOTAL.Size = new System.Drawing.Size(441, 24);
            this.ItemForPRICETOTAL.Text = "Totaal prijs";
            this.ItemForPRICETOTAL.TextSize = new System.Drawing.Size(203, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 551);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(929, 20);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // CalculationRuleDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 601);
            this.Controls.Add(this.dataLayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "CalculationRuleDetails";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Calculation regel details";
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FK_CALCULATIONTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationRuleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_ITEMTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_OUTSOURCEDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_SUBCALCULATIONTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_UNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_WORKACTIVITYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FK_WORKFLOWSTATETextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_PRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_SALESPRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SALESPRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALPRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TOTALSALESPRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.USERCHANGEDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_PRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_QUANTITYTOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_SALESPRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkActivityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutsourcedActivityTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubCalculationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QUANTITYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICEPERKGTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICEPERKGTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICEPERMTRTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICEPERMTRTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_LENGTHPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_WIDTHPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_PRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_SALESPRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_PRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OUTSOURCED_SALESPRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_QUANTITYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_PRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WORKACTIVITY_SALESPRICEPERUNITTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PRICETOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_LENGTHTOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM_WIDTHTOTALTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SEQUENCENUMBERTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_CALCULATION)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_ITEM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_OUTSOURCED)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_UNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_SUBCALCULATION)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_WORKACTIVITY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFK_WORKFLOWSTATE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUSERCHANGED)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubCalculation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQUANTITY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSEQUENCENUMBER)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICEPERKG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICEPERKG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICEPERMTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICEPERMTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_LENGTHPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_WIDTHPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_LENGTHTOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_WIDTHTOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_PRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForITEM_SALESPRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_PRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_QUANTITYTOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_SALESPRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_QUANTITY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_PRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWORKACTIVITY_SALESPRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.U)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_PRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_SALESPRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOutsourcedActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_PRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOUTSOURCED_SALESPRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSALESPRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALPRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTOTALSALESPRICEPERUNIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPRICETOTAL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit FK_CALCULATIONTextEdit;
        private System.Windows.Forms.BindingSource calculationRuleBindingSource;
        private DevExpress.XtraEditors.TextEdit FK_ITEMTextEdit;
        private DevExpress.XtraEditors.TextEdit FK_OUTSOURCEDTextEdit;
        private DevExpress.XtraEditors.TextEdit FK_SUBCALCULATIONTextEdit;
        private DevExpress.XtraEditors.TextEdit FK_UNITTextEdit;
        private DevExpress.XtraEditors.TextEdit FK_WORKACTIVITYTextEdit;
        private DevExpress.XtraEditors.TextEdit FK_WORKFLOWSTATETextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_PRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_SALESPRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit OUTSOURCED_PRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit OUTSOURCED_SALESPRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit IdTextEdit;
        private DevExpress.XtraEditors.TextEdit SALESPRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit TOTALPRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit TOTALSALESPRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit USERCHANGEDTextEdit;
        private DevExpress.XtraEditors.TextEdit WORKACTIVITY_PRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit WORKACTIVITY_QUANTITYTOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit WORKACTIVITY_SALESPRICETOTALTextEdit;
        private DevExpress.XtraEditors.RichTextEdit DescriptionTextEdit;
        private DevExpress.XtraEditors.TextEdit CalculationTextEdit;
        private DevExpress.XtraEditors.TextEdit UnitTextEdit;
        private DevExpress.XtraEditors.TextEdit ItemTextEdit;
        private DevExpress.XtraEditors.TextEdit WorkActivityTextEdit;
        private DevExpress.XtraEditors.TextEdit OutsourcedActivityTextEdit;
        private DevExpress.XtraEditors.TextEdit SubCalculationTextEdit;
        private DevExpress.XtraEditors.TextEdit QUANTITYTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_PRICEPERKGTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_SALESPRICEPERKGTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_PRICEPERMTRTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_SALESPRICEPERMTRTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_LENGTHPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_WIDTHPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_PRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_SALESPRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit OUTSOURCED_PRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit OUTSOURCED_SALESPRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit WORKACTIVITY_QUANTITYTextEdit;
        private DevExpress.XtraEditors.TextEdit WORKACTIVITY_PRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit WORKACTIVITY_SALESPRICEPERUNITTextEdit;
        private DevExpress.XtraEditors.TextEdit PRICETOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_LENGTHTOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit ITEM_WIDTHTOTALTextEdit;
        private DevExpress.XtraEditors.TextEdit SEQUENCENUMBERTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_CALCULATION;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_ITEM;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_OUTSOURCED;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_UNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_SUBCALCULATION;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_WORKACTIVITY;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFK_WORKFLOWSTATE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUSERCHANGED;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalculation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubCalculation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQUANTITY;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSEQUENCENUMBER;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_PRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_SALESPRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForItem;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_PRICEPERKG;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_SALESPRICEPERKG;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_PRICEPERMTR;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_SALESPRICEPERMTR;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_LENGTHPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_WIDTHPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_PRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_SALESPRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_LENGTHTOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForITEM_WIDTHTOTAL;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWORKACTIVITY_PRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWORKACTIVITY_QUANTITYTOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWORKACTIVITY_SALESPRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkActivity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWORKACTIVITY_QUANTITY;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWORKACTIVITY_PRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWORKACTIVITY_SALESPRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSALESPRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTOTALPRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTOTALSALESPRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlGroup U;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOUTSOURCED_PRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOUTSOURCED_SALESPRICETOTAL;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOutsourcedActivity;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOUTSOURCED_PRICEPERUNIT;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOUTSOURCED_SALESPRICEPERUNIT;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}