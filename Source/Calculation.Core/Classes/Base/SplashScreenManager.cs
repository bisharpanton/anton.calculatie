﻿using DevExpress.XtraSplashScreen;

namespace Calculation.Core.Classes.Base
{
    public class CustomSplashScreenManager
    {
        private bool IsShown { get; set; }

        public void ShowWaitForm(string description)
        {
            if (!IsShown)
            {
                SplashScreenManager.ShowDefaultWaitForm(string.Empty, description);
            }

            IsShown = true;
        }

        public void CloseWaitForm()
        {
            if (IsShown)
            {
                SplashScreenManager.CloseForm();
            }

            IsShown = false;
        }
    }
}