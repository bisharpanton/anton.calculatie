namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum WarrantyCause : int
    {
        
        /// <summary>
        /// Failure
        /// </summary>
        [Description("Failure")]
        Failure = 1,
        
        /// <summary>
        /// Maintenance
        /// </summary>
        [Description("Maintenance")]
        Maintenance = 2,
        
        /// <summary>
        /// Sales
        /// </summary>
        [Description("Sales")]
        Sales = 3,
    }
}
