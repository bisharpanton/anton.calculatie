﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;

public class C_CALCULATIONRULE_Delete_Custom : IDeleteScript
{
    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        DeleteAllItemsFromRecordset("C_CALCULATIONLOG", string.Format("FK_CALCULATIONRULE = '{0}'", data["PK_C_CALCULATIONRULE"]));

        DeleteLinksToFlatCalculation(data);

        return true;
    }


    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        return true;
    }

    private void DeleteLinksToFlatCalculation(RowData data)
    {
        var rsRecords = GetRecordset("C_FLATCALCULATION", "", 
            $"FK_CALCULATIONRULE = {data["PK_C_CALCULATIONRULE"]}", "");

        if (rsRecords.RecordCount <= 0) return;
        
        rsRecords.UpdateWhenMoveRecord = false;
        rsRecords.MoveFirst();
        
        while (!rsRecords.EOF)
        {
            rsRecords.Delete();
            rsRecords.MoveNext();
        }

        rsRecords.MoveFirst();
        rsRecords.Update();
    }

    private void DeleteAllItemsFromRecordset(string tableName, string filter)
    {
        var rsRecords = GetRecordset(tableName, "", filter, "");
        if (rsRecords.RecordCount <= 0) return;
        rsRecords.UpdateWhenMoveRecord = false;
        rsRecords.MoveFirst();
        while (!rsRecords.EOF)
        {
            rsRecords.Delete();
            rsRecords.MoveNext();
        }

        rsRecords.MoveFirst();
        rsRecords.Update();
    }

    public ScriptRecordset GetRecordset(string tableName, string columns, string filter, string sort)
    {
        return new ScriptRecordset(ScriptRecordset.GetDataSet(tableName, columns, filter, sort));
    }
}
