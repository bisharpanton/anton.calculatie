﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using Calculation.Core;
using Calculation.Core.Interfaces;
using Calculation.Win.Communication;
using DevExpress.XtraSplashScreen;
using Ninject;

namespace Calculation.Win
{
    internal static class Program
    {
        private static IKernel Kernel { get; set; }

        [STAThread]
        private static void Main(string[] args)
        {
            if (CommunicationHelper.SendToActiveClients())
            {
                return;
            }

            if (Properties.Settings.Default.CallUpgrade)
            {
                try
                {
                    Properties.Settings.Default.Upgrade();
                    Properties.Settings.Default.CallUpgrade = false;
                    Properties.Settings.Default.Save();
                }
                catch (Exception ex)
                {
                    // ignore settings upgrade errors
                }
            }

            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            DevExpress.UserSkins.BonusSkins.Register();

            var defaultSkinName = Properties.Settings.Default.SkinName;

            if (!string.IsNullOrEmpty(defaultSkinName))
                DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = defaultSkinName;

            Kernel = new StandardKernel();
            Kernel.Load(new MainModule());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SplashScreenManager.ShowDefaultWaitForm("", "Verbinding maken met Ridder iQ");

            var _sessionFactory = Kernel.Get<ISessionFactory>();
            var loginForm = Kernel.Get<LoginForm>();

            var loginSucceeded = _sessionFactory.TryToLoginWithGivenCompanyAndUser(loginForm.GetPassword());

            SplashScreenManager.CloseForm();

            if (!loginSucceeded)
            {
                //Open Login Form indien het inloggen met bestaande settings niet lukt

                Application.Run(loginForm);

                if (loginForm.DialogResult != DialogResult.OK)
                    return;
            }

            loginForm.Dispose();

            var argHelper = Kernel.Get<IArgumentHelper>();
            if (argHelper.CalculationId <= 0)
            {
                using (var session = _sessionFactory.GetSession())
                {
                    var calcId = session.Sdk.OpenBrowseForm("C_CALCULATION", "PK_C_CALCULATION") as int? ?? 0;
                    if (calcId <= 0)
                    {
                        return;
                    }

                    argHelper.CalculationId = calcId;
                }
            }

            SplashScreenManager.ShowDefaultWaitForm("", "Ophalen calculatie info");
            var mainForm = Kernel.Get<MainForm>();
            SplashScreenManager.CloseForm();

            Application.Run(mainForm);
        }


        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject as Exception);
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            HandleException(e.Exception);
        }

        private static void HandleException(Exception ex)
        {
            using (var errorForm = new ErrorForm(ex))
            {
                errorForm.ShowDialog();
            }
        }
    }
}
