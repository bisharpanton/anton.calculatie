﻿namespace StartCalculation
{
    using System;
    using System.Collections.Generic;

    using Ridder.Common.ADO;
    using Ridder.Common.Script;

    public static class ScriptRecordsetExtensions
    {
        public static IEnumerable<ScriptRecordset> AsEnumerable(this ScriptRecordset recordset)
        {
            recordset.MoveFirst();
            while (!recordset.EOF)
            {
                yield return recordset;
                recordset.MoveNext();
            }
        }

        public static IEnumerable<ScriptRecordset> AsEnumerableReverse(this ScriptRecordset recordset)
        {
            recordset.MoveLast();
            while (!recordset.BOF)
            {
                yield return recordset;
                recordset.MovePrevious();
            }
        }

        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int size)
        {
            IList<TSource> items = source as IList<TSource> ?? new List<TSource>(source);
            for (int i = 0; i < items.Count; i = i + size)
            {
                if (items.Count < i + size)
                {
                    size = items.Count - i;
                }
                TSource[] val = new TSource[size];

                for (int j = i, index = 0; j < i + size; j++)
                {
                    val[index++] = items[j];
                }
                yield return val;
            }
        }

        public static T GetValue<T>(this ScriptRecordset recordset, string column, T defaultValue = default(T))
        {
            var val = recordset.Fields[column].Value != DBNull.Value ? (T)recordset.Fields[column].Value : defaultValue;
            return val;
        }

        public static T GetValue<T>(this RowData rowData, string column, T defaultValue = default(T))
        {
            var val = rowData[column] != DBNull.Value ? (T)rowData[column] : defaultValue;
            return val;
        }
    }
}