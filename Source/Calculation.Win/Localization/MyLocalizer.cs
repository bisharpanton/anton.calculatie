﻿namespace Calculation.Win.Localization
{
    public class NullTextControlLocalizer : DevExpress.XtraEditors.Controls.Localizer
    {
        public override string GetLocalizedString(DevExpress.XtraEditors.Controls.StringId id)
        {
            if (id == DevExpress.XtraEditors.Controls.StringId.LookUpEditValueIsNull)
                return "";
            return base.GetLocalizedString(id);
        }
    }
}