﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calculation.Core.Interfaces;
using Calculation.Win.CredentialManagement;
using Calculation.Win.Properties;
using Ridder.Client.SDK;
using Ridder.Client.SDK.Extensions;
using Ridder.Common;
using ProgressBarStyle = System.Windows.Forms.ProgressBarStyle;

namespace Calculation.Win
{
    public partial class LoginForm : Form
    {
        private readonly ISessionFactory _sessionFactory;

        private readonly BusyHelper _busy;
        private readonly BindingList<LoginCompany> _companies = new BindingList<LoginCompany>();
        private readonly BindingList<LoginCompanyUser> _users = new BindingList<LoginCompanyUser>();
        private const string CredentialManagerTarget = "RidderCalculationTool";

        private readonly bool _userNameFromIq;
        private readonly bool _companyNameFromIq;

        public LoginForm(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;

            AskIQClientPath = false;

            var oldCursor = Cursor;
            _busy = BusyHelper.Create(
                this,
                f => {
                    btnLogin.Visible = false;
                    users.Enabled = false;
                    administrations.Enabled = false;
                    txtPassword.Enabled = false;

                    progressBar1.Style = ProgressBarStyle.Marquee;
                    progressBar1.MarqueeAnimationSpeed = 100;
                    progressBar1.Visible = true;

                    oldCursor = Cursor;
                    Application.UseWaitCursor = true;
                },
                f => {
                    users.Enabled = !_userNameFromIq;
                    administrations.Enabled = !_companyNameFromIq;
                    txtPassword.Enabled = true;
                    btnLogin.Visible = true;

                    progressBar1.MarqueeAnimationSpeed = 0;
                    progressBar1.Style = ProgressBarStyle.Blocks;
                    progressBar1.Value = progressBar1.Minimum;
                    progressBar1.Visible = false;

                    Application.UseWaitCursor = false;
                    Cursor = oldCursor;
                });

            IQClientPath = Settings.Default.SDK_ClientPath;
            CompanyName = Settings.Default.SDK_CompanyName;
            Username = Settings.Default.SDK_UserName;

            InitializeComponent();

            // init bindings
            administrations.ValueMember = "CompanyName";
            administrations.DisplayMember = "CompanyName";
            administrations.DataSource = _companies;

            users.ValueMember = "UserName";
            users.DisplayMember = "UserName";
            users.DataSource = _users;

            if (!string.IsNullOrEmpty(_sessionFactory.UserName))
            {
                Username = _sessionFactory.UserName;
                _userNameFromIq = true;
            }
            if (!string.IsNullOrEmpty(_sessionFactory.CompanyName))
            {
                CompanyName = _sessionFactory.CompanyName;
                _companyNameFromIq = true;
            }
            Password = GetPassword();
        }

        public bool AskIQClientPath { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public new string CompanyName { get; set; }

        public SdkSession Sdk { get; set; }

        public string IQClientPath { get; set; }

        protected override async void OnLoad(EventArgs e)
        {
            users.Text = Username;
            administrations.Text = CompanyName;
            txtPassword.Text = Password;

            if (AskIQClientPath && (string.IsNullOrEmpty(IQClientPath) || !Directory.Exists(IQClientPath)))
            {
                ChooseRidderIQClientPath();
            }

            using (_busy.Busy())
            {
                await LoadAdministrations();
            }

            if (!string.IsNullOrEmpty(Username))
            {
                users.SelectedValue = Username;
                if (!string.IsNullOrEmpty(Password))
                {
                    this.txtPassword.Text = Password;
                    btnLogin.Select();
                }
                else
                {
                    txtPassword.Select();
                }
            }
            else
            {
                users.Select();
            }

            base.OnLoad(e);
        }

        private void administrations_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUsers(_companies.FirstOrDefault(x => Equals(x.CompanyName, administrations.SelectedValue)));
            users.Select();

            UpdateSettings();
        }

        private bool ChooseRidderIQClientPath()
        {
            using (var sf = new OpenFileDialog())
            {
                if (!string.IsNullOrEmpty(IQClientPath)
                    && Directory.Exists(IQClientPath))
                {
                    sf.InitialDirectory = IQClientPath;
                }

                sf.CheckFileExists = true;
                sf.FileName = "Client.exe";
                sf.Filter = "Ridder IQ Client|Client.exe";
                sf.CheckPathExists = true;

                if (sf.ShowDialog() == DialogResult.OK)
                {
                    IQClientPath = Path.GetDirectoryName(sf.FileName);
                    UpdateSettings();
                    return true;
                }
            }

            return false;
        }

        private Task<LoginCompany[]> GetAdministrationsAsync()
        {
            return Task.Run(
                () => {

                    //Ophalen bestaande SDK sessie mislukt, initialiseer een nieuwe SDK sessie waarbij niet ingelogd wordt, maar waarop we wel de administraties kunnen opvragen.
                    var companies = new LoginCompany[] { };
                    var sdkSmall = new RidderIQSDK();

                    if (!string.IsNullOrEmpty(IQClientPath))
                    {
                        sdkSmall.SetRidderIQClientPath(IQClientPath);
                    }

                    companies = sdkSmall.GetAdministrations();

                    return companies;
                });
        }

        private async Task LoadAdministrations()
        {
            try
            {
                var companies = await GetAdministrationsAsync();
                LoadAdministrations(companies);
            }
            catch (SdkException ex)
            {
                MessageBox.Show(
                    ex.Result.GetResult(),
                    "SDK Fout",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Onbekende fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadAdministrations(LoginCompany[] companies)
        {
            if (companies.Length == 0)
            {
                return;
            }

            _companies.RaiseListChangedEvents = false;
            _companies.Clear();

            foreach (var company in companies)
            {
                _companies.Add(company);
            }

            _companies.RaiseListChangedEvents = true;
            _companies.ResetBindings();

            if (!string.IsNullOrEmpty(CompanyName)
                && _companies.Any(x => x.CompanyName == CompanyName))
            {
                administrations.SelectedValue = CompanyName;
                LoadUsers(companies.FirstOrDefault(x => Equals(x.CompanyName, administrations.SelectedValue)));
            }
            else if (companies.Length > 0)
            {
                administrations.SelectedIndex = 0;
                LoadUsers(companies.FirstOrDefault(x => Equals(x.CompanyName, administrations.SelectedValue)));
            }
        }

        private void LoadUsers(LoginCompany company)
        {
            if (company == null)
            {
                _users.Clear();
                return;
            }

            _users.RaiseListChangedEvents = false;
            _users.Clear();
            foreach (var user in company.Users)
            {
                _users.Add(user);
            }

            _users.RaiseListChangedEvents = true;
            _users.ResetBindings();

            if (!string.IsNullOrEmpty(Username)
                && _users.Any(x => Username.Equals(x.UserName, StringComparison.OrdinalIgnoreCase)))
            {
                users.SelectedValue = Username;
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            var userName = users.Text;
            var password = this.txtPassword.Text;
            var companyName = administrations.Text;

            using (_busy.Busy())
            {
                try
                {
                    var loginResult = await Task.Run(() => _sessionFactory.Login(userName, companyName, password)).ConfigureAwait(true);
                    if (!loginResult.Success)
                    {
                        MessageBox.Show(
                            this,
                            loginResult.ErrorMessage,
                            "Ridder Calculatie Tool",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Ridder Calculatie Tool", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            Enabled = false;
            try
            {
                UpdateSettings();
                SavePassword();
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Enabled = true;
                Close();
            }
        }

        private void UpdateSettings()
        {
            // save settings
            Username = users.Text;
            CompanyName = administrations.Text;
            Password = txtPassword.Text;

            Settings.Default.SDK_ClientPath = IQClientPath;
            Settings.Default.SDK_CompanyName = CompanyName;
            Settings.Default.SDK_UserName = Username;
            Settings.Default.Save();
        }

        private void SavePassword()
        {
            var networkCredentials = new NetworkCredential(Username, Password);
            CredentialManager.SaveCredentials($"{CredentialManagerTarget}{Username}", networkCredentials);
        }

        public string GetPassword()
        {
            var credentials = CredentialManager.GetCredentials($"{CredentialManagerTarget}{Username}");
            if (credentials != null)
            {
                return credentials.Password;
            }
            return "";
        }

        private void users_SelectedIndexChanged(object sender, EventArgs e)
        {
            Username = users.Text;
            Password = GetPassword();
            txtPassword.Text = Password;
            txtPassword.SelectAll();
            txtPassword.Select();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (ChooseRidderIQClientPath())
            {
                using (_busy.Busy())
                {
                    await LoadAdministrations();
                }
            }
        }

        private void administrations_Leave(object sender, EventArgs e)
        {
            UpdateSettings();
        }

        private void users_Leave(object sender, EventArgs e)
        {
            UpdateSettings();
        }
    }
}
