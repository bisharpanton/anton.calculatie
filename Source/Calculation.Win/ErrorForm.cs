﻿using System;

namespace Calculation.Win
{
    public partial class ErrorForm : DevExpress.XtraEditors.XtraForm
    {
        private Exception Exception { get; set; }

        public ErrorForm(Exception ex)
        {
            Exception = ex;
            InitializeComponent();

            edtDateTime.EditValue = DateTime.Now;
            edtMessage.EditValue = ex.Message;
            edtStackTrace.EditValue = ex.StackTrace;
        }
    }
}