﻿using System;
using System.Diagnostics;
using System.Linq;
using Calculation.Core;

namespace Calculation.Win.Communication
{
    internal static class CommunicationHelper
    {
        private const string CommunicationPropertyName = "anton.calculation";
        private const int CommunicationPropertyValue = 1337;

        public static void SetupCommunication(IntPtr windowHandle)
        {
            Win32.SetProp(windowHandle, CommunicationPropertyName, CommunicationPropertyValue);
        }

        public static bool SendToActiveClients()
        {
            var argumentHelper = new ArgumentHelper();

            if (argumentHelper.CalculationId <= 0)
            {
                return false;
            }

            var processPointers = GetProcessPointers();

            if (!processPointers.Any())
            {
                return false;
            }

            var data = $"{argumentHelper.CompanyName}|{argumentHelper.CalculationId}";

            var bytes = System.Text.Encoding.Default.GetBytes(data);

            var cds = new Win32.COPYDATASTRUCT
            {
                dwData = new IntPtr(58425L),
                cbData = bytes.Length + 1,
                lpData = data
            };

            foreach (var processPointer in processPointers)
            {
                var sendMessage = Win32.SendMessage(processPointer, Win32.WM_COPYDATA, 0, ref cds);
                if (sendMessage == 1)
                {
                    return true;
                }
            }

            return false;
        }

        private static int[] GetProcessPointers()
        {
            var result = Process.GetProcesses()
                .OrderBy(x => x.Id)
                .Select(x =>
                {
                    try
                    {
                        return x.MainWindowHandle;
                    }
                    catch
                    {
                        return IntPtr.Zero;
                    }
                })
                .Where(x => x != IntPtr.Zero && Win32.GetProp(x, CommunicationPropertyName) == CommunicationPropertyValue)
                .Select(x => x.ToInt32())
                .ToArray();

            return result;
        }
    }
}
