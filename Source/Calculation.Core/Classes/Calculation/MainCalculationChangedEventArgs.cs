using System;

namespace Calculation.Core.Classes.Calculation
{
    public class MainCalculationChangedEventArgs : EventArgs
    {
        public Models.Calculation Calculation { get; }

        public MainCalculationChangedEventArgs(Models.Calculation calculation)
        {
            Calculation = calculation;
        }
    }
}