﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class OfferWorkActivity
    {
        [ForeignKey("FK_OFFER")]
        public Offer Offer{ get; set; }
    }
}