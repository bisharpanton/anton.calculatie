//  <auto-generated />
namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    
    
    ///  <summary>
    ///  	 Table: R_OFFERDETAILITEM
    ///  </summary>
    ///  	ALTERNATIVE, CAUSE, CONVERTEDTOORDER, COSTPRICE, CREATOR, DATECHANGED, DATECREATED, DEALERFEE, DELIVERYDATE, DELIVERYPERIOD
    ///  	DESCRIPTION, DISCOUNTAMOUNT, DISCOUNTPERCENTAGE, EXTERNALKEY, EXTERNALSALESCODE, FK_COSTCENTER, FK_COSTUNIT, FK_DELIVERYADDRESS, FK_DELIVERYRELATION, FK_DETAILGROUP
    ///  	FK_GENERALLEDGERACCOUNT, FK_ITEM, FK_ITEMWAREHOUSE, FK_OFFER, FK_REVENUE, FK_SALESUNIT, FK_SETUPMITRELEFT, FK_SETUPMITRERIGHT, FK_SHIPPINGTYPE, FK_STATISTICCODE
    ///  	FK_SUPPLIER, FK_VATCODE, FK_WAREHOUSELOCATION, FK_WORKFLOWSTATE, GROSSSALESPRICE, LENGTH, LINENUMBER, MEMO, MITREANGLELEFT, MITREANGLERIGHT
    ///  	NETSALESAMOUNT, PK_R_OFFERDETAILITEM, PLAINTEXT_MEMO, PURCHASESPECIFICSIZE, QUANTITY, RECORDLINK, REGISTRATIONPATH, SAWINGCODE, SCALENUMBER, SENDDATE
    ///  	SHIPPINGPLACE, SURFACEAREA, THICKNESS, TOTALAMOUNT, USERCHANGED, VATAMOUNT, WARRANTY, WEIGHT, WIDTH
    ///  
    ///  	Filter used: FK_TABLEINFO = '8a0ca4d1-9669-4a96-918f-4f75e13f8807' AND (ISSECRET = 0)
    ///  	Included columns: FK_OFFER, FK_ITEM, GROSSSALESPRICE, QUANTITY, FK_ITEMWAREHOUSE
    ///  	Excluded columns: 
    ///  </remarks>
    [Table("R_OFFERDETAILITEM")]
    public partial class OfferItem
    {
        
        private int _FK_ITEM;
        
        private int _FK_ITEMWAREHOUSE;
        
        private int _FK_OFFER;
        
        private System.Nullable<double> _GROSSSALESPRICE;
        
        private int _id;
        
        private double _QUANTITY;
        
        /// <summary>
        /// 	Item
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OFFERDETAILITEM
        /// </remarks>
        [Column("FK_ITEM")]
        public virtual int FK_ITEM
        {
            get
            {
                return this._FK_ITEM;
            }
            set
            {
                this._FK_ITEM = value;
            }
        }
        
        /// <summary>
        /// 	Item Warehouse
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OFFERDETAILITEM
        /// </remarks>
        [Column("FK_ITEMWAREHOUSE")]
        public virtual int FK_ITEMWAREHOUSE
        {
            get
            {
                return this._FK_ITEMWAREHOUSE;
            }
            set
            {
                this._FK_ITEMWAREHOUSE = value;
            }
        }
        
        /// <summary>
        /// 	Offer
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// 	Table: R_OFFERDETAILITEM
        /// </remarks>
        [Column("FK_OFFER")]
        public virtual int FK_OFFER
        {
            get
            {
                return this._FK_OFFER;
            }
            set
            {
                this._FK_OFFER = value;
            }
        }
        
        /// <summary>
        /// 	Gross sales price
        /// </summary>
        /// <remarks>
        /// 	DataType: Money
        /// 	Size: 8
        /// </remarks>
        [Column("GROSSSALESPRICE")]
        public virtual System.Nullable<double> GROSSSALESPRICE
        {
            get
            {
                return this._GROSSSALESPRICE;
            }
            set
            {
                this._GROSSSALESPRICE = value;
            }
        }
        
        /// <summary>
        /// 	Offer detail items id
        /// </summary>
        /// <remarks>
        /// 	DataType: Integer
        /// 	Size: 4
        /// </remarks>
        [PrimaryKey("PK_R_OFFERDETAILITEM")]
        public virtual int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
        
        /// <summary>
        /// 	Quantity
        /// </summary>
        /// <remarks>
        /// 	DataType: Float
        /// 	Size: 8
        /// </remarks>
        [Column("QUANTITY")]
        public virtual double QUANTITY
        {
            get
            {
                return this._QUANTITY;
            }
            set
            {
                this._QUANTITY = value;
            }
        }
    }
}
