﻿namespace Calculation.Core.Interfaces
{
    public interface IMoveable
    {
        int SEQUENCENUMBER { get; set; }
        void MoveUp();
        void MoveDown();
    }
}