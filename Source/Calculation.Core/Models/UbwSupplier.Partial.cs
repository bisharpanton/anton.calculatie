﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class UbwSupplier
    {
        [ForeignKey("FK_SUPPLIER")]
        [Include]
        public Relation Supplier { get; set; }

        [Ignore]
        public string RelationName => Supplier?.NAME;
    }
}
