﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Windows.Forms;
using Calculation.Core.Classes.Base;
using Calculation.Core.Interfaces;
using Calculation.Core.Models;

namespace Calculation.Core.Classes.Calculation
{
    public class CalculationFactory : ICalculationFactory
    {
        private IEnumerable<object> _clonedObjects;
        private readonly List<CalculationRule> _objectsInvalid = new List<CalculationRule>();
        private readonly ISessionFactory _sessionFactory;
        private readonly DatabaseHelper _databaseHelper;

        private int LockedCalculationId { get; set; }
        public Models.Calculation MainCalculation { get; set; }
        public BindingList<Models.Calculation> DataSourceCalculations { get; set; } = new BindingList<Models.Calculation>();
        public BindingList<CalculationRule> DataSource { get; set; } = new BindingList<CalculationRule>();
        public BindingList<CalculationTailRule> DataSourceTailRules { get; set; } = new BindingList<CalculationTailRule>();

        public CalculationFactory(ISessionFactory sessionFactory, DatabaseHelper databaseHelper)
        {
            _sessionFactory = sessionFactory;
            _databaseHelper = databaseHelper;

            DatabaseHelper.ObjectAdded += (sender, args) =>
            {
                if (args.Object is Models.Calculation)
                {
                    var calculation = (Models.Calculation)args.Object;
                    calculation.MainCalculation = MainCalculation;
                    calculation.MainCalculation.Calculations.Add(calculation);
                    calculation.FK_MAINCALCULATION = MainCalculation.Id;
                    calculation.Totals = DatabaseHelper.Create<CalculationTotals>();
                    calculation.Totals.Calculation = calculation;
                    CalculateHelper.Recalculate(calculation);
                    CalculateHelper.Recalculate(MainCalculation);
                    //DataSourceCalculations.Add(calculation);
                }

                if (args.Object is CalculationRule)
                {
                    var calculationRule = (CalculationRule)args.Object;
                    calculationRule.DisableDataChanges = true;

                    if (calculationRule.Calculation == null)
                    {
                        calculationRule.Calculation = MainCalculation;
                        calculationRule.FK_CALCULATION = MainCalculation.Id;
                        calculationRule.SEQUENCENUMBER = MainCalculation.CalculationRules.Count > 0 ? (MainCalculation.CalculationRules.Max(x => x.SEQUENCENUMBER) + 1) : 0;
                    }

                    calculationRule.DisableDataChanges = false;

                    if (calculationRule.SubCalculation != null)
                    {
                        DataSourceCalculations.Add(calculationRule.SubCalculation);
                    }

                    if(!calculationRule.Calculation.CalculationRules.Contains(calculationRule))
                        calculationRule.Calculation.CalculationRules.Add(calculationRule);

                    if(calculationRule.Calculation == MainCalculation && !DataSource.Contains(calculationRule))
                        DataSource.Add(calculationRule);

                    CalculateHelper.Recalculate(MainCalculation);
                    CalculateHelper.UpdatePercentageTailRules(MainCalculation);
                }

                if (args.Object is CalculationTotals)
                {
                    var calculationTotals = (CalculationTotals)args.Object;
                    calculationTotals.Calculation = MainCalculation;
                }

                if (args.Object is CalculationTailRule)
                {
                    var calculationTailRule = (CalculationTailRule)args.Object;
                    calculationTailRule.Calculation = MainCalculation;
                    calculationTailRule.CalculationId = MainCalculation.Id;
                    calculationTailRule.SequenceNumber = MainCalculation.CalculationTailRules.Count > 0 ? (MainCalculation.CalculationTailRules.Max(x => x.SequenceNumber) + 1) : 0;
                    MainCalculation.CalculationTailRules.Add(calculationTailRule);

                    if (!DataSourceTailRules.Contains(calculationTailRule))
                        DataSourceTailRules.Add(calculationTailRule);

                    CalculateHelper.Recalculate(MainCalculation);
                }
            };

            DatabaseHelper.ObjectChanged += (sender, e) =>
            {
                if (!(e.DatabaseDatabaseObject is CalculationTotals))
                {
                    CalculateHelper.Recalculate(MainCalculation);
                }

                if (e.DatabaseDatabaseObject is CalculationRule)
                {
                    CalculateHelper.UpdatePercentageTailRules(MainCalculation);
                }
            };

            //var changedEventObs = Observable.FromEventPattern<ExtendedObjectChangedEventArgs>(ev => DatabaseHelper.ObjectChanged += ev, ev => DatabaseHelper.ObjectChanged -= ev);
            //changedEventObs.Throttle(TimeSpan.FromSeconds(.5)).Subscribe(e =>
            //{
            //    if(!(e.EventArgs.DatabaseDatabaseObject is CalculationTotals))
            //        CalculateHelper.Recalculate(MainCalculation);
            //});
            var deletedEventObs = Observable.FromEventPattern<ObjectChangedEventArgs>(ev => DatabaseHelper.ObjectDeleted += ev, ev => DatabaseHelper.ObjectDeleted -= ev);
            deletedEventObs.Throttle(TimeSpan.FromSeconds(.5)).Subscribe(e =>
            {
                if(!e.EventArgs.Object.Equals(MainCalculation))
                    CalculateHelper.Recalculate(MainCalculation);
            });

            DatabaseHelper.ObjectDeleted += (sender, args) =>
            {

                if (args.Object is Models.Calculation)
                {
                    var calculation = (Models.Calculation)args.Object;
                    /*
                    DatabaseHelper.Remove(calculation.Totals);

                    foreach (var calculationTailRule in calculation.CalculationTailRules)
                        DatabaseHelper.Remove(calculationTailRule);

                    foreach (var subCalculation in calculation.Calculations)
                        DatabaseHelper.Remove(subCalculation);
                    */
                    var oldMain = MainCalculation;
                    SetMainCalculation(calculation.MainCalculation);
                    DatabaseHelper.Remove(DataSource.First(x => x.FK_SUBCALCULATION.HasValue && x.FK_SUBCALCULATION == calculation.Id && x.FK_CALCULATION == calculation.FK_MAINCALCULATION));
                    SetMainCalculation(oldMain);
                }

                if (args.Object is CalculationRule)
                {
                    var calculationRule = (CalculationRule)args.Object;

                    if (MainCalculation.CalculationRules.Contains(calculationRule))
                        MainCalculation.CalculationRules.Remove(calculationRule);

                    if (calculationRule.Calculation.CalculationRules.Contains(calculationRule))
                        calculationRule.Calculation.CalculationRules.Remove(calculationRule);

                    if (DataSource.Contains(calculationRule))
                        DataSource.Remove(calculationRule);

                    if (calculationRule.SubCalculation != null)
                    {
                        foreach (var subCalculationCalculationRule in calculationRule.SubCalculation.CalculationRules)
                        {
                            DataSourceCalculations.Remove(subCalculationCalculationRule.SubCalculation);
                        }
                        DataSourceCalculations.Remove(calculationRule.SubCalculation);
                    }

                    CalculateHelper.Recalculate(MainCalculation);
                    CalculateHelper.UpdatePercentageTailRules(MainCalculation);
                }

                if (args.Object is CalculationTailRule)
                {
                    var calculationTailRule = (CalculationTailRule)args.Object;

                    if (MainCalculation.CalculationTailRules.Contains(calculationTailRule))
                        MainCalculation.CalculationTailRules.Remove(calculationTailRule);

                    if (calculationTailRule.Calculation.CalculationTailRules.Contains(calculationTailRule))
                        calculationTailRule.Calculation.CalculationTailRules.Remove(calculationTailRule);

                    if (DataSourceTailRules.Contains(calculationTailRule))
                        DataSourceTailRules.Remove(calculationTailRule);

                    CalculateHelper.Recalculate(MainCalculation);
                }
            };
        }

        public void LockCalculation(int calculationId)
        {
            using (var session = _sessionFactory.GetSession())
            {
                var calculation = session.Get<Models.Calculation>(calculationId);
                calculation.LOCKED = true;
                calculation.LOCKEDAT = DateTime.Now;
                calculation.LOCKEDBY = session.Sdk.GetUserInfo().CurrentUserName;
                session.Update(calculation);
                LockedCalculationId = calculationId;
            }
        }

        public void UnLockCalculation()
        {
            if (LockedCalculationId <= 0)
                return;

            using (var session = _sessionFactory.GetSession())
            {
                var calculation = session.Get<Models.Calculation>(LockedCalculationId);
                calculation.LOCKED = false;
                calculation.LOCKEDAT = null;
                calculation.LOCKEDBY = null;
                session.Update(calculation);
            }
        }

        private bool settingMainCalculation = false;

        public void SetMainCalculation(Models.Calculation newMainCalculation)
        {
            if (settingMainCalculation || Equals(newMainCalculation, MainCalculation))
                return;

            try
            {
                settingMainCalculation = true;

                if (MainCalculation == null)
                {
                    DataSourceCalculations.Clear();
                    DataSourceCalculations.Add(newMainCalculation);

                    foreach (var item in newMainCalculation.GetSubCalculations())
                        DataSourceCalculations.Add(item);
                }

                MainCalculation = newMainCalculation;

                DataSource.Clear();

                foreach (var item in newMainCalculation.CalculationRules)
                    DataSource.Add(item);

                DataSourceTailRules.Clear();

                foreach (var item in newMainCalculation.CalculationTailRules)
                    DataSourceTailRules.Add(item);

                DoMainCalculationChanged(newMainCalculation);

            }
            finally
            {
                settingMainCalculation = false;
            }
        }

        public event EventHandler<MainCalculationChangedEventArgs> MainCalculationChanged;

        public void DoMainCalculationChanged(Models.Calculation calculation)
        {
            MainCalculationChanged?.Invoke(this, new MainCalculationChangedEventArgs(calculation));
        }

        public void CancelObjectAdded(IDatabaseObject addedObject)
        {
            DatabaseHelper.Remove(addedObject);

            if (MainCalculation.CalculationRules.Contains(addedObject))
                MainCalculation.CalculationRules.Remove((CalculationRule)addedObject);
        }

        public bool HasChangedObjects()
        {
            return DatabaseHelper.DatabaseObjectStates.Any(x => x.State != DatabaseObjectStateEnum.Unmodified);
        }

        public void CloneObjects(IEnumerable<object> objectsToClone)
        {
            MainCalculation.DisableDataChanges = true;
            _clonedObjects = CloneHelper.CloneObjects(objectsToClone);
            MainCalculation.DisableDataChanges = false;
        }

        public Type GetCloneObjectType()
        {
            var first = _clonedObjects.First();
            return first.GetType();
        }

        public bool HasClonedObjects()
        {
            return _clonedObjects?.Any() ?? false;
        }

        public void InsertClonedObjects()
        {
            foreach (var clonedObject in _clonedObjects)
            {
                // creating a new clone else it will be the same in memory when pasting multiple times
                var newObject = CloneHelper.CloneObject(clonedObject);
                DatabaseHelper.Add(newObject as IDatabaseObject);
            }
        }

        public void ObjectMoveUp(IMoveable ruleToMoveUp)
        {
            var ruleAbove = DataSource.Cast<IMoveable>().OrderByDescending(x => x.SEQUENCENUMBER).FirstOrDefault(x => x.SEQUENCENUMBER < ruleToMoveUp.SEQUENCENUMBER);
            ruleAbove?.MoveDown();
            ruleToMoveUp.MoveUp();
        }

        public void ObjectMoveDown(IMoveable ruleToMoveDown)
        {
            var ruleBelow = DataSource.Cast<IMoveable>().OrderBy(x => x.SEQUENCENUMBER).FirstOrDefault(x => x.SEQUENCENUMBER > ruleToMoveDown.SEQUENCENUMBER);
            ruleBelow?.MoveUp();
            ruleToMoveDown.MoveDown();
        }

        public void ReorderDatasource()
        {
            var newSequenceNumber = 1;
            foreach (var calculationRule in DataSource)
            {
                calculationRule.SEQUENCENUMBER = newSequenceNumber++;
            }
        }

        public void DeleteObjects<T>(List<T> rulesToDelete) where T : IDatabaseObject
        {
            foreach (var rule in rulesToDelete)
            {
                DatabaseHelper.Remove(rule as IDatabaseObject);
                (rule as IDisposable)?.Dispose();
            }
        }

        public void DeleteObjects(List<Models.Calculation> rulesToDelete)
        {
        }

        public void ObjectInvalid(CalculationRule invalidRule)
        {
            if (!_objectsInvalid.Contains(invalidRule))
                _objectsInvalid.Add(invalidRule);
        }

        public void ObjectValid(CalculationRule validRule)
        {
            if (_objectsInvalid.Contains(validRule))
                _objectsInvalid.Remove(validRule);
        }

        public bool HasInvalidObjects()
        {
            return _objectsInvalid.Count > 0;
        }

        public Models.Calculation GetMainCalculation()
        {
            return _databaseHelper.Calculations.First(x => x.FK_MAINCALCULATION == null);
        }

        public void ConvertCalculation(int mainCalculationId)
        {
            var session = _sessionFactory.GetSession();
            session.Sdk.ExecuteWorkflowEvent("C_CALCULATION", mainCalculationId, new Guid("33512f6c-873b-49c5-b4db-bb046f5acd4d"));
        }
    }
}
