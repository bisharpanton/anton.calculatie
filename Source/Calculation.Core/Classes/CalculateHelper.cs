﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calculation.Core.Models;

namespace Calculation.Core.Classes
{
    public static class CalculateHelper
    {
        public static CalculationTotals Recalculate(Models.Calculation calculation)
        {
            var calculationRules = calculation.CalculationRules.ToArray();

            if (calculation.Totals == null)
                calculation.Totals = DatabaseHelper.Create<CalculationTotals>();

            var result = calculation.Totals;
            var nonSubCalculationRules = calculationRules.Where(x => x.SubCalculation == null).ToArray();
            var subCalculationRules = calculationRules.Where(x => x.SubCalculation != null).ToArray();

            foreach (var subCalculationRule in subCalculationRules)
            {
                subCalculationRule.Recalculate(string.Empty);
            }

            result.TotalCostPriceItems = nonSubCalculationRules.Sum(x => x.ITEM_PRICETOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalCostPriceItems);
            result.TotalCostPriceOutsourced = nonSubCalculationRules.Sum(x => x.OUTSOURCED_PRICETOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalCostPriceOutsourced);
            result.TotalCostPriceWorkActivity = nonSubCalculationRules.Sum(x => x.WORKACTIVITY_PRICETOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalCostPriceWorkActivity);

            result.TotalSalesPriceItems = nonSubCalculationRules.Sum(x => x.ITEM_SALESPRICETOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalSalesPriceItems);
            result.TotalSalesPriceOutsourced = nonSubCalculationRules.Sum(x => x.OUTSOURCED_SALESPRICETOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalSalesPriceOutsourced);
            result.TotalSalesPriceWorkActivity = nonSubCalculationRules.Sum(x => x.WORKACTIVITY_SALESPRICETOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalSalesPriceWorkActivity);

            result.TotalDifferenceItems = result.TotalSalesPriceItems - result.TotalCostPriceItems;
            result.TotalDifferenceOutsourced = result.TotalSalesPriceOutsourced - result.TotalCostPriceOutsourced;
            result.TotalDifferenceWorkActivity = result.TotalSalesPriceWorkActivity - result.TotalCostPriceWorkActivity;

            var currentTailRuleTotal = RecalculateTailRulesSalesPrice(calculation, result);
            result.TotalSalesPriceTailRules = currentTailRuleTotal;

            result.TotalCostPrice = result.TotalCostPriceItems +
                                    result.TotalCostPriceOutsourced +
                                    result.TotalCostPriceWorkActivity +
                                    result.TotalCostPriceTailRules;
            result.TotalSalesPrice = result.TotalSalesPriceItems +
                                     result.TotalSalesPriceOutsourced +
                                     result.TotalSalesPriceWorkActivity +
                                     result.TotalSalesPriceTailRules;
            result.TotalDifference = result.TotalSalesPrice - result.TotalCostPrice;

            result.TotalCostPriceHours = nonSubCalculationRules.Sum(x => x.WORKACTIVITY_QUANTITYTOTAL ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TotalCostPriceHours);
            result.TotalSalesPriceHours = result.TotalCostPriceHours;

            var parentTotals = GetParentTotals(calculation);
            foreach (var parentTotal in parentTotals)
            {
                if (parentTotal.Calculation != null)
                {
                    Recalculate(parentTotal.Calculation);
                }
            }

            result.TOTALKGSTEEL = nonSubCalculationRules.Where(x => x.Item != null && x.Item.ProfileItem).Sum(x => x.ITEM_LENGTHTOTAL * x.Item.WEIGHT ?? 0);// + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALKGSTEEL);
            //result.TOTALKGSTEEL += nonSubCalculationRules.Where(x => x.Item != null && x.Item.SurfaceItem).Sum(x => x.ITEM_LENGTHTOTAL * x.ITEM_WIDTHTOTAL * x.Item.WEIGHT ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALKGSTEEL);
            result.TOTALKGSTEEL += nonSubCalculationRules.Where(x => x.Item != null && x.Item.SurfaceItem).Sum(x => x.ITEM_LENGTHPERUNIT * x.ITEM_WIDTHPERUNIT * x.QUANTITY * x.Item.WEIGHT ?? 0);// + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALKGSTEEL);
            result.TOTALKGSTEEL += nonSubCalculationRules.Where(x => x.Item != null && x.Item.MatItem).Sum(x => x.QUANTITY * x.Item.WEIGHT ?? 0);// + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALKGSTEEL);
            result.TOTALKGSTEEL += nonSubCalculationRules.Where(x => x.Item != null && x.Item.ConcreteSteelItem).Sum(x => x.QUANTITY);// + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALKGSTEEL);
            result.TOTALKGSTEEL += nonSubCalculationRules.Where(x => x.Item != null && x.Item.SupporterItem).Sum(x => x.QUANTITY * x.Item.WEIGHT ?? 0);
            result.TOTALKGSTEEL += subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALKGSTEEL);

            result.TOTALSURFACEAREA = nonSubCalculationRules.Where(x => x.Item != null && x.Item.ProfileItem).Sum(x => x.ITEM_LENGTHTOTAL * x.Item.PAINTAREA ?? 0);// + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALSURFACEAREA);
            //result.TOTALSURFACEAREA += nonSubCalculationRules.Where(x => x.Item != null && x.Item.SurfaceItem).Sum(x => x.ITEM_LENGTHTOTAL * x.ITEM_WIDTHTOTAL * x.Item.PAINTAREA ?? 0) + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALSURFACEAREA);
            result.TOTALSURFACEAREA += nonSubCalculationRules.Where(x => x.Item != null && x.Item.SurfaceItem).Sum(x => x.ITEM_LENGTHPERUNIT * x.ITEM_WIDTHPERUNIT * x.QUANTITY * x.Item.PAINTAREA ?? 0);// + subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALSURFACEAREA);
            result.TOTALSURFACEAREA += subCalculationRules.Sum(x => x.QUANTITY * x.SubCalculation.Totals.TOTALSURFACEAREA);

            result.TotalPricePerKg = result.TOTALKGSTEEL == 0 ? 0.0 : result.TotalCostPrice / result.TOTALKGSTEEL;
            result.TotalSalesPricePerKg = result.TOTALKGSTEEL == 0 ? 0.0 : result.TotalSalesPrice / result.TOTALKGSTEEL;
            result.TotalDifferencePerKg = result.TotalSalesPricePerKg - result.TotalPricePerKg;

            OnRecalculated(result);
            return result;
        }

        public static void UpdatePercentageTailRules(Models.Calculation calculation)
        {
            var totalSalesPrice = calculation.Totals.TotalSalesPrice;

            var tailRulesPercentage = calculation.CalculationTailRules.Where(x => x.Percentage != 0.0).ToList();

            foreach (var tailRule in tailRulesPercentage)
            {
                tailRule.Total = totalSalesPrice * (tailRule.Percentage / 100);
            }

            OnUpdateTailRules();
        }

        public static CalculationTotals RecalculateTailRulesCostPrice(Models.Calculation calculation, CalculationTotals totals)
        {
            totals.TotalCostPriceTailRules = 0;
            return totals;
        }

        public static double RecalculateTailRulesSalesPrice(Models.Calculation calculation, CalculationTotals totals)
        {
            // only top level calculations can have tail rules
            if (calculation.MainCalculation != null || calculation.FK_MAINCALCULATION != null)
                return 0.0;

            if (!calculation.CalculationTailRules.Any())
                return 0.0;

            var totalSalesPrice = totals.TotalSalesPriceItems +
                                  totals.TotalSalesPriceOutsourced +
                                  totals.TotalSalesPriceWorkActivity;

            var result = 0.0;
            foreach (var tailRule in calculation.CalculationTailRules.OrderBy(x => x.SequenceNumber))
            {
                tailRule.DisableDataChanges = true;
                var ruleResult = 0.0;

                if (tailRule.Amount != 0.0)
                {
                    ruleResult = tailRule.Amount;
                    result += tailRule.Amount;
                }

                if (tailRule.Percentage != 0.0)
                {
                    ruleResult = totalSalesPrice / 100 * tailRule.Percentage;
                    result += ruleResult;
                }

                tailRule.Total = ruleResult;
                tailRule.DisableDataChanges = false;
            }
            return result;
        }

        public static List<CalculationTotals> GetParentTotals(Models.Calculation calculation)
        {
            var result = new List<CalculationTotals>();

            if (calculation.MainCalculation != null)
            {
                result.AddRange(GetParentTotals(calculation.MainCalculation));
                result.Add(calculation.MainCalculation.Totals);
            }

            return result;
        }

        public static List<CalculationTotals> GetChildTotals(Models.Calculation calculation)
        {
            var result = new List<CalculationTotals>();

            foreach (var childCalculation in calculation.CalculationRules.Where(x=>x.SubCalculation != null).Select(x=>x.SubCalculation))
            {
                result.AddRange(GetChildTotals(childCalculation));
                result.Add(childCalculation.Totals);
            }

            return result;
        }

        public static event EventHandler<CalculateEventArgs> Recalculated;

        private static void OnRecalculated(CalculationTotals result)
        {
            Recalculated?.Invoke(null, new CalculateEventArgs(result));
        }

        public static event EventHandler<CalculateEventArgs> RefreshTailRules;

        private static void OnUpdateTailRules()
        {
            RefreshTailRules?.Invoke(null, null);
        }
    }

    public class CalculateEventArgs : EventArgs
    {
        public CalculationTotals Result { get; }

        public CalculateEventArgs(CalculationTotals result)
        {
            Result = result;
        }
    }
}