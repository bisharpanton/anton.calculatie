﻿using Calculation.Core.Classes;
using Calculation.Core.Classes.Base;
using Calculation.Core.Classes.Calculation;
using Calculation.Core.Interfaces;
using Ninject.Modules;

namespace Calculation.Core
{
    public class MainModule : NinjectModule
    {
        public override void Load()
        {
            Bind<CalculationFactory>().To<CalculationFactory>().InSingletonScope();
            Bind<ISessionFactory>().To<SessionFactory>().InSingletonScope();
            Bind<ReportFactory>().ToSelf().InSingletonScope().WithConstructorArgument("tableName", "C_CALCULATION");
            Bind<CustomSplashScreenManager>().ToSelf().InSingletonScope();
            Bind<DatabaseHelper>().ToSelf().InSingletonScope();
            Bind<IArgumentHelper>().To<ArgumentHelper>().InSingletonScope();
        }
    }
}