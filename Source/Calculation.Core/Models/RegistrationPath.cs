namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum RegistrationPath : int
    {
        
        /// <summary>
        /// Manual WIP entry
        /// </summary>
        [Description("Manual WIP entry")]
        Manual_WIP_entry = 1,
        
        /// <summary>
        /// Normative WIP entry
        /// </summary>
        [Description("Normative WIP entry")]
        Normative_WIP_entry = 2,
        
        /// <summary>
        /// Purchase when needed
        /// </summary>
        [Description("Purchase when needed")]
        Purchase_when_needed = 3,
    }
}
