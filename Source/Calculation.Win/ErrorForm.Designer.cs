﻿namespace Calculation.Win
{
    partial class ErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.edtMessage = new DevExpress.XtraEditors.TextEdit();
            this.edtDateTime = new DevExpress.XtraEditors.TextEdit();
            this.edtStackTrace = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciDateTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciMessage = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciStackTrace = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtMessage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtDateTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStackTrace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStackTrace)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.edtMessage);
            this.layoutControl1.Controls.Add(this.edtDateTime);
            this.layoutControl1.Controls.Add(this.edtStackTrace);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(846, 265, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(515, 313);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // edtMessage
            // 
            this.edtMessage.Location = new System.Drawing.Point(83, 36);
            this.edtMessage.Name = "edtMessage";
            this.edtMessage.Properties.ReadOnly = true;
            this.edtMessage.Size = new System.Drawing.Size(420, 20);
            this.edtMessage.StyleController = this.layoutControl1;
            this.edtMessage.TabIndex = 5;
            // 
            // edtDateTime
            // 
            this.edtDateTime.Location = new System.Drawing.Point(83, 12);
            this.edtDateTime.Name = "edtDateTime";
            this.edtDateTime.Properties.ReadOnly = true;
            this.edtDateTime.Size = new System.Drawing.Size(420, 20);
            this.edtDateTime.StyleController = this.layoutControl1;
            this.edtDateTime.TabIndex = 4;
            // 
            // edtStackTrace
            // 
            this.edtStackTrace.Location = new System.Drawing.Point(12, 76);
            this.edtStackTrace.Name = "edtStackTrace";
            this.edtStackTrace.Properties.ReadOnly = true;
            this.edtStackTrace.Size = new System.Drawing.Size(491, 225);
            this.edtStackTrace.StyleController = this.layoutControl1;
            this.edtStackTrace.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciDateTime,
            this.lciMessage,
            this.lciStackTrace});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(515, 313);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lciDateTime
            // 
            this.lciDateTime.Control = this.edtDateTime;
            this.lciDateTime.Location = new System.Drawing.Point(0, 0);
            this.lciDateTime.Name = "lciDateTime";
            this.lciDateTime.Size = new System.Drawing.Size(495, 24);
            this.lciDateTime.Text = "Datum en tijd:";
            this.lciDateTime.TextSize = new System.Drawing.Size(68, 13);
            // 
            // lciMessage
            // 
            this.lciMessage.Control = this.edtMessage;
            this.lciMessage.Location = new System.Drawing.Point(0, 24);
            this.lciMessage.Name = "lciMessage";
            this.lciMessage.Size = new System.Drawing.Size(495, 24);
            this.lciMessage.Text = "Bericht:";
            this.lciMessage.TextSize = new System.Drawing.Size(68, 13);
            // 
            // lciStackTrace
            // 
            this.lciStackTrace.Control = this.edtStackTrace;
            this.lciStackTrace.Location = new System.Drawing.Point(0, 48);
            this.lciStackTrace.Name = "lciStackTrace";
            this.lciStackTrace.Size = new System.Drawing.Size(495, 245);
            this.lciStackTrace.Text = "Stacktrace:";
            this.lciStackTrace.TextLocation = DevExpress.Utils.Locations.Top;
            this.lciStackTrace.TextSize = new System.Drawing.Size(68, 13);
            // 
            // ErrorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 313);
            this.Controls.Add(this.layoutControl1);
            this.Name = "ErrorForm";
            this.ShowIcon = false;
            this.Text = "Foutmelding";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.edtMessage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtDateTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStackTrace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciStackTrace)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit edtMessage;
        private DevExpress.XtraEditors.TextEdit edtDateTime;
        private DevExpress.XtraEditors.MemoEdit edtStackTrace;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem lciDateTime;
        private DevExpress.XtraLayout.LayoutControlItem lciMessage;
        private DevExpress.XtraLayout.LayoutControlItem lciStackTrace;
    }
}