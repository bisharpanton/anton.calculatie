namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum CalculationLogType : int
    {
        
        /// <summary>
        /// Info
        /// </summary>
        [Description("Info")]
        Info = 1,
        
        /// <summary>
        /// Warning
        /// </summary>
        [Description("Warning")]
        Warning = 2,
        
        /// <summary>
        /// Error
        /// </summary>
        [Description("Error")]
        Error = 3,
    }
}
