﻿using System.Collections.Generic;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class Offer
    {
        public Offer()
        {
            RulesItem = new List<OfferItem>();
            RulesOutsourced = new List<OfferOutSourced>();
            RulesWorkactivity = new List<OfferWorkActivity>();
        }

        [HasMany("FK_OFFER")]
        public List<OfferItem> RulesItem { get; protected set; }

        [HasMany("FK_OFFER")]
        public List<OfferOutSourced> RulesOutsourced { get; protected set; }

        [HasMany("FK_OFFER")]
        public List<OfferWorkActivity> RulesWorkactivity { get; protected set; }

    }
}