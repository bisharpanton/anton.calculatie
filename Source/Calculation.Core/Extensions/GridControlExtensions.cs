﻿using System.Collections.Generic;
using System.Linq;
using Calculation.Core.Classes.Base;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace Calculation.Core.Extensions
{
    public static class GridControlExtensions
    {
        public static GridView GetFocusedView(this GridControl gridControl)
        {
            return gridControl.FocusedView as GridView;
        }

        public static bool HasPreviousRecord(this GridControl gridControl)
        {
            var view = gridControl.GetFocusedView();
            var row = view.GetRow(view.FocusedRowHandle - 1);
            return row != null;
        }

        public static IDatabaseObject GetPreviousRecord(this GridControl gridControl)
        {
            var view = gridControl.GetFocusedView();
            var row = view.GetRow(view.FocusedRowHandle - 1);
            return (IDatabaseObject)row;
        }

        public static bool HasNextRecord(this GridControl gridControl)
        {
            var view = gridControl.GetFocusedView();
            var row = view.GetRow(view.FocusedRowHandle + 1);
            return row != null;
        }

        public static IDatabaseObject GetNextRecord(this GridControl gridControl)
        {
            var view = gridControl.GetFocusedView();
            var row = view.GetRow(view.FocusedRowHandle + 1);
            return (IDatabaseObject)row;
        }

        public static bool HasFocusedRecord(this GridControl gridControl)
        {
            var view = gridControl.GetFocusedView();
            var row = view.GetRow(view.FocusedRowHandle);
            return row != null;
        }

        public static IDatabaseObject GetFocusedRecord(this GridControl gridControl)
        {
            var view = gridControl.GetFocusedView();
            var row = view.GetRow(view.FocusedRowHandle);
            return row as IDatabaseObject;
        }


        public static IEnumerable<object> GetGridViewObjects(this GridView view)
        {
            if (view.SelectedRowsCount <= 0)
            {
                return new[]
                {
                    view.GridControl.GetFocusedRecord()
                };
            }

            var result = view.GridControl.GetFocusedView()
                .GetSelectedRows()
                .Select(rowHandle => view.GetRow(rowHandle) as IDatabaseObject)
                .Where(x => x != null)
                .ToArray();

            return result;
        }
    }
}