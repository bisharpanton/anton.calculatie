﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Data;
using Ridder.Common.WorkflowModel.Activities;
using System.Drawing.Printing;

public class RidderScript : WorkflowScriptInfo
{
    public void Execute()
    {
        //GMUNK
        //KOpieer de calculaties naar de nieuwe revisie

        var id = (int)RecordId;

        var rsOffer = GetRecordset("R_OFFER", "OFFERNUMBER", string.Format("PK_R_OFFER = {0}", id), "");
        rsOffer.MoveFirst();
        var offerNumber = rsOffer.Fields["OFFERNUMBER"].Value;

        var rsOfferRevision = GetRecordset("R_OFFER", "PK_R_OFFER", string.Format("OFFERNUMBER = {0}", offerNumber), "REVISION DESC");
        rsOfferRevision.MoveFirst();
        var newOfferId = (int)rsOfferRevision.Fields["PK_R_OFFER"].Value;

        var offerDetails = GetRecordset("R_OFFERDETAILASSEMBLY", "FK_ASSEMBLY", string.Format("FK_OFFER = {0}", id), "")
            .DataTable.AsEnumerable();

        var offerCalculations = GetRecordset("C_CALCULATION", "PK_C_CALCULATION, FK_ASSEMBLY, FK_OFFER, QUANTITY",
            string.Format("FK_OFFER = {0}", id), "").DataTable.AsEnumerable();

        foreach (var offerDetail in offerDetails)
        {
            // Calculatie van de huidige offerteregel 
            var calculation = offerCalculations.FirstOrDefault(x =>
                x.Field<int?>("FK_ASSEMBLY").HasValue && x.Field<int>("FK_ASSEMBLY") == offerDetail.Field<int>("FK_ASSEMBLY"));
            
            if (calculation == null)
            {
                throw new Exception(string.Format("Er is geen of meer dan 1 calculatie gevonden voor offerteregel '{0}'", 
                    offerDetail.Field<int>("PK_R_OFFERDETAILASSEMBLY")));
            }

            // Calculatie kopiëren naar nieuwe offerte
            var param = new Dictionary<string, object>
            {
                {"CalculationToCopy", calculation.Field<int>("PK_C_CALCULATION")},
                {"Quantity", calculation.Field<double>("QUANTITY")}
            };
            ExecuteWorkflowEvent("R_OFFER", newOfferId, new Guid("680764f0-64c7-4a43-9bb4-59a588b573eb"), param);

            var rsNewOfferCalculations = GetRecordset("C_CALCULATION", "PK_C_CALCULATION, FK_OFFER", 
                string.Format("FK_OFFER = {0} AND FK_ASSEMBLY IS NULL", newOfferId), "");
            if (rsNewOfferCalculations.RecordCount != 1)
            {
                throw new Exception("De nieuwe calculatie kan niet gevonden worden in de nieuwe offerte!");
            }

            rsNewOfferCalculations.MoveFirst();

            // Calculatie omzetten naar stuklijst
            ExecuteWorkflowEvent("C_CALCULATION", (int) rsNewOfferCalculations.Fields["PK_C_CALCULATION"].Value,
                new Guid("33512f6c-873b-49c5-b4db-bb046f5acd4d"));

            // Nieuwste offerteregel weggooien
            var rsOfferDetails = GetRecordset("R_OFFERDETAILASSEMBLY", "FK_OFFER, FK_ASSEMBLY", 
                string.Format("FK_OFFER = {0}", newOfferId), "DATECREATED DESC");
            rsOfferDetails.UpdateWhenMoveRecord = false;
            rsOfferDetails.MoveFirst();
            rsOfferDetails.Delete();
            rsOfferDetails.Update();

            var rsNewCalculationWithAssembly = GetRecordset("C_CALCULATION", "FK_ASSEMBLY", 
                string.Format("PK_C_CALCULATION = {0}", (int)rsNewOfferCalculations.Fields["PK_C_CALCULATION"].Value), "");
            rsNewCalculationWithAssembly.MoveFirst();
            var assemblyId = (int)rsNewCalculationWithAssembly.Fields["FK_ASSEMBLY"].Value;

            rsOfferDetails = GetRecordset("R_OFFERDETAILASSEMBLY", "FK_OFFER, FK_ASSEMBLY", 
                string.Format("FK_OFFER = {0} AND FK_ASSEMBLY = {1}", newOfferId, offerDetail.Field<int>("FK_ASSEMBLY")), "");
            rsOfferDetails.UpdateWhenMoveRecord = false;
            rsOfferDetails.MoveFirst();
            while (!rsOfferDetails.EOF)
            {
                rsOfferDetails.Fields["FK_ASSEMBLY"].Value = assemblyId;
                rsOfferDetails.MoveNext();
            }

            rsOfferDetails.MoveFirst();
            rsOfferDetails.Update();
        }
    }
}
