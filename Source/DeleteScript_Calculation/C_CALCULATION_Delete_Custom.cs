﻿using System;
using ADODB;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using Ridder.Common.Script;
using Ridder.Common.Framework;
using Ridder.Common.Login;
using System.Data;
using System.Linq;

public class C_CALCULATION_Delete_Custom : IDeleteScript
{
    //When returning false fill reason to give meaningfull message to user
    public bool BeforeDelete(RowData data, ref string reason)
    {
        var sh = new ScriptHelper();
        var rsCalculations = sh.GetRecordset("C_CALCULATION", "", string.Format("FK_MAINCALCULATION = {0}", data["PK_C_CALCULATION"]), "");
        if (rsCalculations.RecordCount > 0)
        {
            rsCalculations.MoveFirst();
            foreach (var calc in rsCalculations.DataTable.AsEnumerable())
            {
                calc.SetField("MARKEDFORDELETE", true);
                calc.SetField("FK_MAINCALCULATION", DBNull.Value);
            }
            rsCalculations.Update();
        }
        return true;
    }

    //When returning false fill reason to give meaningfull message to user
    public bool AfterDelete(RowData data, ref string reason)
    {
        var sh = new ScriptHelper();
        var rsCalculationTotals = sh.GetRecordset("C_CALCULATIONTOTALS", "", string.Format("PK_C_CALCULATIONTOTALS = {0}", data["FK_CALCULATIONTOTAL"]), "");
        if (rsCalculationTotals.RecordCount > 0)
        {
            rsCalculationTotals.MoveFirst();
            foreach (var total in rsCalculationTotals.DataTable.AsEnumerable())
            {
                total.Delete();
            }
            rsCalculationTotals.Update();
        }

        var rsCalculations = sh.GetRecordset("C_CALCULATION", "", "MARKEDFORDELETE = 1", "");
        if (rsCalculations.RecordCount > 0)
        {
            rsCalculations.MoveFirst();
            foreach (var calc in rsCalculations.DataTable.AsEnumerable())
            {
                calc.Delete();
            }
            rsCalculations.Update();
        }

        return true;
    }
}
