using System;
using Calculation.Core.Classes.Base;

namespace Calculation.Core.Classes
{
    public class ObjectChangedEventArgs : EventArgs
    {
        public object Object { get; }
        public DatabaseObjectStateEnum NewState { get; }

        public ObjectChangedEventArgs(object @object, DatabaseObjectStateEnum newState)
        {
            Object = @object;
            NewState = newState;
        }
    }

    public class ExtendedObjectChangedEventArgs : EventArgs
    {
        public IDatabaseObject DatabaseDatabaseObject { get; }
        public string PropertyName { get; }
        public object OldValue { get; }
        public object NewValue { get; }

        public ExtendedObjectChangedEventArgs(IDatabaseObject databaseDatabaseObject, string propertyName, object oldValue, object newValue)
        {
            DatabaseDatabaseObject = databaseDatabaseObject;
            PropertyName = propertyName;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}