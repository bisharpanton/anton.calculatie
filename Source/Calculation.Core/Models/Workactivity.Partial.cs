﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class WorkActivity
    {
        public override string ToString()
        {
            return CODE;
        }

        [ForeignKey("FK_WAGEGROUP")]
        [Include]
        public WageGroup WageGroup { get; set; }

        public double CostPrice => _PRICEOPERATIONTIME + WageGroup?.COSTPRICE ?? 0.0;

        public double SalesPrice => _SALESPRICEOPERATIONTIME + WageGroup?.SALESPRICE ?? 0.0;
    }
}