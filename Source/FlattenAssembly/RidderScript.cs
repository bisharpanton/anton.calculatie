﻿using ADODB;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml;
using Ridder.Common.ADO;
using Ridder.Common.Choices;
using Ridder.Common.Search;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using Ridder.Common.Script;
using System.Drawing;
using FlattenAssembly;

public class RidderScript : CommandScript
{
    public string Error { get; set; }

    public void Execute()
    {
        Error = string.Empty;

        if (System.Diagnostics.Debugger.IsAttached)
        {
            System.Diagnostics.Debugger.Break();
        }

        if (FormDataAwareFunctions == null || FormDataAwareFunctions.TableName != "R_ASSEMBLY")
        {
            MessageBox.Show("Dit script kan alleen uitgevoerd worden vanaf de stuklijsten tabel!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        if (FormDataAwareFunctions.CurrentRecord == null || FormDataAwareFunctions.GetSelectedRecords().Length != 1)
        {
            MessageBox.Show("Dit script mag alleen vanaf een stuklijst uitgevoerd worden!", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        var assemblyId = FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue() as int? ?? 0;

        if (assemblyId == 0)
        {
            throw new Exception("Het assemblyId van de stuklijst is niet correct!");
        }

        // Zo leeg gooien?
        var rsAssemblyExport = GetRecordset("C_FLATASSEMBLY", "", string.Format("FK_MAINASSEMBLY = {0}", assemblyId), "");
        rsAssemblyExport.MoveFirst();
        foreach (var assemblyExport in rsAssemblyExport.DataTable.AsEnumerable())
        {
            assemblyExport.Delete();
        }
        //rsAssemblyExport.Update();

        rsAssemblyExport.UpdateWhenMoveRecord = false;
        rsAssemblyExport.UseDataChanges = false;
        rsAssemblyExport.MoveFirst();

        rsAssemblyExport = ExportStuklijst(assemblyId, rsAssemblyExport, assemblyId);

        rsAssemblyExport.Update();

        if (!string.IsNullOrEmpty(Error))
        {
            MessageBox.Show(Error);
        }
    }

    public ScriptRecordset ExportStuklijst(int mainAssemblyId, ScriptRecordset rsAssemblyExport, int assemblyId, int niveau = 1, string tree = "", double multiplier = 1)
    {
        var niveauTree = tree;
        if (string.IsNullOrEmpty(niveauTree))
        {
            niveauTree += niveau;
        }

        int index = 1;
        
        var rsAssemblyDetailItems = GetRecordset("R_ASSEMBLYDETAILITEM", "PK_R_ASSEMBLYDETAILITEM, FK_ITEM, QUANTITY, POSITION, FK_ASSEMBLY", string.Format("FK_ASSEMBLY = {0}", assemblyId), "");
        rsAssemblyDetailItems.MoveFirst();
        foreach (var rsAssemblyDetailItem in rsAssemblyDetailItems.DataTable.AsEnumerable())
        {
            var recordTree = string.Format("{0}.{1}", niveauTree, index);
            //var exportRecord = CreateExportRecordItem(mainAssemblyId, rsAssemblyDetailItem, niveau, recordTree, multiplier);
            //rsAssemblyExport.AddNew(exportRecord.Keys, exportRecord.Values);
            rsAssemblyExport = CreateExportRecordItem(rsAssemblyExport, mainAssemblyId, rsAssemblyDetailItem, niveau, recordTree, multiplier);
            var artikelId = rsAssemblyDetailItem.Field<int>("FK_ITEM");
            if (IsMaakdeel(artikelId))
            {
                var artikelAssemblyId = GetVoorkeursStuklijstId(artikelId);
                if (artikelAssemblyId == 0)
                {
                    Error += "\nGeen stuklijst gevonden voor artikel: " + artikelId;
                }
                else
                {
                    var quantity = rsAssemblyDetailItem.Field<double>("QUANTITY");
                    rsAssemblyExport = ExportStuklijst(mainAssemblyId, rsAssemblyExport, artikelAssemblyId, niveau + 1, recordTree, multiplier * quantity);
                }
            }
            index++;
        }

        var rsAssemblyDetailMiscs = GetRecordset("R_ASSEMBLYDETAILMISC", "PK_R_ASSEMBLYDETAILMISC, FK_MISC, QUANTITY, POSITION, FK_ASSEMBLY", string.Format("FK_ASSEMBLY = {0}", assemblyId), "");
        rsAssemblyDetailMiscs.MoveFirst();
        foreach (var rsAssemblyDetailMisc in rsAssemblyDetailMiscs.DataTable.AsEnumerable())
        {
            var recordTree = string.Format("{0}.{1}", niveauTree, index);
            rsAssemblyExport = CreateExportRecordMisc(rsAssemblyExport, mainAssemblyId, rsAssemblyDetailMisc, niveau, recordTree, multiplier);
            index++;
        }

        var rsAssemblyDetailWorkActivities = GetRecordset("R_ASSEMBLYDETAILWORKACTIVITY", "PK_R_ASSEMBLYDETAILWORKACTIVITY, FK_WORKACTIVITY, OPERATIONTIME, POSITION, FK_ASSEMBLY", string.Format("FK_ASSEMBLY = {0}", assemblyId), "");
        rsAssemblyDetailWorkActivities.MoveFirst();
        foreach (var rsAssemblyDetailWorkActivity in rsAssemblyDetailWorkActivities.DataTable.AsEnumerable())
        {
            var recordTree = string.Format("{0}.{1}", niveauTree, index);
            rsAssemblyExport = CreateExportRecordWorkActivity(rsAssemblyExport, mainAssemblyId, rsAssemblyDetailWorkActivity, niveau, recordTree, multiplier);
            index++;
        }

        var rsAssemblyDetailOuts = GetRecordset("R_ASSEMBLYDETAILOUTSOURCED", "PK_R_ASSEMBLYDETAILOUTSOURCED, QUANTITY, POSITION, FK_ASSEMBLY", string.Format("FK_ASSEMBLY = {0}", assemblyId), "");
        rsAssemblyDetailOuts.MoveFirst();
        foreach (var rsAssemblyDetailOut in rsAssemblyDetailOuts.DataTable.AsEnumerable())
        {
            var recordTree = string.Format("{0}.{1}", niveauTree, index);
            rsAssemblyExport = CreateExportRecordOut(rsAssemblyExport, mainAssemblyId, rsAssemblyDetailOut, niveau, recordTree, multiplier);
            index++;
        }

        var rsAssemblyDetailSubAssemblies = GetRecordset("R_ASSEMBLYDETAILSUBASSEMBLY", "PK_R_ASSEMBLYDETAILSUBASSEMBLY, FK_SUBASSEMBLY, QUANTITY, POSITION, FK_ASSEMBLY", string.Format("FK_ASSEMBLY = {0}", assemblyId), "");
        rsAssemblyDetailSubAssemblies.MoveFirst();

        foreach (var rsAssemblyDetailSubAssembly in rsAssemblyDetailSubAssemblies.DataTable.AsEnumerable())
        {
            var recordTree = string.Format("{0}.{1}", niveauTree, index);
            rsAssemblyExport = CreateExportRecordAssembly(rsAssemblyExport, mainAssemblyId, rsAssemblyDetailSubAssembly, niveau, recordTree, multiplier);
            var quantity = rsAssemblyDetailSubAssembly.Field<double>("QUANTITY");
            rsAssemblyExport = ExportStuklijst(mainAssemblyId, rsAssemblyExport, rsAssemblyDetailSubAssembly.Field<int>("FK_SUBASSEMBLY"), niveau + 1, recordTree, multiplier * quantity);
            index++;
        }

        return rsAssemblyExport;
    }

    public ScriptRecordset CreateExportRecordItem(ScriptRecordset rsAssemblyExport, int mainAssemblyId, DataRow item, int niveau, string tree, double multiplier)
    {
        rsAssemblyExport.AddNew();
        rsAssemblyExport.SetFieldValue("FK_MAINASSEMBLY", mainAssemblyId);
        rsAssemblyExport.SetFieldValue("FK_ASSEMBLYDETAILITEM", item.Field<int>("PK_R_ASSEMBLYDETAILITEM"));
        rsAssemblyExport.SetFieldValue("POSITION", item.Field<string>("POSITION"));
        rsAssemblyExport.SetFieldValue("TOTALQUANTITY", item.Field<double>("QUANTITY") * multiplier);
        rsAssemblyExport.SetFieldValue("NIVEAU", niveau);
        rsAssemblyExport.SetFieldValue("TREE", tree);

        return rsAssemblyExport;
    }

    public ScriptRecordset CreateExportRecordMisc(ScriptRecordset rsAssemblyExport, int mainAssemblyId, DataRow misc, int niveau, string tree, double multiplier)
    {
        rsAssemblyExport.AddNew();
        rsAssemblyExport.SetFieldValue("FK_MAINASSEMBLY", mainAssemblyId);
        rsAssemblyExport.SetFieldValue("FK_ASSEMBLYDETAILMISC", misc.Field<int>("PK_R_ASSEMBLYDETAILMISC"));
        rsAssemblyExport.SetFieldValue("POSITION", misc.Field<string>("POSITION"));
        rsAssemblyExport.SetFieldValue("TOTALQUANTITY", misc.Field<double>("QUANTITY") * multiplier);
        rsAssemblyExport.SetFieldValue("NIVEAU", niveau);
        rsAssemblyExport.SetFieldValue("TREE", tree);

        return rsAssemblyExport;
    }

    public ScriptRecordset CreateExportRecordWorkActivity(ScriptRecordset rsAssemblyExport, int mainAssemblyId, DataRow misc, int niveau, string tree, double multiplier)
    {
        rsAssemblyExport.AddNew();
        rsAssemblyExport.SetFieldValue("FK_MAINASSEMBLY", mainAssemblyId);
        rsAssemblyExport.SetFieldValue("FK_ASSEMBLYDETAILWORKACTIVITY", misc.Field<int>("PK_R_ASSEMBLYDETAILWORKACTIVITY"));
        rsAssemblyExport.SetFieldValue("POSITION", misc.Field<string>("POSITION"));
        rsAssemblyExport.SetFieldValue("TOTALOPERATIONTIME", misc.Field<long>("OPERATIONTIME") * multiplier);
        rsAssemblyExport.SetFieldValue("NIVEAU", niveau);
        rsAssemblyExport.SetFieldValue("TREE", tree);

        return rsAssemblyExport;
    }

    public ScriptRecordset CreateExportRecordOut(ScriptRecordset rsAssemblyExport, int mainAssemblyId, DataRow misc, int niveau, string tree, double multiplier)
    {
        rsAssemblyExport.AddNew();
        rsAssemblyExport.SetFieldValue("FK_MAINASSEMBLY", mainAssemblyId);
        rsAssemblyExport.SetFieldValue("FK_ASSEMBLYDETAILOUT", misc.Field<int>("PK_R_ASSEMBLYDETAILOUTSOURCED"));
        rsAssemblyExport.SetFieldValue("POSITION", misc.Field<string>("POSITION"));
        rsAssemblyExport.SetFieldValue("TOTALQUANTITY", misc.Field<double>("QUANTITY") * multiplier);
        rsAssemblyExport.SetFieldValue("NIVEAU", niveau);
        rsAssemblyExport.SetFieldValue("TREE", tree);

        return rsAssemblyExport;
    }

    public ScriptRecordset CreateExportRecordAssembly(ScriptRecordset rsAssemblyExport, int mainAssemblyId, DataRow assembly, int niveau, string tree, double multiplier)
    {
        rsAssemblyExport.AddNew();

        rsAssemblyExport.SetFieldValue("FK_MAINASSEMBLY", mainAssemblyId);
        rsAssemblyExport.SetFieldValue("FK_ASSEMBLYDETAILSUBASSEMBLY", assembly.Field<int>("PK_R_ASSEMBLYDETAILSUBASSEMBLY"));
        rsAssemblyExport.SetFieldValue("POSITION", assembly.Field<string>("POSITION"));
        rsAssemblyExport.SetFieldValue("TOTALQUANTITY", assembly.Field<double>("QUANTITY") * multiplier);
        rsAssemblyExport.SetFieldValue("NIVEAU", niveau);
        rsAssemblyExport.SetFieldValue("TREE", tree);

        return rsAssemblyExport;
    }

    public bool IsMaakdeel(int artikelId)
    {
        var rsArtikel = GetRecordset("R_ITEM", "FK_ITEMUNIT", string.Format("PK_R_ITEM = {0}", artikelId), "");
        rsArtikel.MoveFirst();
        var rsArtikelEenheid = GetRecordset("R_ITEMUNIT", "KIND", string.Format("PK_R_ITEMUNIT = {0}", rsArtikel.Fields["FK_ITEMUNIT"].Value), "");
        rsArtikelEenheid.MoveFirst();

        if ((int)rsArtikelEenheid.Fields["KIND"].Value == 3)
        {
            return true;
        }
        return false;
    }

    public int GetVoorkeursStuklijstId(int artikelId)
    {
        var rsVoorkeursStuklijst = GetRecordset("R_ASSEMBLY", "PK_R_ASSEMBLY", string.Format("FK_ITEM = {0} and MAINASSEMBLY = 1", artikelId), "");
        rsVoorkeursStuklijst.MoveFirst();
        if (rsVoorkeursStuklijst.RecordCount == 0)
        {
            return 0;
        }
        return rsVoorkeursStuklijst.Fields["PK_R_ASSEMBLY"].Value as int? ?? 0;
    }
}