#Requires -Version 2.0

# from: https://github.com/psake/psake
<#
.SYNOPSIS
  This is a helper function that runs a scriptblock and checks the PS variable $lastexitcode
  to see if an error occcured. If an error is detected then an exception is thrown.
  This function allows you to run command-line programs without having to
  explicitly check the $lastexitcode variable.
.EXAMPLE
  exec { svn info $repository_trunk } "Error executing SVN. Please verify SVN command-line client is installed"
#>
function Exec
{
    [CmdletBinding()]
    param(
        [Parameter(Position=0,Mandatory=1)][scriptblock]$cmd,
        [Parameter(Position=1,Mandatory=0)][string]$errorMessage = ($msgs.error_bad_command -f $cmd)
    )

    $global:lastexitcode = 0
    & $cmd
    if ($lastexitcode -ne 0) {
        throw ("Exec: " + $errorMessage)
    }
}

# taken from Invoke-MsBuild, slightly modified for vs15 support
#
# Copyright (c) 2013 Daniel Schroeder, iQmetrix
# https://github.com/deadlydog/Invoke-MsBuild
function Get-MsBuildPath([switch] $Use32BitMsBuild)
{
<#
    .SYNOPSIS
    Gets the path to the latest version of MsBuild.exe. Throws an exception if MsBuild.exe is not found.
    
    .DESCRIPTION
    Gets the path to the latest version of MsBuild.exe. Throws an exception if MsBuild.exe is not found.
#>
    # VS / MSBUILD 2017 don't use registry, so lookup paths
    $vs15Paths = @(
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe",
        "${env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe"
    )
    
    foreach ($path in $vs15Paths) {
        if (Test-Path $path)
        {
            return $path
        }
    }

    $registryPathToMsBuildToolsVersions = 'HKLM:\SOFTWARE\Microsoft\MSBuild\ToolsVersions\'
    if ($Use32BitMsBuild)
    {
        # If the 32-bit path exists, use it, otherwise stick with the current path (which will be the 64-bit path on 64-bit machines, and the 32-bit path on 32-bit machines).
        $registryPathTo32BitMsBuildToolsVersions = 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\MSBuild\ToolsVersions\'
        if (Test-Path -Path $registryPathTo32BitMsBuildToolsVersions)
        {
            $registryPathToMsBuildToolsVersions = $registryPathTo32BitMsBuildToolsVersions
        }
    }

    # Get the path to the directory that the latest version of MsBuild is in.
    $msBuildToolsVersionsStrings = Get-ChildItem -Path $registryPathToMsBuildToolsVersions | Where-Object { $_ -match '[0-9]+\.[0-9]' } | Select-Object -ExpandProperty PsChildName
    $msBuildToolsVersions = @{}
    $msBuildToolsVersionsStrings | ForEach-Object {$msBuildToolsVersions.Add($_ -as [double], $_)}
    $largestMsBuildToolsVersion = ($msBuildToolsVersions.GetEnumerator() | Sort-Object -Descending -Property Name | Select-Object -First 1).Value
    $registryPathToMsBuildToolsLatestVersion = Join-Path -Path $registryPathToMsBuildToolsVersions -ChildPath ("{0:n1}" -f $largestMsBuildToolsVersion)
    $msBuildToolsVersionsKeyToUse = Get-Item -Path $registryPathToMsBuildToolsLatestVersion
    $msBuildDirectoryPath = $msBuildToolsVersionsKeyToUse | Get-ItemProperty -Name 'MSBuildToolsPath' | Select -ExpandProperty 'MSBuildToolsPath'

    if(!$msBuildDirectoryPath)
    {
        throw 'The registry on this system does not appear to contain the path to the MsBuild.exe directory.'
    }

    # Get the path to the MsBuild executable.
    $msBuildPath = (Join-Path -Path $msBuildDirectoryPath -ChildPath 'msbuild.exe')

    if(!(Test-Path $msBuildPath -PathType Leaf))
    {
        throw "MsBuild.exe was not found on this system at the path specified in the registry, '$msBuildPath'."
    }

    return $msBuildPath
}

Export-ModuleMember -Function Exec, Get-MsBuildPath