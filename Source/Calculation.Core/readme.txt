﻿Ridder Client SDK Extensions
=============================

TODO

Getting Started
----------------

### Add SDK settings to your application configuration.

Example App.config / Web.config settings:

    <appSettings>
        <add key="SDK_UserName" value="Administrator" />
        <add key="SDK_Password" value="" />
        <add key="SDK_CompanyName" value="TESTbedrijf" />
        <add key="SDK_PersistSession" value="true" />
        <add key="SDK_RidderIQClientPath" value="C:\Program Files (x86)\Ridder iQ Handson\Bin" />
    </appSettings>


### Create and initialize a new instance of SdkConnection

Note: you normally need only one instance of SdkConnection. Store it somewhere global (in a container or static var/class).

    connection = new SdkConnection();


### Start using sessions.

Querying and modifying recordsets and entities is done trough a SdkSession.

Create a new session and get some data using the recordset methods:
    
    using (var session = connection.OpenSession())
    {
        var rs = session.GetRecordset("R_ADDRESS", "CITY = 'Harderwijk'");
        while (!rs.EOF)
        {
            // ...
            rs.MoveNext();
        }
    }

Or using mapped classes (entities):

    using (var session = connection.OpenSession())
    {
        var country = new Country() { Code = "TST", Name = "Test" };
        session.Insert(country);

        var code = "TST";
        var allcountries = session.Find<Country>("CODE = '{0}'", code);
    }

Custom Sdk settings
--------------------

You don't have to use AppSettings to configure the Sdk extensions. 
Instead, create a SdkConnection with your own configuration values like this:
  
  SdkConfiguration configuration = new SdkConfiguration();
  configuration.UserName = "username";
  configuration.CompanyName = "company";
  // etc.
  SdkConnection connection = new SdkConnection(configuration);


LINQ support
-------------

Download the 'Ridder.Client.SDK.Extensions.Linq' package.

Example linq query:

    using (var session = connection.OpenSession())
    {
        var codes = new List<string>("NL", "BE");

        var countryQuery = from c in session.Query<Country>()
                           where codes.Contains(c.Code) || c.Code == null
                           select c;
        
        // Note that 'countryQuery' represents just the query (it has not executed yet)
        
        // query is executed here as: ((CODE IN ('NL','BE')) OR (CODE IS NULL))
        var countries = countryQuery.ToList(); 
        
        // this will hit the database (sdk) again!
        var count = countryQuery.Count();

        countryQuery = countryQuery.Where(x=>x.HouseNumber == 1);

        // query is executed here as: (((CODE IN ('NL','BE')) OR (CODE IS NULL)) AND (HOUSENUMBER = 1))
        countries = countryQuery.ToList(); 
    }
