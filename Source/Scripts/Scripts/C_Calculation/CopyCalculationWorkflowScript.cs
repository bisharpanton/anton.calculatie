﻿namespace Scripts.C_Calculation
{
    using ADODB;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Ridder.Common.ADO;
    using Ridder.Common.Choices;
    using Ridder.Common.Search;
    using System.Linq;
    using System.Data;
    using Ridder.Common.WorkflowModel.Activities;
    using System.Drawing.Printing;

    public class CopyCalculationWorkflowScript : WorkflowScriptInfo
    {
        public void Execute()
        {
            var calculationToCopy = (int)Parameters["CalculationToCopy"].Value;
            double quantity = Convert.ToDouble(Parameters["QUANTITY"].Value);



            CopyCalculation(calculationToCopy, quantity: quantity);
        }

        // Columns to copy
        private static readonly List<string> calculationColumnsToCopy = new List<string> { "QUANTITY", "FK_UNIT", "DESCRIPTION" };
        private static readonly List<string> tailrulesColumnsToCopy = new List<string> { "TOTAL", "SEQUENCENUMBER", "PERCENTAGE", "DESCRIPTION", "AMOUNT" };

        // Columns to ignore
        private static readonly List<string> totalsColumnsNotToCopy = new List<string> { "USERCHANGED", "PK_C_CALCULATIONTOTALS", "FK_WORKFLOWSTATE", "EXTERNALKEY", "DATECREATED", "DATECHANGED", "CREATOR", "RECORDLINK" };
        private static readonly List<string> calculationRuleColumnsNotToCopy = new List<string> { "USERCHANGED", "PK_C_CALCULATIONRULE", "FK_WORKFLOWSTATE", "EXTERNALKEY", "DATECREATED", "DATECHANGED", "CREATOR", "RECORDLINK" };

        public int CopyCalculation(int calculationId, int mainCalculation = 0, double quantity = 0)
        {
            var rsExistingCalculation = GetRecordset("C_CALCULATION", "", string.Format("PK_C_CALCULATION = {0}", calculationId), "");
            rsExistingCalculation.MoveFirst();

            var rsNewCalculation = GetRecordset("C_CALCULATION", "", "PK_C_CALCULATION = -1", "");
            rsNewCalculation.AddNew();
            
            foreach (DataColumn column in rsNewCalculation.DataTable.Columns)
            {
                if (column.ColumnName == "FK_OFFER" && mainCalculation == 0)
                {
                    rsNewCalculation.Fields[column.ColumnName].Value = (int)RecordId;
                }
                else if (column.ColumnName == "FK_MAINCALCULATION" && mainCalculation > 0)
                {
                    rsNewCalculation.Fields[column.ColumnName].Value = mainCalculation;
                }
                else if (column.ColumnName == "FK_CALCULATIONTOTAL")
                {
                    var totalsId = rsExistingCalculation.Fields[column.ColumnName].Value as int? ?? 0;
                    if (totalsId > 0)
                    {
                        rsNewCalculation.Fields[column.ColumnName].Value = CopyTotals(totalsId);
                    }
                }
                else if (calculationColumnsToCopy.Contains(column.ColumnName))
                {
                    rsNewCalculation.Fields[column.ColumnName].Value = rsExistingCalculation.Fields[column.ColumnName].Value;
                }
            }

            if (quantity > 0)
            {
                rsNewCalculation.Fields["QUANTITY"].Value = quantity;
            }
            rsNewCalculation.Update();
            var newCalculationId = (int)rsNewCalculation.Fields["PK_C_CALCULATION"].Value;

            CopyCalculationRules(calculationId, newCalculationId);
            CopyTailRules(calculationId, newCalculationId);

            return newCalculationId;
        }

        public void CopyCalculationRules(int oldCalculation, int newCalculation)
        {
            var rsExistingCalculationRules = GetRecordset("C_CALCULATIONRULE", "", string.Format("FK_CALCULATION = {0}", oldCalculation), "");
            rsExistingCalculationRules.MoveFirst();

            var rsNewCalculationRules = GetRecordset("C_CALCULATIONRULE", "", "PK_C_CALCULATIONRULE = -1", "");

            while (!rsExistingCalculationRules.EOF)
            {
                rsNewCalculationRules.AddNew();
                foreach (DataColumn column in rsNewCalculationRules.DataTable.Columns)
                {
                    if (column.ColumnName == "FK_SUBCALCULATION" && rsExistingCalculationRules.Fields[column.ColumnName].Value as int? > 0)
                    {
                        rsNewCalculationRules.Fields[column.ColumnName].Value = CopyCalculation((int)rsExistingCalculationRules.Fields[column.ColumnName].Value, newCalculation);
                    }
                    else if (column.ColumnName == "FK_CALCULATION")
                    {
                        rsNewCalculationRules.Fields[column.ColumnName].Value = newCalculation;
                    }
                    else if (!calculationRuleColumnsNotToCopy.Contains(column.ColumnName))
                    {
                        rsNewCalculationRules.Fields[column.ColumnName].Value = rsExistingCalculationRules.Fields[column.ColumnName].Value;
                    }
                }
                rsExistingCalculationRules.MoveNext();
            }

            if (rsNewCalculationRules.RecordCount > 0)
            {
                rsNewCalculationRules.Update();
            }
        }

        public void CopyTailRules(int oldCalculation, int newCalculation)
        {
            var rsExistingTailRules = GetRecordset("C_CALCULATIONTAILRULES", "", string.Format("FK_CALCULATION = {0}", oldCalculation), "");
            rsExistingTailRules.MoveFirst();

            var rsNewCalculationTailRules = GetRecordset("C_CALCULATIONTAILRULES", "", "PK_C_CALCULATIONTAILRULES = -1", "");

            while (!rsExistingTailRules.EOF)
            {
                rsNewCalculationTailRules.AddNew();
                foreach (DataColumn column in rsNewCalculationTailRules.DataTable.Columns)
                {
                    if (column.ColumnName == "FK_CALCULATION")
                    {
                        rsNewCalculationTailRules.Fields[column.ColumnName].Value = newCalculation;
                    }
                    else if (tailrulesColumnsToCopy.Contains(column.ColumnName))
                    {
                        rsNewCalculationTailRules.Fields[column.ColumnName].Value = rsExistingTailRules.Fields[column.ColumnName].Value;
                    }
                }
                rsExistingTailRules.MoveNext();
            }

            if (rsNewCalculationTailRules.RecordCount > 0)
            {
                rsNewCalculationTailRules.Update();
            }
        }

        public int CopyTotals(int calculationTotalsId)
        {
            var rsExistingTotals = GetRecordset("C_CALCULATIONTOTALS", "", string.Format("PK_C_CALCULATIONTOTALS = {0}", calculationTotalsId), "");
            rsExistingTotals.MoveFirst();

            var rsNewCalculationTotals = GetRecordset("C_CALCULATIONTOTALS", "", "PK_C_CALCULATIONTOTALS = -1", "");
            rsNewCalculationTotals.AddNew();

            foreach (DataColumn column in rsNewCalculationTotals.DataTable.Columns)
            {
                if (!totalsColumnsNotToCopy.Contains(column.ColumnName))
                {
                    rsNewCalculationTotals.Fields[column.ColumnName].Value = rsExistingTotals.Fields[column.ColumnName].Value;
                }
            }

            rsNewCalculationTotals.Update();
            var newCalculationTotalsId = (int)rsNewCalculationTotals.Fields["PK_C_CALCULATIONTOTALS"].Value;

            return newCalculationTotalsId;

        }
    }
}
