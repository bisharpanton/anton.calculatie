using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using Calculation.Core.Interfaces;
using Calculation.Core.Models;
using Ridder.Client.SDK.Extensions;
using Ridder.Client.SDK.SDKParameters;

namespace Calculation.Core.Classes.Calculation
{
    public class ReportFactory
    {
        private readonly ISessionFactory _sessionFactory;
        private readonly CalculationFactory _calculationFactory;
        private readonly string _tableName;
        public List<Report> Reports { get; set; }

        public ReportFactory(ISessionFactory sessionFactory, CalculationFactory calculationFactory,  string tableName)
        {
            _sessionFactory = sessionFactory;
            _calculationFactory = calculationFactory;
            _tableName = tableName;
        }

        public void LoadDatabaseObjects()
        {
            using (var session = _sessionFactory.GetSession())
            {
                var tableInfo = session.Find<TableInfo>(x => x.TABLENAME == _tableName).FirstOrDefault();

                if (tableInfo == null)
                    throw new ArgumentNullException("tableInfo");

                Reports = session.Find<Report>().Where(x => x.FK_TABLEINFO == tableInfo.Id).ToList();
            }
        }

        public void ShowReport(Guid reportId)
        {
            if (reportId == Guid.Empty)
                throw new ArgumentNullException(nameof(reportId), "Report ID is not set");

            if(_calculationFactory.MainCalculation == null)
                throw new ArgumentNullException(nameof(_calculationFactory.MainCalculation), "No calculation selected");

            using (var session = _sessionFactory.GetSession())
            {
                var flattenCalculationWFID = new Guid("4e2325d7-3a85-467a-896f-a0ac4c0a3be8");
                var result = session.Sdk.ExecuteWorkflowEvent("C_CALCULATION", _calculationFactory.MainCalculation.Id, flattenCalculationWFID);
                if (result.HasError)
                {
                    throw new ArgumentException("Er is iets fout gegaan bij het platslaan van de calculatie:\n" + result.GetResult(), nameof(flattenCalculationWFID));
                }

                var fileName = Path.Combine(Path.GetTempPath(), $"{DateTime.Now:ddMMyyyyHHmmss}.pdf");
                session.Sdk.ExportReport(_calculationFactory.MainCalculation.Id, _tableName, DesignerScope.User, reportId, Guid.Empty, fileName, ExportType.Pdf, false);

                if (File.Exists(fileName))
                {
                    Thread.Sleep(1000);
                    Process.Start(fileName);
                }
            }
        }
    }
}