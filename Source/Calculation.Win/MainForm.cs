﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Calculation.Core.Classes;
using Calculation.Core.Classes.Base;
using Calculation.Core.Classes.Calculation;
using Calculation.Core.Extensions;
using Calculation.Core.Interfaces;
using Calculation.Core.Models;
using Calculation.Win.Communication;
using Calculation.Win.Localization;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.DragDrop;
using DevExpress.Utils.Extensions;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.Commands;
using DevExpress.XtraTreeList;
using Ridder.Client.SDK.Extensions;
using Calculation = Calculation.Core.Models.Calculation;
using InsertType = DevExpress.XtraLayout.Utils.InsertType;

namespace Calculation.Win
{
    public partial class MainForm : RibbonForm
    {
        public bool HasSupporterMaterial
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.Item.SupporterItem) ?? false;
            }
        }

        public bool HasConcreteSteelMaterial
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.Item.ConcreteSteelItem) ?? false;
            }
        }

        public bool HasMatMaterial
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.Item.MatItem) ?? false;
            }
        }

        public bool HasProfileMaterial
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.Item.ProfileItem) ?? false;
            }
        }

        public bool HasSurfaceMaterial
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.Item.SurfaceItem) ?? false;
            }
        }

        public bool HasMultipleItemSuppliers
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.Item.HasMultipleSuppliers) ?? false;
            }
        }

        public bool HasMultipleUbwSuppliers
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.OutsourcedActivity != null && x.OutsourcedActivity.HasMultipleSuppliers) ?? false;
            }
        }

        public bool HasItemSupplier
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.Item != null && x.FK_ALTERNATEITEMSUPPLIER.HasValue) ?? false;
            }
        }

        public bool HasUbwSupplier
        {
            get
            {
                return _calculationFactory.MainCalculation?.CalculationRules.Any(x => x.OutsourcedActivity != null && x.FK_ALTERNATEUBWSUPPLIER.HasValue) ?? false;
            }
        }

        public bool ShowCostPrices => edtPriceFilter.SelectedIndex == 0 || edtPriceFilter.SelectedIndex == 1;
        public bool ShowSalesPrices => edtPriceFilter.SelectedIndex == 0 || edtPriceFilter.SelectedIndex == 2;
        private static readonly Color CostPriceColor = Color.FromArgb(255, 255, 153);
        private static readonly Color SalesPriceColor = Color.FromArgb(102, 255, 204);
        private readonly CustomSplashScreenManager _splashScreenManager;
        private readonly CalculationFactory _calculationFactory;
        private readonly ReportFactory _reportFactory;
        private readonly IArgumentHelper _argumentHelper;
        private readonly DatabaseHelper _databaseHelper;
        private readonly ISessionFactory _sessionFactory;
        private const string BandDefault = "Algemeen";
        private const string BandItem = "Artikel";
        private const string BandOutsourced = "Uitbesteed werk";
        private const string BandWorkActivity = "Bewerking";
        private const string BandTotals = "Totalen";

        public MainForm(CustomSplashScreenManager splashScreenManager, CalculationFactory calculationFactory, ReportFactory reportFactory, IArgumentHelper argumentHelper, DatabaseHelper databaseHelper, ISessionFactory sessionFactory)
        {
            _splashScreenManager = splashScreenManager;
            _calculationFactory = calculationFactory;
            _reportFactory = reportFactory;
            _argumentHelper = argumentHelper;
            _databaseHelper = databaseHelper;
            _sessionFactory = sessionFactory;

            InitializeComponent();

            _calculationFactory.LockCalculation(argumentHelper.CalculationId);
            _calculationFactory.SetMainCalculation(_databaseHelper.Calculations.First(x => x.FK_MAINCALCULATION == null));
            _reportFactory.LoadDatabaseObjects();
            InitializeBarButtons();
            InitializeReportButton();
            InitializeTree();
            InitializeGrid();
            InitializeGridControl(gcMain, treeListCalculations, _calculationFactory);
            InitializeTailRulesGrid();
            ApplySettings();
            UpdateKiloFieldVisibility();

            CalculateHelper.Recalculate(_calculationFactory.MainCalculation);
            UpdateBarButtons();

            edtShowPerKiloFields.CheckedChanged += (sender, args) =>
            {
                UpdateKiloFieldVisibility();
            };

            Shown += (sender, args) =>
            {
                CommunicationHelper.SetupCommunication(Handle);
            };

            FormClosing += (sender, args) =>
            {
                if (_calculationFactory.HasChangedObjects())
                {
                    var result = XtraMessageBox.Show("Er zijn wijzigingen, wil je deze nog opslaan?", "Vraag", MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                    if (result == DialogResult.Yes)
                    {
                        if (args.Cancel)
                        {
                            XtraMessageBox.Show("Er zijn fouten, deze moeten eerst opgelost worden voordat er opgeslagen kan worden.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        _splashScreenManager.ShowWaitForm("Opslaan wijzigingen");

                        databaseHelper.SaveChanges();

                        _splashScreenManager.CloseWaitForm();
                    }
                    else if (result == DialogResult.No)
                    {
                        args.Cancel = false;
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        args.Cancel = true;
                    }
                }

                if (!args.Cancel)
                {
                    SaveSettings();
                    _calculationFactory.UnLockCalculation();
                }
            };

            edtPriceFilter.SelectedIndexChanged += (sender, args) =>
            {
                lcgCostPrices.Visibility = ShowCostPrices ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcgSalesPrices.Visibility = ShowSalesPrices ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcgDifferences.Visibility = ShowCostPrices && ShowSalesPrices ? LayoutVisibility.Always : LayoutVisibility.Never;

                InitializeGridColumns();
                gridView1.UpdateColumns();
            };

            CalculateHelper.Recalculated += (sender, args) =>
            {
                dataLayoutControl1.Invoke(new Action(() =>
                {
                    dataLayoutControl1.Refresh();
                }));
            };

            CalculateHelper.RefreshTailRules += (sender, args) =>
            {
                gcTailRules.Invoke(new Action(() =>
                {
                    gcTailRules.RefreshDataSource();
                }));
            };

            _calculationFactory.MainCalculationChanged += (sender, args) =>
            {
                var findNodeByFieldValue = treeListCalculations.FindNodeByFieldValue("Id", args.Calculation.Id);
                treeListCalculations.FocusedNode = findNodeByFieldValue;
                gcTailRules.Enabled = args.Calculation.MainCalculation == null;
                dataLayoutControl1.DataSource = _calculationFactory.MainCalculation.Totals;
            };

            DatabaseHelper.ObjectAdded += (sender, args) =>
            {
                UpdateBarButtons();

                if (args.Object is Core.Models.Calculation)
                    treeListCalculations.RefreshDataSource();
            };
            DatabaseHelper.ObjectChanged += (sender, args) =>
            {
                Invoke(new Action(UpdateBarButtons));
            };
            DatabaseHelper.ObjectDeleted += (sender, args) =>
            {
                UpdateBarButtons();
                treeListCalculations.RefreshDataSource();
                gcMain.RefreshDataSource();
                gcTailRules.RefreshDataSource();
            };

            // update ui if main calculation is set (and mainc
            if (_calculationFactory.MainCalculation != null)
            {
                var findNodeByFieldValue = treeListCalculations.FindNodeByFieldValue("Id", _calculationFactory.MainCalculation.Id);
                treeListCalculations.FocusedNode = findNodeByFieldValue;
                gcTailRules.Enabled = _calculationFactory.MainCalculation == null;
                dataLayoutControl1.DataSource = _calculationFactory.MainCalculation.Totals;
            }
        }

        private void UpdateKiloFieldVisibility()
        {
            dataLayoutControl1.GetItemByControl(TOTALKGSTEELTextEdit).Visibility = edtShowPerKiloFields.Checked ? LayoutVisibility.Always : LayoutVisibility.Never;
            dataLayoutControl1.GetItemByControl(TOTALSURFACEAREATextEdit).Visibility = edtShowPerKiloFields.Checked ? LayoutVisibility.Always : LayoutVisibility.Never;

            dataLayoutControl1.GetItemByControl(TotalPricePerKgTextEdit).Visibility = edtShowPerKiloFields.Checked ? LayoutVisibility.Always : LayoutVisibility.Never;
            dataLayoutControl1.GetItemByControl(TotalSalesPricePerKgTextEdit).Visibility = edtShowPerKiloFields.Checked ? LayoutVisibility.Always : LayoutVisibility.Never;
            dataLayoutControl1.GetItemByControl(TotalDifferencePerKgTextEdit).Visibility = edtShowPerKiloFields.Checked ? LayoutVisibility.Always : LayoutVisibility.Never;
        }

        protected override void WndProc(ref Message message)
        {
            switch (message.Msg)
            {
                case Win32.WM_COPYDATA:
                    {
                        if (message.GetLParam(typeof(Win32.COPYDATASTRUCT)) is Win32.COPYDATASTRUCT cds && HandleMessageFromOtherInstance(cds.lpData))
                        {
                            message.Result = new IntPtr(1);
                            return;
                        }

                        break;
                    }
            }

            base.WndProc(ref message);
        }

        private bool HandleMessageFromOtherInstance(string message)
        {
            if (string.IsNullOrWhiteSpace(message) || !message.Contains("|"))
            {
                return false;
            }

            var strings = message.Split('|');

            if (strings.Length != 2)
            {
                return false;
            }

            var companyName = strings[0];

            if (!_sessionFactory.CompanyName.Equals(companyName, StringComparison.Ordinal))
            {
                return false;
            }

            var calculationIdString = strings[1];

            if (!int.TryParse(calculationIdString, out var calculationId))
            {
                return false;
            }

            try
            {
                OpenCalculation(calculationId);
            }
            catch (Exception ex)
            {
                using (var errorForm = new ErrorForm(ex))
                {
                    errorForm.ShowDialog();
                }
            }

            return true;
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.FilterIndex = edtPriceFilter.SelectedIndex < 0 ? 0 : edtPriceFilter.SelectedIndex;
            Properties.Settings.Default.WindowMaximized = this.IsMaximized();

            if (!Properties.Settings.Default.WindowMaximized)
            {
                Properties.Settings.Default.WindowSize = this.Size;
                Properties.Settings.Default.WindowLocation = this.Location;
            }
            Properties.Settings.Default.SkinName = DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName;
            Properties.Settings.Default.ShowPerKiloFields = edtShowPerKiloFields.Checked;

            foreach (GridBand band in gridView1.Bands)
            {
                switch (band.Caption)
                {
                    case BandDefault:
                        Properties.Settings.Default.BandGeneralVisible = band.Visible;
                        break;
                    case BandItem:
                        Properties.Settings.Default.BandItemVisible = band.Visible;
                        break;
                    case BandOutsourced:
                        Properties.Settings.Default.BandOutsourcedVisible = band.Visible;
                        break;
                    case BandWorkActivity:
                        Properties.Settings.Default.BandWorkActivityVisible = band.Visible;
                        break;
                    case BandTotals:
                        Properties.Settings.Default.BandTotalsVisible = band.Visible;
                        break;
                }
            }

            Properties.Settings.Default.Save();
        }

        private void ApplySettings()
        {
            edtPriceFilter.SelectedIndex = Properties.Settings.Default.FilterIndex;

            if (Properties.Settings.Default.WindowMaximized)
                this.WindowState = FormWindowState.Maximized;

            this.Size = Properties.Settings.Default.WindowSize;
            this.Location = Properties.Settings.Default.WindowLocation;

            edtShowPerKiloFields.Checked = Properties.Settings.Default.ShowPerKiloFields;
        }

        public void UpdateBarButtons()
        {
            barButtonMoveUp.Enabled = treeListCalculations.Focused == false && gridView1.IsFocusedView && gcMain.HasPreviousRecord() && gcMain.HasFocusedRecord() && gcMain.GetFocusedRecord() is IMoveable;
            barButtonMoveDown.Enabled = treeListCalculations.Focused == false && gridView1.IsFocusedView && gcMain.HasNextRecord() && gcMain.HasFocusedRecord() && gcMain.GetFocusedRecord() is IMoveable;

            barButtonCopy.Enabled = gcMain.IsFocused;
            barButtonPaste.Enabled = false;
            if (_calculationFactory.HasClonedObjects() && barButtonPaste.Enabled == false)
            {
                barButtonPaste.Enabled = _calculationFactory.GetCloneObjectType() == typeof(Core.Models.Calculation) &&
                                         treeListCalculations.Focused && false;
            }

            if (_calculationFactory.HasClonedObjects() && barButtonPaste.Enabled == false)
            {
                barButtonPaste.Enabled =
                    _calculationFactory.GetCloneObjectType() == typeof(Core.Models.CalculationRule) && gcMain.IsFocused;
            }

            barButtonCut.Enabled = gcMain.Focused;

            barButtonDelete.Enabled = (gcMain.Focused || gcTailRules.Focused || treeListCalculations.Focused);
                                      // (treeListCalculations.Focused && _calculationFactory.MainCalculation?.FK_MAINCALCULATION != null ||
                                       // gcMain.Focused || gcTailRules.Focused) && !_calculationFactory.HasChangedObjects();

            barButtonSave.Enabled = _calculationFactory.HasChangedObjects();
            barButtonConvert.Enabled = !_calculationFactory.HasChangedObjects(); ;
            barButtonPrint.Enabled = _reportFactory.Reports.Any();

            btnTailRulesInsert.Enabled = true;
            btnTailRulesDelete.Enabled = gvTailRules.IsFocusedView && gcTailRules.HasFocusedRecord();

            barButtonRedo.Enabled = DatabaseHelper.RedoStack.Any();
            barButtonUndo.Enabled = DatabaseHelper.UndoStack.Any();
        }

        private void CloneCalculationRules(Core.Models.Calculation selectedCalculation)
        {
            var session = _sessionFactory.GetSession();
            var totals = session.Find<CalculationTotals>(x => x.Id == selectedCalculation.FK_CALCULATIONTOTAL).FirstOrDefault()?.Clone();

            var calculation = DatabaseHelper.Create<Core.Models.Calculation>();
            calculation.DESCRIPTION = selectedCalculation.DESCRIPTION;
            calculation.Totals = totals;
            calculation.FK_CALCULATIONTOTAL = totals?.Id;

            var subCalculationRule = new CalculationRule();
            subCalculationRule.SubCalculation = calculation;
            subCalculationRule.Description = calculation.DESCRIPTION;
            DatabaseHelper.Add(subCalculationRule);

            _calculationFactory.SetMainCalculation(calculation);

            DatabaseHelper.Add(totals);

            var calcs = session.Find<Core.Models.Calculation>(x => x.FK_MAINCALCULATION == selectedCalculation.Id);

            foreach (var calc in calcs)
            {
                CloneCalculationRules(calc);
                _calculationFactory.SetMainCalculation(calculation);
            }

            var calcRules = session.Find<CalculationRule>(x => x.FK_CALCULATION == selectedCalculation.Id && x.FK_SUBCALCULATION == null);
            foreach (var calculationRule in calcRules)
            {
                var clone = calculationRule.Clone();
                clone.Calculation = calculation;
                calculation.CalculationRules.Add(clone);
                DatabaseHelper.Add(clone);
            }

            CalculateHelper.Recalculate(calculation);
        }

        private void InitializeBarButtons()
        {
            barButtonMoveUp.ItemClick += (sender, args) =>
            {
                _calculationFactory.ObjectMoveUp((IMoveable)gcMain.GetFocusedRecord());
                gcMain.RefreshDataSource();
            };
            barButtonMoveDown.ItemClick += (sender, args) =>
            {
                _calculationFactory.ObjectMoveDown((IMoveable)gcMain.GetFocusedRecord());
                gcMain.RefreshDataSource();
            };

            InsertMenuCalculation.ItemClick += (sender, args) =>
            {
                using (var selectForm = new SelectCalculationForm(_calculationFactory, _databaseHelper))
                {
                    DatabaseHelper.DisableChangeTracking = false;
                    DatabaseHelper.MergeRecords = true;
                    if (selectForm.ShowDialog() != DialogResult.OK)
                    {
                        DatabaseHelper.MergeRecords = false;
                        DatabaseHelper.DisableChangeTracking = true;
                        return;
                    }

                    var selectedCalculation = selectForm.Result;

                    if (selectedCalculation.Id > 0)
                    {
                        // Als een bestaande geselecteerd is nemen we alleen de omschrijving over
                        var calculation = DatabaseHelper.Create<Core.Models.Calculation>();
                        calculation.DESCRIPTION = selectedCalculation.DESCRIPTION;
                        selectedCalculation = calculation;
                    }

                    var subCalculationRule = new CalculationRule();
                    subCalculationRule.SubCalculation = selectedCalculation;
                    subCalculationRule.Description = selectedCalculation.DESCRIPTION;
                    DatabaseHelper.Add(subCalculationRule);

                    _calculationFactory.SetMainCalculation(selectedCalculation);

                    DatabaseHelper.MergeRecords = false;
                    DatabaseHelper.DisableChangeTracking = true;
                }
            };

            InsertMenuExistingCalculation.ItemClick += (sender, args) =>
            {
                using (var session = _sessionFactory.GetSession())
                {
                    if (!(session.Sdk.OpenBrowseForm("C_CALCULATION", "PK_C_CALCULATION", Handle) is int selectedCalculationId))
                    {
                        return;
                    }

                    var calculation = session.Get<Core.Models.Calculation>(selectedCalculationId);

                    DatabaseHelper.DisableChangeTracking = false;
                    DatabaseHelper.MergeRecords = true;

                    CloneCalculationRules(calculation);

                    DatabaseHelper.MergeRecords = false;
                    DatabaseHelper.DisableChangeTracking = true;
                }
            };

            InsertMenuCalculationRule.ItemClick += (sender, args) =>
            {
                gridView1.AddNewRow();
                gridView1.Focus();
            };

            barButtonSave.ItemClick += (sender, args) =>
            {
                _splashScreenManager.ShowWaitForm("Opslaan wijzigingen");

                gridView1.UpdateCurrentRow();

                if (_calculationFactory.HasInvalidObjects())
                {
                    XtraMessageBox.Show(
                        "Er zijn regels met fouten, deze moeten eerst opgelost worden voordat er opgeslagen kan worden",
                        "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    _splashScreenManager.CloseWaitForm();

                    return;
                }

                var errorMessage = _databaseHelper.SaveChanges();

                if(!string.IsNullOrEmpty(errorMessage))
                {
                    XtraMessageBox.Show(
                        $"Opslaan mislukt, oorzaak: {errorMessage}",
                        "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    _splashScreenManager.CloseWaitForm();

                    return;
                }

                // Convert calculation to offer detail
                var startTimeConverting = DateTime.Now;

                _calculationFactory.ConvertCalculation(_calculationFactory.GetMainCalculation().Id);

                var session = _sessionFactory.GetSession();
                var calculationLogs = session.Find<CalculationLog>(x => x.DATECREATED > startTimeConverting && x.CREATOR == _sessionFactory.UserName).ToArray();

                if (calculationLogs.Any(x => x.TYPE == CalculationLogType.Error))
                {
                    foreach (var calculationLog in calculationLogs)
                    {
                        calculationLog.Calculation = _databaseHelper.Calculations.FirstOrDefault(x => x.Id == calculationLog.FK_CALCULATION);
                        if (calculationLog.CalculationRule != null)
                        {
                            calculationLog.CalculationRule.Calculation = _databaseHelper.Calculations.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_CALCULATION);
                            calculationLog.CalculationRule.SubCalculation = _databaseHelper.Calculations.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_SUBCALCULATION);
                            calculationLog.CalculationRule.Unit = _databaseHelper.Units.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_UNIT);
                            calculationLog.CalculationRule.Item = _databaseHelper.Items.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_ITEM);
                            calculationLog.CalculationRule.OutsourcedActivity = _databaseHelper.OutsourcedActivities.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_OUTSOURCED);
                            calculationLog.CalculationRule.WorkActivity = _databaseHelper.WorkActivities.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_WORKACTIVITY);
                        }
                    }

                    new CalculationLogForm(calculationLogs).ShowDialog();
                    return;
                }


                // Sla de calculatie plat na het opslaan
                var flattenCalculationWFID = new Guid("4e2325d7-3a85-467a-896f-a0ac4c0a3be8");

                var mainCalcId = _calculationFactory.GetMainCalculation().Id;

                var result = session.Sdk.ExecuteWorkflowEvent("C_CALCULATION", mainCalcId, flattenCalculationWFID);
                if (result.HasError)
                {
                    throw new ArgumentException("Er is iets fout gegaan bij het platslaan van de calculatie:\n" + result.GetResult(), nameof(flattenCalculationWFID));
                }

                gcMain.RefreshDataSource();

                UpdateBarButtons();

                _splashScreenManager.CloseWaitForm();
            };

            barButtonCopy.ItemClick += (sender, args) =>
            {
                object[] objectsToClone;

                if (gcMain.IsFocused)
                {
                    var view = gcMain.GetFocusedView();
                    objectsToClone = view.GetSelectedRows()
                        .Select(rowHandle => view.GetRow(rowHandle))
                        .Where(x => x != null)
                        .ToArray();
                }
                else if (treeListCalculations.Focused)
                {
                    objectsToClone = treeListCalculations.Selection
                        .Select(item => treeListCalculations.GetDataRecordByNode(item))
                        .Where(x => x != null)
                        .ToArray();
                }
                else return;

                _calculationFactory.CloneObjects(objectsToClone);
                UpdateBarButtons();
            };

            barButtonPaste.ItemClick += (sender, args) =>
            {
                _calculationFactory.InsertClonedObjects();
                CalculateHelper.Recalculate(_calculationFactory.MainCalculation);
                gcMain.RefreshDataSource();
            };

            barButtonCut.ItemClick += (sender, args) =>
            {
                object[] objectsToClone;

                if (gcMain.IsFocused)
                {
                    var view = gcMain.GetFocusedView();
                    objectsToClone = view.GetSelectedRows()
                        .Select(rowHandle => view.GetRow(rowHandle))
                        .Where(x => x != null)
                        .ToArray();
                }
                else if (treeListCalculations.Focused)
                {
                    objectsToClone = treeListCalculations.Selection
                        .Select(item => treeListCalculations.GetDataRecordByNode(item))
                        .Where(x => x != null)
                        .ToArray();
                }
                else return;

                _calculationFactory.CloneObjects(objectsToClone);

                foreach (var o in objectsToClone)
                    DatabaseHelper.Remove(o);
            };

            barButtonDelete.ItemClick += (sender, args) =>
            {
                if (gridView1.IsFocusedView)
                {
                    var selectedCalculationRules = gridView1.GetGridViewObjects();

                    foreach (var calculationRule in selectedCalculationRules)
                    {
                        DatabaseHelper.Remove(calculationRule);
                    }

                    return;
                }

                if (gvTailRules.IsFocusedView)
                {
                    var objects = gvTailRules.GetGridViewObjects();

                    foreach (var rule in objects)
                    {
                        DatabaseHelper.Remove(rule);
                    }

                    return;
                }

                if (gcMain.Focused)
                {
                    var selectedCalculations = treeListCalculations.GetTreeListObjects();
                    foreach (var selectedCalculation in selectedCalculations)
                        DatabaseHelper.Remove(selectedCalculation);

                    CalculateHelper.Recalculate(_calculationFactory.MainCalculation);
                }
            };

            barButtonClose.ItemClick += (sender, args) =>
            {
                Close();
            };

            barButtonUndo.ItemClick += (sender, args) =>
            {
                _databaseHelper.Undo();
                gridView1.RefreshData();
                UpdateBarButtons();
            };

            barButtonRedo.ItemClick += (sender, args) =>
            {
                _databaseHelper.Redo();
                gridView1.RefreshData();
                UpdateBarButtons();
            };

            barButtonConvert.ItemClick += (sender, args) =>
            {
                if (XtraMessageBox.Show($"Weet je zeker dat je calculatie '{_calculationFactory.GetMainCalculation().DESCRIPTION}' wilt omzetten?", "Vraag", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                try
                {
                    _splashScreenManager.ShowWaitForm("Opslaan wijzigingen");

                    var startTimeConverting = DateTime.Now;

                    _calculationFactory.ConvertCalculation(_calculationFactory.MainCalculation.Id);

                    var session = _sessionFactory.GetSession();
                    var calculationLogs = session.Find<CalculationLog>(x => x.DATECREATED > startTimeConverting && x.CREATOR == _sessionFactory.UserName).ToArray();

                    if (calculationLogs.Any(x => x.TYPE == CalculationLogType.Error))
                    {
                        foreach (var calculationLog in calculationLogs)
                        {
                            calculationLog.Calculation = _databaseHelper.Calculations.FirstOrDefault(x => x.Id == calculationLog.FK_CALCULATION);
                            if (calculationLog.CalculationRule != null)
                            {
                                calculationLog.CalculationRule.Calculation = _databaseHelper.Calculations.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_CALCULATION);
                                calculationLog.CalculationRule.SubCalculation = _databaseHelper.Calculations.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_SUBCALCULATION);
                                calculationLog.CalculationRule.Unit = _databaseHelper.Units.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_UNIT);
                                calculationLog.CalculationRule.Item = _databaseHelper.Items.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_ITEM);
                                calculationLog.CalculationRule.OutsourcedActivity = _databaseHelper.OutsourcedActivities.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_OUTSOURCED);
                                calculationLog.CalculationRule.WorkActivity = _databaseHelper.WorkActivities.FirstOrDefault(x => x.Id == calculationLog.CalculationRule.FK_WORKACTIVITY);
                            }
                        }

                        new CalculationLogForm(calculationLogs).ShowDialog();
                        return;
                    }

                    if (XtraMessageBox.Show($"De calculatie is succesvol omgezet, wilt u de stuklijst openen?", "Vraag", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    {
                        return;
                    }

                    var calculation = session.Get<Core.Models.Calculation>(_calculationFactory.GetMainCalculation().Id);
                    session.Sdk.OpenEditForm("R_ASSEMBLY", calculation.FK_ASSEMBLY.Value, null, Process.GetCurrentProcess().MainWindowHandle);
                }
                catch (Exception ex)
                {
                    new ErrorForm(ex).ShowDialog();
                }
                finally
                {
                    _splashScreenManager.CloseWaitForm();
                }
            };

            barButtonMultiSelect.Checked = gridView1.OptionsSelection.MultiSelect;
            barButtonMultiSelect.CheckedChanged += (sender, args) =>
            {
                gridView1.OptionsSelection.MultiSelect = barButtonMultiSelect.Checked;
            };
        }

        public void InitializeReportButton()
        {
            foreach (var report in _reportFactory.Reports)
            {
                var reportButton = new BarButtonItem
                {
                    Caption = report.REPORTNAME,
                    Tag = report.Id
                };
                reportButton.ItemClick += (sender, args) =>
                {
                    _splashScreenManager.ShowWaitForm("Genereren rapport");

                    if (_calculationFactory.HasChangedObjects())
                    {
                        var result = XtraMessageBox.Show("Er zijn wijzigingen, wil je deze nog opslaan voordat het rapport afgedrukt wordt?", "Vraag", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                        if (result == DialogResult.Yes)
                        {
                            _databaseHelper.SaveChanges();
                        }
                    }

                    _reportFactory.ShowReport((Guid)args.Item.Tag);

                    _splashScreenManager.CloseWaitForm();
                };

                reportMenu.AddItem(reportButton);
            }
        }

        private void InitializeTree()
        {
            treeListCalculations.GotFocus += (sender, args) =>
            {
                UpdateBarButtons();
            };
            treeListCalculations.CalcNodeDragImageIndex += (sender, args) =>
            {
                var tl = sender as TreeList;
                var dropEffect = tl.GetDragDropEffect(tl.FocusedNode, MousePosition);
                if (dropEffect == DragDropEffects.None)
                    args.ImageIndex = -1; // no icon
            };
            treeListCalculations.FocusedNodeChanged += (sender, args) =>
            {
                var focusedRecord = treeListCalculations.GetFocusedRecord();
                var focusedCalculation = focusedRecord as Core.Models.Calculation;

                if (focusedCalculation != null)
                {
                    _calculationFactory.SetMainCalculation(focusedCalculation);
                    gridView1.Focus();
                }
            };
            treeListCalculations.KeyDown += (sender, args) =>
            {
                if (args.KeyCode == Keys.Enter)
                {
                    gridView1.Focus();
                }
            };

            //var datasource = new List<Core.Models.Calculation>();
            //datasource.Add(_calculationFactory.MainCalculation);
            //datasource.AddRange(_calculationFactory.MainCalculation.CalculationRules.Where(x => x.SubCalculation != null).Select(x => x.SubCalculation));
            treeListCalculations.DataSource = _calculationFactory.DataSourceCalculations;
            treeListCalculations.ExpandAll();
        }

        private void InitializeGrid()
        {
            gcMain.BeginUpdate();
            gcMain.DataSource = _calculationFactory.DataSource;
            gcMain.EndUpdate();

            var formatRule = new GridFormatRule();
            formatRule.ApplyToRow = true;
            formatRule.Rule = new FormatConditionRuleExpression
            {
                Expression = "[SubCalculation] IS NOT NULL",
                Appearance =
                {
                    BackColor = Color.FromName("ActiveCaption")
                }
            };

            gridView1.FormatRules.Add(formatRule);
            gridView1.OptionsSelection.EnableAppearanceFocusedRow = false;

            InitializeGridColumns();

            gcMain.GotFocus += (sender, args) =>
            {
                UpdateBarButtons();
            };

            gridView1.InitNewRow += (sender, args) =>
            {
                var newAddedObject = gcMain.GetFocusedRecord();
                DatabaseHelper.Add(newAddedObject);
            };

            gridView1.FocusedRowChanged += (sender, args) =>
            {
                UpdateBarButtons();
            };

            gridView1.InvalidRowException += (sender, args) =>
            {
                args.ExceptionMode = ExceptionMode.NoAction;
            };

            gridView1.ValidateRow += (sender, args) =>
            {
                var focusedView = gcMain.GetFocusedView();
                var focusedRule = gcMain.GetFocusedRecord() as CalculationRule;

                if (focusedRule != null)
                {
                    if (focusedRule.Unit == null)
                    {
                        focusedView.SetColumnError(focusedView.Columns["Unit"], "Dit veld is verplicht");
                        args.Valid = false;
                    }

                    if (focusedRule.OutsourcedActivity?.HasMultipleSuppliers == true && focusedRule.OutsourcedActivity?.ISPUMP == true && focusedRule.AlternateUbwSupplier == null)
                    {
                        focusedView.SetColumnError(focusedView.Columns["AlternateUbwSupplier"], "Dit veld is verplicht");
                        args.Valid = false;
                    }

                    if (focusedRule.Item?.HasMultipleSuppliers == true && focusedRule.Item?.ItemGroup?.ISCONCRETE == true && focusedRule.AlternateItemSupplier == null)
                    {
                        focusedView.SetColumnError(focusedView.Columns["AlternateItemSupplier"], "Dit veld is verplicht");
                        args.Valid = false;
                    }
                }

                if (!args.Valid)
                    _calculationFactory.ObjectInvalid(focusedRule);
                else
                    _calculationFactory.ObjectValid(focusedRule);
            };

            gridView1.CustomRowCellEdit += (sender, args) =>
            {
                if (args.Column.FieldName == "AlternateItemSupplier")
                {
                    var item = gridView1.GetRowCellValue(args.RowHandle, "Item") as Item;
                    var itemSupplier = gridView1.GetRowCellValue(args.RowHandle, "AlternateItemSupplier") as ItemSupplier;
                    var itemSuppliers = _databaseHelper.ItemSuppliers.GroupBy(x => x.FK_RELATION).Select(x => x.FirstOrDefault(y => y.Id == itemSupplier?.Id) ?? x.First()).ToList();
                    if (item != null)
                    {
                        itemSuppliers = _databaseHelper.ItemSuppliers.Where(x => x.FK_ITEM == item.Id).ToList();
                    }

                    var lookUpEdit = new RepositoryItemGridLookUpEdit();
                    lookUpEdit.DataSource = itemSuppliers;
                    lookUpEdit.DisplayMember = "RelationName";
                    lookUpEdit.BestFitMode = BestFitMode.BestFitResizePopup;
                    var gvItemSupplier = new GridView();
                    gvItemSupplier.OptionsView.ShowAutoFilterRow = true;
                    gvItemSupplier.CreateColumn("Id", "Id", false);
                    gvItemSupplier.CreateColumn("RelationName", "Relatie");
                    gvItemSupplier.CreateCurrencyColumn("PURCHASEPRICE", "Inkoop prijs", false);
                    lookUpEdit.View = gvItemSupplier;
                    lookUpEdit.KeyDown += OnLookUpEditOnKeyDown;

                    args.RepositoryItem = lookUpEdit;
                }

                if (args.Column.FieldName == "AlternateUbwSupplier")
                {
                    var ubw = gridView1.GetRowCellValue(args.RowHandle, "OutsourcedActivity") as OutsourcedActivity;
                    var ubwSupplier = gridView1.GetRowCellValue(args.RowHandle, "AlternateUbwSupplier") as UbwSupplier;
                    var ubwSuppliers = _databaseHelper.UbwSuppliers.GroupBy(x => x.FK_SUPPLIER).Select(x => x.FirstOrDefault(y => y.Id == ubwSupplier?.Id) ?? x.First()).ToList();
                    if (ubw != null)
                    {
                        ubwSuppliers = _databaseHelper.UbwSuppliers.Where(x => x.FK_OUTSOURCEDACTIVITY == ubw.Id).ToList();
                    }

                    var lookUpEdit = new RepositoryItemGridLookUpEdit();
                    lookUpEdit.DataSource = ubwSuppliers;
                    lookUpEdit.DisplayMember = "RelationName";
                    lookUpEdit.BestFitMode = BestFitMode.BestFitResizePopup;
                    var gvUbwSupplier = new GridView();
                    gvUbwSupplier.OptionsView.ShowAutoFilterRow = true;
                    gvUbwSupplier.CreateColumn("Id", "Id", false);
                    gvUbwSupplier.CreateColumn("RelationName", "Relatie");
                    gvUbwSupplier.CreateCurrencyColumn("RATE", "Inkoop prijs", false);
                    lookUpEdit.View = gvUbwSupplier;
                    lookUpEdit.KeyDown += OnLookUpEditOnKeyDown;

                    args.RepositoryItem = lookUpEdit;
                }

                if (args.Column.FieldName == "Item")
                {
                    var itemSupplier = gridView1.GetRowCellValue(args.RowHandle, "AlternateItemSupplier") as ItemSupplier;
                    var items = _databaseHelper.Items;
                    if (itemSupplier != null)
                    {
                        var itemIds = _databaseHelper.ItemSuppliers.Where(x => x.FK_RELATION == itemSupplier.FK_RELATION).Select(x => x.FK_ITEM);
                        items = _databaseHelper.Items.Where(x => itemIds.Contains(x.Id)).ToList();
                    }

                    var lookUpEdit = new RepositoryItemGridLookUpEdit();
                    lookUpEdit.DataSource = items;
                    lookUpEdit.DisplayMember = "CODE";
                    lookUpEdit.BestFitMode = BestFitMode.BestFitResizePopup;
                    var gvItem = new GridView();
                    gvItem.OptionsView.ShowAutoFilterRow = true;
                    gvItem.CreateColumn("Id", "Id", false);
                    gvItem.CreateColumn("CODE", "Code");
                    gvItem.CreateColumn("DESCRIPTION", "Omschrijving");
                    gvItem.CreateColumn("ItemUnit", "Eenheid");
                    gvItem.CreateCurrencyColumn("STANDARDPURCHASEPRICE", "Inkoop prijs", false);
                    gvItem.CreateCurrencyColumn("DEFAULTSALESPRICE", "Verkoop prijs", false);
                    lookUpEdit.View = gvItem;
                    lookUpEdit.KeyDown += OnLookUpEditOnKeyDown;

                    args.RepositoryItem = lookUpEdit;
                }

                if (args.Column.FieldName == "OutsourcedActivity")
                {
                    var ubwSupplier = gridView1.GetRowCellValue(args.RowHandle, "AlternateUbwSupplier") as UbwSupplier;
                    var ubws = _databaseHelper.OutsourcedActivities;
                    if (ubwSupplier != null)
                    {
                        var ubwIds = _databaseHelper.UbwSuppliers.Where(x => x.FK_SUPPLIER == ubwSupplier.FK_SUPPLIER).Select(x => x.FK_OUTSOURCEDACTIVITY);
                        ubws = _databaseHelper.OutsourcedActivities.Where(x => ubwIds.Contains(x.Id)).ToList();
                    }

                    var lookUpEdit = new RepositoryItemGridLookUpEdit();
                    lookUpEdit.DataSource = ubws;
                    lookUpEdit.DisplayMember = "CODE";
                    lookUpEdit.BestFitMode = BestFitMode.BestFitResizePopup;
                    var gvWorkActivity = new GridView();
                    gvWorkActivity.OptionsView.ShowAutoFilterRow = true;
                    gvWorkActivity.CreateColumn(nameof(WorkActivity.Id), "Id", false);
                    gvWorkActivity.CreateColumn(nameof(WorkActivity.CODE), "Code");
                    gvWorkActivity.CreateColumn(nameof(WorkActivity.DESCRIPTION), "Omschrijving");
                    gvWorkActivity.CreateCurrencyColumn(nameof(WorkActivity.CostPrice), "Inkoop prijs");
                    gvWorkActivity.CreateCurrencyColumn(nameof(WorkActivity.SalesPrice), "Verkoop prijs");
                    lookUpEdit.View = gvWorkActivity;
                    lookUpEdit.KeyDown += OnLookUpEditOnKeyDown;

                    args.RepositoryItem = lookUpEdit;
                }
            };

            gridView1.FocusedRowChanged += GridViewColumnUpdate;

            gridView1.ShownEditor += GridViewColumnUpdate;

            gridView1.CellValueChanging += (sender, args) =>
            {
                Debug.WriteLine($"Cell value changing :{args.Column.Caption}");
                DatabaseHelper.DisableChangeTracking = false;
            };
            gridView1.CellValueChanged += (sender, args) =>
            {
                if (args.Column.Caption == "Artikel" && args.Value != null)
                {
                    var calculationRule = gcMain.GetFocusedRecord() as CalculationRule;

                    if (calculationRule != null)
                    {
                        var itemSupplier = _databaseHelper.ItemSuppliers.FirstOrDefault(x => x.FK_ITEM == ((Item)args.Value).Id && x.FK_RELATION == calculationRule.AlternateItemSupplier?.FK_RELATION);
                        if (itemSupplier != null)
                        {
                            calculationRule.AlternateItemSupplier = itemSupplier;
                            calculationRule.FK_ALTERNATEITEMSUPPLIER = itemSupplier.Id;
                        }
                    }
                }
                if (args.Column.Caption == "Ubw." && args.Value != null)
                {
                    var calculationRule = gcMain.GetFocusedRecord() as CalculationRule;

                    if (calculationRule != null)
                    {
                        var ubwSupplier = _databaseHelper.UbwSuppliers.FirstOrDefault(x => x.FK_OUTSOURCEDACTIVITY == ((OutsourcedActivity)args.Value).Id && x.FK_SUPPLIER == calculationRule.AlternateUbwSupplier?.FK_SUPPLIER);
                        if (ubwSupplier != null)
                        {
                            calculationRule.AlternateUbwSupplier = ubwSupplier;
                            calculationRule.FK_ALTERNATEUBWSUPPLIER = ubwSupplier.Id;
                        }
                    }
                }

                Debug.WriteLine($"Cell value changed :{args.Column.Caption}");
                DatabaseHelper.DisableChangeTracking = true;
            };

            gridView1.SelectionChanged += (sender, args) =>
            {
                if (gridView1.SelectedRowsCount <= 1)
                {
                    dataLayoutControl1.DataSource = _calculationFactory.MainCalculation.Totals;
                    return;
                }

                var selectedCalculationRules = gridView1.GetSelectedRows()
                    .Select(x => (CalculationRule)gridView1.GetRow(x));
                var result = new CalculationTotals();

                foreach (var selectedCalculationRule in selectedCalculationRules)
                {
                    if (selectedCalculationRule.SubCalculation == null)
                    {
                        result.TotalCostPriceHours += selectedCalculationRule.WORKACTIVITY_QUANTITYTOTAL ?? 0;
                        result.TotalCostPriceItems += selectedCalculationRule.ITEM_PRICETOTAL ?? 0;
                        result.TotalCostPriceWorkActivity += selectedCalculationRule.WORKACTIVITY_PRICETOTAL ?? 0;
                        result.TotalCostPriceOutsourced += selectedCalculationRule.OUTSOURCED_PRICETOTAL ?? 0;

                        result.TotalSalesPriceHours += selectedCalculationRule.WORKACTIVITY_QUANTITYTOTAL ?? 0;
                        result.TotalSalesPriceItems += selectedCalculationRule.ITEM_SALESPRICETOTAL ?? 0;
                        result.TotalSalesPriceWorkActivity += selectedCalculationRule.WORKACTIVITY_SALESPRICETOTAL ?? 0;
                        result.TotalSalesPriceOutsourced += selectedCalculationRule.OUTSOURCED_SALESPRICETOTAL ?? 0;

                        result.TOTALKGSTEEL += selectedCalculationRule.Item?.WEIGHT * selectedCalculationRule.ITEM_LENGTHTOTAL ?? 0;
                        result.TOTALSURFACEAREA += selectedCalculationRule.Item?.PAINTAREA * selectedCalculationRule.ITEM_LENGTHTOTAL ?? 0;
                    }
                    else
                    {
                        result.TotalCostPriceHours += selectedCalculationRule.SubCalculation.Totals.TotalCostPriceHours;
                        result.TotalCostPriceItems += selectedCalculationRule.SubCalculation.Totals.TotalCostPriceItems;
                        result.TotalCostPriceWorkActivity += selectedCalculationRule.SubCalculation.Totals.TotalCostPriceWorkActivity;
                        result.TotalCostPriceOutsourced += selectedCalculationRule.SubCalculation.Totals.TotalCostPriceOutsourced;

                        result.TotalSalesPriceHours += selectedCalculationRule.SubCalculation.Totals.TotalSalesPriceHours;
                        result.TotalSalesPriceItems += selectedCalculationRule.SubCalculation.Totals.TotalSalesPriceItems;
                        result.TotalSalesPriceWorkActivity += selectedCalculationRule.SubCalculation.Totals.TotalSalesPriceWorkActivity;
                        result.TotalSalesPriceOutsourced += selectedCalculationRule.SubCalculation.Totals.TotalSalesPriceOutsourced;

                        result.TOTALKGSTEEL += selectedCalculationRule.SubCalculation.Totals.TOTALKGSTEEL;
                        result.TOTALSURFACEAREA += selectedCalculationRule.SubCalculation.Totals.TOTALSURFACEAREA;
                    }
                }

                dataLayoutControl1.DataSource = result;
            };

            gridView1.PopupMenuShowing += (sender, e) =>
            {
                var btnInsertAbove = new DXMenuItem("Nieuwe regel boven invoegen", (o, args) =>
                {
                    var index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                    var item = new CalculationRule();
                    _calculationFactory.DataSource.Insert(index, item);
                    DatabaseHelper.Add(item);
                    _calculationFactory.ReorderDatasource();
                    gridView1.RefreshData();
                });
                btnInsertAbove.Image = imageCollection1.Images["moveup_16x16.png"];
                e.Menu.Items.Add(btnInsertAbove);

                var btnInsertBelow = new DXMenuItem("Nieuwe regel onder invoegen", (o, args) =>
                {
                    var index = gridView1.GetDataSourceRowIndex(gridView1.FocusedRowHandle);
                    var item = new CalculationRule();
                    _calculationFactory.DataSource.Insert(index + 1, item);
                    DatabaseHelper.Add(item);
                    _calculationFactory.ReorderDatasource();
                    gridView1.RefreshData();
                });
                e.Menu.Items.Add(btnInsertBelow);
                btnInsertBelow.Image = imageCollection1.Images["movedown_16x16.png"];
            };

        }

        private void GridViewColumnUpdate(object sender, EventArgs args)
        {
            gridView1.UpdateColumns();
            var richTextEdit = gridView1.ActiveEditor as RichTextEdit;
            if (richTextEdit != null)
            {
                foreach (var control in richTextEdit.Controls.Cast<RichEditControl>())
                {
                    control.PopupMenuShowing += (o, eventArgs) =>
                        {
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.IncreaseIndent);
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.DecreaseIndent);
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.ShowParagraphForm);
                            eventArgs.Menu.RemoveMenuItem(new RichEditCommandId(111));
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.CreateBookmark);
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.ShowBookmarkForm);
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.CreateHyperlink);
                            eventArgs.Menu.RemoveMenuItem(RichEditCommandId.NewCommentContentMenu);
                        };
                }
            }

            bool activeEditorHasValue = gridView1.ActiveEditor != null;

            if (activeEditorHasValue)
            {
                gridView1.ActiveEditor.Properties.ReadOnly = false;
            }

            var calculationRule = gcMain.GetFocusedRecord() as CalculationRule;

            if (calculationRule == null)
                return;

            if (calculationRule.SubCalculation != null && activeEditorHasValue)
            {
                switch (gridView1.FocusedColumn.FieldName)
                {
                    case nameof(CalculationRule.WORKACTIVITY_PRICEPERUNIT):
                    case nameof(CalculationRule.WORKACTIVITY_SALESPRICEPERUNIT):
                    case nameof(CalculationRule.WORKACTIVITY_QUANTITY):
                    case nameof(CalculationRule.OUTSOURCED_SALESPRICEPERUNIT):
                    case nameof(CalculationRule.OUTSOURCED_PRICEPERUNIT):
                    case nameof(CalculationRule.ITEM_SALESPRICEPERUNIT):
                    case nameof(CalculationRule.ITEM_PRICEPERUNIT):
                    case nameof(CalculationRule.ITEM_LENGTHPERUNIT):
                    case nameof(CalculationRule.WorkActivity):
                    case nameof(CalculationRule.OutsourcedActivity):
                    case nameof(CalculationRule.Item):
                        gridView1.ActiveEditor.Properties.ReadOnly = true;
                        break;
                }
            }

            // if selected item is surface item
            if (calculationRule.Item != null
                && calculationRule.Item.SurfaceItem
                && activeEditorHasValue)
            {
                switch (gridView1.FocusedColumn.FieldName)
                {
                    case nameof(CalculationRule.ITEM_SALESPRICEPERUNIT):
                    case nameof(CalculationRule.ITEM_PRICEPERUNIT):
                    case nameof(CalculationRule.ITEM_PRICEPERMTR):
                    case nameof(CalculationRule.ITEM_SALESPRICEPERMTR):
                        gridView1.ActiveEditor.Properties.ReadOnly = true;
                        break;
                }
            }

            // if selected item is supporter item
            if (calculationRule.Item != null
                && calculationRule.Item.SupporterItem
                && activeEditorHasValue)
            {
                switch (gridView1.FocusedColumn.FieldName)
                {
                    case nameof(CalculationRule.ITEM_WIDTHPERUNIT):
                    case nameof(CalculationRule.ITEM_LENGTHPERUNIT):
                    case nameof(CalculationRule.ITEM_PRICEPERMTR):
                    case nameof(CalculationRule.ITEM_SALESPRICEPERMTR):
                        gridView1.ActiveEditor.Properties.ReadOnly = true;
                        break;
                }
            }

            // if selected item is profile or mat or concrete steel item
            if (calculationRule.Item != null
                && (calculationRule.Item.ProfileItem
                || calculationRule.Item.MatItem
                || calculationRule.Item.ConcreteSteelItem
                || calculationRule.Item.SupporterItem)
                && !calculationRule.Item.SurfaceItem
                && !calculationRule.Item.SupporterItem
                && activeEditorHasValue)
            {
                switch (gridView1.FocusedColumn.FieldName)
                {
                    case nameof(CalculationRule.ITEM_SALESPRICEPERUNIT):
                    case nameof(CalculationRule.ITEM_PRICEPERUNIT):
                        gridView1.ActiveEditor.Properties.ReadOnly = true;
                        break;
                }
            }

            // if selected item is 'normal' item
            if (activeEditorHasValue && (calculationRule.Item == null || (calculationRule.Item != null && !calculationRule.Item.ProfileItem && !calculationRule.Item.SurfaceItem && !calculationRule.Item.MatItem && !calculationRule.Item.ConcreteSteelItem && !calculationRule.Item.SupporterItem)))
            {
                switch (gridView1.FocusedColumn.FieldName)
                {
                    case nameof(CalculationRule.ITEM_WIDTHPERUNIT):
                    case nameof(CalculationRule.ITEM_LENGTHPERUNIT):
                    case nameof(CalculationRule.ITEM_PRICEPERKG):
                    case nameof(CalculationRule.ITEM_PRICEPERMTR):
                    case nameof(CalculationRule.ITEM_SALESPRICEPERKG):
                    case nameof(CalculationRule.ITEM_SALESPRICEPERMTR):
                        gridView1.ActiveEditor.Properties.ReadOnly = true;
                        break;
                }
            }
        }


        private void InitializeGridColumns()
        {
            gridView1.BeginUpdate();
            gridView1.Columns.Clear();
            gridView1.Bands.Clear();
            Localizer.Active = new NullTextControlLocalizer();

            gcMain.ForceInitialize();

            Func<bool> costPriceFunc = () => ShowCostPrices;
            Func<bool> costPriceAndSteelFuncKg = () => ShowCostPrices && (HasProfileMaterial || HasSurfaceMaterial || HasMatMaterial || HasConcreteSteelMaterial || HasSupporterMaterial);
            Func<bool> costPriceAndSteelFuncMtr = () => ShowCostPrices && (HasProfileMaterial || HasSurfaceMaterial);
            Func<bool> salesPriceFunc = () => ShowSalesPrices;
            Func<bool> salesPriceAndSteelFuncKg = () => ShowSalesPrices && (HasProfileMaterial || HasSurfaceMaterial || HasMatMaterial || HasConcreteSteelMaterial || HasSupporterMaterial);
            Func<bool> salesPriceAndSteelFuncMtr = () => ShowSalesPrices && (HasProfileMaterial || HasSurfaceMaterial);

            var bandDefault = gridView1.Bands.Add();
            bandDefault.Visible = Properties.Settings.Default.BandGeneralVisible;
            bandDefault.Caption = BandDefault;
            bandDefault.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            bandDefault.AppearanceHeader.Font = new Font("Tahoma", 8.25F, FontStyle.Bold);
            bandDefault.Columns.Add(gridView1.CreateColumn("Id", "Id", false) as BandedGridColumn);
            bandDefault.Columns.Add(gridView1.CreateColumn("SEQUENCENUMBER", "Volgnummer", ColumnSortOrder.Ascending, false) as BandedGridColumn);


            var descriptionColumn = gridView1.Columns.Add();
            descriptionColumn.FieldName = "Description";
            descriptionColumn.Caption = "Omschrijving";
            descriptionColumn.Visible = true;
            // descriptionColumn.Width = 175;
            var descriptionColumnEdit = new RepositoryItemRichTextEdit();
            descriptionColumn.ColumnEdit = descriptionColumnEdit;
            bandDefault.Columns.Add(descriptionColumn);

            bandDefault.Columns.Add(gridView1.CreateNumberColumn("QUANTITY", "Aantal") as BandedGridColumn);

            var colUnit = gridView1.CreateColumn("Unit", "Eenh.");
            colUnit.ColumnEdit = new RepositoryItemGridLookUpEdit();
            ((RepositoryItemGridLookUpEdit)colUnit.ColumnEdit).DataSource = _databaseHelper.Units;
            ((RepositoryItemGridLookUpEdit)colUnit.ColumnEdit).DisplayMember = "CODE";
            ((RepositoryItemGridLookUpEdit)colUnit.ColumnEdit).BestFitMode = BestFitMode.BestFitResizePopup;
            var gvUnit = new GridView();
            gvUnit.OptionsView.ShowAutoFilterRow = true;
            gvUnit.CreateColumn("Id", "Id", false);
            gvUnit.CreateColumn("CODE", "Code");
            gvUnit.CreateColumn("DESCRIPTION", "Omschrijving");
            ((RepositoryItemGridLookUpEdit)colUnit.ColumnEdit).View = gvUnit;
            ((RepositoryItemGridLookUpEdit)colUnit.ColumnEdit).KeyDown += OnLookUpEditOnKeyDown;
            bandDefault.Columns.Add(colUnit as BandedGridColumn);

            var bandItem = gridView1.Bands.Add();
            bandItem.Visible = Properties.Settings.Default.BandItemVisible;
            bandItem.Caption = BandItem;
            bandItem.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            bandItem.AppearanceHeader.Font = new Font("Tahoma", 8.25F, FontStyle.Bold);
            var colItem = gridView1.CreateColumn("Item", "Artikel");
            colItem.ColumnEdit = new RepositoryItemGridLookUpEdit();
            ((RepositoryItemGridLookUpEdit)colItem.ColumnEdit).DataSource = _databaseHelper.Items;
            ((RepositoryItemGridLookUpEdit)colItem.ColumnEdit).DisplayMember = "CODE";
            ((RepositoryItemGridLookUpEdit)colItem.ColumnEdit).BestFitMode = BestFitMode.BestFitResizePopup;
            var gvItem = new GridView();
            gvItem.OptionsView.ShowAutoFilterRow = true;
            gvItem.CreateColumn("Id", "Id", false);
            gvItem.CreateColumn("CODE", "Code");
            gvItem.CreateColumn("DESCRIPTION", "Omschrijving");
            gvItem.CreateColumn("ItemUnit", "Eenheid");
            gvItem.CreateCurrencyColumn("STANDARDPURCHASEPRICE", "Inkoop prijs", false);
            gvItem.CreateCurrencyColumn("DEFAULTSALESPRICE", "Verkoop prijs", false);
            ((RepositoryItemGridLookUpEdit)colItem.ColumnEdit).View = gvItem;
            ((RepositoryItemGridLookUpEdit)colItem.ColumnEdit).KeyDown += OnLookUpEditOnKeyDown;
            bandItem.Columns.Add(colItem as BandedGridColumn);

            var colItemSupplier = gridView1.CreateColumn("AlternateItemSupplier", "Leverancier");
            colItemSupplier.ColumnEdit = new RepositoryItemGridLookUpEdit();
            ((RepositoryItemGridLookUpEdit)colItemSupplier.ColumnEdit).DataSource = _databaseHelper.ItemSuppliers.GroupBy(x => x.FK_RELATION).Select(x => x.First()).ToList();
            ((RepositoryItemGridLookUpEdit)colItemSupplier.ColumnEdit).DisplayMember = "RelationName";
            ((RepositoryItemGridLookUpEdit)colItemSupplier.ColumnEdit).BestFitMode = BestFitMode.BestFitResizePopup;
            ((RepositoryItemGridLookUpEdit)colItemSupplier.ColumnEdit).NullText = "";
            var gvItemSupplier = new GridView();
            gvItemSupplier.OptionsView.ShowAutoFilterRow = true;
            gvItemSupplier.CreateColumn("Id", "Id", false);
            gvItemSupplier.CreateColumn("RelationName", "Relatie");
            gvItemSupplier.CreateCurrencyColumn("PURCHASEPRICE", "Inkoop prijs", false);
            ((RepositoryItemGridLookUpEdit)colItemSupplier.ColumnEdit).View = gvItemSupplier;
            ((RepositoryItemGridLookUpEdit)colItemSupplier.ColumnEdit).KeyDown += OnLookUpEditOnKeyDown;
            bandItem.Columns.Add(colItemSupplier as BandedGridColumn);

            bandItem.Columns.Add(gridView1.CreateNumberColumn("ITEM_LENGTHPERUNIT", "Lengte/eenh.", true, () => (HasProfileMaterial || HasSurfaceMaterial)) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateNumberColumn("ITEM_LENGTHTOTAL", "Lengte tot.", false, () => (HasProfileMaterial || HasSurfaceMaterial)) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateNumberColumn("ITEM_WIDTHPERUNIT", "Breedte/eenh.", true, () => HasSurfaceMaterial) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateNumberColumn("ITEM_WIDTHTOTAL", "Breedte tot.", false, () => HasSurfaceMaterial) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_PRICEPERKG", "Prijs/kg", CostPriceColor, costPriceAndSteelFuncKg) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_SALESPRICEPERKG", "Verk. prijs/kg", SalesPriceColor, salesPriceAndSteelFuncKg) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_PRICEPERMTR", "Prijs/mtr", CostPriceColor, costPriceAndSteelFuncMtr) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_SALESPRICEPERMTR", "Verk. prijs/mtr", SalesPriceColor, salesPriceAndSteelFuncMtr) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_PRICEPERUNIT", "Prijs/eenh.", CostPriceColor, costPriceFunc) as BandedGridColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_SALESPRICEPERUNIT", "Verk. prijs/eenh.", SalesPriceColor, salesPriceFunc) as BandedGridColumn);
            var itemPriceTotalColumn = gridView1.CreateCurrencyColumn("ITEM_PRICETOTAL", "Prijs tot.", CostPriceColor, costPriceFunc, false) as BandedGridColumn;
            bandItem.Columns.Add(itemPriceTotalColumn);
            bandItem.Columns.Add(gridView1.CreateCurrencyColumn("ITEM_SALESPRICETOTAL", "Verk. prijs tot.", SalesPriceColor, salesPriceFunc, false) as BandedGridColumn);

            var bandWorkActivity = gridView1.Bands.Add();
            bandWorkActivity.Visible = Properties.Settings.Default.BandWorkActivityVisible;
            bandWorkActivity.Caption = BandWorkActivity;
            bandWorkActivity.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            bandWorkActivity.AppearanceHeader.Font = new Font("Tahoma", 8.25F, FontStyle.Bold);
            var colWorkActivity = gridView1.CreateColumn("WorkActivity", "Bewerking");
            colWorkActivity.ColumnEdit = new RepositoryItemGridLookUpEdit();
            ((RepositoryItemGridLookUpEdit)colWorkActivity.ColumnEdit).DataSource = _databaseHelper.WorkActivities;
            ((RepositoryItemGridLookUpEdit)colWorkActivity.ColumnEdit).DisplayMember = "CODE";
            ((RepositoryItemGridLookUpEdit)colWorkActivity.ColumnEdit).BestFitMode = BestFitMode.BestFitResizePopup;
            var gvWorkActivity = new GridView();
            gvWorkActivity.OptionsView.ShowAutoFilterRow = true;
            gvWorkActivity.CreateColumn(nameof(WorkActivity.Id), "Id", false);
            gvWorkActivity.CreateColumn(nameof(WorkActivity.CODE), "Code");
            gvWorkActivity.CreateColumn(nameof(WorkActivity.DESCRIPTION), "Omschrijving");
            gvWorkActivity.CreateCurrencyColumn(nameof(WorkActivity.CostPrice), "Inkoop prijs");
            gvWorkActivity.CreateCurrencyColumn(nameof(WorkActivity.SalesPrice), "Verkoop prijs");
            ((RepositoryItemGridLookUpEdit)colWorkActivity.ColumnEdit).View = gvWorkActivity;
            ((RepositoryItemGridLookUpEdit)colWorkActivity.ColumnEdit).KeyDown += OnLookUpEditOnKeyDown;
            bandWorkActivity.Columns.Add(colWorkActivity as BandedGridColumn);
            bandWorkActivity.Columns.Add(gridView1.CreateNumberColumn("WORKACTIVITY_QUANTITY", "Mu/eenh.") as BandedGridColumn);
            bandWorkActivity.Columns.Add(gridView1.CreateNumberColumn("WORKACTIVITY_QUANTITYTOTAL", "Mu/tot.", false) as BandedGridColumn);
            bandWorkActivity.Columns.Add(gridView1.CreateCurrencyColumn("WORKACTIVITY_PRICEPERUNIT", "Prijs/eenh.", CostPriceColor, costPriceFunc) as BandedGridColumn);
            bandWorkActivity.Columns.Add(gridView1.CreateCurrencyColumn("WORKACTIVITY_SALESPRICEPERUNIT", "Verk. prijs/eenh.", SalesPriceColor, salesPriceFunc) as BandedGridColumn);
            bandWorkActivity.Columns.Add(gridView1.CreateCurrencyColumn("WORKACTIVITY_PRICETOTAL", "Arbeid tot.", CostPriceColor, costPriceFunc, false) as BandedGridColumn);
            bandWorkActivity.Columns.Add(gridView1.CreateCurrencyColumn("WORKACTIVITY_SALESPRICETOTAL", "Arbeid tot.", SalesPriceColor, salesPriceFunc,
                false) as BandedGridColumn);

            var bandOutsourced = gridView1.Bands.Add();
            bandOutsourced.Visible = Properties.Settings.Default.BandOutsourcedVisible;
            bandOutsourced.Caption = BandOutsourced;
            bandOutsourced.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            bandOutsourced.AppearanceHeader.Font = new Font("Tahoma", 8.25F, FontStyle.Bold);
            var colOutsourced = gridView1.CreateColumn("OutsourcedActivity", "Ubw.");
            colOutsourced.ColumnEdit = new RepositoryItemGridLookUpEdit();
            ((RepositoryItemGridLookUpEdit)colOutsourced.ColumnEdit).DataSource = _databaseHelper.OutsourcedActivities;
            ((RepositoryItemGridLookUpEdit)colOutsourced.ColumnEdit).DisplayMember = "CODE";
            ((RepositoryItemGridLookUpEdit)colOutsourced.ColumnEdit).BestFitMode = BestFitMode.BestFitResizePopup;
            var gvOutsourced = new GridView();
            gvOutsourced.OptionsView.ShowAutoFilterRow = true;
            gvOutsourced.CreateColumn(nameof(OutsourcedActivity.Id), "Id", false);
            gvOutsourced.CreateColumn(nameof(OutsourcedActivity.CODE), "Code");
            gvOutsourced.CreateColumn(nameof(OutsourcedActivity.DESCRIPTION), "Omschrijving");
            gvOutsourced.CreateColumn(nameof(OutsourcedActivity.Unit), "Eenheid");
            gvOutsourced.CreateCurrencyColumn(nameof(OutsourcedActivity.PURCHASEPRICE), "Inkoop prijs");
            gvOutsourced.CreateCurrencyColumn(nameof(OutsourcedActivity.SALESPRICE), "Verkoop prijs");
            ((RepositoryItemGridLookUpEdit)colOutsourced.ColumnEdit).View = gvOutsourced;
            ((RepositoryItemGridLookUpEdit)colOutsourced.ColumnEdit).KeyDown += OnLookUpEditOnKeyDown;
            bandOutsourced.Columns.Add(colOutsourced as BandedGridColumn);

            var colUbwSupplier = gridView1.CreateColumn("AlternateUbwSupplier", "Leverancier");
            colUbwSupplier.ColumnEdit = new RepositoryItemGridLookUpEdit();
            ((RepositoryItemGridLookUpEdit)colUbwSupplier.ColumnEdit).DataSource = _databaseHelper.UbwSuppliers.GroupBy(x => x.FK_SUPPLIER).Select(x => x.First()).ToList();
            ((RepositoryItemGridLookUpEdit)colUbwSupplier.ColumnEdit).DisplayMember = "RelationName";
            ((RepositoryItemGridLookUpEdit)colUbwSupplier.ColumnEdit).BestFitMode = BestFitMode.BestFitResizePopup;
            var gvUbwSupplier = new GridView();
            gvUbwSupplier.OptionsView.ShowAutoFilterRow = true;
            gvUbwSupplier.CreateColumn("Id", "Id", false);
            gvUbwSupplier.CreateColumn("RelationName", "Relatie");
            gvUbwSupplier.CreateCurrencyColumn("RATE", "Inkoop prijs", false);
            ((RepositoryItemGridLookUpEdit)colUbwSupplier.ColumnEdit).View = gvUbwSupplier;
            ((RepositoryItemGridLookUpEdit)colUbwSupplier.ColumnEdit).KeyDown += OnLookUpEditOnKeyDown;
            bandOutsourced.Columns.Add(colUbwSupplier as BandedGridColumn);

            bandOutsourced.Columns.Add(gridView1.CreateCurrencyColumn("OUTSOURCED_PRICEPERUNIT", "Ubw/prijs per eenheid", CostPriceColor, costPriceFunc) as BandedGridColumn);
            bandOutsourced.Columns.Add(gridView1.CreateCurrencyColumn("OUTSOURCED_SALESPRICEPERUNIT", "Ubw/ verk. prijs per eenheid", SalesPriceColor,salesPriceFunc) as BandedGridColumn);
            bandOutsourced.Columns.Add(gridView1.CreateCurrencyColumn("OUTSOURCED_PRICETOTAL", "Ubw/totaal", CostPriceColor, costPriceFunc, false) as BandedGridColumn);
            bandOutsourced.Columns.Add(gridView1.CreateCurrencyColumn("OUTSOURCED_SALESPRICETOTAL", "Ubw/totaal", SalesPriceColor, salesPriceFunc, false) as BandedGridColumn);

            var bandTotals = gridView1.Bands.Add();
            bandTotals.Visible = Properties.Settings.Default.BandTotalsVisible;
            bandTotals.Caption = BandTotals;
            bandTotals.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
            bandTotals.AppearanceHeader.Font = new Font("Tahoma", 8.25F, FontStyle.Bold);
            bandTotals.Columns.Add(gridView1.CreateCurrencyColumn("TOTALPRICEPERUNIT", "Totaal/eenh.", CostPriceColor, costPriceFunc, false) as BandedGridColumn);
            bandTotals.Columns.Add(gridView1.CreateCurrencyColumn("TOTALSALESPRICEPERUNIT", "Totaal verk. prijs/eenh.", SalesPriceColor,salesPriceFunc, false) as BandedGridColumn);
            bandTotals.Columns.Add(gridView1.CreateCurrencyColumn("PRICETOTAL", "Totaal", CostPriceColor, costPriceFunc, false) as BandedGridColumn);
            bandTotals.Columns.Add(gridView1.CreateCurrencyColumn("SALESPRICETOTAL", "Totaal", SalesPriceColor, salesPriceFunc, false) as BandedGridColumn);

            gridView1.OptionsView.ShowGroupExpandCollapseButtons = true;
            gridView1.OptionsDetail.EnableMasterViewMode = true;
            gridView1.OptionsDetail.AllowZoomDetail = true;

            gridView1.DoubleClick += (sender, args) =>
            {
                var view = (GridView)sender;
                var pt = view.GridControl.PointToClient(MousePosition);
                var info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell)
                {
                    var calculationRule = gridView1.GetRow(info.RowHandle) as CalculationRule;
                    if (calculationRule?.FK_SUBCALCULATION == null)
                        return;

                    _calculationFactory.SetMainCalculation(calculationRule.SubCalculation);
                    gridView1.UpdateColumns();
                }
            };

            descriptionColumn.BestFit();
            gridView1.EndUpdate();

            edtColumnFilter.Properties.SelectAllItemVisible = false;

            foreach (GridBand band in gridView1.Bands)
            {
                edtColumnFilter.Properties.Items.Add(band, band.Caption, band.Visible ? CheckState.Checked : CheckState.Unchecked, true);
            }

            edtColumnFilter.ItemCheck += (sender, args) =>
            {
                var checkedListBoxItem = edtColumnFilter.Properties.Items.ElementAt(args.Index);
                if (checkedListBoxItem.Value is GridBand band)
                {
                    band.Visible = args.State == CheckState.Checked;
                }
            };
        }

        private static void InitializeGridControl(GridControl gridControl, TreeList treelist, CalculationFactory calculationFactory)
        {
            gridControl.KeyDown += (sender, args) =>
            {
                var gridView = gridControl.GetFocusedView();

                if (args.Control && args.KeyCode == Keys.Enter)
                {
                    treelist.Focus();
                    args.Handled = true;
                }
                if (args.KeyCode == Keys.Escape && gridView.FocusedRowHandle == GridControl.NewItemRowHandle)
                {
                    var rule = gridControl.GetFocusedRecord();
                    calculationFactory.CancelObjectAdded(rule);
                }
            };

            gridControl.EditorKeyDown += (sender, args) =>
            {
                if (args.Control && args.KeyCode == Keys.Enter)
                {
                    treelist.Focus();
                    args.Handled = true;
                }
            };
        }

        private void InitializeTailRulesGrid()
        {
            gcTailRules.BeginUpdate();
            gcTailRules.DataSource = _calculationFactory.DataSourceTailRules;
            gcTailRules.EndUpdate();

            gvTailRules.BeginUpdate();
            gvTailRules.Columns.Clear();

            gcMain.ForceInitialize();

            gvTailRules.CreateColumn("Id", "Id", false);
            gvTailRules.CreateColumn("SequenceNumber", "Volgnummer", ColumnSortOrder.Ascending, false);
            gvTailRules.CreateColumn("Description", "Omschrijving");
            gvTailRules.CreateCurrencyColumn("Amount", "Bedrag");
            gvTailRules.CreateNumberColumn("Percentage", "Percentage");
            gvTailRules.CreateCurrencyColumn("Total", "Totaal", false);

            gvTailRules.EndUpdate();

            gcTailRules.GotFocus += (sender, args) =>
            {
                UpdateBarButtons();
            };

            gvTailRules.InitNewRow += (sender, args) =>
            {
                var newAddedObject = gcTailRules.GetFocusedRecord();
                DatabaseHelper.Add(newAddedObject);
            };

            gvTailRules.FocusedRowChanged += (sender, args) =>
            {
                UpdateBarButtons();
            };

            gvTailRules.InvalidRowException += (sender, args) =>
            {
                args.ExceptionMode = ExceptionMode.NoAction;
            };

            gvTailRules.ValidateRow += (sender, args) =>
            {
                var focusedView = gcTailRules.GetFocusedView();
                var focusedRule = gcTailRules.GetFocusedRecord() as CalculationTailRule;
                args.Valid = true;

                if (focusedRule != null)
                {
                    if (focusedRule.Amount != 0 && focusedRule.Percentage != 0)
                    {
                        focusedView.SetColumnError(focusedView.Columns["Amount"], "Er kan maar één veld ingevuld zijn");
                        focusedView.SetColumnError(focusedView.Columns["Percentage"], "Er kan maar één veld ingevuld zijn");

                        args.Valid = false;
                    }
                }
            };
        }

        private void OnLookUpEditOnKeyDown(object editSender, KeyEventArgs editArgs)
        {
            if (editArgs.KeyCode == Keys.Delete
                && editSender is GridLookUpEdit)
            {
                ((GridLookUpEdit)editSender).EditValue = null;
            }
        }

        private void barButtonOpen_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var session = _sessionFactory.GetSession())
            {
                var newCalculationId = session.Sdk.OpenBrowseFormWithFilter("C_CALCULATION", "PK_C_CALCULATION", "FK_MAINCALCULATION IS NULL", Handle) as int? ?? 0;

                if (newCalculationId <= 0)
                {
                    return;
                }

                OpenCalculation(newCalculationId);
            }
        }

        private void OpenCalculation(int newCalculationId)
        {
            try
            {
                if (_calculationFactory.HasChangedObjects())
                {
                    var result = XtraMessageBox.Show("Er zijn wijzigingen, wil je deze nog opslaan?", "Vraag", MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                    if (result == DialogResult.Yes)
                    {
                        // if (args.Cancel)
                        // {
                        //     XtraMessageBox.Show("Er zijn fouten, deze moeten eerst opgelost worden voordat er opgeslagen kan worden.", "Fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //     return;
                        // }

                        _splashScreenManager.ShowWaitForm("Opslaan wijzigingen");

                        _databaseHelper.SaveChanges();

                        _splashScreenManager.CloseWaitForm();
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        return;
                    }
                }

                SaveSettings();
                _calculationFactory.UnLockCalculation();

                _splashScreenManager.ShowWaitForm("Calculatie laden...");

                _argumentHelper.CalculationId = newCalculationId;
                _calculationFactory.LockCalculation(newCalculationId);
                _databaseHelper.LoadCalculation(_sessionFactory, newCalculationId);
                _calculationFactory.MainCalculation = null;
                _calculationFactory.SetMainCalculation(_databaseHelper.Calculations.First(x => x.FK_MAINCALCULATION == null));
                treeListCalculations.ExpandAll();
                UpdateBarButtons();
            }
            finally
            {
                _splashScreenManager.CloseWaitForm();
            }
        }

        private void dragDropEventsGridView_DragDrop(object sender, DevExpress.Utils.DragDrop.DragDropEventArgs e)
        {
            if (e.Action == DragDropActions.None)
                return;

            var hitPoint = gcMain.PointToClient(Cursor.Position);
            var hitInfo = gridView1.CalcHitInfo(hitPoint);
            var targetRowHandle = hitInfo.RowHandle;
            var targetRow = gridView1.GetRow(targetRowHandle) as CalculationRule;
            var targetRowIndex = gridView1.GetDataSourceRowIndex(targetRowHandle);

            var sourceTable = gcMain.DataSource as BindingList<CalculationRule>;
            var sourceHandles = e.GetData<int[]>();

            var draggedRows = sourceHandles
                .Select(sourceHandle => gridView1.GetDataSourceRowIndex(sourceHandle))
                .Select(oldRowIndex => sourceTable[oldRowIndex])
                .ToList();

            int newRowIndex;

            switch (e.InsertType)
            {
                case DevExpress.Utils.DragDrop.InsertType.Before:
                    newRowIndex = targetRowIndex > sourceHandles[sourceHandles.Length - 1] ? targetRowIndex - 1 : targetRowIndex;
                    for (var i = draggedRows.Count - 1; i >= 0; i--) {
                        var oldRow = draggedRows[i];
                        _calculationFactory.DataSource.Remove(oldRow);
                        _calculationFactory.DataSource.Insert(newRowIndex, oldRow);
                    }

                    break;
                case DevExpress.Utils.DragDrop.InsertType.After:
                    newRowIndex = targetRowIndex < sourceHandles[0] ? targetRowIndex + 1 : targetRowIndex;

                    foreach (var oldRow in draggedRows)
                    {
                        _calculationFactory.DataSource.Remove(oldRow);
                        _calculationFactory.DataSource.Insert(newRowIndex, oldRow);
                    }

                    break;
                default:
                    newRowIndex = -1;
                    break;
            }

            _calculationFactory.ReorderDatasource();

            // var insertedIndex = gridView1.GetRowHandle(newRowIndex);
            // gridView1.FocusedRowHandle = insertedIndex;
            // gridView1.SelectRow(gridView1.FocusedRowHandle);
        }

        private void dragDropEventsGridView_DragOver(object sender, DragOverEventArgs e)
        {
            DragOverGridEventArgs args = DragOverGridEventArgs.GetDragOverGridEventArgs(e);
            e.InsertType = args.InsertType;
            e.InsertIndicatorLocation = args.InsertIndicatorLocation;
            e.Action = args.Action;
            Cursor.Current = args.Cursor;
            args.Handled = true;
        }
    }
}
