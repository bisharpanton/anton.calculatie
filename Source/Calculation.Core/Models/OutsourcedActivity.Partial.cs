﻿using System.Collections.Generic;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class OutsourcedActivity
    {
        [ForeignKey("FK_UNIT"), Include]
        public Unit Unit { get; set; }

        private double? _SALESPRICE;
        public double? SALESPRICE => PURCHASEPRICE * (SALESMARKUP + 1);

        public override string ToString()
        {
            return CODE;
        }

        [HasMany("FK_OUTSOURCEDACTIVITY"), Include]
        public List<UbwSupplier> UbwSuppliers { get; set; }

        public bool HasMultipleSuppliers => UbwSuppliers != null && UbwSuppliers.Count > 1;
    }
}