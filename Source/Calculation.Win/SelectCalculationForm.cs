﻿using System.Linq;
using System.Windows.Forms;
using Calculation.Core.Classes;
using Calculation.Core.Classes.Calculation;
using Calculation.Core.Extensions;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace Calculation.Win
{
    public partial class SelectCalculationForm : DevExpress.XtraEditors.XtraForm
    {
        public Core.Models.Calculation Result { get; set; }

        public SelectCalculationForm(CalculationFactory calculationFactory, DatabaseHelper databaseHelper)
        {
            InitializeComponent();
            edtDescription.Visible = true;
            label1.Visible = true;

            gcMain.DataSource = databaseHelper.Calculations.Where(x => x.Id != calculationFactory.MainCalculation.Id);

            this.KeyUp += OnKeyUp;
            gcMain.KeyUp += OnKeyUp;
            gvMain.DoubleClick += (sender, args) =>
            {
                var view = (GridView)sender;
                var selectedCalculation = view.GridControl.GetFocusedRecord() as Core.Models.Calculation;
                var pt = view.GridControl.PointToClient(MousePosition);
                var info = view.CalcHitInfo(pt);
                if (info.InRow || info.InRowCell || selectedCalculation != null)
                {
                    if (selectedCalculation == null)
                    {
                        selectedCalculation = (Core.Models.Calculation)gvMain.GetRow(info.RowHandle);
                    }

                    Result = selectedCalculation;

                    if (Result == null)
                    {
                        return;
                    }

                    DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
            };

            edtDescription.KeyUp += (sender, args) =>
            {
                if (args.KeyCode == Keys.Escape)
                {
                    this.Close();
                    return;
                }
                if (args.KeyCode == Keys.Enter && !string.IsNullOrEmpty(edtDescription.Text))
                {
                    var calculation = DatabaseHelper.Create<Core.Models.Calculation>();
                    calculation.DESCRIPTION = edtDescription.Text;
                    Result = calculation;
                    DialogResult = DialogResult.OK;
                    this.Close();
                    return;
                }
            };
        }

        private void OnKeyUp(object sender, KeyEventArgs keyEventArgs)
        {
            if (keyEventArgs.KeyCode == Keys.Escape)
            {
                this.Close();
                return;
            }
            if (keyEventArgs.KeyCode == Keys.Enter)
            {
                Result = gcMain.GetFocusedRecord() as Core.Models.Calculation;

                if (Result == null)
                    return;

                DialogResult = DialogResult.OK;
                this.Close();
                return;
            }
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            var selectedCalculation = gcMain.GetFocusedRecord() as Core.Models.Calculation;

            if (string.IsNullOrEmpty(edtDescription.Text) && selectedCalculation == null)
            {
                var message = !edtDescription.Visible
                    ? "Selecteer een calculatie"
                    : "Voer een omschrijving in of selecteer een bestaande calculatie.";
                XtraMessageBox.Show(message, 
                    "Ridder iQ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            if (selectedCalculation != null)
            {
                Result = selectedCalculation;

                if (Result == null)
                {
                    return;
                }

                DialogResult = DialogResult.OK;
                this.Close();
                return;
            }

            var calculation = DatabaseHelper.Create<Core.Models.Calculation>();
            calculation.DESCRIPTION = edtDescription.Text;
            Result = calculation;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void SelectCalculationForm_Load(object sender, System.EventArgs e)
        {
            gvMain.FocusedRowHandle = GridControl.AutoFilterRowHandle; //Eliminate that default the first row is selected
        }
    }
}