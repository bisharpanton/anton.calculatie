﻿namespace Calculation.Core.Classes
{
    public interface ICommand
    {
        void Execute();
        void UnExecute();
    }
}