﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Interfaces
{
    public interface ISessionFactory
    {
        string UserName { get; }
        string Password { get; set; }
        string CompanyName { get; }
        SdkConfiguration GetSettings();
        SdkSession GetSession();
        ISessionLoginResult Login(string userName, string companyName, string password);
        bool TryToLoginWithGivenCompanyAndUser(string password);
    }
}