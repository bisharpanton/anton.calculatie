﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    [Table("C_TAILRULES")]
    public partial class TailRule
    {
        [PrimaryKey]
        public int Id { get; set; }

        [Column("PERCENTAGE")]
        public double Percentage { get; set; }

        [Column("AMOUNT")]
        public double Amount { get; set; }

        [Column("DESCRIPTION")]
        public string Description { get; set; }
    }
}