using System;
using System.Windows.Forms;

namespace Calculation.Win
{
    public class BusyHelper<T> : BusyHelper
        where T : Control
    {
        private readonly T control;

        private readonly Action<T> before;

        private readonly Action<T> after;

        public BusyHelper(T control, Action<T> before, Action<T> after)
        {
            this.control = control;
            this.before = before;
            this.after = after;
        }

        protected override IDisposable GetBusy()
        {
            if (before != null)
            {
                if (control.InvokeRequired)
                {
                    control.Invoke(before, control);
                }
                else
                {
                    before(control);
                }
            }

            return new Waiter(control, after);
        }

        private class Waiter : IDisposable
        {
            private readonly T control;

            private readonly Action<T> after;

            public Waiter(T control, Action<T> after)
            {
                this.control = control;
                this.after = after;
            }

            public void Dispose()
            {
                if (after != null)
                {
                    if (control.InvokeRequired)
                    {
                        control.Invoke(after, control);
                    }
                    else
                    {
                        after(control);
                    }
                }
            }            
        }
    }

    public abstract class BusyHelper
    {
        public static BusyHelper<T> Create<T>(T control, Action<T> before, Action<T> after)
            where T : Form
        {
            return new BusyHelper<T>(control, before, after);
        }

        public IDisposable Busy()
        {
            return GetBusy();
        }

        protected abstract IDisposable GetBusy();
    }
}