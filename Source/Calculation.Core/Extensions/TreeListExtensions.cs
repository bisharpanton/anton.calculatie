﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Calculation.Core.Classes.Base;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;

namespace Calculation.Core.Extensions
{
    public static class TreeListExtensions
    {
        public static IDatabaseObject GetFocusedRecord(this TreeList treeList)
        {
            var record = treeList.GetDataRecordByNode(treeList.FocusedNode);
            return (IDatabaseObject) record;
        }

        public static DragDropEffects GetDragDropEffect(this TreeList tl, TreeListNode dragNode, Point mousePosition)
        {
            return tl.CalcHitInfo(tl.PointToClient(mousePosition)).Node == null ? DragDropEffects.None : DragDropEffects.All;
        }

        public static IEnumerable<object> GetTreeListObjects(this TreeList tl)
        {
            if (tl.Selection.Count <= 0)
            {
                return new[]
                {
                    tl.FocusedNode
                };
            }

            var result = tl.Selection
                .Select(tl.GetDataRecordByNode)
                .Where(x => x != null)
                .ToArray();

            return result;
        }
    }
}