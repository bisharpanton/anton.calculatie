﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Classes
{
    public static class CloneHelper
    {
        public static object CloneObject(object objectToClone)
        {
            //step : 1 Get the type of source object and create a new instance of that type
            Type typeSource = objectToClone.GetType();
            object objTarget = Activator.CreateInstance(typeSource);

            //Step2 : Get all the properties of source object type
            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Step : 3 Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //Step : 4 check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsClass || property.PropertyType.IsEnum || property.PropertyType == typeof(string))
                    {
                        if (property.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(PrimaryKeyAttribute)) != null)
                            continue;

                        property.SetValue(objTarget, property.GetValue(objectToClone, null), null);
                    }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    else
                    {
                        object objPropertyValue = property.GetValue(objectToClone, null);
                        if (objPropertyValue == null)
                        {
                            property.SetValue(objTarget, null, null);
                        }
                        else
                        {
                            //property.SetValue(objTarget, CloneObject(objPropertyValue), null);
                        }
                    }
                }
            }

            return objTarget;
        }

        public static IEnumerable<object> CloneObjects(IEnumerable<object> objectsToClone)
        {
            var result = new List<object>();
            foreach (var objectToClone in objectsToClone)
            {
                result.Add(CloneObject(objectToClone));
            }
            return result;
        }
    }
}