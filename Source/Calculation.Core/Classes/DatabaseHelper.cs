﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Calculation.Core.Classes.Base;
using Calculation.Core.Interfaces;
using Calculation.Core.Models;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Classes
{
    public class DatabaseHelper
    {
        private readonly ISessionFactory _sessionFactory;
        public IEnumerable<Unit> Units { get; set; }
        public IEnumerable<Item> Items { get; set; }
        public IEnumerable<ItemSupplier> ItemSuppliers { get; set; }
        public IEnumerable<UbwSupplier> UbwSuppliers { get; set; }
        public IEnumerable<WorkActivity> WorkActivities { get; set; }
        public IEnumerable<OutsourcedActivity> OutsourcedActivities { get; set; }
        public IList<Models.Calculation> Calculations { get; set; }
        public IEnumerable<CalculationRule> CalculationRules { get; set; }
        public IEnumerable<CalculationTotals> CalculationTotals { get; set; }
        public IEnumerable<CalculationTailRule> CalculationTailRules { get; set; }
        public IEnumerable<TailRule> TailRules { get; set; }

        public static bool IsLoading;

        public static List<DatabaseObjectState> DatabaseObjectStates { get; } = new List<DatabaseObjectState>();
        public static Stack<ICommand> UndoStack { get; set; } = new Stack<ICommand>();
        public static Stack<ICommand> RedoStack { get; set; } = new Stack<ICommand>();

        public DatabaseHelper(ISessionFactory sessionFactory, IArgumentHelper argumentHelper)
        {
            IsLoading = true;
            _sessionFactory = sessionFactory;

            LoadDefaultData(sessionFactory);
            LoadCalculation(sessionFactory, argumentHelper.CalculationId);

            ObjectAdded += (sender, args) =>
            {
                if (args.Object is Models.Calculation)
                {
                    var calculation = (Models.Calculation)args.Object;
                    var newId = Calculations.Min(x => x.Id) - 1;
                    calculation.Id = newId >= 0 ? -1 : newId;
                    Calculations.Add(calculation);
                }

                if (args.Object is CalculationRule)
                {
                    var calculationRule = (CalculationRule)args.Object;
                    if (calculationRule.QUANTITY < 0)
                    {
                        calculationRule.QUANTITY = 0;
                    }

                    if (!calculationRule.FK_UNIT.HasValue)
                    {
                        calculationRule.Unit = Units.FirstOrDefault(x => x.CODE.Equals("STUKS", StringComparison.OrdinalIgnoreCase));
                    }
                    else
                    {
                        calculationRule.Unit = Units.FirstOrDefault(x => x.Id == calculationRule.FK_UNIT);
                    }

                    if (calculationRule.FK_ITEM.HasValue)
                    {
                        calculationRule.Item = Items.FirstOrDefault(x => x.Id == calculationRule.FK_ITEM);
                    }
                    if (calculationRule.FK_OUTSOURCED.HasValue)
                    {
                        calculationRule.OutsourcedActivity = OutsourcedActivities.FirstOrDefault(x => x.Id == calculationRule.FK_OUTSOURCED);
                    }
                    if (calculationRule.FK_WORKACTIVITY.HasValue)
                    {
                        calculationRule.WorkActivity = WorkActivities.FirstOrDefault(x => x.Id == calculationRule.FK_WORKACTIVITY);
                    }
                }

                var databaseObject = args.Object as IDatabaseObject;
                if (databaseObject != null && !DisableChangeTracking)
                {
                    AddCommandToUndoStack(new ObjectCreated(databaseObject));
                    Debug.WriteLine($"Object created added: {databaseObject.GetType()} UndoStackCount: {UndoStack.Count}");
                }
            };
            ObjectChanged += (sender, args) =>
            {
                var databaseObject = args.DatabaseDatabaseObject;
                if (databaseObject != null && !DisableChangeTracking)
                {
                    AddCommandToUndoStack(new ObjectChanged(args));
                    Debug.WriteLine($"Object changed added: {args.PropertyName} UndoStackCount: {UndoStack.Count}");
                }
            };
            ObjectDeleted += (sender, args) =>
            {
                //if (args.Object is Models.Calculation)
                //{
                //    var calculation = (Models.Calculation)args.Object;
                //    Calculations.Remove(calculation);
                //}

                var databaseObject = args.Object as IDatabaseObject;
                if (databaseObject != null && !DisableChangeTracking)
                {
                    AddCommandToUndoStack(new ObjectDeleted(databaseObject));
                }
            };
            IsLoading = false;
        }

        private void LoadDefaultData(ISessionFactory sessionFactory)
        {
            using (var session = sessionFactory.GetSession())
            {
                var startTime = Stopwatch.GetTimestamp();

                var rsAssemblySettings = session.Sdk.CreateRecordset("R_ASSEMBLYSETTINGS", "FK_DEFAULTSEMIMANUFACTURE", "", "");
                rsAssemblySettings.MoveFirst();
                var semiItemId = rsAssemblySettings.Fields["FK_DEFAULTSEMIMANUFACTURE"].Value as int? ?? 0;
                var semiItem = session.Find<Item>(x => x.Id == semiItemId).First();
                var fkItemUnit = semiItem.FK_ITEMUNIT;
                Debug.Print($"FK_DEFAULTSEMIMANUFACTURE TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                Items = session.Find<Item>(x => !x.UNMARKETABLE && x.FK_ITEMUNIT != fkItemUnit).ToArray();
                Debug.Print($"Items TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                ItemSuppliers = Items.Where(x => x.ItemSuppliers != null).SelectMany(x => x.ItemSuppliers).ToArray();
                Debug.Print($"ItemSuppliers TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                WorkActivities = session.Find<WorkActivity>(x => x.WORKACTIVITYTYPE == WorkActivityType.Direct_hours && !x.INACTIVE).ToArray();
                Debug.Print($"WorkActivities TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                OutsourcedActivities = session.Find<OutsourcedActivity>(x => !x.UNMARKETABLE).ToArray();
                Debug.Print($"OutsourcedActivities TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                var ubwIds = OutsourcedActivities.Select(i => i.Id).ToList();
                UbwSuppliers = session.Find<UbwSupplier>(x => ubwIds.Contains(x.FK_OUTSOURCEDACTIVITY)).ToArray();
                Debug.Print($"UbwSuppliers TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                Units = session.Find<Unit>().ToArray();
                Debug.Print($"Units TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");

                TailRules = session.Find<TailRule>().ToList();
                Debug.Print($"TailRules TOOK: {TimeSpan.FromTicks(Stopwatch.GetTimestamp() - startTime).TotalMilliseconds}ms");
            }
        }

        public void LoadCalculation(ISessionFactory sessionFactory, int calculationId)
        {
            using (var session = sessionFactory.GetSession())
            {
                var cals = session.Find<Models.Calculation>(x => x.Id == calculationId).ToList();
                cals.AddRange(GetAllCalculations(session, calculationId));
                Calculations = cals;

                var allCalculationIds = cals.Select(x => x.Id).Distinct().ToArray();
                var allCalculationTotalIds = cals.Where(x => x.FK_CALCULATIONTOTAL != null)
                    .Select(x => x.FK_CALCULATIONTOTAL.Value).Distinct().ToArray();
                CalculationRules = session.Find<CalculationRule>(x => allCalculationIds.Contains(x.FK_CALCULATION))
                    .OrderBy(x => x.SEQUENCENUMBER).ToList();
                CalculationTotals = session.Find<CalculationTotals>(x => allCalculationTotalIds.Contains(x.Id)).ToList();
                CalculationTailRules = session.Find<CalculationTailRule>(x => allCalculationIds.Contains(x.CalculationId)).ToList();

                DatabaseObjectStates.Clear();
                DatabaseObjectStates.AddRange(Calculations.Select(x => new DatabaseObjectState(x)));
                DatabaseObjectStates.AddRange(CalculationRules.Select(x => new DatabaseObjectState(x)));
                DatabaseObjectStates.AddRange(CalculationTotals.Select(x => new DatabaseObjectState(x)));
                DatabaseObjectStates.AddRange(CalculationTailRules.Select(x => new DatabaseObjectState(x)));

                UndoStack.Clear();
                RedoStack.Clear();

                foreach (var calculation in Calculations)
                {
                    calculation.DisableDataChanges = true;
                    calculation.Unit = Units.FirstOrDefault(x => x.Id == calculation.FK_UNIT);
                    calculation.MainCalculation = Calculations.FirstOrDefault(x => x.Id == calculation.FK_MAINCALCULATION);
                    calculation.Calculations = Calculations
                        .Where(x => x.FK_MAINCALCULATION != null && x.FK_MAINCALCULATION == calculation.Id).ToList();
                    calculation.CalculationRules = CalculationRules.Where(x => x.FK_CALCULATION == calculation.Id).ToList();
                    calculation.CalculationTailRules =
                        CalculationTailRules.Where(x => x.CalculationId == calculation.Id).ToList();

                    foreach (var calculationTailRule in calculation.CalculationTailRules)
                    {
                        calculationTailRule.Calculation = calculation;
                    }

                    calculation.Totals = CalculationTotals.FirstOrDefault(x => x.Id == calculation.FK_CALCULATIONTOTAL);

                    if (calculation.Totals == null)
                        calculation.Totals = Create<CalculationTotals>();

                    if (calculation.Totals != null)
                        calculation.Totals.Calculation = calculation;

                    calculation.DisableDataChanges = false;
                }

                foreach (var calculationRule in Calculations.SelectMany(x => x.CalculationRules))
                {
                    calculationRule.DisableDataChanges = true;
                    calculationRule.Item = Items.FirstOrDefault(x => x.Id == calculationRule.FK_ITEM);
                    calculationRule.AlternateItemSupplier =
                        ItemSuppliers.FirstOrDefault(x => x.Id == calculationRule.FK_ALTERNATEITEMSUPPLIER);
                    calculationRule.AlternateUbwSupplier =
                        UbwSuppliers.FirstOrDefault(x => x.Id == calculationRule.FK_ALTERNATEUBWSUPPLIER);
                    calculationRule.Unit = Units.FirstOrDefault(x => x.Id == calculationRule.FK_UNIT);
                    calculationRule.WorkActivity = WorkActivities.FirstOrDefault(x => x.Id == calculationRule.FK_WORKACTIVITY);
                    calculationRule.OutsourcedActivity =
                        OutsourcedActivities.FirstOrDefault(x => x.Id == calculationRule.FK_OUTSOURCED);
                    calculationRule.SubCalculation =
                        Calculations.FirstOrDefault(x => x.Id == calculationRule.FK_SUBCALCULATION);
                    calculationRule.Calculation = Calculations.FirstOrDefault(x => x.Id == calculationRule.FK_CALCULATION);
                    calculationRule.DisableDataChanges = false;
                }
            }
        }

        public static bool DisableChangeTracking { get; set; } = true;
        public static bool MergeRecords { get; set; } = false;

        public IEnumerable<Models.Calculation> GetAllCalculations(SdkSession session, int calculationId)
        {
            var result = new List<Models.Calculation>();
            var subCalculations = session.Find<Models.Calculation>(x => x.FK_MAINCALCULATION == calculationId);
            foreach (var subCalculation in subCalculations)
            {
                result.Add(subCalculation);
                result.AddRange(GetAllCalculations(session, subCalculation.Id));
            }
            return result;
        }

        public static T Create<T>() where T : IDatabaseObject
        {
            var result = Activator.CreateInstance<T>();
            Add(result);
            return result;
        }

        public static void Add<T>(T newAddedObject) where T : IDatabaseObject
        {
            var objectState = new DatabaseObjectState(newAddedObject)
            {
                State = DatabaseObjectStateEnum.New
            };
            DatabaseObjectStates.Add(objectState);
            ObjectAdded?.Invoke(null, new ObjectChangedEventArgs(newAddedObject, DatabaseObjectStateEnum.New));
        }

        public static void Remove<T>(T objectToRemove)
        {
            var databaseObjectState = DatabaseObjectStates.FirstOrDefault(x => x.DatabaseObject.Equals(objectToRemove));
            if (databaseObjectState == null)
                return;

            databaseObjectState.State = DatabaseObjectStateEnum.MarkForDelete;

            var dbObject = objectToRemove as IDatabaseObject;
            // if object doesn't have an ID it's saved in DB yet. We delete it from object state cache
            if (dbObject != null && dbObject.Id <= 0)
                DatabaseObjectStates.Remove(databaseObjectState);

            ObjectDeleted?.Invoke(null, new ObjectChangedEventArgs(objectToRemove, DatabaseObjectStateEnum.MarkForDelete));
        }

        public string SaveChanges()
        {
            using (var session = _sessionFactory.GetSession())
            {
                var newObjectStates = DatabaseObjectStates.Where(x => x.State == DatabaseObjectStateEnum.New).ToArray();

                if (newObjectStates.Any(x => x.DatabaseObject.GetType() == typeof(CalculationTotals)))
                {
                    foreach (var newObject in newObjectStates.Where(x => x.DatabaseObject.GetType() == typeof(CalculationTotals)))
                    {
                        try
                        {
                            session.Insert(newObject.DatabaseObject);
                            newObject.State = DatabaseObjectStateEnum.Unmodified;
                        }
                        catch (Exception ex)
                        {
                            return $"ERROR: {ex}";
                        }
                    }
                }
                if (newObjectStates.Any(x => x.DatabaseObject.GetType() == typeof(Models.Calculation)))
                {
                    foreach (var newObject in newObjectStates.Where(x => x.DatabaseObject.GetType() == typeof(Models.Calculation)))
                    {
                        try
                        {
                            session.Insert(newObject.DatabaseObject);
                            newObject.State = DatabaseObjectStateEnum.Unmodified;
                        }
                        catch (Exception ex)
                        {
                            return $"ERROR: {ex}";
                        }
                    }
                }
                if (newObjectStates.Any(x => x.DatabaseObject.GetType() == typeof(CalculationRule)))
                {
                    foreach (var newObject in newObjectStates.Where(x => x.DatabaseObject.GetType() == typeof(CalculationRule)))
                    {
                        try
                        {
                            session.Insert(newObject.DatabaseObject);
                            newObject.State = DatabaseObjectStateEnum.Unmodified;
                        }
                        catch (Exception ex)
                        {
                            return $"ERROR: {ex}";
                        }
                    }
                }

                newObjectStates = DatabaseObjectStates.Where(x => x.State == DatabaseObjectStateEnum.New).ToArray();
                foreach (var newObject in newObjectStates)
                {
                    try
                    {
                        session.Insert(newObject.DatabaseObject);
                        newObject.State = DatabaseObjectStateEnum.Unmodified;
                    }
                    catch (Exception ex)
                    {
                        return $"ERROR: {ex}";
                    }
                }

                var updateableObjects = DatabaseObjectStates.Where(x => x.State == DatabaseObjectStateEnum.Modified).ToArray();
                while (updateableObjects.Any())
                {
                    foreach (var newObject in updateableObjects)
                    {
                        try
                        {
                            session.Update(newObject.DatabaseObject);
                            newObject.State = DatabaseObjectStateEnum.Unmodified;

                            if (newObject.DatabaseObject is CalculationRule rule && rule.Calculation.FK_MAINCALCULATION != null)
                            {
                                UpdateMainCalculationsRecursive(session, rule);
                            }
                        }
                        catch (Exception ex)
                        {
                            return $"ERROR: {ex}";
                        }
                    }
                    updateableObjects = DatabaseObjectStates.Where(x => x.State == DatabaseObjectStateEnum.Modified).ToArray();
                }

                var deletableObjects = DatabaseObjectStates.Where(x => x.State == DatabaseObjectStateEnum.MarkForDelete).ToArray();
                while (deletableObjects.Any())
                {
                    foreach (var newObject in deletableObjects)
                    {
                        try
                        {
                            if (newObject.DatabaseObject is CalculationRule rule)
                            {
                                DeleteSubcalculationsRecursive(session, rule);
                            }

                            session.Delete(newObject.DatabaseObject);
                            newObject.State = DatabaseObjectStateEnum.Unmodified;
                        }
                        catch (Exception ex)
                        {
                            return $"ERROR: {ex}";
                        }
                    }
                    deletableObjects = DatabaseObjectStates.Where(x => x.State == DatabaseObjectStateEnum.MarkForDelete).ToArray();
                }
            }

            return string.Empty;
        }

        private void UpdateMainCalculationsRecursive(SdkSession session, CalculationRule rule)
        {
            try
            {
                var calculationRuleThisSubCalculationAtParent = rule.Calculation.MainCalculation
                    .CalculationRules.First(x => x.FK_SUBCALCULATION == rule.FK_CALCULATION);

                if (calculationRuleThisSubCalculationAtParent.Calculation.FK_MAINCALCULATION != null)
                {
                    UpdateMainCalculationsRecursive(session, calculationRuleThisSubCalculationAtParent);
                }

                session.Update(calculationRuleThisSubCalculationAtParent);
            }
            catch (Exception e)
            {
            }
        }

        public void DeleteSubcalculationsRecursive(SdkSession session, CalculationRule rule)
        {
            try
            {
                if (rule.SubCalculation != null)
                {
                    foreach (var calcRule in rule.SubCalculation.CalculationRules.ToList())
                    {
                        DeleteSubcalculationsRecursive(session, calcRule);
                    }

                    session.Delete(rule.SubCalculation);
                }
            }
            catch (Exception e)
            {
            }
        }

        public void Undo()
        {
            if (UndoStack.Count <= 0)
            {
                return;
            }

            Debug.WriteLine($"Before UndoStackCount: {UndoStack.Count} RedoStackCount: {RedoStack.Count}");
            DisableChangeTracking = true;
            var command = UndoStack.Pop();
            RedoStack.Push(command);
            command.UnExecute();
            DisableChangeTracking = false;
            Debug.WriteLine($"After UndoStackCount: {UndoStack.Count} RedoStackCount: {RedoStack.Count}");
        }

        public void Redo()
        {
            if (RedoStack.Count <= 0)
            {
                return;
            }

            Debug.WriteLine($"Before UndoStackCount: {UndoStack.Count} RedoStackCount: {RedoStack.Count}");
            DisableChangeTracking = true;
            var command = RedoStack.Pop();
            UndoStack.Push(command);
            command.Execute();
            DisableChangeTracking = false;
            Debug.WriteLine($"After UndoStackCount: {UndoStack.Count} RedoStackCount: {RedoStack.Count}");
        }

        public void AddCommandToUndoStack(ICommand command)
        {
            if (!MergeRecords)
            {
                UndoStack.Push(command);
                return;
            }

            var lastRecord = UndoStack.LastOrDefault();

            if (lastRecord == null)
            {
                var newRecord = new ObjectGroup();
                newRecord.AddCommand(command);
                UndoStack.Push(newRecord);
                return;
            }

            if (lastRecord is ObjectGroup)
            {
                (lastRecord as ObjectGroup).AddCommand(command);
                return;
            }
            else
            {
                var newRecord = new ObjectGroup();
                newRecord.AddCommand(command);
                UndoStack.Push(newRecord);
            }
        }

        public void AddCommandToRedoStack(ICommand command)
        {
            if (!MergeRecords)
            {
                RedoStack.Push(command);
                return;
            }

            var lastRecord = RedoStack.LastOrDefault();

            if (lastRecord == null)
            {
                var newRecord = new ObjectGroup();
                newRecord.AddCommand(command);
                RedoStack.Push(newRecord);
                return;
            }

            if (lastRecord is ObjectGroup)
            {
                (lastRecord as ObjectGroup).AddCommand(command);
                return;
            }
            else
            {
                var newRecord = new ObjectGroup();
                newRecord.AddCommand(command);
                RedoStack.Push(newRecord);
            }
        }

        public static EventHandler<ObjectChangedEventArgs> ObjectAdded;
        public static EventHandler<ExtendedObjectChangedEventArgs> ObjectChanged;
        public static EventHandler<ObjectChangedEventArgs> ObjectDeleted;
    }

    public class ObjectDeleted : ICommand
    {
        private readonly IDatabaseObject _databaseObject;

        public ObjectDeleted(IDatabaseObject databaseObject)
        {
            _databaseObject = databaseObject;
        }

        public void Execute()
        {
            Debug.WriteLine($"Redo ObjectDeleted {_databaseObject.GetType()} {_databaseObject.Id}");
            DatabaseHelper.Remove(_databaseObject);
        }

        public void UnExecute()
        {
            Debug.WriteLine($"Undo ObjectDeleted {_databaseObject.GetType()} {_databaseObject.Id}");
            DatabaseHelper.Add(_databaseObject);
        }
    }

    public class ObjectGroup : ICommand
    {
        private readonly List<ICommand> _commands = new List<ICommand>();

        public void AddCommand(ICommand command)
        {
            _commands.Add(command);
        }

        public void Execute()
        {
            foreach (var command in _commands)
            {
                command.Execute();
            }
        }

        public void UnExecute()
        {
            foreach (var command in _commands)
            {
                command.UnExecute();
            }
        }
    }

    public class ObjectCreated : ICommand
    {
        private readonly IDatabaseObject _databaseObject;

        public ObjectCreated(IDatabaseObject databaseObject)
        {
            _databaseObject = databaseObject;
        }

        public void Execute()
        {
            Debug.WriteLine($"Redo ObjectCreated {_databaseObject.GetType()} {_databaseObject.Id}");
            DatabaseHelper.Add(_databaseObject);
        }

        public void UnExecute()
        {
            Debug.WriteLine($"Undo ObjectCreated {_databaseObject.GetType()} {_databaseObject.Id}");
            DatabaseHelper.Remove(_databaseObject);
        }
    }

    public class ObjectChanged : ICommand
    {
        private readonly ExtendedObjectChangedEventArgs _changedEventArgs;

        public ObjectChanged(ExtendedObjectChangedEventArgs changedEventArgs)
        {
            _changedEventArgs = changedEventArgs;
        }

        public void Execute()
        {
            Debug.WriteLine($"Redo ObjectChanged {_changedEventArgs.PropertyName}: {_changedEventArgs.OldValue} -> {_changedEventArgs.NewValue}");
            var propertyInfo = _changedEventArgs.DatabaseDatabaseObject.GetType().GetProperty(_changedEventArgs.PropertyName);
            propertyInfo.SetValue(_changedEventArgs.DatabaseDatabaseObject, _changedEventArgs.NewValue);
        }

        public void UnExecute()
        {
            Debug.WriteLine($"Redo ObjectChanged {_changedEventArgs.PropertyName}: {_changedEventArgs.NewValue} -> {_changedEventArgs.OldValue}");
            var propertyInfo = _changedEventArgs.DatabaseDatabaseObject.GetType().GetProperty(_changedEventArgs.PropertyName);
            propertyInfo.SetValue(_changedEventArgs.DatabaseDatabaseObject, _changedEventArgs.OldValue);
        }
    }
}
