﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace Calculation.Core.Extensions
{
    public static class GridViewExtensions
    {
        public static GridColumn CreateColumn(this GridView gridView, string fieldName, string caption, Func<bool> visibilityFunc)
        {
            var result = gridView.Columns.Add();
            result.FieldName = fieldName;
            result.Caption = caption;
            result.Tag = visibilityFunc;

            return result;
        }
        
        public static GridColumn CreateColumn(this GridView gridView, string fieldName, string caption, bool visible = true)
        {
            var result = gridView.Columns.Add();
            result.FieldName = fieldName;
            result.Caption = caption;
            result.Visible = visible;

            return result;
        }

        public static GridColumn CreateColumn(this GridView gridView, string fieldName, string caption, ColumnSortOrder sorting, bool visible = true)
        {
            var result = CreateColumn(gridView, fieldName, caption, visible);
            result.SortOrder = sorting;
            return result;
        }

        public static GridColumn CreateNumberColumn(this GridView gridView, string fieldName, string caption, bool allowEdit = true, Func<bool> visibilityFunc = null)
        {
            var result = CreateColumn(gridView, fieldName, caption);
            result.OptionsColumn.AllowEdit = allowEdit;
            result.ColumnEdit = new RepositoryItemSpinEdit();
            result.Tag = visibilityFunc;
            ((RepositoryItemSpinEdit)result.ColumnEdit).Mask.MaskType = MaskType.Numeric;
            ((RepositoryItemSpinEdit)result.ColumnEdit).Mask.UseMaskAsDisplayFormat = false;
            ((RepositoryItemSpinEdit) result.ColumnEdit).DisplayFormat.FormatType = FormatType.Numeric;
            ((RepositoryItemSpinEdit) result.ColumnEdit).DisplayFormat.FormatString = "n2";
            ((RepositoryItemSpinEdit)result.ColumnEdit).EditMask = "n4";

            return result;
        }

        public static GridColumn CreateLookupColumn(this GridView gridView, string fieldName, string caption, IEnumerable datasource)
        {
            var result = CreateColumn(gridView, fieldName, caption);
            result.ColumnEdit = new RepositoryItemLookUpEdit();
            ((RepositoryItemLookUpEdit)result.ColumnEdit).DataSource = datasource;
            ((RepositoryItemLookUpEdit)result.ColumnEdit).DisplayMember = "CODE";
            ((RepositoryItemLookUpEdit)result.ColumnEdit).Columns.Clear();
            ((RepositoryItemLookUpEdit)result.ColumnEdit).Columns.Add(new LookUpColumnInfo("Id", "Id") { Visible = false });
            ((RepositoryItemLookUpEdit)result.ColumnEdit).Columns.Add(new LookUpColumnInfo("CODE", "Code"));
            ((RepositoryItemLookUpEdit)result.ColumnEdit).Columns.Add(new LookUpColumnInfo("DESCRIPTION", "Omschrijving"));
            ((RepositoryItemLookUpEdit)result.ColumnEdit).KeyDown += (sender, args) =>
            {
                if (args.KeyCode == Keys.Delete && sender is LookUpEdit)
                {
                    ((LookUpEdit)sender).EditValue = null;
                }
            };

            return result;
        }

        public static GridColumn CreateCurrencyColumn(this GridView gridView, string fieldName, string caption, Color color, Func<bool> visibilityFunc, bool allowEdit = true)
        {
            var result = gridView.CreateColumn(fieldName, caption);
            result.OptionsColumn.AllowEdit = allowEdit;
            result.AppearanceCell.BackColor = color;
            result.Tag = visibilityFunc;
            result.ColumnEdit = new RepositoryItemSpinEdit();
            ((RepositoryItemSpinEdit)result.ColumnEdit).Mask.MaskType = MaskType.Numeric;
            ((RepositoryItemSpinEdit)result.ColumnEdit).Mask.UseMaskAsDisplayFormat = false;
            ((RepositoryItemSpinEdit)result.ColumnEdit).DisplayFormat.FormatType = FormatType.Numeric;
            ((RepositoryItemSpinEdit)result.ColumnEdit).DisplayFormat.FormatString = "c2";
            ((RepositoryItemSpinEdit)result.ColumnEdit).EditMask = "c4";

            return result;
        }

        public static GridColumn CreateCurrencyColumn(this GridView gridView, string fieldName, string caption, bool allowEdit = true)
        {
            var result = gridView.CreateColumn(fieldName, caption);
            result.OptionsColumn.AllowEdit = allowEdit;
            result.ColumnEdit = new RepositoryItemSpinEdit();
            ((RepositoryItemSpinEdit)result.ColumnEdit).Mask.MaskType = MaskType.Numeric;
            ((RepositoryItemSpinEdit)result.ColumnEdit).Mask.UseMaskAsDisplayFormat = false;
            ((RepositoryItemSpinEdit)result.ColumnEdit).DisplayFormat.FormatType = FormatType.Numeric;
            ((RepositoryItemSpinEdit)result.ColumnEdit).DisplayFormat.FormatString = "c2";
            ((RepositoryItemSpinEdit)result.ColumnEdit).EditMask = "c4";

            return result;
        }

        public static void UpdateColumns(this GridView gridView)
        {
            foreach (GridColumn column in gridView.Columns)
            {
                var tag = column.Tag as Func<bool>;

                if (tag == null)
                    continue;

                var result = tag.Invoke();
                column.Visible = result;
            }
        }

    }
}