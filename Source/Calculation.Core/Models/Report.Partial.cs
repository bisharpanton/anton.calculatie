﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class Report
    {
		[ForeignKey("FK_TABLEINFO")]
        public TableInfo TableInfo { get; set; }
    }
}