﻿namespace Calculation.Win
{
    partial class CalculationLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalculationLogForm));
            this.gcLogs = new DevExpress.XtraGrid.GridControl();
            this.calculationLogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvLogs = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCREATOR = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDATECHANGED = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDATECREATED = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEXTERNALKEY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFK_CALCULATION = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFK_WORKFLOWSTATE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPLAINTEXT_MESSAGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUSERCHANGED = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalculation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalculationRule = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemRichTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit();
            this.colTYPE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMESSAGE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabControl = new DevExpress.XtraTab.XtraTabControl();
            this.tabPageLogs = new DevExpress.XtraTab.XtraTabPage();
            this.tabPageDetails = new DevExpress.XtraTab.XtraTabPage();
            this.detailsLayoutControl = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.TYPEImageComboBoxEdit = new DevExpress.XtraEditors.ImageComboBoxEdit();
            this.detailLayoutControlBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.CalculationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MESSAGETextEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CalculationRuleTextEdit = new DevExpress.XtraEditors.RichTextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMESSAGE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTYPE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalculation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalculationRule = new DevExpress.XtraLayout.LayoutControlItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonOpenCalculationRule = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationLogBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageLogs.SuspendLayout();
            this.tabPageDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailsLayoutControl)).BeginInit();
            this.detailsLayoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TYPEImageComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailLayoutControlBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MESSAGETextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculationRuleTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMESSAGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTYPE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculationRule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcLogs
            // 
            this.gcLogs.DataSource = this.calculationLogBindingSource;
            this.gcLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLogs.Location = new System.Drawing.Point(5, 5);
            this.gcLogs.MainView = this.gvLogs;
            this.gcLogs.Name = "gcLogs";
            this.gcLogs.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRichTextEdit1});
            this.gcLogs.Size = new System.Drawing.Size(924, 405);
            this.gcLogs.TabIndex = 0;
            this.gcLogs.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLogs});
            // 
            // calculationLogBindingSource
            // 
            this.calculationLogBindingSource.DataSource = typeof(Calculation.Core.Models.CalculationLog);
            // 
            // gvLogs
            // 
            this.gvLogs.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCREATOR,
            this.colDATECHANGED,
            this.colDATECREATED,
            this.colEXTERNALKEY,
            this.colFK_CALCULATION,
            this.colFK_WORKFLOWSTATE,
            this.colId,
            this.colPLAINTEXT_MESSAGE,
            this.colUSERCHANGED,
            this.colCalculation,
            this.colCalculationRule,
            this.colTYPE,
            this.colMESSAGE});
            this.gvLogs.GridControl = this.gcLogs;
            this.gvLogs.Name = "gvLogs";
            this.gvLogs.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvLogs.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvLogs.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            this.gvLogs.OptionsBehavior.Editable = false;
            this.gvLogs.OptionsBehavior.ReadOnly = true;
            this.gvLogs.OptionsView.ShowGroupPanel = false;
            this.gvLogs.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colId, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colCREATOR
            // 
            this.colCREATOR.FieldName = "CREATOR";
            this.colCREATOR.Name = "colCREATOR";
            // 
            // colDATECHANGED
            // 
            this.colDATECHANGED.FieldName = "DATECHANGED";
            this.colDATECHANGED.Name = "colDATECHANGED";
            // 
            // colDATECREATED
            // 
            this.colDATECREATED.FieldName = "DATECREATED";
            this.colDATECREATED.Name = "colDATECREATED";
            // 
            // colEXTERNALKEY
            // 
            this.colEXTERNALKEY.FieldName = "EXTERNALKEY";
            this.colEXTERNALKEY.Name = "colEXTERNALKEY";
            // 
            // colFK_CALCULATION
            // 
            this.colFK_CALCULATION.FieldName = "FK_CALCULATION";
            this.colFK_CALCULATION.Name = "colFK_CALCULATION";
            // 
            // colFK_WORKFLOWSTATE
            // 
            this.colFK_WORKFLOWSTATE.FieldName = "FK_WORKFLOWSTATE";
            this.colFK_WORKFLOWSTATE.Name = "colFK_WORKFLOWSTATE";
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colPLAINTEXT_MESSAGE
            // 
            this.colPLAINTEXT_MESSAGE.FieldName = "PLAINTEXT_MESSAGE";
            this.colPLAINTEXT_MESSAGE.Name = "colPLAINTEXT_MESSAGE";
            // 
            // colUSERCHANGED
            // 
            this.colUSERCHANGED.FieldName = "USERCHANGED";
            this.colUSERCHANGED.Name = "colUSERCHANGED";
            // 
            // colCalculation
            // 
            this.colCalculation.Caption = "Calculatie";
            this.colCalculation.FieldName = "Calculation.DESCRIPTION";
            this.colCalculation.Name = "colCalculation";
            this.colCalculation.Visible = true;
            this.colCalculation.VisibleIndex = 0;
            this.colCalculation.Width = 194;
            // 
            // colCalculationRule
            // 
            this.colCalculationRule.Caption = "Calculatie Regel";
            this.colCalculationRule.ColumnEdit = this.repositoryItemRichTextEdit1;
            this.colCalculationRule.FieldName = "CalculationRule.Description";
            this.colCalculationRule.Name = "colCalculationRule";
            this.colCalculationRule.Visible = true;
            this.colCalculationRule.VisibleIndex = 1;
            this.colCalculationRule.Width = 199;
            // 
            // repositoryItemRichTextEdit1
            // 
            this.repositoryItemRichTextEdit1.Name = "repositoryItemRichTextEdit1";
            this.repositoryItemRichTextEdit1.OptionsExport.Rtf.ExportTheme = true;
            this.repositoryItemRichTextEdit1.ShowCaretInReadOnly = false;
            // 
            // colTYPE
            // 
            this.colTYPE.Caption = "Type";
            this.colTYPE.FieldName = "TYPE";
            this.colTYPE.Name = "colTYPE";
            this.colTYPE.Visible = true;
            this.colTYPE.VisibleIndex = 2;
            this.colTYPE.Width = 100;
            // 
            // colMESSAGE
            // 
            this.colMESSAGE.Caption = "Bericht";
            this.colMESSAGE.FieldName = "MESSAGE";
            this.colMESSAGE.Name = "colMESSAGE";
            this.colMESSAGE.Visible = true;
            this.colMESSAGE.VisibleIndex = 3;
            this.colMESSAGE.Width = 413;
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(5, 5);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedTabPage = this.tabPageLogs;
            this.tabControl.Size = new System.Drawing.Size(940, 443);
            this.tabControl.TabIndex = 1;
            this.tabControl.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabPageLogs,
            this.tabPageDetails});
            // 
            // tabPageLogs
            // 
            this.tabPageLogs.Controls.Add(this.gcLogs);
            this.tabPageLogs.Name = "tabPageLogs";
            this.tabPageLogs.Padding = new System.Windows.Forms.Padding(5);
            this.tabPageLogs.Size = new System.Drawing.Size(934, 415);
            this.tabPageLogs.Text = "Alle logs";
            // 
            // tabPageDetails
            // 
            this.tabPageDetails.Controls.Add(this.detailsLayoutControl);
            this.tabPageDetails.Controls.Add(this.ribbonControl1);
            this.tabPageDetails.Name = "tabPageDetails";
            this.tabPageDetails.Padding = new System.Windows.Forms.Padding(5);
            this.tabPageDetails.Size = new System.Drawing.Size(934, 415);
            this.tabPageDetails.Text = "Details";
            // 
            // detailsLayoutControl
            // 
            this.detailsLayoutControl.AllowCustomization = false;
            this.detailsLayoutControl.Controls.Add(this.TYPEImageComboBoxEdit);
            this.detailsLayoutControl.Controls.Add(this.CalculationTextEdit);
            this.detailsLayoutControl.Controls.Add(this.MESSAGETextEdit);
            this.detailsLayoutControl.Controls.Add(this.CalculationRuleTextEdit);
            this.detailsLayoutControl.DataSource = this.detailLayoutControlBindingSource;
            this.detailsLayoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailsLayoutControl.Location = new System.Drawing.Point(5, 82);
            this.detailsLayoutControl.Name = "detailsLayoutControl";
            this.detailsLayoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(905, 418, 450, 400);
            this.detailsLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.detailsLayoutControl.Root = this.layoutControlGroup1;
            this.detailsLayoutControl.Size = new System.Drawing.Size(924, 328);
            this.detailsLayoutControl.TabIndex = 0;
            this.detailsLayoutControl.Text = "dataLayoutControl1";
            // 
            // TYPEImageComboBoxEdit
            // 
            this.TYPEImageComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.detailLayoutControlBindingSource, "TYPE", true));
            this.TYPEImageComboBoxEdit.Location = new System.Drawing.Point(88, 68);
            this.TYPEImageComboBoxEdit.Name = "TYPEImageComboBoxEdit";
            this.TYPEImageComboBoxEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TYPEImageComboBoxEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TYPEImageComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TYPEImageComboBoxEdit.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Info", Calculation.Core.Models.CalculationLogType.Info, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Warning", Calculation.Core.Models.CalculationLogType.Warning, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Error", Calculation.Core.Models.CalculationLogType.Error, 2)});
            this.TYPEImageComboBoxEdit.Properties.ReadOnly = true;
            this.TYPEImageComboBoxEdit.Properties.UseCtrlScroll = true;
            this.TYPEImageComboBoxEdit.Size = new System.Drawing.Size(824, 20);
            this.TYPEImageComboBoxEdit.StyleController = this.detailsLayoutControl;
            this.TYPEImageComboBoxEdit.TabIndex = 14;
            // 
            // detailLayoutControlBindingSource
            // 
            this.detailLayoutControlBindingSource.DataSource = typeof(Calculation.Core.Models.CalculationLog);
            // 
            // CalculationTextEdit
            // 
            this.CalculationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.detailLayoutControlBindingSource, "Calculation.DESCRIPTION", true));
            this.CalculationTextEdit.Location = new System.Drawing.Point(88, 12);
            this.CalculationTextEdit.Name = "CalculationTextEdit";
            this.CalculationTextEdit.Properties.ReadOnly = true;
            this.CalculationTextEdit.Size = new System.Drawing.Size(824, 20);
            this.CalculationTextEdit.StyleController = this.detailsLayoutControl;
            this.CalculationTextEdit.TabIndex = 16;
            // 
            // MESSAGETextEdit
            // 
            this.MESSAGETextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.detailLayoutControlBindingSource, "MESSAGE", true));
            this.MESSAGETextEdit.Location = new System.Drawing.Point(12, 108);
            this.MESSAGETextEdit.Name = "MESSAGETextEdit";
            this.MESSAGETextEdit.Properties.ReadOnly = true;
            this.MESSAGETextEdit.Size = new System.Drawing.Size(900, 208);
            this.MESSAGETextEdit.StyleController = this.detailsLayoutControl;
            this.MESSAGETextEdit.TabIndex = 11;
            // 
            // CalculationRuleTextEdit
            // 
            this.CalculationRuleTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.detailLayoutControlBindingSource, "CalculationRule.DESCRIPTION", true));
            this.CalculationRuleTextEdit.Location = new System.Drawing.Point(88, 36);
            this.CalculationRuleTextEdit.Name = "CalculationRuleTextEdit";
            this.CalculationRuleTextEdit.Properties.OptionsExport.Rtf.ExportTheme = true;
            this.CalculationRuleTextEdit.Properties.OptionsVerticalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Hidden;
            this.CalculationRuleTextEdit.Properties.ReadOnly = true;
            this.CalculationRuleTextEdit.Size = new System.Drawing.Size(824, 28);
            this.CalculationRuleTextEdit.StyleController = this.detailsLayoutControl;
            this.CalculationRuleTextEdit.TabIndex = 17;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(924, 328);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMESSAGE,
            this.ItemForTYPE,
            this.ItemForCalculation,
            this.ItemForCalculationRule});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(904, 308);
            // 
            // ItemForMESSAGE
            // 
            this.ItemForMESSAGE.Control = this.MESSAGETextEdit;
            this.ItemForMESSAGE.Location = new System.Drawing.Point(0, 80);
            this.ItemForMESSAGE.Name = "ItemForMESSAGE";
            this.ItemForMESSAGE.Size = new System.Drawing.Size(904, 228);
            this.ItemForMESSAGE.Text = "Bericht";
            this.ItemForMESSAGE.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForMESSAGE.TextSize = new System.Drawing.Size(73, 13);
            // 
            // ItemForTYPE
            // 
            this.ItemForTYPE.Control = this.TYPEImageComboBoxEdit;
            this.ItemForTYPE.Location = new System.Drawing.Point(0, 56);
            this.ItemForTYPE.Name = "ItemForTYPE";
            this.ItemForTYPE.Size = new System.Drawing.Size(904, 24);
            this.ItemForTYPE.Text = "Type";
            this.ItemForTYPE.TextSize = new System.Drawing.Size(73, 13);
            // 
            // ItemForCalculation
            // 
            this.ItemForCalculation.Control = this.CalculationTextEdit;
            this.ItemForCalculation.Location = new System.Drawing.Point(0, 0);
            this.ItemForCalculation.Name = "ItemForCalculation";
            this.ItemForCalculation.Size = new System.Drawing.Size(904, 24);
            this.ItemForCalculation.Text = "Calculatie";
            this.ItemForCalculation.TextSize = new System.Drawing.Size(73, 13);
            // 
            // ItemForCalculationRule
            // 
            this.ItemForCalculationRule.Control = this.CalculationRuleTextEdit;
            this.ItemForCalculationRule.Location = new System.Drawing.Point(0, 24);
            this.ItemForCalculationRule.Name = "ItemForCalculationRule";
            this.ItemForCalculationRule.Size = new System.Drawing.Size(904, 32);
            this.ItemForCalculationRule.Text = "Calculatie regel";
            this.ItemForCalculationRule.TextSize = new System.Drawing.Size(73, 13);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.AllowMdiChildButtons = false;
            this.ribbonControl1.AllowMinimizeRibbon = false;
            this.ribbonControl1.DrawGroupCaptions = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonOpenCalculationRule});
            this.ribbonControl1.Location = new System.Drawing.Point(5, 5);
            this.ribbonControl1.MaxItemId = 3;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowCategoryInCaption = false;
            this.ribbonControl1.ShowDisplayOptionsMenuButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersInFormCaption = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbonControl1.ShowQatLocationSelector = false;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(924, 77);
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            this.ribbonControl1.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // barButtonOpenCalculationRule
            // 
            this.barButtonOpenCalculationRule.Caption = "Open calculatie regel";
            this.barButtonOpenCalculationRule.Id = 2;
            this.barButtonOpenCalculationRule.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonOpenCalculationRule.ImageOptions.Image")));
            this.barButtonOpenCalculationRule.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonOpenCalculationRule.ImageOptions.LargeImage")));
            this.barButtonOpenCalculationRule.Name = "barButtonOpenCalculationRule";
            this.barButtonOpenCalculationRule.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonOpenCalculationRule);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // CalculationLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(950, 453);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "CalculationLogForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Calculatie logs";
            ((System.ComponentModel.ISupportInitialize)(this.gcLogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calculationLogBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRichTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageLogs.ResumeLayout(false);
            this.tabPageDetails.ResumeLayout(false);
            this.tabPageDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailsLayoutControl)).EndInit();
            this.detailsLayoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TYPEImageComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detailLayoutControlBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MESSAGETextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculationRuleTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMESSAGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTYPE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculationRule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcLogs;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLogs;
        private System.Windows.Forms.BindingSource calculationLogBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCREATOR;
        private DevExpress.XtraGrid.Columns.GridColumn colDATECHANGED;
        private DevExpress.XtraGrid.Columns.GridColumn colDATECREATED;
        private DevExpress.XtraGrid.Columns.GridColumn colEXTERNALKEY;
        private DevExpress.XtraGrid.Columns.GridColumn colFK_CALCULATION;
        private DevExpress.XtraGrid.Columns.GridColumn colFK_WORKFLOWSTATE;
        private DevExpress.XtraGrid.Columns.GridColumn colMESSAGE;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colPLAINTEXT_MESSAGE;
        private DevExpress.XtraGrid.Columns.GridColumn colTYPE;
        private DevExpress.XtraGrid.Columns.GridColumn colUSERCHANGED;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculation;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculationRule;
        private DevExpress.XtraTab.XtraTabControl tabControl;
        private DevExpress.XtraTab.XtraTabPage tabPageLogs;
        private DevExpress.XtraTab.XtraTabPage tabPageDetails;
        private DevExpress.XtraDataLayout.DataLayoutControl detailsLayoutControl;
        private System.Windows.Forms.BindingSource detailLayoutControlBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.ImageComboBoxEdit TYPEImageComboBoxEdit;
        private DevExpress.XtraEditors.TextEdit CalculationTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMESSAGE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTYPE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalculation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalculationRule;
        private DevExpress.XtraEditors.MemoEdit MESSAGETextEdit;
        private DevExpress.XtraEditors.RichTextEdit CalculationRuleTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemRichTextEdit repositoryItemRichTextEdit1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonOpenCalculationRule;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
    }
}