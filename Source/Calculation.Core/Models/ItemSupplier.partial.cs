﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class ItemSupplier
    {
        [ForeignKey("FK_RELATION")]
        [Include]
        public Relation Relation { get; set; }

        [Ignore]
        public string RelationName => Relation?.NAME;
    }
}
