﻿using Ridder.Common.Script;

public class RidderScript : CommandScript
{
    public void Execute()
    {
        var rsAllLockedCalculations = this.GetRecordset("C_CALCULATION", "LOCKED, LOCKEDBY, LOCKEDAT", "LOCKED = 1", "");

        if (rsAllLockedCalculations.RecordCount<=0)
            return;

        rsAllLockedCalculations.UseDataChanges = false;
        rsAllLockedCalculations.UpdateWhenMoveRecord = false;
        rsAllLockedCalculations.MoveFirst();

        while (!rsAllLockedCalculations.EOF)
        {
            rsAllLockedCalculations.SetFieldValue("LOCKED", false);
            rsAllLockedCalculations.SetFieldValue("LOCKEDBY", System.DBNull.Value);
            rsAllLockedCalculations.SetFieldValue("LOCKEDAT", System.DBNull.Value);
            rsAllLockedCalculations.MoveNext();
        }

        rsAllLockedCalculations.MoveFirst();
        rsAllLockedCalculations.Update2();
    }
}