namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum OperationOrSetup : int
    {
        
        /// <summary>
        /// Operation
        /// </summary>
        [Description("Operation")]
        Operation = 1,
        
        /// <summary>
        /// Setup
        /// </summary>
        [Description("Setup")]
        Setup = 2,
    }
}
