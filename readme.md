# Readme - Anton calculatie tool

Gegevens Anton:
Jacco Braas
0226-216002
0615969915

## Installatie
De installatie van een nieuwe versie van van de calculatie bij Anton op de servers wordt door Infotheek gedaan.
Door middel van een mail naar servicedesk@infotheek.com te sturen met de volgende inhoud:
```
Hallo, 
Zojuist heb ik een nieuwe versie gemaakt, wil je deze installeren?
Het bestand heet ‘Anton Calculatie-1.0.10.1308.msi’.
```

Zij weten de FTP gegevens van Anton en kunnen daar de nieuwste versie vanaf halen en installeren.
Anton heeft namelijk 3 terminal servers op welke de installatie gedaan moet worden.
Omdat zij in-control willen blijven over de servers doen zij de installatie.

Contactpersoon bij Infotheek heet: Michel Suvaal

## Tabel structuur Ridder iQ
In Ridder iQ zijn een aantal tabellen aangemaakt waar deze tool mee werkt. Deze staan hieronder beschreven.

| Tabel naam | Omschrijving |  
|------------|--------------|   
| C_CALCULATION | Calculatie |
| C_CALCULATIONLOG | Calculatie log |
| C_CALCULATIONRULE | Calculatie regel |
| C_CALCULATIONTAILRULE | Calculatie staart regel |  
