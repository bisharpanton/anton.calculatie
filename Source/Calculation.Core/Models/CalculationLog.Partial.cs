﻿using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class CalculationLog
    {
        [ForeignKey("FK_CALCULATION"), Include]
        public Calculation Calculation { get; set; }

        [ForeignKey("FK_CALCULATIONRULE"), Include]
        public CalculationRule CalculationRule { get; set; }
    }
}