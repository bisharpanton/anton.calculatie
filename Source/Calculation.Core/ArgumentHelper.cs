﻿using System;
using System.Collections.Generic;
using Calculation.Core.Interfaces;

namespace Calculation.Core
{
    public class ArgumentHelper : IArgumentHelper
    {
        public ArgumentHelper()
        {
            var args = Environment.GetCommandLineArgs();
            
#if DEBUG
            var calculationId = 14;
            var tempArgs0 = $"";
            var tempArgs1 = $"{"Administrator"}";
            var tempArgs2 = $"{"Administrator"}|{"Anton Groep"}";
            var tempArgs3 = $"{"Administrator"}|{"Anton Groep"}|{"Admin#2014"}";
            var tempArgs4 = $"{"SDK_TEST"}|{"Anton Groep"}|{""}|{calculationId}";
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(tempArgs4);

            args = new[] { "", Convert.ToBase64String(plainTextBytes) };
#endif
            
            if (args.Length <= 1)
            {
                return;
            }

            var base64EncodedBytes = Convert.FromBase64String(args[1]);
            var arg = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            var arguments = arg.Split('|');

            if (arguments.Length >= 1)
            {
                UserName = arguments[0];
            }
            if (arguments.Length >= 2)
            {
                CompanyName = arguments[1];
            }
            if (arguments.Length >= 3)
            {
                Password = arguments[2];
            }
            if (arguments.Length >= 4)
            {
                if (int.TryParse(arguments[3], out var calcId))
                {
                    CalculationId = calcId;
                }
            }

            //Password = "";
        }

        public string UserName { get; set; }

        public string CompanyName { get; set; }

        public int CalculationId { get; set; }

        public string Password { get; set; }
    }
}