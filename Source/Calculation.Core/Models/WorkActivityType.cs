namespace Calculation.Core.Models
{
    using System;
    using Ridder.Client.SDK.Extensions;
    using System.ComponentModel;
    
    
    public enum WorkActivityType : int
    {
        
        /// <summary>
        /// Start working time
        /// </summary>
        [Description("Start working time")]
        Start_working_time = 1,
        
        /// <summary>
        /// End working time
        /// </summary>
        [Description("End working time")]
        End_working_time = 2,
        
        /// <summary>
        /// Continue last job
        /// </summary>
        [Description("Continue last job")]
        Continue_last_job = 3,
        
        /// <summary>
        /// Part-time absence
        /// </summary>
        [Description("Part-time absence")]
        Part_time_absence = 4,
        
        /// <summary>
        /// Break
        /// </summary>
        [Description("Break")]
        Break = 5,
        
        /// <summary>
        /// Illness
        /// </summary>
        [Description("Illness")]
        Illness = 6,
        
        /// <summary>
        /// Time off in lieu
        /// </summary>
        [Description("Time off in lieu")]
        Time_off_in_lieu = 7,
        
        /// <summary>
        /// Discontinuity
        /// </summary>
        [Description("Discontinuity")]
        Discontinuity = 8,
        
        /// <summary>
        /// Bank holiday
        /// </summary>
        [Description("Bank holiday")]
        Bank_holiday = 9,
        
        /// <summary>
        /// Holiday
        /// </summary>
        [Description("Holiday")]
        Holiday = 10,
        
        /// <summary>
        /// Travelling hours
        /// </summary>
        [Description("Travelling hours")]
        Travelling_hours = 11,
        
        /// <summary>
        /// Paid absence
        /// </summary>
        [Description("Paid absence")]
        Paid_absence = 12,
        
        /// <summary>
        /// Illegal absence
        /// </summary>
        [Description("Illegal absence")]
        Illegal_absence = 13,
        
        /// <summary>
        /// Unpayed absence
        /// </summary>
        [Description("Unpayed absence")]
        Unpayed_absence = 14,
        
        /// <summary>
        /// Indirect hours
        /// </summary>
        [Description("Indirect hours")]
        Indirect_hours = 15,
        
        /// <summary>
        /// Direct hours
        /// </summary>
        [Description("Direct hours")]
        Direct_hours = 16,
    }
}
