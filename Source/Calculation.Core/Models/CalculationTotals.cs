﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Calculation.Core.Annotations;
using Calculation.Core.Classes;
using Calculation.Core.Classes.Base;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    [Table("C_CALCULATIONTOTALS")]
    public class CalculationTotals : IDatabaseObject, INotifyPropertyChanged
    {
        [PrimaryKey]
        public int Id
        {
            get { return _id; }
            set
            {
                OnCustomPropertyChanged(ref _id, value);
            }
        }

        public event EventHandler<ExtendedObjectChangedEventArgs> CustomPropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnCustomPropertyChanged<T>(ref T privateField, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (Equals(newValue, privateField))
                return;

            var oldValue = privateField;
            privateField = newValue;

            if (propertyName == nameof(Id) && Calculation != null)
            {
                Calculation.FK_CALCULATIONTOTAL = Id;
            }

            CustomPropertyChanged?.Invoke(this, new ExtendedObjectChangedEventArgs(this, propertyName, oldValue, newValue));
            OnPropertyChanged(propertyName);
        }

        [Ignore]
        public Calculation Calculation { get; set; }

        [Ignore]
        public double DifferencePercentageOverCostPrice => TotalCostPrice == 0 ? 0 : TotalDifference / TotalCostPrice;

        [Ignore]
        public double DifferencePercentageOverSalesPrice => TotalSalesPrice == 0 ? 0 : TotalDifference / TotalSalesPrice;

        private double _totalCostPriceHours;
        private double _totalCostPriceWorkActivity;
        private double _totalCostPriceItems;
        private double _totalDifferenceTailRules;
        private double _totalDifferenceOutsourced;
        private double _totalDifferenceItems;
        private double _totalDifferenceWorkActivity;
        private double _totalDifferenceHours;
        private double _totalSalesPriceTailRules;
        private double _totalSalesPriceOutsourced;
        private double _totalSalesPriceItems;
        private double _totalSalesPriceWorkActivity;
        private double _totalSalesPriceHours;
        private double _totalCostPriceTailRules;
        private double _totalCostPriceOutsourced;
        private double _totalCostPrice;
        private double _totalDifference;
        private double _totalSalesPrice;
        private double _totalDifferencePerUnit;
        private double _totalSalesPricePerUnit;
        private double _totalCostPricePerUnit;
        private double _totaalPriceKgSteel; 
        private double _totaalSalesPriceKgSteel; 
        private double _totaalDifferencePriceKgSteel; 
        private double _totaalKgSteel; 
        private double _totalSurfaceArea; 
        private int _id;

        public CalculationTotals()
        {
            
        }

        public double TotalCostPriceHours
        {
            get { return _totalCostPriceHours; }
            set
            {
                OnCustomPropertyChanged(ref _totalCostPriceHours, value);
            }
        }

        public double TotalCostPriceWorkActivity
        {
            get { return _totalCostPriceWorkActivity; }
            set
            {
                OnCustomPropertyChanged(ref _totalCostPriceWorkActivity, value);
            }
        }

        public double TotalCostPriceItems
        {
            get { return _totalCostPriceItems; }
            set
            {
                OnCustomPropertyChanged(ref _totalCostPriceItems, value);
            }
        }

        public double TotalCostPriceOutsourced
        {
            get { return _totalCostPriceOutsourced; }
            set
            {
                OnCustomPropertyChanged(ref _totalCostPriceOutsourced, value);
            }
        }

        public double TotalCostPriceTailRules
        {
            get { return _totalCostPriceTailRules; }
            set
            {
                OnCustomPropertyChanged(ref _totalCostPriceTailRules, value);
            }
        }

        public double TotalSalesPriceHours
        {
            get { return _totalSalesPriceHours; }
            set
            {
                OnCustomPropertyChanged(ref _totalSalesPriceHours, value);
            }
        }

        public double TotalSalesPriceWorkActivity
        {
            get { return _totalSalesPriceWorkActivity; }
            set
            {
                OnCustomPropertyChanged(ref _totalSalesPriceWorkActivity, value);
            }
        }

        public double TotalSalesPriceItems
        {
            get { return _totalSalesPriceItems; }
            set
            {
                OnCustomPropertyChanged(ref _totalSalesPriceItems, value);
            }
        }

        public double TotalSalesPriceOutsourced
        {
            get { return _totalSalesPriceOutsourced; }
            set
            {
                OnCustomPropertyChanged(ref _totalSalesPriceOutsourced, value);
            }
        }

        public double TotalSalesPriceTailRules
        {
            get { return _totalSalesPriceTailRules; }
            set
            {
                OnCustomPropertyChanged(ref _totalSalesPriceTailRules, value);
            }
        }

        public double TotalDifferenceHours
        {
            get { return _totalDifferenceHours; }
            set
            {
                OnCustomPropertyChanged(ref _totalDifferenceHours, value);
            }
        }

        public double TotalDifferenceWorkActivity
        {
            get { return _totalDifferenceWorkActivity; }
            set
            {
                OnCustomPropertyChanged(ref _totalDifferenceWorkActivity, value);
            }
        }

        public double TotalDifferenceItems
        {
            get { return _totalDifferenceItems; }
            set
            {
                OnCustomPropertyChanged(ref _totalDifferenceItems, value);
            }
        }

        public double TotalDifferenceOutsourced
        {
            get { return _totalDifferenceOutsourced; }
            set
            {
                OnCustomPropertyChanged(ref _totalDifferenceOutsourced, value);
            }
        }

        public double TotalDifferenceTailRules
        {
            get { return _totalDifferenceTailRules; }
            set
            {
                OnCustomPropertyChanged(ref _totalDifferenceTailRules, value);
            }
        }

        public double TotalCostPrice
        {
            get { return _totalCostPrice; }
            set
            {
                OnCustomPropertyChanged(ref _totalCostPrice, value);
            }
        }

        public double TotalSalesPrice
        {
            get { return _totalSalesPrice; }
            set
            {
                OnCustomPropertyChanged(ref _totalSalesPrice, value);
            }
        }

        public double TotalDifference
        {
            get { return _totalDifference; }
            set
            {
                OnCustomPropertyChanged(ref _totalDifference, value);
            }
        }

        public double TotalPricePerKg
        {
            get { return _totaalPriceKgSteel; }
            set
            {
                OnCustomPropertyChanged(ref _totaalPriceKgSteel, value);
            }
        }

        public double TotalSalesPricePerKg
        {
            get { return _totaalSalesPriceKgSteel; }
            set
            {
                OnCustomPropertyChanged(ref _totaalSalesPriceKgSteel, value);
            }
        }

        public double TotalDifferencePerKg
        {
            get { return _totaalDifferencePriceKgSteel; }
            set
            {
                OnCustomPropertyChanged(ref _totaalDifferencePriceKgSteel, value);
            }
        }

        public double TOTALKGSTEEL
        {
            get { return _totaalKgSteel; }
            set
            {
                OnCustomPropertyChanged(ref _totaalKgSteel, value);
            }
        }

        public double TOTALSURFACEAREA
        {
            get { return _totalSurfaceArea; }
            set
            {
                OnCustomPropertyChanged(ref _totalSurfaceArea, value);
            }
        }

        public int CompareTo(object obj)
        {
            return 1;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}