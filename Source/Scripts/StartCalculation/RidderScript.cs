﻿using System.Windows.Forms;
using Ridder.Common.Script;

public class RidderScript : CommandScript
{
    private readonly string TableName = "C_CALCULATION";

    public void Execute()
    {
        var calculationId = GetCalculationId();

        if (calculationId == null)
            return;

        var rsCalculation = GetRecordset("C_CALCULATION", "LOCKED, LOCKEDBY, LOCKEDAT", string.Format("PK_C_CALCULATION = '{0}'", calculationId), "");
        if (rsCalculation.RecordCount <= 0)
        {
            MessageBox.Show("Geen calculatie gevonden", "Waarschuwing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return;
        }

        rsCalculation.MoveFirst();

        if ((bool)rsCalculation.Fields["LOCKED"].Value)
        {
            MessageBox.Show(string.Format("Deze calculatie wordt reeds aangepast door {0} sinds {1}.", rsCalculation.Fields["LOCKEDBY"].Value, rsCalculation.Fields["LOCKEDAT"].Value), "Waarschuwing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return;
        }

        // todo get path from setting
        var exePath = @"C:\Program Files (x86)\Anton Calculatie\Calculation.Win.exe";
        var argument = string.Format("{0}|{1}|{2}", GetUserInfo().CurrentUserName, GetUserInfo().CompanyName, calculationId);
        System.Diagnostics.Process.Start(exePath, System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(argument)));
    }

    public object GetCalculationId()
    {
        if (this.FormDataAwareFunctions == null)
        {
            var id = this.OpenBrowseFormWithFilter(TableName, string.Format("PK_{0}", TableName), "FK_MAINCALCULATION IS NULL", true);
            return id;
        }

        if (this.FormDataAwareFunctions.TableName == "R_ASSEMBLY")
        {
            var rsAssembly = GetRecordset("R_ASSEMBLY", "FK_CALCULATION", string.Format("PK_R_ASSEMBLY = '{0}'", this.FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue()), "");
            rsAssembly.MoveFirst();
            return rsAssembly.Fields["FK_CALCULATION"].Value;
        }

        if (this.FormDataAwareFunctions.TableName == "R_OFFERDETAILASSEMBLY")
        {
            var rsAssembly = GetRecordset("R_ASSEMBLY", "FK_CALCULATION", string.Format("PK_R_ASSEMBLY = '{0}'", this.FormDataAwareFunctions.CurrentRecord.GetCurrentRecordValue("FK_ASSEMBLY")), "");
            rsAssembly.MoveFirst();
            return rsAssembly.Fields["FK_CALCULATION"].Value;
        }

        if (this.FormDataAwareFunctions.TableName == TableName)
        {
            return this.FormDataAwareFunctions.CurrentRecord.GetPrimaryKeyValue();
        }

        return this.OpenBrowseFormWithFilter(TableName, string.Format("PK_{0}", TableName), "FK_MAINCALCULATION IS NULL", true);
    }
}