﻿using System;
using System.Collections.Generic;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class Item
    {
        [ForeignKey("FK_ITEMUNIT"), Include]
        public ItemUnit ItemUnit { get; set; }

        [ForeignKey("FK_ITEMGROUP"), Include]
        public ItemGroup ItemGroup { get; set; }

        [Ignore]
        public bool ProfileItem
        {
            get
            {
                if (string.IsNullOrEmpty(ItemGroup?.CODE))
                    return false;

                if (!int.TryParse(ItemGroup.CODE, out int itemUnitCode))
                    return false;

                if (ItemUnit?.CODE.Equals("PLAAT", StringComparison.OrdinalIgnoreCase) ?? false)
                    return false;

                if (ItemUnit?.CODE.Equals("BETONMAT", StringComparison.OrdinalIgnoreCase) ?? false)
                    return false;

                if (ItemUnit?.CODE.Equals("BETONSTAAL", StringComparison.OrdinalIgnoreCase) ?? false)
                    return false;

                if (ItemUnit?.CODE.Equals("SUPPORTERS", StringComparison.OrdinalIgnoreCase) ?? false)
                    return false;

                return itemUnitCode >= 1000 && itemUnitCode <= 1280;
            }
        }

        [Ignore]
        public bool SurfaceItem => ItemUnit?.CODE.Equals("PLAAT", StringComparison.OrdinalIgnoreCase) ?? false;

        [Ignore]
        public bool MatItem => ItemUnit?.CODE.Equals("BETONMAT", StringComparison.OrdinalIgnoreCase) ?? false;

        [Ignore]
        public bool ConcreteSteelItem => ItemUnit?.CODE.Equals("BETONSTAAL", StringComparison.OrdinalIgnoreCase) ?? false;

        [Ignore]
        public bool SupporterItem => ItemUnit?.CODE.Equals("SUPPORTERS", StringComparison.OrdinalIgnoreCase) ?? false;

        public override string ToString()
        {
            return CODE;
        }

        [HasMany("FK_ITEM"), Include]
        public List<ItemSupplier> ItemSuppliers { get; set; }

        public bool HasMultipleSuppliers => ItemSuppliers != null && ItemSuppliers.Count > 1;
    }
}