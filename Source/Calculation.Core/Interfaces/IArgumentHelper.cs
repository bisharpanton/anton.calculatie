﻿namespace Calculation.Core.Interfaces
{
    public interface IArgumentHelper
    {
        string UserName { get; set; }

        string CompanyName { get; set; }

        int CalculationId { get; set; }

        string Password { get; set; }
    }
}