﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Calculation.Core.Annotations;
using Calculation.Core.Classes;
using Calculation.Core.Classes.Base;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class Calculation : IDatabaseObject, IDisposable, IComparable
    {
        private double _total;
        private int _id;
        private Unit _unit;
        private int? _fkCalculationtotal;
        private string _description;
        private double _quantity;

        [PrimaryKey]
        public int Id
        {
            get { return _id; }
            set
            {
                OnCustomPropertyChanged(ref _id, value);
            }
        }

        public event EventHandler<ExtendedObjectChangedEventArgs> CustomPropertyChanged;

        [HasMany("FK_CALCULATION")]
        public List<CalculationTailRule> CalculationTailRules { get; set; } = new List<CalculationTailRule>();

        [HasMany("FK_CALCULATION")]
        public List<CalculationRule> CalculationRules { get; set; } = new List<CalculationRule>();

        [HasMany("FK_MAINCALCULATION")]
        public List<Calculation> Calculations { get; set; } = new List<Calculation>();

        [ForeignKey("FK_MAINCALCULATION")]
        public Calculation MainCalculation { get; set; }

        [Column("FK_CALCULATIONTOTAL")]
        public virtual int? FK_CALCULATIONTOTAL
        {
            get { return _fkCalculationtotal; }
            set
            {
                OnCustomPropertyChanged(ref _fkCalculationtotal, value);
            }
        }

        [ForeignKey("FK_UNIT")]
        public Unit Unit
        {
            get { return _unit; }
            set
            {
                FK_UNIT = value?.Id;
                OnCustomPropertyChanged(ref _unit, value);
            }
        }

        [Column("DESCRIPTION")]
        public string DESCRIPTION
        {
            get { return _description; }
            set
            {
                OnCustomPropertyChanged(ref _description, value);
            }
        }

        [Column("QUANTITY")]
        public double QUANTITY
        {
            get { return _quantity; }
            set
            {
                OnCustomPropertyChanged(ref _quantity, value);
            }
        }

        [Ignore]
        public CalculationTotals Totals { get; set; }

        [Ignore]
        public bool DisableDataChanges { get; set; }

        [Ignore]
        public double Total
        {
            get { return _total; }
            set
            {
                OnCustomPropertyChanged(ref _total, value);
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnCustomPropertyChanged<T>(ref T privateField, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (Equals(newValue, privateField))
                return;

            var oldValue = privateField;
            privateField = newValue;

            if (DisableDataChanges || DatabaseHelper.IsLoading)
                return;

            if (propertyName == nameof(Id))
            {
                CalculationRules.ForEach(x => x.FK_CALCULATION = Id);
                Calculations.ForEach(x => x.FK_MAINCALCULATION = Id);
                CalculationTailRules.ForEach(x => x.CalculationId = Id);

                if (MainCalculation != null)
                {
                    var calculationRules = this.MainCalculation.CalculationRules.Where(x => x.SubCalculation != null && x.SubCalculation.Equals(this)).ToList();
                    calculationRules.ForEach(x => x.FK_SUBCALCULATION = Id);
                }
            }

            CustomPropertyChanged?.Invoke(this, new ExtendedObjectChangedEventArgs(this, propertyName, oldValue, newValue));
        }

        public void Dispose()
        {
        }

        public override bool Equals(object obj)
        {
            var objCalc = obj as Calculation;

            if (objCalc == null)
                return false;

            if (objCalc.Id == Id)
                return true;

            return false;
        }

        public int CompareTo(object obj)
        {
            return 10;
        }

        public IEnumerable<Calculation> GetSubCalculations()
        {
            var result = new List<Calculation>();

            foreach (var calculationRule in CalculationRules.OrderBy(x=>x.SEQUENCENUMBER))
            {
                if (calculationRule.SubCalculation == null)
                {
                    continue;
                }

                result.Add(calculationRule.SubCalculation);
                result.AddRange(calculationRule.SubCalculation.GetSubCalculations());
            }

            return result;
        }

        public override string ToString()
        {
            return DESCRIPTION;
        }
    }

    public class CalculationComparer : IEqualityComparer
    {
        public bool Equals(object x, object y)
        {
            var xCalc = x as Calculation;
            var yCalc = y as Calculation;

            if (x == null || y == null)
                return false;

            if (xCalc.Id == yCalc.Id)
                return true;

            return false;
        }

        public int GetHashCode(object obj)
        {
            throw new NotImplementedException();
        }
    }
}