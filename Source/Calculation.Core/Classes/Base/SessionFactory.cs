﻿using System;
using Calculation.Core.Interfaces;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Classes.Base
{
    public class SessionFactory : ISessionFactory
    {
        public string UserName
        {
            get => ArgumentHelper.UserName;
            set => ArgumentHelper.UserName = value;
        }

        public string CompanyName
        {
            get => ArgumentHelper.CompanyName;
            set => ArgumentHelper.CompanyName = value;
        }
        public string Password { get; set; }
        public IArgumentHelper ArgumentHelper { get; }

        public SessionFactory(IArgumentHelper argumentHelper)
        {
            ArgumentHelper = argumentHelper;
        }

        public SdkConfiguration GetSettings()
        {
            var result = new SdkConfiguration
            {
                UserName = UserName,
                Password = Password ?? ArgumentHelper.Password ?? string.Empty,
                CompanyName = CompanyName,
                PersistSession = true,
            };

            return result;
        }

        public SdkSession GetSession()
        {
            var sdkConfig = GetSettings();
            return new SdkSession(sdkConfig);
        }

        public ISessionLoginResult Login(string userName, string companyName, string password)
        {
            UserName = userName;
            CompanyName = companyName;
            Password = password;

            using (var session = GetSession())
            {
                try
                {
                    session.Login();

                    return new SessionLoginResult { Success = true };
                }
                catch (Exception ex)
                {
                    Password = string.Empty;
                    return new SessionLoginResult { ErrorMessage = ex.Message};
                }
            }
        }

        public bool TryToLoginWithGivenCompanyAndUser(string password)
        {
            if(CompanyName == null)
            {
                //Company niet meegegeven als parameter. Calculatie module wordt los opgestart
                return false;
            }

            Password = password;

            var session = GetSession();

            try
            {
                session.Login();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}
