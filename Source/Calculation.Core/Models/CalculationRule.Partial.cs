﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Calculation.Core.Annotations;
using Calculation.Core.Classes;
using Calculation.Core.Classes.Base;
using Calculation.Core.Interfaces;
using Ridder.Client.SDK.Extensions;

namespace Calculation.Core.Models
{
    public partial class CalculationRule : IDatabaseObject, INotifyPropertyChanged, IMoveable, IComparable
    {
        private double _itemWidthtotal;
        private double? _itemLengthtotal;
        private double? _pricetotal;
        private double _workactivityQuantity;
        private double _outsourcedPriceperunit;
        private double _itemPriceperunit;
        private double _itemLengthperunit;
        private double _itemWidthperunit;
        private double _itemPriceperkg;
        private double _itemPriceperMtr;
        private double _itemSalesPriceperkg;
        private double _itemSalesPriceperMtr;
        private double _quantity;
        private OutsourcedActivity _outsourcedActivity1;
        private WorkActivity _workActivity1;
        private Item _item1;
        private Unit _unit1;
        private Calculation _calculation1;
        private Calculation _subcalculation;
        private double _itemSalespriceperunit;
        private double _outsourcedSalespriceperunit;
        private double _workactivityPriceperunit;
        private double _workactivitySalespriceperunit;
        private int _sequencenumber;
        private string _description;
        private WageGroup _wagegroup;
        private double? _wagegroup_costprice;
        private double? _wagegroup_salesprice;
        private ItemSupplier _alternateItemSupplier;
        private UbwSupplier _alternateUbwSupplier;

        [Column("DESCRIPTION")]
        public string Description
        {
            get { return _description; }
            set
            {
                //if (Equals(value, _description)) return;
                //_description = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _description, value);
            }
        }

        [ForeignKey("FK_CALCULATION")]
        public Calculation Calculation
        {
            get { return _calculation1; }
            set
            {
                if (Equals(value, _calculation1)) return;
                _calculation1 = value;
                OnPropertyChanged();
            }
        }

        [ForeignKey("FK_UNIT")]
        public Unit Unit
        {
            get { return _unit1; }
            set
            {
                //if (Equals(value, _unit1)) return;
                //_unit1 = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _unit1, value);
            }
        }

        [ForeignKey("FK_ITEM")]
        public Item Item
        {
            get { return _item1; }
            set
            {
                //if (Equals(value, _item1)) return;
                //_item1 = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _item1, value);
            }
        }

        [ForeignKey("FK_ALTERNATEITEMSUPPLIER")]
        [Include]
        public ItemSupplier AlternateItemSupplier
        {
            get { return _alternateItemSupplier; }
            set
            {
                OnCustomPropertyChanged(ref _alternateItemSupplier, value);
            }
        }

        [ForeignKey("FK_ALTERNATEUBWSUPPLIER")]
        [Include]
        public UbwSupplier AlternateUbwSupplier
        {
            get { return _alternateUbwSupplier; }
            set
            {
                OnCustomPropertyChanged(ref _alternateUbwSupplier, value);
            }
        }

        [ForeignKey("FK_WORKACTIVITY")]
        public WorkActivity WorkActivity
        {
            get { return _workActivity1; }
            set
            {
                //if (Equals(value, _workActivity1)) return;
                //_workActivity1 = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _workActivity1, value);
            }
        }

        [ForeignKey("FK_OUTSOURCED")]
        public OutsourcedActivity OutsourcedActivity
        {
            get { return _outsourcedActivity1; }
            set
            {
                //if (Equals(value, _outsourcedActivity1)) return;
                //_outsourcedActivity1 = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _outsourcedActivity1, value);
            }
        }

        [ForeignKey("FK_SUBCALCULATION")]
        public Calculation SubCalculation
        {
            get { return _subcalculation; }
            set
            {
                if (Equals(value, _subcalculation)) return;
                _subcalculation = value;
                OnPropertyChanged();
            }
        }

        [Column("QUANTITY")]
        public double QUANTITY
        {
            get { return _quantity; }
            set
            {
                //if (value.Equals(_quantity)) return;
                //_quantity = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _quantity, value);
            }
        }

        [Column("ITEM_PRICEPERKG")]
        public double ITEM_PRICEPERKG
        {
            get { return _itemPriceperkg; }
            set
            {
                //if (value.Equals(_itemPriceperkg)) return;
                //_itemPriceperkg = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _itemPriceperkg, value);
            }
        }

        [Column("ITEM_SALESPRICEPERKG")]
        public double ITEM_SALESPRICEPERKG
        {
            get { return _itemSalesPriceperkg; }
            set
            {
                //if (value.Equals(_itemSalesPriceperkg)) return;
                //_itemSalesPriceperkg = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _itemSalesPriceperkg, value);
            }
        }

        [Column("ITEM_PRICEPERMTR")]
        public double ITEM_PRICEPERMTR
        {
            get { return _itemPriceperMtr; }
            set
            {
                OnCustomPropertyChanged(ref _itemPriceperMtr, value);
            }
        }

        [Column("ITEM_SALESPRICEPERMTR")]
        public double ITEM_SALESPRICEPERMTR
        {
            get { return _itemSalesPriceperMtr; }
            set
            {
                OnCustomPropertyChanged(ref _itemSalesPriceperMtr, value);
            }
        }

        [Column("ITEM_LENGTHPERUNIT")]
        public double ITEM_LENGTHPERUNIT
        {
            get { return _itemLengthperunit; }
            set
            {
                //if (value.Equals(_itemLengthperunit)) return;
                //_itemLengthperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _itemLengthperunit, value);
            }
        }

        [Column("ITEM_WIDTHPERUNIT")]
        public double ITEM_WIDTHPERUNIT
        {
            get { return _itemWidthperunit; }
            set
            {
                OnCustomPropertyChanged(ref _itemWidthperunit, value);
            }
        }

        [Column("ITEM_PRICEPERUNIT")]
        public double ITEM_PRICEPERUNIT
        {
            get { return _itemPriceperunit; }
            set
            {
                //if (value.Equals(_itemPriceperunit)) return;
                //_itemPriceperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _itemPriceperunit, value);
            }
        }

        [Column("ITEM_SALESPRICEPERUNIT")]
        public double ITEM_SALESPRICEPERUNIT
        {
            get { return _itemSalespriceperunit; }
            set
            {
                //if (value.Equals(_itemSalespriceperunit)) return;
                //_itemSalespriceperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _itemSalespriceperunit, value);
            }
        }

        [Column("OUTSOURCED_PRICEPERUNIT")]
        public double OUTSOURCED_PRICEPERUNIT
        {
            get { return _outsourcedPriceperunit; }
            set
            {
                //if (value.Equals(_outsourcedPriceperunit)) return;
                //_outsourcedPriceperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _outsourcedPriceperunit, value);
            }
        }

        [Column("OUTSOURCED_SALESPRICEPERUNIT")]
        public double OUTSOURCED_SALESPRICEPERUNIT
        {
            get { return _outsourcedSalespriceperunit; }
            set
            {
                //if (value.Equals(_outsourcedSalespriceperunit)) return;
                //_outsourcedSalespriceperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _outsourcedSalespriceperunit, value);
            }
        }

        [Column("WORKACTIVITY_QUANTITY")]
        public double WORKACTIVITY_QUANTITY
        {
            get { return _workactivityQuantity; }
            set
            {
                //if (value.Equals(_workactivityQuantity)) return;
                //_workactivityQuantity = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _workactivityQuantity, value);
            }
        }

        [Column("WORKACTIVITY_PRICEPERUNIT")]
        public double WORKACTIVITY_PRICEPERUNIT
        {
            get { return _workactivityPriceperunit; }// + (WorkActivity?.WageGroup?.COSTPRICE ?? 0); }
            set
            {
                //if (value.Equals(_workactivityPriceperunit)) return;
                //_workactivityPriceperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _workactivityPriceperunit, value);
            }
        }

        [Column("WORKACTIVITY_SALESPRICEPERUNIT")]
        public double WORKACTIVITY_SALESPRICEPERUNIT
        {
            get { return _workactivitySalespriceperunit; }// + (WorkActivity?.WageGroup?.SALESPRICE ?? 0); }
            set
            {
                //if (value.Equals(_workactivitySalespriceperunit)) return;
                //_workactivitySalespriceperunit = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _workactivitySalespriceperunit, value);
            }
        }

        [Column("PRICETOTAL")]
        public double? PRICETOTAL
        {
            get { return _pricetotal; }
            set
            {
                if (value.Equals(_pricetotal))
                    return;

                _pricetotal = value;
                //OnPropertyChanged();

                //OnCustomPropertyChanged(ref _pricetotal, value);
            }
        }

        [Column("ITEM_LENGTHTOTAL")]
        public double? ITEM_LENGTHTOTAL
        {
            get { return _itemLengthtotal; }
            set
            {
                OnCustomPropertyChanged(ref _itemLengthtotal, value);
            }
        }

        [Column("ITEM_WIDTHTOTAL")]
        public double ITEM_WIDTHTOTAL
        {
            get { return _itemWidthtotal; }
            set
            {
                OnCustomPropertyChanged(ref _itemWidthtotal, value);
            }
        }


        [Column("SEQUENCENUMBER")]
        public int SEQUENCENUMBER
        {
            get { return _sequencenumber; }
            set
            {
                //if (value == _sequencenumber) return;
                //_sequencenumber = value;
                //OnPropertyChanged();
                OnCustomPropertyChanged(ref _sequencenumber, value);
            }
        }

        [Ignore]
        public bool DisableDataChanges { get; set; }

        public void MoveUp()
        {
            SEQUENCENUMBER--;
        }

        public void MoveDown()
        {
            SEQUENCENUMBER++;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (DisableDataChanges || DatabaseHelper.IsLoading)
                return;

            if (propertyName == nameof(Item))
            {
                FK_ITEM = Item?.Id;

                if (Item == null)
                {
                    ITEM_LENGTHPERUNIT = 0;
                    ITEM_LENGTHTOTAL = 0;
                    ITEM_PRICEPERKG = 0;
                    ITEM_SALESPRICEPERKG = 0;
                    ITEM_PRICEPERMTR = 0;
                    ITEM_SALESPRICEPERMTR = 0;
                }
                else
                {
                    ITEM_PRICEPERUNIT = Item?.STANDARDPURCHASEPRICE ?? 0;
                    ITEM_SALESPRICEPERUNIT = Item?.DEFAULTSALESPRICE ?? 0;
                    ITEM_PRICEPERKG = Item?.STANDARDPURCHASEPRICE ?? 0;
                    ITEM_SALESPRICEPERKG = Item?.DEFAULTSALESPRICE ?? 0;
                }
            }

            // Total length * Item.Weight * Priceperunit
            if (Item != null && Item.ProfileItem)
            {
                ITEM_LENGTHTOTAL = ITEM_LENGTHPERUNIT * QUANTITY;

                ITEM_PRICEPERMTR = ITEM_PRICEPERKG * Item.WEIGHT ?? 0;
                ITEM_SALESPRICEPERMTR = ITEM_SALESPRICEPERKG * Item.WEIGHT ?? 0;

                ITEM_PRICETOTAL = ITEM_LENGTHTOTAL * ITEM_PRICEPERMTR ?? 0;
                ITEM_SALESPRICETOTAL = ITEM_LENGTHTOTAL * ITEM_SALESPRICEPERMTR ?? 0;

                ITEM_PRICEPERUNIT = ITEM_LENGTHPERUNIT * ITEM_PRICEPERMTR;
                ITEM_SALESPRICEPERUNIT = ITEM_LENGTHPERUNIT * ITEM_SALESPRICEPERMTR;
            }
            else if (Item != null && Item.SurfaceItem)
            {
                ITEM_LENGTHTOTAL = ITEM_LENGTHPERUNIT * QUANTITY;
                ITEM_WIDTHTOTAL = ITEM_WIDTHPERUNIT * QUANTITY;

                ITEM_PRICEPERMTR = ITEM_PRICEPERKG * Item.WEIGHT ?? 0;
                ITEM_SALESPRICEPERMTR = ITEM_SALESPRICEPERKG * Item.WEIGHT ?? 0;

                ITEM_PRICEPERUNIT = ITEM_LENGTHPERUNIT * ITEM_WIDTHPERUNIT * Item.WEIGHT * ITEM_PRICEPERKG ?? 0;
                ITEM_SALESPRICEPERUNIT = ITEM_LENGTHPERUNIT * ITEM_WIDTHPERUNIT * Item.WEIGHT * ITEM_SALESPRICEPERKG ?? 0;

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }
            else if (Item != null && Item.MatItem)
            {
                ITEM_PRICEPERUNIT = Item.WEIGHT * ITEM_PRICEPERKG ?? 0;
                ITEM_SALESPRICEPERUNIT = Item.WEIGHT * ITEM_SALESPRICEPERKG ?? 0;

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }
            else if (Item != null && Item.ConcreteSteelItem)
            {
                ITEM_PRICEPERUNIT = ITEM_PRICEPERKG;
                ITEM_SALESPRICEPERUNIT = ITEM_SALESPRICEPERKG;

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }
            else if (Item != null && Item.SupporterItem)
            {
                if (propertyName == nameof(ITEM_PRICEPERUNIT) || propertyName == nameof(ITEM_SALESPRICEPERUNIT))
                {
                    ITEM_PRICEPERKG = ITEM_PRICEPERUNIT / Item.WEIGHT ?? 0;
                    ITEM_SALESPRICEPERKG = ITEM_SALESPRICEPERUNIT / Item.WEIGHT ?? 0;
                } 
                else if (propertyName == nameof(ITEM_PRICEPERKG) || propertyName == nameof(ITEM_SALESPRICEPERKG))
                {
                    ITEM_PRICEPERUNIT = ITEM_PRICEPERKG * Item.WEIGHT ?? 0;
                    ITEM_SALESPRICEPERUNIT = ITEM_SALESPRICEPERKG * Item.WEIGHT ?? 0;
                }
                else if (propertyName == nameof(FK_ITEM))
                {
                    ITEM_PRICEPERUNIT = Item?.STANDARDPURCHASEPRICE ?? 0;
                    ITEM_SALESPRICEPERUNIT = Item?.DEFAULTSALESPRICE ?? 0;
                }

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }
            else
            {
                ITEM_PRICETOTAL = QUANTITY * ITEM_PRICEPERUNIT;
                ITEM_SALESPRICETOTAL = QUANTITY * ITEM_SALESPRICEPERUNIT;
            }

            // if costprice is not set and user sets sales price, it should be copied to costprice
            if (Item != null && ITEM_PRICEPERUNIT <= 0.0 && ITEM_SALESPRICEPERUNIT > 0.0)
            {
                ITEM_PRICEPERUNIT = ITEM_SALESPRICEPERUNIT;
            }

            if (propertyName == nameof(WorkActivity))
            {
                FK_WORKACTIVITY = WorkActivity?.Id;
                WORKACTIVITY_PRICEPERUNIT = WorkActivity?.PRICEOPERATIONTIME ?? 0;
                WORKACTIVITY_SALESPRICEPERUNIT = WorkActivity?.SALESPRICEOPERATIONTIME ?? 0;
            }
            if (propertyName == nameof(OutsourcedActivity))
            {
                FK_OUTSOURCED = OutsourcedActivity?.Id;
                OUTSOURCED_PRICEPERUNIT = OutsourcedActivity?.PURCHASEPRICE ?? 0;
                OUTSOURCED_SALESPRICEPERUNIT = OutsourcedActivity?.PURCHASEPRICE * (OutsourcedActivity?.SALESMARKUP + 1) ?? 0;
            }
            if (propertyName == nameof(SubCalculation))
            {
                FK_SUBCALCULATION = SubCalculation?.Id;
                Description = SubCalculation?.DESCRIPTION;
                QUANTITY = SubCalculation?.QUANTITY ?? 0;
                Unit = SubCalculation?.Unit;
            }
            if (propertyName == nameof(Unit))
            {
                FK_UNIT = Unit?.Id;
            }

            if (propertyName == nameof(Description) && SubCalculation != null)
            {
                Statics.RichEditControl.RtfText = Description;
                SubCalculation.DESCRIPTION = string.IsNullOrEmpty(Statics.RichEditControl.Text) ? Description : Statics.RichEditControl.Text;
            }

            WORKACTIVITY_QUANTITYTOTAL = QUANTITY * WORKACTIVITY_QUANTITY;
            WORKACTIVITY_PRICETOTAL = WORKACTIVITY_QUANTITYTOTAL * WORKACTIVITY_PRICEPERUNIT;
            WORKACTIVITY_SALESPRICETOTAL = WORKACTIVITY_QUANTITYTOTAL * WORKACTIVITY_SALESPRICEPERUNIT;

            OUTSOURCED_PRICETOTAL = QUANTITY * OUTSOURCED_PRICEPERUNIT;
            OUTSOURCED_SALESPRICETOTAL = QUANTITY * OUTSOURCED_SALESPRICEPERUNIT;

            PRICETOTAL = ITEM_PRICETOTAL + WORKACTIVITY_PRICETOTAL + OUTSOURCED_PRICETOTAL;
            SALESPRICETOTAL = ITEM_SALESPRICETOTAL + WORKACTIVITY_SALESPRICETOTAL + OUTSOURCED_SALESPRICETOTAL;
            TOTALPRICEPERUNIT = PRICETOTAL / (QUANTITY == 0 ? 1 : QUANTITY);
            TOTALSALESPRICEPERUNIT = SALESPRICETOTAL / (QUANTITY == 0 ? 1 : QUANTITY);

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int CompareTo(object obj)
        {
            return 5;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnCustomPropertyChanged<T>(ref T privateField, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (Equals(newValue, privateField))
                return;

            var oldValue = privateField;
            privateField = newValue;

            if (DisableDataChanges || DatabaseHelper.IsLoading)
                return;

            Recalculate(propertyName);

            CustomPropertyChanged?.Invoke(this, new ExtendedObjectChangedEventArgs(this, propertyName, oldValue, newValue));
        }

        public event EventHandler<ExtendedObjectChangedEventArgs> CustomPropertyChanged;

        public void Recalculate(string propertyName)
        {
            ITEM_LENGTHTOTAL = ITEM_LENGTHPERUNIT * QUANTITY;
            ITEM_WIDTHTOTAL = ITEM_WIDTHPERUNIT * QUANTITY;

            if (propertyName == nameof(AlternateItemSupplier) && Item != null)
            {
                FK_ALTERNATEITEMSUPPLIER = AlternateItemSupplier?.Id;
                if (AlternateItemSupplier == null)
                {
                    if (Item.ProfileItem || Item.SurfaceItem || Item.MatItem || Item.ConcreteSteelItem)
                    {
                        ITEM_PRICEPERKG = Item?.STANDARDPURCHASEPRICE ?? 0;
                        ITEM_SALESPRICEPERKG = Item?.DEFAULTSALESPRICE ?? 0;
                    }
                    else if (Item.SupporterItem)
                    {
                        ITEM_PRICEPERKG = Item?.STANDARDPURCHASEPRICE / Item?.WEIGHT ?? 0;
                        ITEM_SALESPRICEPERKG = Item?.DEFAULTSALESPRICE / Item?.WEIGHT ?? 0;
                    }
                    else if (!Item.ProfileItem && !Item.SurfaceItem && !Item.MatItem && !Item.ConcreteSteelItem && !Item.SupporterItem)
                    {
                        ITEM_LENGTHPERUNIT = 0;
                        ITEM_LENGTHTOTAL = 0;
                        ITEM_WIDTHPERUNIT = 0;
                        ITEM_WIDTHTOTAL = 0;

                        ITEM_PRICEPERUNIT = Item?.STANDARDPURCHASEPRICE ?? 0;
                        ITEM_PRICETOTAL = QUANTITY * ITEM_PRICEPERUNIT;
                        ITEM_SALESPRICEPERUNIT = Item?.DEFAULTSALESPRICE ?? 0;
                        ITEM_SALESPRICETOTAL = QUANTITY * ITEM_SALESPRICEPERUNIT;
                    }
                }
                else
                {
                    ITEM_PRICEPERKG = AlternateItemSupplier?.PURCHASEPRICE ?? 0;
                    ITEM_PRICEPERUNIT = AlternateItemSupplier?.PURCHASEPRICE ?? 0;
                }
            }

            if (propertyName == nameof(AlternateUbwSupplier) && OutsourcedActivity != null)
            {
                FK_ALTERNATEUBWSUPPLIER = AlternateUbwSupplier?.Id;
                if (AlternateUbwSupplier == null)
                {
                    OUTSOURCED_PRICEPERUNIT = OutsourcedActivity?.PURCHASEPRICE ?? 0;
                    OUTSOURCED_SALESPRICEPERUNIT = OutsourcedActivity?.PURCHASEPRICE * (OutsourcedActivity?.SALESMARKUP + 1) ?? 0;
                }
                else
                {
                    OUTSOURCED_PRICEPERUNIT = AlternateUbwSupplier?.RATE ?? 0;
                    OUTSOURCED_SALESPRICEPERUNIT = AlternateUbwSupplier?.RATE * (OutsourcedActivity?.SALESMARKUP + 1) ?? 0;
                }
            }

            if (propertyName == nameof(Item))
            {
                FK_ITEM = Item?.Id;

                if (Item == null)
                {
                    ITEM_PRICEPERKG = 0;
                    ITEM_SALESPRICEPERKG = 0;
                    ITEM_PRICEPERMTR = 0;
                    ITEM_SALESPRICEPERMTR = 0;
                }
                else if (Item.ProfileItem || Item.SurfaceItem || Item.MatItem || Item.ConcreteSteelItem)
                {
                    if (AlternateItemSupplier != null)
                    {
                        ITEM_PRICEPERKG = AlternateItemSupplier.PURCHASEPRICE ?? 0;
                    }
                    else
                    {
                        ITEM_PRICEPERKG = Item?.STANDARDPURCHASEPRICE ?? 0;
                    }
                    ITEM_SALESPRICEPERKG = Item?.DEFAULTSALESPRICE ?? 0;
                }
                else if (Item.SupporterItem)
                {
                    if (AlternateItemSupplier != null)
                    {
                        ITEM_PRICEPERKG = AlternateItemSupplier.PURCHASEPRICE / Item?.WEIGHT ?? 0;
                    }
                    else
                    {
                        ITEM_PRICEPERKG = Item?.STANDARDPURCHASEPRICE / Item?.WEIGHT ?? 0;
                    }
                    ITEM_SALESPRICEPERKG = Item?.DEFAULTSALESPRICE / Item?.WEIGHT ?? 0;
                }
                else if (!Item.ProfileItem && !Item.SurfaceItem && !Item.MatItem && !Item.ConcreteSteelItem && !Item.SupporterItem)
                {
                    ITEM_LENGTHPERUNIT = 0;
                    ITEM_LENGTHTOTAL = 0;
                    ITEM_WIDTHPERUNIT = 0;
                    ITEM_WIDTHTOTAL = 0;
                    if (AlternateItemSupplier != null)
                    {
                        ITEM_PRICEPERUNIT = AlternateItemSupplier.PURCHASEPRICE ?? 0;
                    }
                    else
                    {
                        ITEM_PRICEPERUNIT = Item?.STANDARDPURCHASEPRICE ?? 0;
                    }
                    ITEM_PRICETOTAL = QUANTITY * ITEM_PRICEPERUNIT;
                    ITEM_SALESPRICEPERUNIT = Item?.DEFAULTSALESPRICE ?? 0;
                    ITEM_SALESPRICETOTAL = QUANTITY * ITEM_SALESPRICEPERUNIT;
                }
            }

            if (propertyName == nameof(ITEM_PRICEPERKG))
            {
                ITEM_PRICEPERMTR = ITEM_PRICEPERKG * Item?.WEIGHT ?? 0;
            }
            if (propertyName == nameof(ITEM_SALESPRICEPERKG))
            {
                ITEM_SALESPRICEPERMTR = ITEM_SALESPRICEPERKG * Item?.WEIGHT ?? 0;
            }

            if (propertyName == nameof(ITEM_PRICEPERMTR))
            {
                ITEM_PRICEPERKG = ITEM_PRICEPERMTR / Item?.WEIGHT ?? 0;
            }
            if (propertyName == nameof(ITEM_SALESPRICEPERMTR))
            {
                ITEM_SALESPRICEPERKG = ITEM_SALESPRICEPERMTR / Item?.WEIGHT ?? 0;
            }

            if (Item == null && ITEM_PRICEPERUNIT == 0.0 && propertyName == nameof(ITEM_SALESPRICEPERUNIT))
            {
                ITEM_PRICEPERUNIT = ITEM_SALESPRICEPERUNIT;
            }

            if (Item == null && ITEM_SALESPRICEPERUNIT == 0.0 && propertyName == nameof(ITEM_PRICEPERUNIT))
            {
                ITEM_SALESPRICEPERUNIT = ITEM_PRICEPERUNIT;
            }

            if (Item != null && Item.ProfileItem && !Item.SurfaceItem)
            {
                ITEM_PRICETOTAL = ITEM_LENGTHTOTAL * ITEM_PRICEPERMTR ?? 0;
                ITEM_SALESPRICETOTAL = ITEM_LENGTHTOTAL * ITEM_SALESPRICEPERMTR ?? 0;

                ITEM_PRICEPERUNIT = ITEM_LENGTHPERUNIT * ITEM_PRICEPERMTR;
                ITEM_SALESPRICEPERUNIT = ITEM_LENGTHPERUNIT * ITEM_SALESPRICEPERMTR;
            }

            if (Item != null && Item.SurfaceItem)
            {
                //ITEM_PRICETOTAL = (ITEM_LENGTHTOTAL * ITEM_WIDTHTOTAL) * ITEM_PRICEPERMTR ?? 0;
                //ITEM_SALESPRICETOTAL = (ITEM_LENGTHTOTAL * ITEM_WIDTHTOTAL) * ITEM_SALESPRICEPERMTR ?? 0;

                ITEM_PRICEPERUNIT = (ITEM_LENGTHPERUNIT * ITEM_WIDTHPERUNIT) * Item.WEIGHT * ITEM_PRICEPERKG ?? 0;
                ITEM_SALESPRICEPERUNIT = (ITEM_LENGTHPERUNIT * ITEM_WIDTHPERUNIT) * Item.WEIGHT * ITEM_SALESPRICEPERKG ?? 0;

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }

            if (Item != null && Item.MatItem)
            {
                ITEM_PRICEPERUNIT = Item.WEIGHT * ITEM_PRICEPERKG ?? 0;
                ITEM_SALESPRICEPERUNIT = Item.WEIGHT * ITEM_SALESPRICEPERKG ?? 0;

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }

            if (Item != null && Item.ConcreteSteelItem)
            {
                ITEM_PRICEPERUNIT = ITEM_PRICEPERKG;
                ITEM_SALESPRICEPERUNIT = ITEM_SALESPRICEPERKG;

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }

            if (Item != null && Item.SupporterItem)
            {
                if (propertyName == nameof(ITEM_PRICEPERUNIT) || propertyName == nameof(ITEM_SALESPRICEPERUNIT))
                {
                    ITEM_PRICEPERKG = ITEM_PRICEPERUNIT / Item.WEIGHT ?? 0;
                    ITEM_SALESPRICEPERKG = ITEM_SALESPRICEPERUNIT / Item.WEIGHT ?? 0;
                } 
                else if (propertyName == nameof(ITEM_PRICEPERKG) || propertyName == nameof(ITEM_SALESPRICEPERKG))
                {
                    ITEM_PRICEPERUNIT = ITEM_PRICEPERKG * Item.WEIGHT ?? 0;
                    ITEM_SALESPRICEPERUNIT = ITEM_SALESPRICEPERKG * Item.WEIGHT ?? 0;
                }
                else if (propertyName == nameof(FK_ITEM))
                {
                    ITEM_PRICEPERUNIT = Item?.STANDARDPURCHASEPRICE ?? 0;
                    ITEM_SALESPRICEPERUNIT = Item?.DEFAULTSALESPRICE ?? 0;
                }

                ITEM_PRICETOTAL = ITEM_PRICEPERUNIT * QUANTITY;
                ITEM_SALESPRICETOTAL = ITEM_SALESPRICEPERUNIT * QUANTITY;
            }

            if (Item == null || (!Item.ProfileItem && !Item.SurfaceItem && !Item.MatItem && !Item.ConcreteSteelItem && !Item.SupporterItem))
            {
                ITEM_PRICETOTAL = QUANTITY * ITEM_PRICEPERUNIT;
                ITEM_SALESPRICETOTAL = QUANTITY * ITEM_SALESPRICEPERUNIT;
            }

            if (propertyName == nameof(WorkActivity))
            {
                FK_WORKACTIVITY = WorkActivity?.Id;
                WORKACTIVITY_PRICEPERUNIT = WorkActivity?.CostPrice ?? 0;
                WORKACTIVITY_SALESPRICEPERUNIT = WorkActivity?.SalesPrice ?? 0;
            }
            if (propertyName == nameof(OutsourcedActivity))
            {
                FK_OUTSOURCED = OutsourcedActivity?.Id;

                if (AlternateUbwSupplier != null)
                {
                    OUTSOURCED_PRICEPERUNIT = AlternateUbwSupplier.RATE ?? 0;
                    OUTSOURCED_SALESPRICEPERUNIT = AlternateUbwSupplier.RATE * (OutsourcedActivity?.SALESMARKUP + 1) ?? 0;
                }
                else
                {
                    OUTSOURCED_PRICEPERUNIT = OutsourcedActivity?.PURCHASEPRICE ?? 0;
                    OUTSOURCED_SALESPRICEPERUNIT = OutsourcedActivity?.PURCHASEPRICE * (OutsourcedActivity?.SALESMARKUP + 1) ?? 0;
                }
            }
            if (propertyName == nameof(SubCalculation))
            {
                FK_SUBCALCULATION = SubCalculation?.Id;
                Description = SubCalculation?.DESCRIPTION;
                QUANTITY = SubCalculation?.QUANTITY ?? 0;
                Unit = SubCalculation?.Unit;
            }

            if (propertyName == nameof(Unit))
            {
                FK_UNIT = Unit?.Id;
            }

            if (propertyName == nameof(Description) && SubCalculation != null)
            {
                Statics.RichEditControl.RtfText = Description;
                SubCalculation.DESCRIPTION = string.IsNullOrEmpty(Statics.RichEditControl.Text) ? Description : Statics.RichEditControl.Text;
            }

            WORKACTIVITY_QUANTITYTOTAL = QUANTITY * WORKACTIVITY_QUANTITY;
            WORKACTIVITY_PRICETOTAL = WORKACTIVITY_QUANTITYTOTAL * WORKACTIVITY_PRICEPERUNIT;
            WORKACTIVITY_SALESPRICETOTAL = WORKACTIVITY_QUANTITYTOTAL * WORKACTIVITY_SALESPRICEPERUNIT;

            OUTSOURCED_PRICETOTAL = QUANTITY * OUTSOURCED_PRICEPERUNIT;
            OUTSOURCED_SALESPRICETOTAL = QUANTITY * OUTSOURCED_SALESPRICEPERUNIT;

            if (SubCalculation != null && SubCalculation.Totals != null)
            {
                ITEM_PRICETOTAL = SubCalculation.Totals.TotalCostPriceItems;
                ITEM_SALESPRICETOTAL = SubCalculation.Totals.TotalSalesPriceItems;

                WORKACTIVITY_QUANTITYTOTAL = SubCalculation.Totals.TotalSalesPriceHours;
                WORKACTIVITY_PRICETOTAL = SubCalculation.Totals.TotalCostPriceWorkActivity;
                WORKACTIVITY_SALESPRICETOTAL = SubCalculation.Totals.TotalSalesPriceWorkActivity;

                OUTSOURCED_PRICETOTAL = SubCalculation.Totals.TotalCostPriceOutsourced;
                OUTSOURCED_SALESPRICETOTAL = SubCalculation.Totals.TotalSalesPriceOutsourced;

                TOTALPRICEPERUNIT = SubCalculation.Totals.TotalCostPrice;
                TOTALSALESPRICEPERUNIT = SubCalculation.Totals.TotalSalesPrice;

                PRICETOTAL = TOTALPRICEPERUNIT * QUANTITY;
                SALESPRICETOTAL = TOTALSALESPRICEPERUNIT * QUANTITY;
            }
            else
            {
                PRICETOTAL = ITEM_PRICETOTAL + WORKACTIVITY_PRICETOTAL + OUTSOURCED_PRICETOTAL;
                SALESPRICETOTAL = ITEM_SALESPRICETOTAL + WORKACTIVITY_SALESPRICETOTAL + OUTSOURCED_SALESPRICETOTAL;
                TOTALPRICEPERUNIT = PRICETOTAL / (QUANTITY == 0 ? 1 : QUANTITY);
                TOTALSALESPRICEPERUNIT = SALESPRICETOTAL / (QUANTITY == 0 ? 1 : QUANTITY);
            }
        }
    }
}
