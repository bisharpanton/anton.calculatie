﻿using Calculation.Core.Interfaces;

namespace Calculation.Core.Classes.Base
{
    public class SessionLoginResult : ISessionLoginResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}