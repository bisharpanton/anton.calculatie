namespace FlattenAssembly
{
    using System;

    public static class Log
    {
        public static string FileName;
        public static string Format = "[{0}] {1:yyyy-MM-dd HH:mm:ss} | {2}\r\n";
        public static bool LogToConsole = false;
        public static bool LogToFile = true;
        public static bool Enabled = false;

        public static void Info(string message, params object[] args)
        {
            Write("Info", message, args);
        }

        public static void Error(Exception exception)
        {
            Write("Error", exception != null ? exception.ToString() : String.Empty);
        }

        public static void Write(string level, string message, params object[] args)
        {
            if (!Enabled) return;

            try
            {
                if (args != null && args.Length > 0)
                {
                    message = String.Format(message, args);
                }

                message = String.Format(Format, level.ToUpperInvariant(), DateTime.Now, message);

                if (LogToFile && !String.IsNullOrEmpty(FileName))
                {
                    System.IO.File.AppendAllText(FileName, message);
                }

                if (LogToConsole)
                {
                    System.Console.Write(message);
                }
            }
            catch (Exception)
            {
                // Alle fouten m.b.t. logging negeren
            }
        }
    }
}