﻿using System;

namespace Calculation.Core.Classes.Base
{
    public interface IDatabaseObject : IComparable
    {
        int Id { get; set; }

        event EventHandler<ExtendedObjectChangedEventArgs> CustomPropertyChanged;
    }
}