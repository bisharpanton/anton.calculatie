﻿using Calculation.Core.Models;
using DevExpress.Utils;

namespace Calculation.Win
{
    public partial class CalculationRuleDetails : DevExpress.XtraEditors.XtraForm
    {
        private readonly CalculationRule _calculationRule;

        public CalculationRuleDetails(CalculationRule calculationRule)
        {
            _calculationRule = calculationRule;
            InitializeComponent();
            calculationRuleBindingSource.DataSource = calculationRule;
            dataLayoutControl1.OptionsView.IsReadOnly = DefaultBoolean.True;
        }
    }
}